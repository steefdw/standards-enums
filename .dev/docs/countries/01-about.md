---
title: About ISO 3166
---

# About the ISO standard

## ISO 3166: countries and their subdivisions

Codes for the representation of names of countries and their subdivisions. It consists of three parts:

| ISO code                                               | Description                               |
|--------------------------------------------------------|-------------------------------------------|
| [ISO 3166-1](https://en.wikipedia.org/wiki/ISO_3166-1) | Country codes                             |
| [ISO 3166-2](https://en.wikipedia.org/wiki/ISO_3166-2) | Country subdivision code                  |
| [ISO 3166-3](https://en.wikipedia.org/wiki/ISO_3166-3) | Code for formerly used names of countries |


### ISO 3166-1 <small>([wiki](https://en.wikipedia.org/wiki/ISO_3166-1))</small>

**Country codes**, defines codes for the names of countries, dependent territories, and special areas of geographical interest.

!!! info "It defines three sets of country codes"
    #### [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)
    
    **two-letter country codes** which are the most widely used of the three, and used most prominently for the Internet's country code top-level domains (with a few exceptions).

    `Steefdw\StandardsEnums\ISO3166\CountryAlpha2`

    #### [ISO 3166-1 alpha-3](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3)

    **three-letter country codes** which allow a better visual association between the codes and the country names than the alpha-2 codes.

    `Steefdw\StandardsEnums\ISO3166\CountryAlpha3`

    #### [ISO 3166-1 numeric](https://en.wikipedia.org/wiki/ISO_3166-1_numeric)

    **three-digit country codes** which are identical to those developed and maintained by the United Nations Statistics Division, with the advantage of script (writing system) independence, and hence useful for people or systems using non-Latin scripts.
    
    `Steefdw\StandardsEnums\ISO3166\CountryNumeric`

### ISO 3166-2 <small>([wiki](https://en.wikipedia.org/wiki/ISO_3166-2))</small>

**Country subdivision code**, defines codes for the names of the principal subdivisions (e.g., provinces, states, departments, regions) of all countries coded in ISO 3166-1.

### ISO 3166-3 <small>([wiki](https://en.wikipedia.org/wiki/ISO_3166-3))</small>

**Code for formerly used names of countries**, defines codes for country names which have been deleted from ISO 3166-1 since its first publication in 1974.
