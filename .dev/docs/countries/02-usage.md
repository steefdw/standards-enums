---
title: Using the Country Enums
---

# Using the Country Enums
 
The Country Enums act as constants that represent specific country names, codes, flags, and other related information.
To get started, you can retrieve a Country instance from the available options using various methods, such as:

* From an alpha-2 code (`CountryAlpha2::tryFrom('nl')`)
* From an alpha-3 code (`CountryAlpha3::tryFrom('NLD')`)
* From a country flag (`CountryFlag::tryFrom('🇳🇱')`)
* From a numeric country code (`CountryNumeric::tryFrom(528)`)

Once you have the Country instance, you can access various properties such as:

* The name of the country
* Alpha-2 and alpha-3 codes
* Country flag
* Numeric country code

You can also get related data such as the locales ("languages") and currencies for that specific country.

Example Usage:

```php
$country = CountryAlpha2::tryFrom('nl');      // CountryAlpha2::NL

$countryName = $country->getName();           // CountryName::NL
$countryAlpha2 = $countryName->getAlpha2();   // CountryAlpha2::NL
$countryAlpha3 = $countryAlpha2->getAlpha3(); // CountryAlpha3::NL
$countryFlag = $countryAlpha3->getFlag();     // CountryFlag::NL
$countryNumber = $countryFlag->getNumeric();  // CountryNumeric::NL
// (these methods work for all country Enums)

echo $countryName->value;   // 'Netherlands'
echo $countryAlpha2->value; // 'nl'
echo $countryAlpha3->value; // 'NLD'
echo $countryFlag->value;   // '🇳🇱'
echo $countryNumber->value; // '528'

// this also works the other way around:
CountryAlpha2::tryFromCountryName($countryName); // CountryAlpha2::NL
CountryAlpha2::tryFromAlpha2($countryAlpha2);    // CountryAlpha2::NL
CountryAlpha2::tryFromAlpha3($countryAlpha3);    // CountryAlpha2::NL
CountryAlpha2::tryFromFlag($countryFlag);        // CountryAlpha2::NL
CountryAlpha2::tryFromNumeric($countryNumber);   // CountryAlpha2::NL
// (these methods work for all country Enums)
```

## Get currencies from a country:

```php
CountryAlpha2::BE->getCurrencies(); 
// ['EUR' => CurrencyName::EUR]

CountryName::ZW->getCurrencies(); 
// ['USD' => CurrencyName::USD, 'ZWL' => CurrencyName::ZWL]
```

## Helper methods:

!!! abstract "These helper methods work for all country Enums:"
    * [Country*::tryFromName()](./#tryfromname)
    * [Country*::toArray()](./#toarray)
    * [Country*::names()](./#names)
    * [Country*::values()](./#values)


### tryFromName()
```php
$countryName = CountryName::tryFromName('AW'); // CountryName::AW
$countryName->value; // Aruba
```

### toArray()

Gets an array with all names ("keys") / values in the Enum.

```php
CountryName::toArray();
// [
//    'AW' => 'Aruba',
//    'AF' => 'Afghanistan',
//    'AO' => 'Angola',
//    'AI' => 'Anguilla',
//    ...
```

### names()

Gets an array with all names ("keys") in the Enum.

```php
CountryName::names();
// [
//    'AW',
//    'AF',
//    'AO',
//    'AI',
//    ...
```

### values()

Gets an array with all values in the Enum.

```php
CountryName::values();
// [
//    'Aruba',
//    'Afghanistan',
//    'Angola',
//    'Anguilla',
//    ...
```

## Sorting cases:

!!! abstract "These helper sorting methods work for all country Enums:"
    * [Country*::casesSorted()](./#casessorted)
    * [Country*::casesSortedByCountryName()](./#sorted-by-country-name)
    * [Country*::casesSortedByCountryName($language)](./#sorted-by-country-name-translated)

### casesSorted()

Gets an array with the cases sorted by name

```php
CountryName::casesSorted();
// [
//    CountryName::AD,
//    CountryName::AE,
//    CountryName::AF,
//    CountryName::AG,
//    ...
```

### sorted by country name

Gets an array with the cases sorted by country name.

```php
CountryName::casesSortedByCountryName();
// [
//    ...
//    CountryName::AU, // Australia
//    CountryName::AT, // Austria
//    CountryName::AZ, // Azerbaijan
//    ...
```

### sorted by country name (translated)

Gets an array with the cases sorted by country name in the specified language.

```php
$language = CountryNameTranslation::tryFrom('nl_NL'); // normal locale string
$language = CountryNameTranslation::tryFromName('NL_NL'); // or all uppercase

CountryName::casesSortedByCountryName($language);
// [
//    ...
//    CountryName::AU, // Australië
//    CountryName::AZ, // Azerbeidzjan
//    ...
//    CountryName::AT, // Oostenrijk
//    ...
```

??? tip "Current translated languages are:"
    * Albanian
    * Armenian
    * Assamese
    * Belarusian
    * Bengali (India)
    * Bokmål, Norwegian; Norwegian Bokmål
    * Brazilian Portuguese
    * Bulgarian
    * Catalan; Valencian
    * Central Khmer
    * Chinese (Hong Kong)
    * Chinese (Simplified)
    * Chinese (Traditional)
    * Chuvash
    * Croatian
    * Czech
    * Dutch; Flemish
    * Estonian
    * French
    * Friulian
    * Georgian
    * German
    * Hebrew
    * Hindi
    * Hungarian
    * Icelandic
    * Indonesian
    * Irish
    * Italian
    * Kazakh
    * Korean
    * Kurdish (Northern)
    * Lithuanian
    * Occitan (post 1500); Provençal
    * Panjabi; Punjabi
    * Polish
    * Portuguese
    * Romanian; Moldavian; Moldovan
    * Russian
    * Sardinian
    * Slovak
    * Spanish; Castilian
    * Swedish
    * Tamil
    * Turkish
    * Ukrainian
    * Welsh
