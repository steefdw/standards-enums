---
title: About ISO 4217
---
https://en.wikipedia.org/wiki/ISO_4217
# About the ISO standard

## ISO 4217: currencies

Codes for the representation of names of currencies and provides information about the relationships between individual 
currencies and their minor units.

The ISO 4217 code list is used in banking and business globally. In many countries, the ISO 4217 alpha codes for the 
more common currencies are so well known publicly that exchange rates published in newspapers or posted in banks use 
only these to delineate the currencies, instead of translated currency names or ambiguous currency symbols.

This data is published in three lists:

* [List 1](https://www.six-group.com/dam/download/financial-information/data-center/iso-currrency/lists/list-one.xls) 
– Current currency & funds code list
* [List 2](https://www.six-group.com/dam/download/financial-information/data-center/iso-currrency/lists/list-two.doc) 
– Current funds codes
* [List 3](https://www.six-group.com/dam/download/financial-information/data-center/iso-currrency/lists/list-three.xls)
– List of codes for historic denominations of currencies & funds

!!! warning "This package lists only the current codes"
    So the "codes for historic denominations of currencies & funds" are excluded in this package