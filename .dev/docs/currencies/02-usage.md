---
title: Using the Currency Enums
---

# Using the Currency Enums

```php
$currency = CurrencyName::tryFrom('Euro'); // CurrencyName::EUR

$currencyAlpha3 = $currency->getAlpha3()->value;  // 'EUR'
$currencyName   = $currency->getName()->value;    // 'Euro'
$currencyNumber = $currency->getNumeric()->value; // '978'

// this also works the other way around:
CurrencyAlpha3::tryFromAlpha3($currencyAlpha3);     // CurrencyAlpha2::NLD
CurrencyAlpha3::tryFromCurrencyName($currencyName); // CurrencyAlpha2::NLD
CurrencyAlpha3::tryFromNumeric($currencyNumber);    // CurrencyAlpha2::NLD
// these methods work for all currency Enums
```

## Get countries from a currency:

```php
$euro = CurrencyName::EUR;
$countries = $euro->getCountries();
// [
//    'AD' => CountryName::AD, // - Andorra
//    'AT' => CountryName::AT, // - Austria
//    'BE' => CountryName::BE, // - Belgium
//    ...
// ]
```

## Helper methods:

!!! abstract "These helper methods work for all currency Enums:"
    * [Currency*::tryFromName()](./#tryfromname)
    * [Currency*::toArray()](./#toarray)
    * [Currency*::names()](./#names)
    * [Currency*::values()](./#values)


### tryFromName()

All Currency* enums have the Alpha3 code as the name. 

```php
$currencyName = CurrencyName::tryFromName('EUR'); // CurrencyName::EUR
$currencyName->value; // Euro
```

### toArray()

Gets an array with all names ("keys") / values in the Enum.

```php
CurrencyName::toArray();
// [
//    'AED' => 'UAE Dirham',
//    'AFN' => 'Afghani',
//    'ALL' => 'Lek',
//    'AMD' => 'Armenian Dram',
//    'ANG' => 'Netherlands Antillean Guilder',
//    ...
```

### names()

Gets an array with all names ("keys") in the Enum.

```php
CurrencyName::names();
// [
//    'AED',
//    'AFN',
//    'ALL',
//    'AMD',
//    'ANG',
//    ...
```

### values()

Gets an array with all values in the Enum.

```php
CurrencyName::values();
// [
//    'UAE Dirham',
//    'Afghani',
//    'Lek',
//    'Armenian Dram',
//    'Netherlands Antillean Guilder',
//    ...
```

## Sorting cases:

!!! abstract "These helper sorting methods work for all language Enums:"
    * [Currency*::casesSorted()](./#casessorted)
    * [Currency*::casesSortedByCurrencyName()](./#sorted-by-currency-name)
    * [Currency*::casesSortedByCurrencyName($language)](./#sorted-by-currency-name-translated)

### casesSorted()

Gets an array with the cases sorted by name

```php
CurrencyName::casesSorted();
// [
//    CurrencyName::AED, // UAE Dirham
//    CurrencyName::AFN, // Afghani
//    CurrencyName::ALL, // Lek
//    CurrencyName::AMD, // Armenian Dram
//    CurrencyName::ANG, // Netherlands Antillean Guilder
//    ...
```

### sorted by currency name

Gets an array with the cases sorted by currency name.

```php
CurrencyName::casesSortedByCurrencyName();
// [
//    CurrencyName::XUA, // ADB Unit of Account
//    CurrencyName::AFN, // Afghani
//    CurrencyName::DZD, // Algerian Dinar
//    CurrencyName::ARS, // Argentine Peso
//    CurrencyName::AMD, // Armenian Dram
//    ...
```

### sorted by currency name (translated)

Gets an array with the cases sorted by currency name in the specified language.

```php
$language = CurrencyNameTranslation::tryFrom('nl_NL'); // normal locale string
$language = CurrencyNameTranslation::tryFromName('NL_NL'); // or all uppercase

CurrencyName::casesSortedByCurrencyName($language);
// [
//    ...
//    CurrencyName::USD, // Amerikaanse dollar
//    CurrencyName::USN, // Amerikaanse dollar (volgende dag)
//    CurrencyName::AOA, // Angolese kwanza
//    CurrencyName::ANG, // Antilliaanse gulden
//    ...
```

??? tip "Current translated languages are:"
    * Belarusian
    * Catalan; Valencian
    * Chinese (Simplified)
    * Croatian
    * Dutch; Flemish
    * Estonian
    * French
    * German
    * Hindi
    * Hungarian
    * Icelandic
    * Indonesian
    * Irish
    * Korean
    * Lithuanian
    * Polish
    * Romanian; Moldavian; Moldovan
    * Sardinian
    * Spanish; Castilian
    * Tamil
    * Turkish
    * Ukrainian
