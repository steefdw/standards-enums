---
title: Using the Language Enums
---

# Using the Language Enums

```php
$language = LanguageAlpha2::tryFrom('nl'); // LanguageAlpha2::NLD

$languageName = $language->getName()->value;           // 'Dutch; Flemish'
$languageAlpha2 = $languageName->getAlpha2()->value;   // 'nl'
$languageAlpha3 = $languageAlpha2->getAlpha3()->value; // 'nld'
// these methods work for all language Enums

// this also works the other way around:
LanguageAlpha2::tryFromLanguageName($languageName); // LanguageAlpha2::NLD
LanguageAlpha2::tryFromAlpha2($languageAlpha2);     // LanguageAlpha2::NLD
LanguageAlpha2::tryFromAlpha3($languageAlpha3);     // LanguageAlpha2::NLD
// these methods work for all language Enums
```

## Get countries from a language

You can get al list of countries from a language with:

```php
$dutch = LanguageAlpha2::tryFrom('nl');
$countries = $dutch->getCountries();
// [
//   'AW' => CountryName::AW,
//   'BE' => CountryName::BE,
//   'BQ' => CountryName::BQ,
//   'CW' => CountryName::CW,
//   'NL' => CountryName::NL,
//   'SR' => CountryName::SR,
//   'SX' => CountryName::SX,
// ]
```

## Get locales from a language

You can get al list of locales from a language with:

```php
$dutch = LanguageAlpha2::tryFrom('nl');
$countries = $dutch->getLocales();
// [
//   'NL_AW' => LocaleDescription::NL_AW,
//   'NL_BE' => LocaleDescription::NL_BE,
//   'NL_BQ' => LocaleDescription::NL_BQ,
//   'NL_CW' => LocaleDescription::NL_CW,
//   'NL_NL' => LocaleDescription::NL_NL,
//   'NL_SR' => LocaleDescription::NL_SR,
//   'NL_SX' => LocaleDescription::NL_SX,
// ]
```

## Helper methods:

!!! abstract "These helper methods work for all language Enums:"
    * [Language*::tryFromName()](./#tryfromname)
    * [Language*::toArray()](./#toarray)
    * [Language*::names()](./#names)
    * [Language*::values()](./#values)


### tryFromName()

All Language* enums have the Alpha3 code as the name. 

```php
$languageName = LanguageName::tryFromName('ENG'); // LanguageName::ENG
$languageName->value; // English
```

### toArray()

Gets an array with all names ("keys") / values in the Enum.

```php
LanguageName::toArray();
// [
//    'AAR' => 'Afar',
//    'ABK' => 'Abkhazian',
//    'ACE' => 'Achinese',
//    'ACH' => 'Acoli',
//    ...
```

### names()

Gets an array with all names ("keys") in the Enum.

```php
LanguageName::names();
// [
//    'AAR',
//    'ABK',
//    'ACE',
//    'ACH',
//    ...
```

### values()

Gets an array with all values in the Enum.

```php
LanguageName::values();
// [
//    'Afar',
//    'Abkhazian',
//    'Achinese',
//    'Acoli',
//    ...
```

## Sorting cases:

!!! abstract "These helper sorting methods work for all language Enums:"
    * [Language*::casesSorted()](./#casessorted)
    * [Language*::casesSortedByLanguageName()](./#sorted-by-language-name)
    * [Language*::casesSortedByLanguageName(LanguageNameTranslation::NL)](./#sorted-by-language-name-translated)


### casesSorted()

Gets an array with the cases sorted by name

```php
LanguageName::casesSorted();
// [
//    LanguageName::AAR, // Afar
//    LanguageName::ABK, // Abkhazian
//    LanguageName::ACE, // Achinese
//    LanguageName::ACH, // Acoli
//    ...
```

### sorted by language name

Gets an array with the cases sorted by language name.

```php
LanguageName::casesSortedByLanguageName();
// [
//    LanguageName::ABK, // Abkhazian
//    LanguageName::ACE, // Achinese
//    LanguageName::ACH, // Acoli
//    LanguageName::ADA, // Adangme
//    ...
```

### sorted by language name (translated)

Gets an array with the cases sorted by language name in the specified language.

```php
$language = LanguageNameTranslation::tryFrom('nl_NL'); // normal locale string
$language = LanguageNameTranslation::tryFromName('NL_NL'); // or all uppercase

LanguageName::casesSortedByLanguageName($language);
// [
//    ...
//    LanguageName::SMA, // Zuid-Sami
//    LanguageName::ZUN, // Zuni
//    LanguageName::SWE, // Zweeds
//    LanguageName::GSW, // Zwitserduits, Alemannisch en Elzassisch
//    ...
```

??? tip "Current translated languages are:"
    * Albanian
    * Assamese
    * Belarusian
    * Brazilian Portuguese
    * Catalan; Valencian
    * Chinese (Hong Kong)
    * Chinese (Simplified)
    * Chinese (Traditional)
    * Czech
    * Dutch; Flemish
    * Estonian
    * French
    * Friulian
    * German
    * Gujarati
    * Hindi
    * Hungarian
    * Icelandic
    * Indonesian
    * Irish
    * Italian
    * Korean
    * Lithuanian
    * Panjabi; Punjabi
    * Polish
    * Portuguese
    * Romanian; Moldavian; Moldovan
    * Russian
    * Sardinian
    * Slovak
    * Spanish; Castilian
    * Swedish
    * Tamil
    * Turkish
    * Ukrainian
