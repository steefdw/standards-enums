---
title: About ISO 639
---

# About the ISO standard

## ISO 639: languages and language groups

Codes for the representation of names for languages and language groups.

The language codes defined in the several sections of ISO 639 are used for bibliographic purposes and, 
in computing and internet environments, as a key element of locale data.

### ISO 639-1 <small>([wiki](https://en.wikipedia.org/wiki/ISO_639-1))</small>

The **two-letter** codes given for each language in this part of the standard are referred to as "Alpha-2" codes.
There are 183 two-letter codes registered as of June 2021. The registered codes cover the world's major languages.

These codes are a useful international and formal shorthand for indicating languages.

!!! info "The ISO 639-1 codes are found in this enum:"
    `Steefdw\StandardsEnums\ISO639\LanguageAlpha2`

### ISO 639-2 <small>([wiki](https://en.wikipedia.org/wiki/ISO_639-2))</small>
 
The **three-letter** codes given for each language in this part of the standard are referred to as "Alpha-3" codes. 
There are 487 entries in [the list of ISO 639-2 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes). 

!!! info "The ISO 639-2 codes are found in this enum:"
    `Steefdw\StandardsEnums\ISO639\LanguageAlpha3`
