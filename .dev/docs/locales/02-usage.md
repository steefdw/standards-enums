---
title: Using the Locales Enums
---

# Using the Locales Enums

Let's use the `nl_BE` locale from the [example](../01-about/#example) on the "about locales" page:

```php
$locale = LocaleCode::tryFromName('nl_BE'); // LocaleCode::NL_BE
// is the same as:
$locale = LocaleCode::NL_BE;
```

## Get the description from a locale

```php
$description = $locale->getDescription(); // LocaleDescription::NL_BE

echo $description->value; // 'Dutch (Belgium)'
```

## Get the language from a locale

```php
$language = $locale->getLanguage(); // LanguageName::NLD

echo $language->value;              // 'Dutch; Flemish'
echo $language->getAlpha2()->value; // 'nl'
echo $language->getAlpha3()->value; // 'nld'
```

## Get the country from a locale

```php
$country = LocaleCode::NL_BE->getCountry(); // CountryName::BE

echo $country->getName()->value;  // 'Belgium'
echo $country->getFlag()->value;  // '🇧🇪'
```

## Get the locales from a language

```php
$dutch = LanguageName::NLD;
$locales = $dutch->getLocales();
// [
//     LocaleDescription::NL_AW, // 'Dutch (Aruba)
//     LocaleDescription::NL_BE, // 'Dutch (Belgium)
//     LocaleDescription::NL_BQ, // 'Dutch (Caribbean Netherlands)
//     LocaleDescription::NL_CW, // 'Dutch (Curaçao)
//     LocaleDescription::NL_NL, // 'Dutch (Netherlands)
//     LocaleDescription::NL_SR, // 'Dutch (Suriname)
//     LocaleDescription::NL_SX, // 'Dutch (Sint Maarten)
// ];
```

## Get the locales from a country

```php
$belgium = CountryName::BE;
$locales = $belgium->getLocales();
// [
//     LocaleDescription::DE_BE', // German (Belgium)
//     LocaleDescription::FR_BE', // French (Belgium)
//     LocaleDescription::LI_BE', // Limburgish (Belgium)
//     LocaleDescription::NL_BE', // Dutch (Belgium)
//     LocaleDescription::WA_BE', // Walloon (Belgium)
// ]
```
