---
title: About locales
---

# About locales

In computing, a locale is a set of parameters that defines the user's language, 
region and any special variant preferences that the user wants to see in their user interface. 

Usually a locale identifier consists of at least a language code and a country/region code. 
Locale is an important aspect of i18n.

!!! info "The locale codes are found in this enum:"
    `Steefdw\StandardsEnums\Locales\LocaleCode`

## Example

```php
 nl_BE
```

| Code | What is it? | ISO code           |
|------|-------------|--------------------|
| nl   | language    | ISO-639-1          |
| BE   | country     | ISO 3166-1 alpha-2 |

