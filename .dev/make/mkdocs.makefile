.PHONY: docs-build
## Create a static site site from the documentation
docs-build:
	docker run --rm -it -v ${PWD}/.dev:/docs squidfunk/mkdocs-material build --site-dir public

.PHONY: docs-watch
## Watch the documentation in the browser
docs-watch:
	@$(call msg_docker, "Docs available at: http://127.0.0.1:8009/")
	docker run --rm -it -p 8009:8000 -v ${PWD}/.dev:/docs squidfunk/mkdocs-material