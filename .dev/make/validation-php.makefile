## Run all PHP validations
validate: syntax cs cs-fixer md stan

## Run all PHP validations (only changed files)
ff: fast-syntax fast-cs fast-cs-fixer fast-stan md

changed-files = `git diff --name-only -- '***.php'`

## Validate the PHP syntax
syntax:
	$(call msg_validate, 'syntax check') && \
	find src tests -name "*.php" -print0 | xargs -0 -n1 -P8 php -l

## Validate the PHP syntax (only changed files)
fast-syntax:
	$(call msg_validate, 'fast syntax check') && \
	changes=$(shell git diff --name-only -- '***.php'); \
	if [ -z $$changes ]; then echo "no changes found, so nothing to validate" && exit; fi && \
	git diff --name-only -- '***.php' | xargs -0 -d'\n' -n1 -P8 php -l

## Validate the PHP code-style (phpcs)
cs:
	$(call msg_validate, 'phpcs') && \
	./vendor/bin/phpcs --standard=.dev/config/phpcs.xml --colors -ps

## Validate the PHP code-style (phpcs)
fast-cs:
	$(call msg_validate, 'fast phpcs') && \
	changes=$(shell git diff --name-only -- '***.php'); \
	if [ -z $$changes ]; then echo "no changes found, so nothing to validate" && exit; fi && \
	./vendor/bin/phpcs $(changed-files) --standard=.dev/config/phpcs.xml --colors -ps

## Validate the PHP code-style (php-cs-fixer)
cs-fixer:
	$(call msg_validate, 'php-cs-fixer') && \
	./vendor/bin/php-cs-fixer fix src tests \
	--dry-run --diff --allow-risky=yes --config=.dev/config/php-cs-fixer.php

## Validate the PHP code-style (php-cs-fixer, only changed files)
fast-cs-fixer:
	$(call msg_validate, 'fast php-cs-fixer') && \
	changes=$(shell git diff --name-only -- '***.php'); \
	if [ -z $$changes ]; then echo "no changes found, so nothing to validate" && exit; fi && \
	./vendor/bin/php-cs-fixer fix $(changed-files) \
	--dry-run --diff --allow-risky=yes --config=.dev/config/php-cs-fixer.php

## Fix the PHP code-style (php-cs-fixer)
cs-fixer-fix:
	$(call msg_fix, 'php-cs-fixer') && \
	./vendor/bin/php-cs-fixer fix src tests \
	--diff --allow-risky=yes --config=.dev/config/php-cs-fixer.php

## Validate the PHP code-style (phpmd)
md:
	$(call msg_validate, 'phpmd') && \
	./vendor/bin/phpmd src ansi .dev/config/phpmd.xml

## Validate the PHP code statically (phpstan)
stan:
	$(call msg_validate, 'phpstan') && \
	./vendor/bin/phpstan analyse --memory-limit=2G --configuration=.dev/config/phpstan.neon

## Validate the PHP code statically (phpstan, only changed files)
fast-stan:
	$(call msg_validate, 'fast phpstan') && \
	changes=$(shell git diff --name-only -- '***.php'); \
	if [ -z $$changes ]; then echo "no changes found, so nothing to validate" && exit; fi && \
	./vendor/bin/phpstan analyse --memory-limit=2G --configuration=.dev/config/phpstan.neon \
	`git diff --name-only -- '***.php'`

## Fix the PHP code-style (phpcbf)
phpcbf-fix:
	$(call msg_fix, 'phpcbf') && \
	./vendor/bin/phpcbf src tests --colors

## Fix the PHP code-style
fix: cs-fixer-fix phpcbf-fix
