include ./.dev/make/main.makefile
include ./.dev/make/validation-php.makefile
include ./.dev/make/mkdocs.makefile

## Run tests quickly
test:
	./vendor/bin/phpunit -c .dev/config/phpunit.xml

## Code Quality: run validations + tests
cq: validate test

## Run `composer install`
composer:
	composer install --no-interaction --prefer-dist
