<?php

declare(strict_types=1);

use Steefdw\StandardsEnums\ISO3166\CountryName;

/**
 * @see https://en.wikipedia.org/wiki/List_of_circulating_currencies
 */
$currencyCountries = [
    'AED' => [                   // UAE Dirham
        'AE' => CountryName::AE, // - United Arab Emirates
    ],
    'AFN' => [                   // Afghani
        'AF' => CountryName::AF, // - Afghanistan
    ],
    'ALL' => [                   // Lek
        'AL' => CountryName::AL, // - Albania
    ],
    'AMD' => [                   // Armenian Dram
        'AM' => CountryName::AM, // - Armenia
    ],
    'ANG' => [                   // Netherlands Antillean Guilder
        'CW' => CountryName::CW, // - Curaçao
        'SX' => CountryName::SX, // - Sint Maarten (Dutch part)
    ],
    'AOA' => [                   // Kwanza
        'AO' => CountryName::AO, // - Angola
    ],
    'ARS' => [                   // Argentine Peso
        'AR' => CountryName::AR, // - Argentina
    ],
    'AUD' => [                   // Australian Dollar
        'AU' => CountryName::AU, // - Australia
        'KI' => CountryName::KI, // - Kiribati
        'NR' => CountryName::NR, // - Nauru
        'TV' => CountryName::TV, // - Tuvalu
    ],
    'AWG' => [                   // Aruban Florin
        'AW' => CountryName::AW, // - Aruba
    ],
    'AZN' => [                   // Azerbaijan Manat
        'AZ' => CountryName::AZ, // - Azerbaijan
    ],
    'BAM' => [                   // Convertible Mark
        'BA' => CountryName::BA, // - Bosnia and Herzegovina
    ],
    'BBD' => [                   // Barbados Dollar
        'BB' => CountryName::BB, // - Barbados
    ],
    'BDT' => [                   // Taka
        'BD' => CountryName::BD, // - Bangladesh
    ],
    'BGN' => [                   // Bulgarian Lev
        'BG' => CountryName::BG, // - Bulgaria
    ],
    'BHD' => [                   // Bahraini Dinar
        'BH' => CountryName::BH, // - Bahrain
    ],
    'BIF' => [                   // Burundi Franc
        'BI' => CountryName::BI, // - Burundi
    ],
    'BMD' => [                   // Bermudian Dollar
        'BM' => CountryName::BM, // - Bermuda
    ],
    'BND' => [                   // Brunei Dollar
        'BN' => CountryName::BN, // - Brunei Darussalam
        'SG' => CountryName::SG, // - Singapore
    ],
    'BOB' => [                   // Boliviano
        'BO' => CountryName::BO, // - Bolivia
    ],
    'BRL' => [                   // Brazilian Real
        'BR' => CountryName::BR, // - Brazil
    ],
    'BSD' => [                   // Bahamian Dollar
        'BS' => CountryName::BS, // - Bahamas
    ],
    'BTN' => [                   // Ngultrum
        'BT' => CountryName::BT, // - Bhutan
    ],
    'BWP' => [                   // Pula
        'BW' => CountryName::BW, // - Botswana
    ],
    'BYN' => [                   // Belarusian Ruble
        'BY' => CountryName::BY, // - Belarus
    ],
    'BZD' => [                   // Belize Dollar
        'BZ' => CountryName::BZ, // - Belize
    ],
    'CAD' => [                   // Canadian Dollar
        'CA' => CountryName::CA, // - Canada
        'PM' => CountryName::PM, // - Saint Pierre and Miquelon
    ],
    'CDF' => [                   // Congolese Franc
        'CD' => CountryName::CD, // - Congo, The Democratic Republic of the
    ],
    'CHF' => [                   // Swiss Franc
        'CH' => CountryName::CH, // - Switzerland
        'LI' => CountryName::LI, // - Liechtenstein
    ],
    'CLP' => [                   // Chilean Peso
        'CL' => CountryName::CL, // - Chile
    ],
    'CNY' => [                   // Yuan Renminbi
        'CN' => CountryName::CN, // - China
    ],
    'COP' => [                   // Colombian Peso
        'CO' => CountryName::CO, // - Colombia
    ],
    'CRC' => [                   // Costa Rican Colon
        'CR' => CountryName::CR, // - Costa Rica
    ],
    'CUP' => [                   // Cuban Peso
        'CU' => CountryName::CU, // - Cuba
    ],
    'CVE' => [                   // Cabo Verde Escudo
        'CV' => CountryName::CV, // - Cabo Verde
    ],
    'CZK' => [                   // Czech Koruna
        'CZ' => CountryName::CZ, // - Czechia
    ],
    'DJF' => [                   // Djibouti Franc
        'DJ' => CountryName::DJ, // - Djibouti
    ],
    'DKK' => [                   // Danish Krone
        'DK' => CountryName::DK, // - Denmark
        'FO' => CountryName::FO, // - Faroe Islands
        'GL' => CountryName::GL, // - Greenland
    ],
    'DOP' => [                   // Dominican Peso
        'DO' => CountryName::DO, // - Dominican Republic
    ],
    'DZD' => [                   // Algerian Dinar
        'DZ' => CountryName::DZ, // - Algeria
    ],
    'EGP' => [                   // Egyptian Pound
        'EG' => CountryName::EG, // - Egypt
    ],
    'ERN' => [                   // Nakfa
        'ER' => CountryName::ER, // - Eritrea
    ],
    'ETB' => [                   // Ethiopian Birr
        'ET' => CountryName::ET, // - Ethiopia
    ],
    'EUR' => [                   // Euro
        'AD' => CountryName::AD, // - Andorra
        'AT' => CountryName::AT, // - Austria
        'BE' => CountryName::BE, // - Belgium
        'CY' => CountryName::CY, // - Cyprus
        'DE' => CountryName::DE, // - Germany
        'EE' => CountryName::EE, // - Estonia
        'ES' => CountryName::ES, // - Spain
        'FI' => CountryName::FI, // - Finland
        'FR' => CountryName::FR, // - France
        'GR' => CountryName::GR, // - Greece
        'HR' => CountryName::HR, // - Croatia
        'IE' => CountryName::IE, // - Ireland
        'IT' => CountryName::IT, // - Italy
        'LT' => CountryName::LT, // - Lithuania
        'LU' => CountryName::LU, // - Luxembourg
        'LV' => CountryName::LV, // - Latvia
        'MC' => CountryName::MC, // - Monaco
        'ME' => CountryName::ME, // - Montenegro
        'MT' => CountryName::MT, // - Malta
        'NL' => CountryName::NL, // - Netherlands
        'PM' => CountryName::PM, // - Saint Pierre and Miquelon
        'PT' => CountryName::PT, // - Portugal
        'SI' => CountryName::SI, // - Slovenia
        'SK' => CountryName::SK, // - Slovakia
        'SM' => CountryName::SM, // - San Marino
        'VA' => CountryName::VA, // - Holy See (Vatican City State)
    ],
    'FJD' => [                   // Fiji Dollar
        'FJ' => CountryName::FJ, // - Fiji
    ],
    'FKP' => [                   // Falkland Islands Pound
        'FK' => CountryName::FK, // - Falkland Islands (Malvinas)
        'GS' => CountryName::GS, // - South Georgia and the South Sandwich Islands
    ],
    'GBP' => [                   // Pound Sterling
        'FK' => CountryName::FK, // - Falkland Islands (Malvinas)
        'GB' => CountryName::GB, // - United Kingdom
        'GG' => CountryName::GG, // - Guernsey
        'GI' => CountryName::GI, // - Gibraltar
        'GS' => CountryName::GS, // - South Georgia and the South Sandwich Islands
        'IM' => CountryName::IM, // - Isle of Man
        'JE' => CountryName::JE, // - Jersey
        'SH' => CountryName::SH, // - Saint Helena, Ascension and Tristan da Cunha
    ],
    'GEL' => [                   // Lari
        'GE' => CountryName::GE, // - Georgia
    ],
    'GHS' => [                   // Ghana Cedi
        'GH' => CountryName::GH, // - Ghana
    ],
    'GIP' => [                   // Gibraltar Pound
        'GI' => CountryName::GI, // - Gibraltar
    ],
    'GMD' => [                   // Dalasi
        'GM' => CountryName::GM, // - Gambia
    ],
    'GNF' => [                   // Guinean Franc
        'GN' => CountryName::GN, // - Guinea
    ],
    'GTQ' => [                   // Quetzal
        'GT' => CountryName::GT, // - Guatemala
    ],
    'GYD' => [                   // Guyana Dollar
        'GY' => CountryName::GY, // - Guyana
    ],
    'HKD' => [                   // Hong Kong Dollar
        'HK' => CountryName::HK, // - Hong Kong
        'MO' => CountryName::MO, // - Macao
    ],
    'HNL' => [                   // Lempira
        'HN' => CountryName::HN, // - Honduras
    ],
    'HTG' => [                   // Gourde
        'HT' => CountryName::HT, // - Haiti
    ],
    'HUF' => [                   // Forint
        'HU' => CountryName::HU, // - Hungary
    ],
    'IDR' => [                   // Rupiah
        'ID' => CountryName::ID, // - Indonesia
    ],
    'ILS' => [                   // New Israeli Sheqel
        'IL' => CountryName::IL, // - Israel
        'PS' => CountryName::PS, // - Palestine, State of
    ],
    'INR' => [                   // Indian Rupee
        'BT' => CountryName::BT, // - Bhutan
        'IN' => CountryName::IN, // - India
        'NP' => CountryName::NP, // - Nepal
    ],
    'IQD' => [                   // Iraqi Dinar
        'IQ' => CountryName::IQ, // - Iraq
    ],
    'IRR' => [                   // Iranian Rial
        'IR' => CountryName::IR, // - Iran
    ],
    'ISK' => [                   // Iceland Krona
        'IS' => CountryName::IS, // - Iceland
    ],
    'JMD' => [                   // Jamaican Dollar
        'JM' => CountryName::JM, // - Jamaica
    ],
    'JOD' => [                   // Jordanian Dinar
        'JO' => CountryName::JO, // - Jordan
        'PS' => CountryName::PS, // - Palestine, State of
    ],
    'JPY' => [                   // Yen
        'JP' => CountryName::JP, // - Japan
    ],
    'KES' => [                   // Kenyan Shilling
        'KE' => CountryName::KE, // - Kenya
    ],
    'KGS' => [                   // Som
        'KG' => CountryName::KG, // - Kyrgyzstan
    ],
    'KHR' => [                   // Riel
        'KH' => CountryName::KH, // - Cambodia
    ],
    'KMF' => [                   // Comorian Franc
        'KM' => CountryName::KM, // - Comoros
    ],
    'KPW' => [                   // North Korean Won
        'KP' => CountryName::KP, // - North Korea
    ],
    'KRW' => [                   // Won
        'KR' => CountryName::KR, // - South Korea
    ],
    'KWD' => [                   // Kuwaiti Dinar
        'KW' => CountryName::KW, // - Kuwait
    ],
    'KYD' => [                   // Cayman Islands Dollar
        'KY' => CountryName::KY, // - Cayman Islands
    ],
    'KZT' => [                   // Tenge
        'KZ' => CountryName::KZ, // - Kazakhstan
    ],
    'LAK' => [                   // Lao Kip
        'LA' => CountryName::LA, // - Laos
    ],
    'LBP' => [                   // Lebanese Pound
        'LB' => CountryName::LB, // - Lebanon
    ],
    'LKR' => [                   // Sri Lanka Rupee
        'LK' => CountryName::LK, // - Sri Lanka
    ],
    'LRD' => [                   // Liberian Dollar
        'LR' => CountryName::LR, // - Liberia
    ],
    'LSL' => [                   // Loti
        'LS' => CountryName::LS, // - Lesotho
    ],
    'LYD' => [                   // Libyan Dinar
        'LY' => CountryName::LY, // - Libya
    ],
    'MAD' => [                   // Moroccan Dirham
        'EH' => CountryName::EH, // - Western Sahara
        'MA' => CountryName::MA, // - Morocco
    ],
    'MDL' => [                   // Moldovan Leu
        'MD' => CountryName::MD, // - Moldova
    ],
    'MGA' => [                   // Malagasy Ariary
        'MG' => CountryName::MG, // - Madagascar
    ],
    'MKD' => [                   // Denar
        'MK' => CountryName::MK, // - North Macedonia
    ],
    'MMK' => [                   // Kyat
        'MM' => CountryName::MM, // - Myanmar
    ],
    'MNT' => [                   // Tugrik
        'MN' => CountryName::MN, // - Mongolia
    ],
    'MOP' => [                   // Pataca
        'MO' => CountryName::MO, // - Macao
    ],
    'MRU' => [                   // Ouguiya
        'MR' => CountryName::MR, // - Mauritania
    ],
    'MUR' => [                   // Mauritius Rupee
        'MU' => CountryName::MU, // - Mauritius
    ],
    'MVR' => [                   // Rufiyaa
        'MV' => CountryName::MV, // - Maldives
    ],
    'MWK' => [                   // Malawi Kwacha
        'MW' => CountryName::MW, // - Malawi
    ],
    'MXN' => [                   // Mexican Peso
        'MX' => CountryName::MX, // - Mexico
    ],
    'MYR' => [                   // Malaysian Ringgit
        'MY' => CountryName::MY, // - Malaysia
    ],
    'MZN' => [                   // Mozambique Metical
        'MZ' => CountryName::MZ, // - Mozambique
    ],
    'NAD' => [                   // Namibia Dollar
        'NA' => CountryName::NA, // - Namibia
    ],
    'NGN' => [                   // Naira
        'NG' => CountryName::NG, // - Nigeria
    ],
    'NIO' => [                   // Cordoba Oro
        'NI' => CountryName::NI, // - Nicaragua
    ],
    'NOK' => [                   // Norwegian Krone
        'NO' => CountryName::NO, // - Norway
    ],
    'NPR' => [                   // Nepalese Rupee
        'NP' => CountryName::NP, // - Nepal
    ],
    'NZD' => [                   // New Zealand Dollar
        'CK' => CountryName::CK, // - Cook Islands
        'NU' => CountryName::NU, // - Niue
        'NZ' => CountryName::NZ, // - New Zealand
        'PN' => CountryName::PN, // - Pitcairn
    ],
    'OMR' => [                   // Rial Omani
        'OM' => CountryName::OM, // - Oman
    ],
    'PAB' => [                   // Balboa
        'PA' => CountryName::PA, // - Panama
    ],
    'PEN' => [                   // Sol
        'PE' => CountryName::PE, // - Peru
    ],
    'PGK' => [                   // Kina
        'PG' => CountryName::PG, // - Papua New Guinea
    ],
    'PHP' => [                   // Philippine Peso
        'PH' => CountryName::PH, // - Philippines
    ],
    'PKR' => [                   // Pakistan Rupee
        'PK' => CountryName::PK, // - Pakistan
    ],
    'PLN' => [                   // Zloty
        'PL' => CountryName::PL, // - Poland
    ],
    'PYG' => [                   // Guarani
        'PY' => CountryName::PY, // - Paraguay
    ],
    'QAR' => [                   // Qatari Rial
        'QA' => CountryName::QA, // - Qatar
    ],
    'RON' => [                   // Romanian Leu
        'RO' => CountryName::RO, // - Romania
    ],
    'RSD' => [                   // Serbian Dinar
        'RS' => CountryName::RS, // - Serbia
    ],
    'RUB' => [                   // Russian Ruble
        'RU' => CountryName::RU, // - Russian Federation
    ],
    'RWF' => [                   // Rwanda Franc
        'RW' => CountryName::RW, // - Rwanda
    ],
    'SAR' => [                   // Saudi Riyal
        'SA' => CountryName::SA, // - Saudi Arabia
    ],
    'SBD' => [                   // Solomon Islands Dollar
        'SB' => CountryName::SB, // - Solomon Islands
    ],
    'SCR' => [                   // Seychelles Rupee
        'SC' => CountryName::SC, // - Seychelles
    ],
    'SDG' => [                   // Sudanese Pound
        'SD' => CountryName::SD, // - Sudan
    ],
    'SEK' => [                   // Swedish Krona
        'SE' => CountryName::SE, // - Sweden
    ],
    'SGD' => [                   // Singapore Dollar
        'BN' => CountryName::BN, // - Brunei Darussalam
        'SG' => CountryName::SG, // - Singapore
    ],
    'SHP' => [                   // Saint Helena Pound
        'SH' => CountryName::SH, // - Saint Helena, Ascension and Tristan da Cunha
    ],
    'SLE' => [                   // Leone
        'SL' => CountryName::SL, // - Sierra Leone
    ],
    'SOS' => [                   // Somali Shilling
        'SO' => CountryName::SO, // - Somalia
    ],
    'SRD' => [                   // Surinam Dollar
        'SR' => CountryName::SR, // - Suriname
    ],
    'SSP' => [                   // South Sudanese Pound
        'SS' => CountryName::SS, // - South Sudan
    ],
    'STN' => [                   // Dobra
        'ST' => CountryName::ST, // - Sao Tome and Principe
    ],
    'SYP' => [                   // Syrian Pound
        'SY' => CountryName::SY, // - Syria
    ],
    'SZL' => [                   // Lilangeni
        'SZ' => CountryName::SZ, // - Eswatini
    ],
    'THB' => [                   // Baht
        'TH' => CountryName::TH, // - Thailand
    ],
    'TJS' => [                   // Somoni
        'TJ' => CountryName::TJ, // - Tajikistan
    ],
    'TMT' => [                   // Turkmenistan New Manat
        'TM' => CountryName::TM, // - Turkmenistan
    ],
    'TND' => [                   // Tunisian Dinar
        'TN' => CountryName::TN, // - Tunisia
    ],
    'TOP' => [                   // Pa’anga
        'TO' => CountryName::TO, // - Tonga
    ],
    'TRY' => [                   // Turkish Lira
        'SY' => CountryName::SY, // - Syria
        'TR' => CountryName::TR, // - Türkiye
    ],
    'TTD' => [                   // Trinidad and Tobago Dollar
        'TT' => CountryName::TT, // - Trinidad and Tobago
    ],
    'TWD' => [                   // New Taiwan Dollar
        'TW' => CountryName::TW, // - Taiwan
    ],
    'TZS' => [                   // Tanzanian Shilling
        'TZ' => CountryName::TZ, // - Tanzania
    ],
    'UAH' => [                   // Hryvnia
        'UA' => CountryName::UA, // - Ukraine
    ],
    'UGX' => [                   // Uganda Shilling
        'UG' => CountryName::UG, // - Uganda
    ],
    'USD' => [                   // US Dollar
        'BQ' => CountryName::BQ, // - Bonaire, Sint Eustatius and Saba
        'EC' => CountryName::EC, // - Ecuador
        'FM' => CountryName::FM, // - Micronesia, Federated States of
        'IO' => CountryName::IO, // - British Indian Ocean Territory
        'KH' => CountryName::KH, // - Cambodia
        'LR' => CountryName::LR, // - Liberia
        'MH' => CountryName::MH, // - Marshall Islands
        'PA' => CountryName::PA, // - Panama
        'PW' => CountryName::PW, // - Palau
        'SV' => CountryName::SV, // - El Salvador
        'TC' => CountryName::TC, // - Turks and Caicos Islands
        'TL' => CountryName::TL, // - Timor-Leste
        'US' => CountryName::US, // - United States
        'VE' => CountryName::VE, // - Venezuela
        'VG' => CountryName::VG, // - Virgin Islands, British
        'ZW' => CountryName::ZW, // - Zimbabwe
    ],
    'UYU' => [                   // Peso Uruguayo
        'UY' => CountryName::UY, // - Uruguay
    ],
    'UZS' => [                   // Uzbekistan Sum
        'UZ' => CountryName::UZ, // - Uzbekistan
    ],
    'VED' => [                   // Bolívar Soberano
        'VE' => CountryName::VE, // - Venezuela
    ],
    'VES' => [                   // Bolívar Soberano
        'VE' => CountryName::VE, // - Venezuela
    ],
    'VND' => [                   // Dong
        'VN' => CountryName::VN, // - Vietnam
    ],
    'VUV' => [                   // Vatu
        'VU' => CountryName::VU, // - Vanuatu
    ],
    'WST' => [                   // Tala
        'WS' => CountryName::WS, // - Samoa
    ],
    'XAF' => [                   // CFA Franc BEAC
        'CF' => CountryName::CF, // - Central African Republic
        'CG' => CountryName::CG, // - Congo
        'CM' => CountryName::CM, // - Cameroon
        'GA' => CountryName::GA, // - Gabon
        'GQ' => CountryName::GQ, // - Equatorial Guinea
        'TD' => CountryName::TD, // - Chad
    ],
    'XCD' => [                   // East Caribbean Dollar
        'AG' => CountryName::AG, // - Antigua and Barbuda
        'AI' => CountryName::AI, // - Anguilla
        'DM' => CountryName::DM, // - Dominica
        'GD' => CountryName::GD, // - Grenada
        'KN' => CountryName::KN, // - Saint Kitts and Nevis
        'LC' => CountryName::LC, // - Saint Lucia
        'MS' => CountryName::MS, // - Montserrat
        'VC' => CountryName::VC, // - Saint Vincent and the Grenadines
    ],
    'XOF' => [                   // CFA Franc BCEAO
        'BF' => CountryName::BF, // - Burkina Faso
        'BJ' => CountryName::BJ, // - Benin
        'CI' => CountryName::CI, // - Côte d'Ivoire
        'GW' => CountryName::GW, // - Guinea-Bissau
        'ML' => CountryName::ML, // - Mali
        'NE' => CountryName::NE, // - Niger
        'SN' => CountryName::SN, // - Senegal
        'TG' => CountryName::TG, // - Togo
    ],
    'XPF' => [                   // CFP Franc
        'NC' => CountryName::NC, // - New Caledonia
        'PF' => CountryName::PF, // - French Polynesia
        'WF' => CountryName::WF, // - Wallis and Futuna
    ],
    'YER' => [                   // Yemeni Rial
        'YE' => CountryName::YE, // - Yemen
    ],
    'ZAR' => [                   // Rand
        'LS' => CountryName::LS, // - Lesotho
        'NA' => CountryName::NA, // - Namibia
        'SZ' => CountryName::SZ, // - Eswatini
        'ZA' => CountryName::ZA, // - South Africa
    ],
    'ZMW' => [                   // Zambian Kwacha
        'ZM' => CountryName::ZM, // - Zambia
    ],
    'ZWL' => [                   // Zimbabwe Dollar
        'ZW' => CountryName::ZW, // - Zimbabwe
    ],
];
