<?php

declare(strict_types=1);

use Steefdw\StandardsEnums\ISO4217\CurrencyName;

/**
 * @see https://en.wikipedia.org/wiki/List_of_circulating_currencies
 */
$countryCurrencies = [
    'AD' => [                       // Andorra
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'AE' => [                       // United Arab Emirates
        'AED' => CurrencyName::AED, // - UAE Dirham
    ],
    'AF' => [                       // Afghanistan
        'AFN' => CurrencyName::AFN, // - Afghani
    ],
    'AG' => [                       // Antigua and Barbuda
        'XCD' => CurrencyName::XCD, // - East Caribbean Dollar
    ],
    'AI' => [                       // Anguilla
        'XCD' => CurrencyName::XCD, // - East Caribbean Dollar
    ],
    'AL' => [                       // Albania
        'ALL' => CurrencyName::ALL, // - Lek
    ],
    'AM' => [                       // Armenia
        'AMD' => CurrencyName::AMD, // - Armenian Dram
    ],
    'AO' => [                       // Angola
        'AOA' => CurrencyName::AOA, // - Kwanza
    ],
    'AR' => [                       // Argentina
        'ARS' => CurrencyName::ARS, // - Argentine Peso
    ],
    'AT' => [                       // Austria
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'AU' => [                       // Australia
        'AUD' => CurrencyName::AUD, // - Australian Dollar
    ],
    'AW' => [                       // Aruba
        'AWG' => CurrencyName::AWG, // - Aruban Florin
    ],
    'AZ' => [                       // Azerbaijan
        'AZN' => CurrencyName::AZN, // - Azerbaijan Manat
    ],
    'BA' => [                       // Bosnia and Herzegovina
        'BAM' => CurrencyName::BAM, // - Convertible Mark
    ],
    'BB' => [                       // Barbados
        'BBD' => CurrencyName::BBD, // - Barbados Dollar
    ],
    'BD' => [                       // Bangladesh
        'BDT' => CurrencyName::BDT, // - Taka
    ],
    'BE' => [                       // Belgium
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'BF' => [                       // Burkina Faso
        'XOF' => CurrencyName::XOF, // - CFA Franc BCEAO
    ],
    'BG' => [                       // Bulgaria
        'BGN' => CurrencyName::BGN, // - Bulgarian Lev
    ],
    'BH' => [                       // Bahrain
        'BHD' => CurrencyName::BHD, // - Bahraini Dinar
    ],
    'BI' => [                       // Burundi
        'BIF' => CurrencyName::BIF, // - Burundi Franc
    ],
    'BJ' => [                       // Benin
        'XOF' => CurrencyName::XOF, // - CFA Franc BCEAO
    ],
    'BM' => [                       // Bermuda
        'BMD' => CurrencyName::BMD, // - Bermudian Dollar
    ],
    'BN' => [                       // Brunei Darussalam
        'BND' => CurrencyName::BND, // - Brunei Dollar
        'SGD' => CurrencyName::SGD, // - Singapore Dollar
    ],
    'BO' => [                       // Bolivia
        'BOB' => CurrencyName::BOB, // - Boliviano
    ],
    'BQ' => [                       // Bonaire, Sint Eustatius and Saba
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'BR' => [                       // Brazil
        'BRL' => CurrencyName::BRL, // - Brazilian Real
    ],
    'BS' => [                       // Bahamas
        'BSD' => CurrencyName::BSD, // - Bahamian Dollar
    ],
    'BT' => [                       // Bhutan
        'BTN' => CurrencyName::BTN, // - Ngultrum
        'INR' => CurrencyName::INR, // - Indian Rupee
    ],
    'BW' => [                       // Botswana
        'BWP' => CurrencyName::BWP, // - Pula
    ],
    'BY' => [                       // Belarus
        'BYN' => CurrencyName::BYN, // - Belarusian Ruble
    ],
    'BZ' => [                       // Belize
        'BZD' => CurrencyName::BZD, // - Belize Dollar
    ],
    'CA' => [                       // Canada
        'CAD' => CurrencyName::CAD, // - Canadian Dollar
    ],
    'CD' => [                       // Congo, The Democratic Republic of the
        'CDF' => CurrencyName::CDF, // - Congolese Franc
    ],
    'CF' => [                       // Central African Republic
        'XAF' => CurrencyName::XAF, // - CFA Franc BEAC
    ],
    'CG' => [                       // Congo
        'XAF' => CurrencyName::XAF, // - CFA Franc BEAC
    ],
    'CH' => [                       // Switzerland
        'CHF' => CurrencyName::CHF, // - Swiss Franc
    ],
    'CI' => [                       // Côte d'Ivoire
        'XOF' => CurrencyName::XOF, // - CFA Franc BCEAO
    ],
    'CK' => [                       // Cook Islands
        'NZD' => CurrencyName::NZD, // - New Zealand Dollar
    ],
    'CL' => [                       // Chile
        'CLP' => CurrencyName::CLP, // - Chilean Peso
    ],
    'CM' => [                       // Cameroon
        'XAF' => CurrencyName::XAF, // - CFA Franc BEAC
    ],
    'CN' => [                       // China
        'CNY' => CurrencyName::CNY, // - Yuan Renminbi
    ],
    'CO' => [                       // Colombia
        'COP' => CurrencyName::COP, // - Colombian Peso
    ],
    'CR' => [                       // Costa Rica
        'CRC' => CurrencyName::CRC, // - Costa Rican Colon
    ],
    'CU' => [                       // Cuba
        'CUP' => CurrencyName::CUP, // - Cuban Peso
    ],
    'CV' => [                       // Cabo Verde
        'CVE' => CurrencyName::CVE, // - Cabo Verde Escudo
    ],
    'CW' => [                       // Curaçao
        'ANG' => CurrencyName::ANG, // - Netherlands Antillean Guilder
    ],
    'CY' => [                       // Cyprus
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'CZ' => [                       // Czechia
        'CZK' => CurrencyName::CZK, // - Czech Koruna
    ],
    'DE' => [                       // Germany
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'DJ' => [                       // Djibouti
        'DJF' => CurrencyName::DJF, // - Djibouti Franc
    ],
    'DK' => [                       // Denmark
        'DKK' => CurrencyName::DKK, // - Danish Krone
    ],
    'DM' => [                       // Dominica
        'XCD' => CurrencyName::XCD, // - East Caribbean Dollar
    ],
    'DO' => [                       // Dominican Republic
        'DOP' => CurrencyName::DOP, // - Dominican Peso
    ],
    'DZ' => [                       // Algeria
        'DZD' => CurrencyName::DZD, // - Algerian Dinar
    ],
    'EC' => [                       // Ecuador
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'EE' => [                       // Estonia
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'EG' => [                       // Egypt
        'EGP' => CurrencyName::EGP, // - Egyptian Pound
    ],
    'EH' => [                       // Western Sahara
        'MAD' => CurrencyName::MAD, // - Moroccan Dirham
    ],
    'ER' => [                       // Eritrea
        'ERN' => CurrencyName::ERN, // - Nakfa
    ],
    'ES' => [                       // Spain
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'ET' => [                       // Ethiopia
        'ETB' => CurrencyName::ETB, // - Ethiopian Birr
    ],
    'FI' => [                       // Finland
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'FJ' => [                       // Fiji
        'FJD' => CurrencyName::FJD, // - Fiji Dollar
    ],
    'FK' => [                       // Falkland Islands (Malvinas)
        'FKP' => CurrencyName::FKP, // - Falkland Islands Pound
        'GBP' => CurrencyName::GBP, // - Pound Sterling
    ],
    'FM' => [                       // Micronesia, Federated States of
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'FO' => [                       // Faroe Islands
        'DKK' => CurrencyName::DKK, // - Danish Krone
    ],
    'FR' => [                       // France
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'GA' => [                       // Gabon
        'XAF' => CurrencyName::XAF, // - CFA Franc BEAC
    ],
    'GB' => [                       // United Kingdom
        'GBP' => CurrencyName::GBP, // - Pound Sterling
    ],
    'GD' => [                       // Grenada
        'XCD' => CurrencyName::XCD, // - East Caribbean Dollar
    ],
    'GE' => [                       // Georgia
        'GEL' => CurrencyName::GEL, // - Lari
    ],
    'GG' => [                       // Guernsey
        'GBP' => CurrencyName::GBP, // - Pound Sterling
    ],
    'GH' => [                       // Ghana
        'GHS' => CurrencyName::GHS, // - Ghana Cedi
    ],
    'GI' => [                       // Gibraltar
        'GBP' => CurrencyName::GBP, // - Pound Sterling
        'GIP' => CurrencyName::GIP, // - Gibraltar Pound
    ],
    'GL' => [                       // Greenland
        'DKK' => CurrencyName::DKK, // - Danish Krone
    ],
    'GM' => [                       // Gambia
        'GMD' => CurrencyName::GMD, // - Dalasi
    ],
    'GN' => [                       // Guinea
        'GNF' => CurrencyName::GNF, // - Guinean Franc
    ],
    'GQ' => [                       // Equatorial Guinea
        'XAF' => CurrencyName::XAF, // - CFA Franc BEAC
    ],
    'GR' => [                       // Greece
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'GS' => [                       // South Georgia and the South Sandwich Islands
        'FKP' => CurrencyName::FKP, // - Falkland Islands Pound
        'GBP' => CurrencyName::GBP, // - Pound Sterling
    ],
    'GT' => [                       // Guatemala
        'GTQ' => CurrencyName::GTQ, // - Quetzal
    ],
    'GW' => [                       // Guinea-Bissau
        'XOF' => CurrencyName::XOF, // - CFA Franc BCEAO
    ],
    'GY' => [                       // Guyana
        'GYD' => CurrencyName::GYD, // - Guyana Dollar
    ],
    'HK' => [                       // Hong Kong
        'HKD' => CurrencyName::HKD, // - Hong Kong Dollar
    ],
    'HN' => [                       // Honduras
        'HNL' => CurrencyName::HNL, // - Lempira
    ],
    'HR' => [                       // Croatia
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'HT' => [                       // Haiti
        'HTG' => CurrencyName::HTG, // - Gourde
    ],
    'HU' => [                       // Hungary
        'HUF' => CurrencyName::HUF, // - Forint
    ],
    'ID' => [                       // Indonesia
        'IDR' => CurrencyName::IDR, // - Rupiah
    ],
    'IE' => [                       // Ireland
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'IL' => [                       // Israel
        'ILS' => CurrencyName::ILS, // - New Israeli Sheqel
    ],
    'IM' => [                       // Isle of Man
        'GBP' => CurrencyName::GBP, // - Pound Sterling
    ],
    'IN' => [                       // India
        'INR' => CurrencyName::INR, // - Indian Rupee
    ],
    'IO' => [                       // British Indian Ocean Territory
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'IQ' => [                       // Iraq
        'IQD' => CurrencyName::IQD, // - Iraqi Dinar
    ],
    'IR' => [                       // Iran
        'IRR' => CurrencyName::IRR, // - Iranian Rial
    ],
    'IS' => [                       // Iceland
        'ISK' => CurrencyName::ISK, // - Iceland Krona
    ],
    'IT' => [                       // Italy
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'JE' => [                       // Jersey
        'GBP' => CurrencyName::GBP, // - Pound Sterling
    ],
    'JM' => [                       // Jamaica
        'JMD' => CurrencyName::JMD, // - Jamaican Dollar
    ],
    'JO' => [                       // Jordan
        'JOD' => CurrencyName::JOD, // - Jordanian Dinar
    ],
    'JP' => [                       // Japan
        'JPY' => CurrencyName::JPY, // - Yen
    ],
    'KE' => [                       // Kenya
        'KES' => CurrencyName::KES, // - Kenyan Shilling
    ],
    'KG' => [                       // Kyrgyzstan
        'KGS' => CurrencyName::KGS, // - Som
    ],
    'KH' => [                       // Cambodia
        'KHR' => CurrencyName::KHR, // - Riel
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'KI' => [                       // Kiribati
        'AUD' => CurrencyName::AUD, // - Australian Dollar
    ],
    'KM' => [                       // Comoros
        'KMF' => CurrencyName::KMF, // - Comorian Franc
    ],
    'KN' => [                       // Saint Kitts and Nevis
        'XCD' => CurrencyName::XCD, // - East Caribbean Dollar
    ],
    'KP' => [                       // North Korea
        'KPW' => CurrencyName::KPW, // - North Korean Won
    ],
    'KR' => [                       // South Korea
        'KRW' => CurrencyName::KRW, // - Won
    ],
    'KW' => [                       // Kuwait
        'KWD' => CurrencyName::KWD, // - Kuwaiti Dinar
    ],
    'KY' => [                       // Cayman Islands
        'KYD' => CurrencyName::KYD, // - Cayman Islands Dollar
    ],
    'KZ' => [                       // Kazakhstan
        'KZT' => CurrencyName::KZT, // - Tenge
    ],
    'LA' => [                       // Laos
        'LAK' => CurrencyName::LAK, // - Lao Kip
    ],
    'LB' => [                       // Lebanon
        'LBP' => CurrencyName::LBP, // - Lebanese Pound
    ],
    'LC' => [                       // Saint Lucia
        'XCD' => CurrencyName::XCD, // - East Caribbean Dollar
    ],
    'LI' => [                       // Liechtenstein
        'CHF' => CurrencyName::CHF, // - Swiss Franc
    ],
    'LK' => [                       // Sri Lanka
        'LKR' => CurrencyName::LKR, // - Sri Lanka Rupee
    ],
    'LR' => [                       // Liberia
        'LRD' => CurrencyName::LRD, // - Liberian Dollar
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'LS' => [                       // Lesotho
        'LSL' => CurrencyName::LSL, // - Loti
        'ZAR' => CurrencyName::ZAR, // - Rand
    ],
    'LT' => [                       // Lithuania
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'LU' => [                       // Luxembourg
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'LV' => [                       // Latvia
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'LY' => [                       // Libya
        'LYD' => CurrencyName::LYD, // - Libyan Dinar
    ],
    'MA' => [                       // Morocco
        'MAD' => CurrencyName::MAD, // - Moroccan Dirham
    ],
    'MC' => [                       // Monaco
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'MD' => [                       // Moldova
        'MDL' => CurrencyName::MDL, // - Moldovan Leu
    ],
    'ME' => [                       // Montenegro
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'MG' => [                       // Madagascar
        'MGA' => CurrencyName::MGA, // - Malagasy Ariary
    ],
    'MH' => [                       // Marshall Islands
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'MK' => [                       // North Macedonia
        'MKD' => CurrencyName::MKD, // - Denar
    ],
    'ML' => [                       // Mali
        'XOF' => CurrencyName::XOF, // - CFA Franc BCEAO
    ],
    'MM' => [                       // Myanmar
        'MMK' => CurrencyName::MMK, // - Kyat
    ],
    'MN' => [                       // Mongolia
        'MNT' => CurrencyName::MNT, // - Tugrik
    ],
    'MO' => [                       // Macao
        'HKD' => CurrencyName::HKD, // - Hong Kong Dollar
        'MOP' => CurrencyName::MOP, // - Pataca
    ],
    'MR' => [                       // Mauritania
        'MRU' => CurrencyName::MRU, // - Ouguiya
    ],
    'MS' => [                       // Montserrat
        'XCD' => CurrencyName::XCD, // - East Caribbean Dollar
    ],
    'MT' => [                       // Malta
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'MU' => [                       // Mauritius
        'MUR' => CurrencyName::MUR, // - Mauritius Rupee
    ],
    'MV' => [                       // Maldives
        'MVR' => CurrencyName::MVR, // - Rufiyaa
    ],
    'MW' => [                       // Malawi
        'MWK' => CurrencyName::MWK, // - Malawi Kwacha
    ],
    'MX' => [                       // Mexico
        'MXN' => CurrencyName::MXN, // - Mexican Peso
    ],
    'MY' => [                       // Malaysia
        'MYR' => CurrencyName::MYR, // - Malaysian Ringgit
    ],
    'MZ' => [                       // Mozambique
        'MZN' => CurrencyName::MZN, // - Mozambique Metical
    ],
    'NA' => [                       // Namibia
        'NAD' => CurrencyName::NAD, // - Namibia Dollar
        'ZAR' => CurrencyName::ZAR, // - Rand
    ],
    'NC' => [                       // New Caledonia
        'XPF' => CurrencyName::XPF, // - CFP Franc
    ],
    'NE' => [                       // Niger
        'XOF' => CurrencyName::XOF, // - CFA Franc BCEAO
    ],
    'NG' => [                       // Nigeria
        'NGN' => CurrencyName::NGN, // - Naira
    ],
    'NI' => [                       // Nicaragua
        'NIO' => CurrencyName::NIO, // - Cordoba Oro
    ],
    'NL' => [                       // Netherlands
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'NO' => [                       // Norway
        'NOK' => CurrencyName::NOK, // - Norwegian Krone
    ],
    'NP' => [                       // Nepal
        'INR' => CurrencyName::INR, // - Indian Rupee
        'NPR' => CurrencyName::NPR, // - Nepalese Rupee
    ],
    'NR' => [                       // Nauru
        'AUD' => CurrencyName::AUD, // - Australian Dollar
    ],
    'NU' => [                       // Niue
        'NZD' => CurrencyName::NZD, // - New Zealand Dollar
    ],
    'NZ' => [                       // New Zealand
        'NZD' => CurrencyName::NZD, // - New Zealand Dollar
    ],
    'OM' => [                       // Oman
        'OMR' => CurrencyName::OMR, // - Rial Omani
    ],
    'PA' => [                       // Panama
        'PAB' => CurrencyName::PAB, // - Balboa
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'PE' => [                       // Peru
        'PEN' => CurrencyName::PEN, // - Sol
    ],
    'PF' => [                       // French Polynesia
        'XPF' => CurrencyName::XPF, // - CFP Franc
    ],
    'PG' => [                       // Papua New Guinea
        'PGK' => CurrencyName::PGK, // - Kina
    ],
    'PH' => [                       // Philippines
        'PHP' => CurrencyName::PHP, // - Philippine Peso
    ],
    'PK' => [                       // Pakistan
        'PKR' => CurrencyName::PKR, // - Pakistan Rupee
    ],
    'PL' => [                       // Poland
        'PLN' => CurrencyName::PLN, // - Zloty
    ],
    'PM' => [                       // Saint Pierre and Miquelon
        'CAD' => CurrencyName::CAD, // - Canadian Dollar
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'PN' => [                       // Pitcairn
        'NZD' => CurrencyName::NZD, // - New Zealand Dollar
    ],
    'PS' => [                       // Palestine, State of
        'ILS' => CurrencyName::ILS, // - New Israeli Sheqel
        'JOD' => CurrencyName::JOD, // - Jordanian Dinar
    ],
    'PT' => [                       // Portugal
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'PW' => [                       // Palau
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'PY' => [                       // Paraguay
        'PYG' => CurrencyName::PYG, // - Guarani
    ],
    'QA' => [                       // Qatar
        'QAR' => CurrencyName::QAR, // - Qatari Rial
    ],
    'RO' => [                       // Romania
        'RON' => CurrencyName::RON, // - Romanian Leu
    ],
    'RS' => [                       // Serbia
        'RSD' => CurrencyName::RSD, // - Serbian Dinar
    ],
    'RU' => [                       // Russian Federation
        'RUB' => CurrencyName::RUB, // - Russian Ruble
    ],
    'RW' => [                       // Rwanda
        'RWF' => CurrencyName::RWF, // - Rwanda Franc
    ],
    'SA' => [                       // Saudi Arabia
        'SAR' => CurrencyName::SAR, // - Saudi Riyal
    ],
    'SB' => [                       // Solomon Islands
        'SBD' => CurrencyName::SBD, // - Solomon Islands Dollar
    ],
    'SC' => [                       // Seychelles
        'SCR' => CurrencyName::SCR, // - Seychelles Rupee
    ],
    'SD' => [                       // Sudan
        'SDG' => CurrencyName::SDG, // - Sudanese Pound
    ],
    'SE' => [                       // Sweden
        'SEK' => CurrencyName::SEK, // - Swedish Krona
    ],
    'SG' => [                       // Singapore
        'BND' => CurrencyName::BND, // - Brunei Dollar
        'SGD' => CurrencyName::SGD, // - Singapore Dollar
    ],
    'SH' => [                       // Saint Helena, Ascension and Tristan da Cunha
        'GBP' => CurrencyName::GBP, // - Pound Sterling
        'SHP' => CurrencyName::SHP, // - Saint Helena Pound
    ],
    'SI' => [                       // Slovenia
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'SK' => [                       // Slovakia
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'SL' => [                       // Sierra Leone
        'SLE' => CurrencyName::SLE, // - Leone
    ],
    'SM' => [                       // San Marino
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'SN' => [                       // Senegal
        'XOF' => CurrencyName::XOF, // - CFA Franc BCEAO
    ],
    'SO' => [                       // Somalia
        'SOS' => CurrencyName::SOS, // - Somali Shilling
    ],
    'SR' => [                       // Suriname
        'SRD' => CurrencyName::SRD, // - Surinam Dollar
    ],
    'SS' => [                       // South Sudan
        'SSP' => CurrencyName::SSP, // - South Sudanese Pound
    ],
    'ST' => [                       // Sao Tome and Principe
        'STN' => CurrencyName::STN, // - Dobra
    ],
    'SV' => [                       // El Salvador
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'SX' => [                       // Sint Maarten (Dutch part)
        'ANG' => CurrencyName::ANG, // - Netherlands Antillean Guilder
    ],
    'SY' => [                       // Syria
        'SYP' => CurrencyName::SYP, // - Syrian Pound
        'TRY' => CurrencyName::TRY, // - Turkish Lira
    ],
    'SZ' => [                       // Eswatini
        'SZL' => CurrencyName::SZL, // - Lilangeni
        'ZAR' => CurrencyName::ZAR, // - Rand
    ],
    'TC' => [                       // Turks and Caicos Islands
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'TD' => [                       // Chad
        'XAF' => CurrencyName::XAF, // - CFA Franc BEAC
    ],
    'TG' => [                       // Togo
        'XOF' => CurrencyName::XOF, // - CFA Franc BCEAO
    ],
    'TH' => [                       // Thailand
        'THB' => CurrencyName::THB, // - Baht
    ],
    'TJ' => [                       // Tajikistan
        'TJS' => CurrencyName::TJS, // - Somoni
    ],
    'TL' => [                       // Timor-Leste
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'TM' => [                       // Turkmenistan
        'TMT' => CurrencyName::TMT, // - Turkmenistan New Manat
    ],
    'TN' => [                       // Tunisia
        'TND' => CurrencyName::TND, // - Tunisian Dinar
    ],
    'TO' => [                       // Tonga
        'TOP' => CurrencyName::TOP, // - Pa’anga
    ],
    'TR' => [                       // Türkiye
        'TRY' => CurrencyName::TRY, // - Turkish Lira
    ],
    'TT' => [                       // Trinidad and Tobago
        'TTD' => CurrencyName::TTD, // - Trinidad and Tobago Dollar
    ],
    'TV' => [                       // Tuvalu
        'AUD' => CurrencyName::AUD, // - Australian Dollar
    ],
    'TW' => [                       // Taiwan
        'TWD' => CurrencyName::TWD, // - New Taiwan Dollar
    ],
    'TZ' => [                       // Tanzania
        'TZS' => CurrencyName::TZS, // - Tanzanian Shilling
    ],
    'UA' => [                       // Ukraine
        'UAH' => CurrencyName::UAH, // - Hryvnia
    ],
    'UG' => [                       // Uganda
        'UGX' => CurrencyName::UGX, // - Uganda Shilling
    ],
    'US' => [                       // United States
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'UY' => [                       // Uruguay
        'UYU' => CurrencyName::UYU, // - Peso Uruguayo
    ],
    'UZ' => [                       // Uzbekistan
        'UZS' => CurrencyName::UZS, // - Uzbekistan Sum
    ],
    'VA' => [                       // Holy See (Vatican City State)
        'EUR' => CurrencyName::EUR, // - Euro
    ],
    'VC' => [                       // Saint Vincent and the Grenadines
        'XCD' => CurrencyName::XCD, // - East Caribbean Dollar
    ],
    'VE' => [                       // Venezuela
        'USD' => CurrencyName::USD, // - US Dollar
        'VED' => CurrencyName::VED, // - Bolívar Soberano
        'VES' => CurrencyName::VES, // - Bolívar Soberano
    ],
    'VG' => [                       // Virgin Islands, British
        'USD' => CurrencyName::USD, // - US Dollar
    ],
    'VN' => [                       // Vietnam
        'VND' => CurrencyName::VND, // - Dong
    ],
    'VU' => [                       // Vanuatu
        'VUV' => CurrencyName::VUV, // - Vatu
    ],
    'WF' => [                       // Wallis and Futuna
        'XPF' => CurrencyName::XPF, // - CFP Franc
    ],
    'WS' => [                       // Samoa
        'WST' => CurrencyName::WST, // - Tala
    ],
    'YE' => [                       // Yemen
        'YER' => CurrencyName::YER, // - Yemeni Rial
    ],
    'ZA' => [                       // South Africa
        'ZAR' => CurrencyName::ZAR, // - Rand
    ],
    'ZM' => [                       // Zambia
        'ZMW' => CurrencyName::ZMW, // - Zambian Kwacha
    ],
    'ZW' => [                       // Zimbabwe
        'USD' => CurrencyName::USD, // - US Dollar
        'ZWL' => CurrencyName::ZWL, // - Zimbabwe Dollar
    ],
];
