<?php

/** @noinspection SpellCheckingInspection */

namespace Steefdw\StandardsEnums\ISO3166;

/**
 * ISO 3166-1 numeric (or numeric-3) codes are three-digit country codes defined in ISO 3166-1,
 * part of the ISO 3166 standard published by the International Organization for Standardization (ISO),
 * to represent countries, dependent territories, and special areas of geographical interest.
 *
 * @see https://en.wikipedia.org/wiki/ISO_3166-1_numeric
 */
enum CountryNumeric: string implements CountryEnumInterface
{
    use CountryEnumTrait;

    // <editor-fold desc="Cases">
    case AW = '533'; // Aruba
    case AF = '004'; // Afghanistan
    case AO = '024'; // Angola
    case AI = '660'; // Anguilla
    case AX = '248'; // Åland Islands
    case AL = '008'; // Albania
    case AD = '020'; // Andorra
    case AE = '784'; // United Arab Emirates
    case AR = '032'; // Argentina
    case AM = '051'; // Armenia
    case AS = '016'; // American Samoa
    case AQ = '010'; // Antarctica
    case TF = '260'; // French Southern Territories
    case AG = '028'; // Antigua and Barbuda
    case AU = '036'; // Australia
    case AT = '040'; // Austria
    case AZ = '031'; // Azerbaijan
    case BI = '108'; // Burundi
    case BE = '056'; // Belgium
    case BJ = '204'; // Benin
    case BQ = '535'; // Bonaire, Sint Eustatius and Saba
    case BF = '854'; // Burkina Faso
    case BD = '050'; // Bangladesh
    case BG = '100'; // Bulgaria
    case BH = '048'; // Bahrain
    case BS = '044'; // Bahamas
    case BA = '070'; // Bosnia and Herzegovina
    case BL = '652'; // Saint Barthélemy
    case BY = '112'; // Belarus
    case BZ = '084'; // Belize
    case BM = '060'; // Bermuda
    case BO = '068'; // Bolivia
    case BR = '076'; // Brazil
    case BB = '052'; // Barbados
    case BN = '096'; // Brunei Darussalam
    case BT = '064'; // Bhutan
    case BV = '074'; // Bouvet Island
    case BW = '072'; // Botswana
    case CF = '140'; // Central African Republic
    case CA = '124'; // Canada
    case CC = '166'; // Cocos (Keeling) Islands
    case CH = '756'; // Switzerland
    case CL = '152'; // Chile
    case CN = '156'; // China
    case CI = '384'; // Côte d'Ivoire
    case CM = '120'; // Cameroon
    case CD = '180'; // Congo, The Democratic Republic of the
    case CG = '178'; // Congo
    case CK = '184'; // Cook Islands
    case CO = '170'; // Colombia
    case KM = '174'; // Comoros
    case CV = '132'; // Cabo Verde
    case CR = '188'; // Costa Rica
    case CU = '192'; // Cuba
    case CW = '531'; // Curaçao
    case CX = '162'; // Christmas Island
    case KY = '136'; // Cayman Islands
    case CY = '196'; // Cyprus
    case CZ = '203'; // Czechia
    case DE = '276'; // Germany
    case DJ = '262'; // Djibouti
    case DM = '212'; // Dominica
    case DK = '208'; // Denmark
    case DO = '214'; // Dominican Republic
    case DZ = '012'; // Algeria
    case EC = '218'; // Ecuador
    case EG = '818'; // Egypt
    case ER = '232'; // Eritrea
    case EH = '732'; // Western Sahara
    case ES = '724'; // Spain
    case EE = '233'; // Estonia
    case ET = '231'; // Ethiopia
    case FI = '246'; // Finland
    case FJ = '242'; // Fiji
    case FK = '238'; // Falkland Islands (Malvinas)
    case FR = '250'; // France
    case FO = '234'; // Faroe Islands
    case FM = '583'; // Micronesia, Federated States of
    case GA = '266'; // Gabon
    case GB = '826'; // United Kingdom
    case GE = '268'; // Georgia
    case GG = '831'; // Guernsey
    case GH = '288'; // Ghana
    case GI = '292'; // Gibraltar
    case GN = '324'; // Guinea
    case GP = '312'; // Guadeloupe
    case GM = '270'; // Gambia
    case GW = '624'; // Guinea-Bissau
    case GQ = '226'; // Equatorial Guinea
    case GR = '300'; // Greece
    case GD = '308'; // Grenada
    case GL = '304'; // Greenland
    case GT = '320'; // Guatemala
    case GF = '254'; // French Guiana
    case GU = '316'; // Guam
    case GY = '328'; // Guyana
    case HK = '344'; // Hong Kong
    case HM = '334'; // Heard Island and McDonald Islands
    case HN = '340'; // Honduras
    case HR = '191'; // Croatia
    case HT = '332'; // Haiti
    case HU = '348'; // Hungary
    case ID = '360'; // Indonesia
    case IM = '833'; // Isle of Man
    case IN = '356'; // India
    case IO = '086'; // British Indian Ocean Territory
    case IE = '372'; // Ireland
    case IR = '364'; // Iran
    case IQ = '368'; // Iraq
    case IS = '352'; // Iceland
    case IL = '376'; // Israel
    case IT = '380'; // Italy
    case JM = '388'; // Jamaica
    case JE = '832'; // Jersey
    case JO = '400'; // Jordan
    case JP = '392'; // Japan
    case KZ = '398'; // Kazakhstan
    case KE = '404'; // Kenya
    case KG = '417'; // Kyrgyzstan
    case KH = '116'; // Cambodia
    case KI = '296'; // Kiribati
    case KN = '659'; // Saint Kitts and Nevis
    case KR = '410'; // South Korea
    case KW = '414'; // Kuwait
    case LA = '418'; // Laos
    case LB = '422'; // Lebanon
    case LR = '430'; // Liberia
    case LY = '434'; // Libya
    case LC = '662'; // Saint Lucia
    case LI = '438'; // Liechtenstein
    case LK = '144'; // Sri Lanka
    case LS = '426'; // Lesotho
    case LT = '440'; // Lithuania
    case LU = '442'; // Luxembourg
    case LV = '428'; // Latvia
    case MO = '446'; // Macao
    case MF = '663'; // Saint Martin (French part)
    case MA = '504'; // Morocco
    case MC = '492'; // Monaco
    case MD = '498'; // Moldova
    case MG = '450'; // Madagascar
    case MV = '462'; // Maldives
    case MX = '484'; // Mexico
    case MH = '584'; // Marshall Islands
    case MK = '807'; // North Macedonia
    case ML = '466'; // Mali
    case MT = '470'; // Malta
    case MM = '104'; // Myanmar
    case ME = '499'; // Montenegro
    case MN = '496'; // Mongolia
    case MP = '580'; // Northern Mariana Islands
    case MZ = '508'; // Mozambique
    case MR = '478'; // Mauritania
    case MS = '500'; // Montserrat
    case MQ = '474'; // Martinique
    case MU = '480'; // Mauritius
    case MW = '454'; // Malawi
    case MY = '458'; // Malaysia
    case YT = '175'; // Mayotte
    case NA = '516'; // Namibia
    case NC = '540'; // New Caledonia
    case NE = '562'; // Niger
    case NF = '574'; // Norfolk Island
    case NG = '566'; // Nigeria
    case NI = '558'; // Nicaragua
    case NU = '570'; // Niue
    case NL = '528'; // Netherlands
    case NO = '578'; // Norway
    case NP = '524'; // Nepal
    case NR = '520'; // Nauru
    case NZ = '554'; // New Zealand
    case OM = '512'; // Oman
    case PK = '586'; // Pakistan
    case PA = '591'; // Panama
    case PN = '612'; // Pitcairn
    case PE = '604'; // Peru
    case PH = '608'; // Philippines
    case PW = '585'; // Palau
    case PG = '598'; // Papua New Guinea
    case PL = '616'; // Poland
    case PR = '630'; // Puerto Rico
    case KP = '408'; // North Korea
    case PT = '620'; // Portugal
    case PY = '600'; // Paraguay
    case PS = '275'; // Palestine, State of
    case PF = '258'; // French Polynesia
    case QA = '634'; // Qatar
    case RE = '638'; // Réunion
    case RO = '642'; // Romania
    case RU = '643'; // Russian Federation
    case RW = '646'; // Rwanda
    case SA = '682'; // Saudi Arabia
    case SD = '729'; // Sudan
    case SN = '686'; // Senegal
    case SG = '702'; // Singapore
    case GS = '239'; // South Georgia and the South Sandwich Islands
    case SH = '654'; // Saint Helena, Ascension and Tristan da Cunha
    case SJ = '744'; // Svalbard and Jan Mayen
    case SB = '090'; // Solomon Islands
    case SL = '694'; // Sierra Leone
    case SV = '222'; // El Salvador
    case SM = '674'; // San Marino
    case SO = '706'; // Somalia
    case PM = '666'; // Saint Pierre and Miquelon
    case RS = '688'; // Serbia
    case SS = '728'; // South Sudan
    case ST = '678'; // Sao Tome and Principe
    case SR = '740'; // Suriname
    case SK = '703'; // Slovakia
    case SI = '705'; // Slovenia
    case SE = '752'; // Sweden
    case SZ = '748'; // Eswatini
    case SX = '534'; // Sint Maarten (Dutch part)
    case SC = '690'; // Seychelles
    case SY = '760'; // Syria
    case TC = '796'; // Turks and Caicos Islands
    case TD = '148'; // Chad
    case TG = '768'; // Togo
    case TH = '764'; // Thailand
    case TJ = '762'; // Tajikistan
    case TK = '772'; // Tokelau
    case TM = '795'; // Turkmenistan
    case TL = '626'; // Timor-Leste
    case TO = '776'; // Tonga
    case TT = '780'; // Trinidad and Tobago
    case TN = '788'; // Tunisia
    case TR = '792'; // Türkiye
    case TV = '798'; // Tuvalu
    case TW = '158'; // Taiwan
    case TZ = '834'; // Tanzania
    case UG = '800'; // Uganda
    case UA = '804'; // Ukraine
    case UM = '581'; // United States Minor Outlying Islands
    case UY = '858'; // Uruguay
    case US = '840'; // United States
    case UZ = '860'; // Uzbekistan
    case VA = '336'; // Holy See (Vatican City State)
    case VC = '670'; // Saint Vincent and the Grenadines
    case VE = '862'; // Venezuela
    case VG = '092'; // Virgin Islands, British
    case VI = '850'; // Virgin Islands, U.S.
    case VN = '704'; // Vietnam
    case VU = '548'; // Vanuatu
    case WF = '876'; // Wallis and Futuna
    case WS = '882'; // Samoa
    case YE = '887'; // Yemen
    case ZA = '710'; // South Africa
    case ZM = '894'; // Zambia
    case ZW = '716'; // Zimbabwe
    // </editor-fold>
}
