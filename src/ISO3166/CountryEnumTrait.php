<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\ISO3166;

use Steefdw\StandardsEnums\EnumTrait;
use Steefdw\StandardsEnums\ISO4217\CurrencyName;
use Steefdw\StandardsEnums\Locales\LocaleDescription;

trait CountryEnumTrait
{
    use EnumTrait;

    public function getName(): ?CountryName
    {
        return CountryName::tryFromName($this->getCountryCode());
    }

    public function getAlpha2(): ?CountryAlpha2
    {
        return CountryAlpha2::tryFromName($this->getCountryCode());
    }

    public function getAlpha3(): ?CountryAlpha3
    {
        return CountryAlpha3::tryFromName($this->getCountryCode());
    }

    public function getFlag(): ?CountryFlag
    {
        return CountryFlag::tryFromName($this->getCountryCode());
    }

    public function getNumeric(): ?CountryNumeric
    {
        return CountryNumeric::tryFromName($this->getCountryCode());
    }

    /**
     * @return array<string,LocaleDescription>
     */
    public function getLocales(): array
    {
        $localeDescriptions = LocaleDescription::cases();
        $currentCountry = '_' . $this->getCountryCode();
        $matches = [];
        foreach ($localeDescriptions as $localeDescription) {
            if (str_ends_with($localeDescription->name, $currentCountry)) {
                $matches[$localeDescription->name] = $localeDescription;
            }
        }

        return $matches;
    }

    /**
     * @return array<string,CurrencyName>
     */
    public function getCurrencies(): array
    {
        /** @var array<string, array<string,CurrencyName>> $countryCurrencies */
        $countryCurrencies = [];
        include dirname(__DIR__) . '/maps/countryCurrencies.php';

        return $countryCurrencies[$this->getCountryCode()] ?? [];
    }

    public static function tryFromAlpha2(CountryAlpha2 $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    public static function tryFromAlpha3(CountryAlpha3 $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    public static function tryFromFlag(CountryFlag $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    public static function tryFromCountryName(CountryName $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    public static function tryFromNumeric(CountryNumeric $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    /**
     * @return array<int, self>
     */
    public static function casesSortedByCountryName(?CountryNameTranslation $translation = null): array
    {
        $translations = $translation ? CountryName::toArrayTranslated($translation) : CountryName::toArray();
        $sortedNames = array_flip($translations);
        ksort($sortedNames);
        $cases = self::getIndexedCases();
        $sortedCases = [];
        foreach ($sortedNames as $alpha2Code) {
            $sortedCases[$alpha2Code] = $cases[$alpha2Code];
        }

        return array_values($sortedCases);
    }

    /**
     * All Country* enums have the Alpha2 code as the name.
     */
    private function getCountryCode(): string
    {
        return mb_strtoupper($this->name);
    }
}
