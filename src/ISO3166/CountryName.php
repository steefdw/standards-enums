<?php

/** @noinspection SpellCheckingInspection */

namespace Steefdw\StandardsEnums\ISO3166;

/**
 * These are the country's English short name used by the ISO 3166/MA.
 *
 * @see https://en.wikipedia.org/wiki/ISO_3166-1
 */
enum CountryName: string implements CountryEnumInterface
{
    use CountryEnumTrait;

    // <editor-fold desc="Cases">
    case AW = 'Aruba';
    case AF = 'Afghanistan';
    case AO = 'Angola';
    case AI = 'Anguilla';
    case AX = 'Åland Islands';
    case AL = 'Albania';
    case AD = 'Andorra';
    case AE = 'United Arab Emirates';
    case AR = 'Argentina';
    case AM = 'Armenia';
    case AS = 'American Samoa';
    case AQ = 'Antarctica';
    case TF = 'French Southern Territories';
    case AG = 'Antigua and Barbuda';
    case AU = 'Australia';
    case AT = 'Austria';
    case AZ = 'Azerbaijan';
    case BI = 'Burundi';
    case BE = 'Belgium';
    case BJ = 'Benin';
    case BQ = 'Bonaire, Sint Eustatius and Saba';
    case BF = 'Burkina Faso';
    case BD = 'Bangladesh';
    case BG = 'Bulgaria';
    case BH = 'Bahrain';
    case BS = 'Bahamas';
    case BA = 'Bosnia and Herzegovina';
    case BL = 'Saint Barthélemy';
    case BY = 'Belarus';
    case BZ = 'Belize';
    case BM = 'Bermuda';
    case BO = 'Bolivia';
    case BR = 'Brazil';
    case BB = 'Barbados';
    case BN = 'Brunei Darussalam';
    case BT = 'Bhutan';
    case BV = 'Bouvet Island';
    case BW = 'Botswana';
    case CF = 'Central African Republic';
    case CA = 'Canada';
    case CC = 'Cocos (Keeling) Islands';
    case CH = 'Switzerland';
    case CL = 'Chile';
    case CN = 'China';
    case CI = 'Côte d\'Ivoire';
    case CM = 'Cameroon';
    case CD = 'Congo, The Democratic Republic of the';
    case CG = 'Congo';
    case CK = 'Cook Islands';
    case CO = 'Colombia';
    case KM = 'Comoros';
    case CV = 'Cabo Verde';
    case CR = 'Costa Rica';
    case CU = 'Cuba';
    case CW = 'Curaçao';
    case CX = 'Christmas Island';
    case KY = 'Cayman Islands';
    case CY = 'Cyprus';
    case CZ = 'Czechia';
    case DE = 'Germany';
    case DJ = 'Djibouti';
    case DM = 'Dominica';
    case DK = 'Denmark';
    case DO = 'Dominican Republic';
    case DZ = 'Algeria';
    case EC = 'Ecuador';
    case EG = 'Egypt';
    case ER = 'Eritrea';
    case EH = 'Western Sahara';
    case ES = 'Spain';
    case EE = 'Estonia';
    case ET = 'Ethiopia';
    case FI = 'Finland';
    case FJ = 'Fiji';
    case FK = 'Falkland Islands (Malvinas)';
    case FR = 'France';
    case FO = 'Faroe Islands';
    case FM = 'Micronesia, Federated States of';
    case GA = 'Gabon';
    case GB = 'United Kingdom';
    case GE = 'Georgia';
    case GG = 'Guernsey';
    case GH = 'Ghana';
    case GI = 'Gibraltar';
    case GN = 'Guinea';
    case GP = 'Guadeloupe';
    case GM = 'Gambia';
    case GW = 'Guinea-Bissau';
    case GQ = 'Equatorial Guinea';
    case GR = 'Greece';
    case GD = 'Grenada';
    case GL = 'Greenland';
    case GT = 'Guatemala';
    case GF = 'French Guiana';
    case GU = 'Guam';
    case GY = 'Guyana';
    case HK = 'Hong Kong';
    case HM = 'Heard Island and McDonald Islands';
    case HN = 'Honduras';
    case HR = 'Croatia';
    case HT = 'Haiti';
    case HU = 'Hungary';
    case ID = 'Indonesia';
    case IM = 'Isle of Man';
    case IN = 'India';
    case IO = 'British Indian Ocean Territory';
    case IE = 'Ireland';
    case IR = 'Iran';
    case IQ = 'Iraq';
    case IS = 'Iceland';
    case IL = 'Israel';
    case IT = 'Italy';
    case JM = 'Jamaica';
    case JE = 'Jersey';
    case JO = 'Jordan';
    case JP = 'Japan';
    case KZ = 'Kazakhstan';
    case KE = 'Kenya';
    case KG = 'Kyrgyzstan';
    case KH = 'Cambodia';
    case KI = 'Kiribati';
    case KN = 'Saint Kitts and Nevis';
    case KR = 'South Korea';
    case KW = 'Kuwait';
    case LA = 'Laos';
    case LB = 'Lebanon';
    case LR = 'Liberia';
    case LY = 'Libya';
    case LC = 'Saint Lucia';
    case LI = 'Liechtenstein';
    case LK = 'Sri Lanka';
    case LS = 'Lesotho';
    case LT = 'Lithuania';
    case LU = 'Luxembourg';
    case LV = 'Latvia';
    case MO = 'Macao';
    case MF = 'Saint Martin (French part)';
    case MA = 'Morocco';
    case MC = 'Monaco';
    case MD = 'Moldova';
    case MG = 'Madagascar';
    case MV = 'Maldives';
    case MX = 'Mexico';
    case MH = 'Marshall Islands';
    case MK = 'North Macedonia';
    case ML = 'Mali';
    case MT = 'Malta';
    case MM = 'Myanmar';
    case ME = 'Montenegro';
    case MN = 'Mongolia';
    case MP = 'Northern Mariana Islands';
    case MZ = 'Mozambique';
    case MR = 'Mauritania';
    case MS = 'Montserrat';
    case MQ = 'Martinique';
    case MU = 'Mauritius';
    case MW = 'Malawi';
    case MY = 'Malaysia';
    case YT = 'Mayotte';
    case NA = 'Namibia';
    case NC = 'New Caledonia';
    case NE = 'Niger';
    case NF = 'Norfolk Island';
    case NG = 'Nigeria';
    case NI = 'Nicaragua';
    case NU = 'Niue';
    case NL = 'Netherlands';
    case NO = 'Norway';
    case NP = 'Nepal';
    case NR = 'Nauru';
    case NZ = 'New Zealand';
    case OM = 'Oman';
    case PK = 'Pakistan';
    case PA = 'Panama';
    case PN = 'Pitcairn';
    case PE = 'Peru';
    case PH = 'Philippines';
    case PW = 'Palau';
    case PG = 'Papua New Guinea';
    case PL = 'Poland';
    case PR = 'Puerto Rico';
    case KP = 'North Korea';
    case PT = 'Portugal';
    case PY = 'Paraguay';
    case PS = 'Palestine, State of';
    case PF = 'French Polynesia';
    case QA = 'Qatar';
    case RE = 'Réunion';
    case RO = 'Romania';
    case RU = 'Russian Federation';
    case RW = 'Rwanda';
    case SA = 'Saudi Arabia';
    case SD = 'Sudan';
    case SN = 'Senegal';
    case SG = 'Singapore';
    case GS = 'South Georgia and the South Sandwich Islands';
    case SH = 'Saint Helena, Ascension and Tristan da Cunha';
    case SJ = 'Svalbard and Jan Mayen';
    case SB = 'Solomon Islands';
    case SL = 'Sierra Leone';
    case SV = 'El Salvador';
    case SM = 'San Marino';
    case SO = 'Somalia';
    case PM = 'Saint Pierre and Miquelon';
    case RS = 'Serbia';
    case SS = 'South Sudan';
    case ST = 'Sao Tome and Principe';
    case SR = 'Suriname';
    case SK = 'Slovakia';
    case SI = 'Slovenia';
    case SE = 'Sweden';
    case SZ = 'Eswatini';
    case SX = 'Sint Maarten (Dutch part)';
    case SC = 'Seychelles';
    case SY = 'Syria';
    case TC = 'Turks and Caicos Islands';
    case TD = 'Chad';
    case TG = 'Togo';
    case TH = 'Thailand';
    case TJ = 'Tajikistan';
    case TK = 'Tokelau';
    case TM = 'Turkmenistan';
    case TL = 'Timor-Leste';
    case TO = 'Tonga';
    case TT = 'Trinidad and Tobago';
    case TN = 'Tunisia';
    case TR = 'Türkiye';
    case TV = 'Tuvalu';
    case TW = 'Taiwan';
    case TZ = 'Tanzania';
    case UG = 'Uganda';
    case UA = 'Ukraine';
    case UM = 'United States Minor Outlying Islands';
    case UY = 'Uruguay';
    case US = 'United States';
    case UZ = 'Uzbekistan';
    case VA = 'Holy See (Vatican City State)';
    case VC = 'Saint Vincent and the Grenadines';
    case VE = 'Venezuela';
    case VG = 'Virgin Islands, British';
    case VI = 'Virgin Islands, U.S.';
    case VN = 'Vietnam';
    case VU = 'Vanuatu';
    case WF = 'Wallis and Futuna';
    case WS = 'Samoa';
    case YE = 'Yemen';
    case ZA = 'South Africa';
    case ZM = 'Zambia';
    case ZW = 'Zimbabwe';
    // </editor-fold>

    /**
     * @throws \Exception
     *
     * @return array<string, string>
     */
    public static function toArrayTranslated(CountryNameTranslation $language): array
    {
        $translations = $language->getTranslations();
        $cases = [];
        foreach (self::cases() as $case) {
            $translation = $translations[$case->value];
            $cases[$case->name] = $translation;
        }

        return $cases;
    }
}
