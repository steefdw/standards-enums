<?php

namespace Steefdw\StandardsEnums\ISO3166;

use BackedEnum;

interface CountryEnumInterface extends BackedEnum
{
    public static function tryFromAlpha2(CountryAlpha2 $enum): ?static;

    public static function tryFromAlpha3(CountryAlpha3 $enum): ?static;

    public static function tryFromFlag(CountryFlag $enum): ?static;

    public static function tryFromCountryName(CountryName $enum): ?static;

    public static function tryFromNumeric(CountryNumeric $enum): ?static;

    /**
     * @return array<int, self>
     */
    public static function casesSortedByCountryName(?CountryNameTranslation $translation = null): array;
}
