<?php

/**
 * Translations in Translation of ISO 4217 to Irish.
 *
 * @noinspection SpellCheckingInspection
 */

return [
    'UAE Dirham'                                                        => 'UAE Dirham',
    'Afghani'                                                           => 'Afgainí',
    'Lek'                                                               => 'Leic',
    'Armenian Dram'                                                     => 'Dram Airméanach',
    'Netherlands Antillean Guilder'                                     => 'Guilder Antillean na hÍsiltíre',
    'Kwanza'                                                            => 'Kwanzaa',
    'Argentine Peso'                                                    => 'Peso na Airgintíne',
    'Australian Dollar'                                                 => 'Dollar Astrálach',
    'Aruban Florin'                                                     => 'Aruban Florin',
    'Azerbaijan Manat'                                                  => 'Manat Asarbaiseáin',
    'Convertible Mark'                                                  => 'Marc inchomhshóite',
    'Barbados Dollar'                                                   => 'Dollar Bharbadóis',
    'Taka'                                                              => 'Taka',
    'Bulgarian Lev'                                                     => 'Lev na Bulgáire',
    'Bahraini Dinar'                                                    => 'Dínear na Bairéine',
    'Burundi Franc'                                                     => 'Franc na Burúine',
    'Bermudian Dollar'                                                  => 'Dollar Beirmiúdach',
    'Brunei Dollar'                                                     => 'Dollar Bhrúiné',
    'Boliviano'                                                         => 'Boliviano',
    'Mvdol'                                                             => 'Mvdol',
    'Brazilian Real'                                                    => 'Real Brasaíleach',
    'Bahamian Dollar'                                                   => 'Dollar na mBahámaí',
    'Ngultrum'                                                          => 'Ngultrum',
    'Pula'                                                              => 'Pula',
    'Belarusian Ruble'                                                  => 'Rúbal Bealarúisis',
    'Belize Dollar'                                                     => 'Dollar na Beilíse',
    'Canadian Dollar'                                                   => 'Dollar Ceanada',
    'Congolese Franc'                                                   => 'Franc Congolese',
    'WIR Euro'                                                          => 'WIR Euro',
    'Swiss Franc'                                                       => 'Franc na hEilvéise',
    'WIR Franc'                                                         => 'WIR Franc',
    'Unidad de Fomento'                                                 => 'Unidad de Fomento',
    'Chilean Peso'                                                      => 'Peso na Sile',
    'Yuan Renminbi'                                                     => 'Yuan Renminbi',
    'Colombian Peso'                                                    => 'Peso na Colóime',
    'Unidad de Valor Real'                                              => 'Unidad de Valor Real',
    'Costa Rican Colon'                                                 => 'Colon Chósta Ríce',
    'Peso Convertible'                                                  => 'Peso chomhshóite',
    'Cuban Peso'                                                        => 'Peso Cúba',
    'Cabo Verde Escudo'                                                 => 'Cabo Verde Escudo',
    'Czech Koruna'                                                      => 'Koruna na Seice',
    'Djibouti Franc'                                                    => 'Franc Djibouti',
    'Danish Krone'                                                      => 'Krone Danmhargach',
    'Dominican Peso'                                                    => 'Peso Doimineacach',
    'Algerian Dinar'                                                    => 'Dínear na hAilgéire',
    'Egyptian Pound'                                                    => 'Punt na hÉigipte',
    'Nakfa'                                                             => 'Nakfa',
    'Ethiopian Birr'                                                    => 'Birr na hAetóipe',
    'Euro'                                                              => 'Euro',
    'Fiji Dollar'                                                       => 'Dollar Fhidsí',
    'Falkland Islands Pound'                                            => 'Punt Oileáin Fháclainne',
    'Pound Sterling'                                                    => 'Steirling punt',
    'Lari'                                                              => 'Lari',
    'Ghana Cedi'                                                        => 'Gána Cedi',
    'Gibraltar Pound'                                                   => 'Punt Ghiobráltair',
    'Dalasi'                                                            => 'Dalasi',
    'Guinean Franc'                                                     => 'Franc Guine',
    'Quetzal'                                                           => 'Quetzal',
    'Guyana Dollar'                                                     => 'Dollar na Guáine',
    'Hong Kong Dollar'                                                  => 'Dollar Hong Cong',
    'Lempira'                                                           => 'Lempira',
    'Kuna'                                                              => 'Kuna',
    'Gourde'                                                            => 'Guairde',
    'Forint'                                                            => 'Forint',
    'Rupiah'                                                            => 'Rúipís',
    'New Israeli Sheqel'                                                => 'Seacláid nua Iosraelach',
    'Indian Rupee'                                                      => 'Rúipí India',
    'Iraqi Dinar'                                                       => 'Dínear Irácach',
    'Iranian Rial'                                                      => 'Rial Iaránach',
    'Iceland Krona'                                                     => 'Íoslainn Krona',
    'Jamaican Dollar'                                                   => 'Dollar Iamácach',
    'Jordanian Dinar'                                                   => 'Dínear Iordánach',
    'Yen'                                                               => 'Yen',
    'Kenyan Shilling'                                                   => 'Scilling Céiniach',
    'Som'                                                               => 'Som',
    'Riel'                                                              => 'Riail',
    'Comorian Franc'                                                    => 'Franc Comorian',
    'North Korean Won'                                                  => 'Won na Cóiré Thuaidh',
    'Won'                                                               => 'Bhuaigh',
    'Kuwaiti Dinar'                                                     => 'Dínear Cuátach',
    'Cayman Islands Dollar'                                             => 'Dollar Oileáin Cayman',
    'Tenge'                                                             => 'Tenge',
    'Lao Kip'                                                           => 'Lao Kip',
    'Lebanese Pound'                                                    => 'Punt na Liobáine',
    'Sri Lanka Rupee'                                                   => 'Rúipí Srí Lanca',
    'Liberian Dollar'                                                   => 'Dollar na Libéire',
    'Loti'                                                              => 'Loti',
    'Libyan Dinar'                                                      => 'Dínear Libia',
    'Moroccan Dirham'                                                   => 'Dirham Mharacó',
    'Moldovan Leu'                                                      => 'Leu Moldóvach',
    'Malagasy Ariary'                                                   => 'Ariary Malagasy',
    'Denar'                                                             => 'Denar',
    'Kyat'                                                              => 'Caid',
    'Tugrik'                                                            => 'Tugraic',
    'Pataca'                                                            => 'Pataca',
    'Ouguiya'                                                           => 'Ouguiya',
    'Mauritius Rupee'                                                   => 'Rúipí Oileán Mhuirís',
    'Rufiyaa'                                                           => 'Rufiyaa',
    'Malawi Kwacha'                                                     => 'Maláiv Kwacha',
    'Mexican Peso'                                                      => 'Peso Meicsiceo',
    'Mexican Unidad de Inversion (UDI)'                                 => 'Unidad Inversion Mheicsiceo (UDI)',
    'Malaysian Ringgit'                                                 => 'Ringgit Malaeisia',
    'Mozambique Metical'                                                => 'Mósaimbíc Metical',
    'Namibia Dollar'                                                    => 'Dollar na Namaibe',
    'Naira'                                                             => 'Naira',
    'Cordoba Oro'                                                       => 'Cordoba Óró',
    'Norwegian Krone'                                                   => 'Krone Ioruach',
    'Nepalese Rupee'                                                    => 'Rúipí Neipeáil',
    'New Zealand Dollar'                                                => 'Dollar na Nua-Shéalainne',
    'Rial Omani'                                                        => 'Rial Omani',
    'Balboa'                                                            => 'Balboa',
    'Sol'                                                               => 'Sól',
    'Kina'                                                              => 'Kina',
    'Philippine Peso'                                                   => 'Peso na hOileáin Fhilipíneacha',
    'Pakistan Rupee'                                                    => 'Rúipí na Pacastáine',
    'Zloty'                                                             => 'Zloty',
    'Guarani'                                                           => 'Guaráinis',
    'Qatari Rial'                                                       => 'Rial Qatari',
    'Romanian Leu'                                                      => 'Leu Rómáinis',
    'Serbian Dinar'                                                     => 'Dinar Seirbis',
    'Russian Ruble'                                                     => 'Rúbal Rúisis',
    'Rwanda Franc'                                                      => 'Franc Ruanda',
    'Saudi Riyal'                                                       => 'Riyal Sádach',
    'Solomon Islands Dollar'                                            => 'Dollar Oileáin Solomon',
    'Seychelles Rupee'                                                  => 'Rúipí na Séiséil',
    'Sudanese Pound'                                                    => 'Punt na Súdáine',
    'Swedish Krona'                                                     => 'Krona Sualannach',
    'Singapore Dollar'                                                  => 'Dollar Siombábach',
    'Saint Helena Pound'                                                => 'Punt San Héilin',
    'Leone'                                                             => 'Leon',
    'Somali Shilling'                                                   => 'Scilling Somáilis',
    'Surinam Dollar'                                                    => 'Dollar Suranam',
    'South Sudanese Pound'                                              => 'Punt na Súdáine Theas',
    'Dobra'                                                             => 'Dobra',
    'El Salvador Colon'                                                 => 'Colon na Salvadóire',
    'Syrian Pound'                                                      => 'Punt Siria',
    'Lilangeni'                                                         => 'Lilangeni',
    'Baht'                                                              => 'Baht',
    'Somoni'                                                            => 'Somoni',
    'Turkmenistan New Manat'                                            => 'Manat Nua Tuircméanastáin',
    'Tunisian Dinar'                                                    => 'Dínear na Túinéise',
    'Pa’anga'                                                           => 'Págana',
    'Turkish Lira'                                                      => 'Lira Tuircis',
    'Trinidad and Tobago Dollar'                                        => 'Dollar Oileáin na Tríonóide agus Tobága',
    'New Taiwan Dollar'                                                 => 'Dollar nua na Téaváine',
    'Tanzanian Shilling'                                                => 'Scilling Tansáin',
    'Hryvnia'                                                           => 'Hryvnia',
    'Uganda Shilling'                                                   => 'Scilling Uganda',
    'US Dollar'                                                         => 'Dollar S.A.M.',
    'US Dollar (Next day)'                                              => 'Dollar SAM (an chéad lá eile)',
    'Uruguay Peso en Unidades Indexadas (UI)'                           => 'Uragua Peso agus Innéacsadas Unidades (UI)',
    'Peso Uruguayo'                                                     => 'Peso Uragua',
    'Unidad Previsional'                                                => 'Unidad Réamhamhairc',
    'Uzbekistan Sum'                                                    => 'Suim na hÚisbéiceastáine',
    'Bolívar Soberano'                                                  => 'Bolívar Soberano',
    'Dong'                                                              => 'Dong',
    'Vatu'                                                              => 'Vatú',
    'Tala'                                                              => 'Tala',
    'CFA Franc BEAC'                                                    => 'CFA Franc BEAC',
    'Silver'                                                            => 'Geal',
    'Gold'                                                              => 'Ór',
    'Bond Markets Unit European Composite Unit (EURCO)'                 => 'Aonad Margaí Bannaí An tAonad Ilchodach Eorpach (EURCO)',
    'Bond Markets Unit European Monetary Unit (E.M.U.-6)'               => 'Aonad Margaí Bannaí An tAonad Airgeadaíochta Eorpach (E.M.U.-6)',
    'Bond Markets Unit European Unit of Account 9 (E.U.A.-9)'           => 'An tAonad um Margaí Bannaí An tAonad Cuntas Eorpach 9 (E.U.A.-9)',
    'Bond Markets Unit European Unit of Account 17 (E.U.A.-17)'         => 'An tAonad um Margaí Bannaí An tAonad Eorpach Cuntas 17 (E.U.A.-17)',
    'East Caribbean Dollar'                                             => 'Dollar na Cairibe Thoir',
    'SDR (Special Drawing Right)'                                       => 'SDR (Ceart Tarraingthe Speisialta)',
    'CFA Franc BCEAO'                                                   => 'CFA Franc BCEAO',
    'Palladium'                                                         => 'Pallaidiam',
    'CFP Franc'                                                         => 'CFP Franc',
    'Platinum'                                                          => 'Platanam',
    'Sucre'                                                             => 'Sucre',
    'Codes specifically reserved for testing purposes'                  => 'Cóid atá curtha in áirithe go sonrach chun críocha tástála',
    'ADB Unit of Account'                                               => 'Aonad Cuntas ADB',
    'The codes assigned for transactions where no currency is involved' => 'Na cóid a shanntar le haghaidh idirbhearta nach bhfuil aon airgeadra i gceist',
    'Yemeni Rial'                                                       => 'Rial Éimin',
    'Rand'                                                              => 'Rand',
    'Zambian Kwacha'                                                    => 'Kwacha na Saimbia',
    'Zimbabwe Dollar'                                                   => 'Dollar Siombábach',
];
