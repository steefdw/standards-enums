<?php

/**
 * Translations in Translation of ISO 4217 to Belarusian.
 *
 * @noinspection SpellCheckingInspection
 */

return [
    'UAE Dirham'                                                        => 'Дырхам (ААЭ)',
    'Afghani'                                                           => 'Афгані',
    'Lek'                                                               => 'Лек',
    'Armenian Dram'                                                     => 'Армянскі драм',
    'Netherlands Antillean Guilder'                                     => 'Нідэрландскі антыльскі гульдэн',
    'Kwanza'                                                            => 'Кванза',
    'Argentine Peso'                                                    => 'Аргентынскае песа',
    'Australian Dollar'                                                 => 'Аўстралійскі долар',
    'Aruban Florin'                                                     => 'Арубанскі флорын',
    'Azerbaijan Manat'                                                  => 'Азербайджанскі манат',
    'Convertible Mark'                                                  => 'Канвертоўная марка',
    'Barbados Dollar'                                                   => 'Барбадоскі долар',
    'Taka'                                                              => 'Така',
    'Bulgarian Lev'                                                     => 'Балгарскі леў',
    'Bahraini Dinar'                                                    => 'Бахрэйнскі дынар',
    'Burundi Franc'                                                     => 'Бурундзійскі франк',
    'Bermudian Dollar'                                                  => 'Бермудскі долар',
    'Brunei Dollar'                                                     => 'Брунейскі долар',
    'Boliviano'                                                         => 'Балівіяна',
    'Mvdol'                                                             => 'Балівійскі мвдол',
    'Brazilian Real'                                                    => 'Бразільскі рэал',
    'Bahamian Dollar'                                                   => 'Багамскі долар',
    'Ngultrum'                                                          => 'Нгультрум',
    'Pula'                                                              => 'Пула',
    'Belarusian Ruble'                                                  => 'Беларускі рубель',
    'Belize Dollar'                                                     => 'Белізскі долар',
    'Canadian Dollar'                                                   => 'Канадскі долар',
    'Congolese Franc'                                                   => 'Кангалезскі франк',
    'WIR Euro'                                                          => 'Еўра WIR',
    'Swiss Franc'                                                       => 'Швейцарскі франк',
    'WIR Franc'                                                         => 'Франк WIR',
    'Unidad de Fomento'                                                 => 'Умоўная разліковая адзінка Чілі',
    'Chilean Peso'                                                      => 'Чылійскае песа',
    'Yuan Renminbi'                                                     => 'Кітайскі юань',
    'Colombian Peso'                                                    => 'Калумбійскае песа',
    'Unidad de Valor Real'                                              => 'Адінка фактычнай вартасці Калумбіі',
    'Costa Rican Colon'                                                 => 'Кастарыканскі калон',
    'Peso Convertible'                                                  => 'Кубінскае канверсоўнае песа',
    'Cuban Peso'                                                        => 'Кубінскае песа',
    'Cabo Verde Escudo'                                                 => 'Эскуда Каба-Вердэ',
    'Czech Koruna'                                                      => 'Чэшская крона',
    'Djibouti Franc'                                                    => 'Джыбуційскі франк',
    'Danish Krone'                                                      => 'Дацкая крона',
    'Dominican Peso'                                                    => 'Дамініканскае песа',
    'Algerian Dinar'                                                    => 'Алжырскі дынар',
    'Egyptian Pound'                                                    => 'Егіпецкі фунт',
    'Nakfa'                                                             => 'Накфа',
    'Ethiopian Birr'                                                    => 'Эфіопскі быр',
    'Euro'                                                              => 'Еўра',
    'Fiji Dollar'                                                       => 'Фіджыйскі долар',
    'Falkland Islands Pound'                                            => 'Фунт Фалклендскіх выспаў',
    'Pound Sterling'                                                    => 'Фунт стэрлінгаў',
    'Lari'                                                              => 'Лары',
    'Ghana Cedi'                                                        => 'Ганскі седзі',
    'Gibraltar Pound'                                                   => 'Гібралтарскі фунт',
    'Dalasi'                                                            => 'Даласі',
    'Guinean Franc'                                                     => 'Гвінейскі франк',
    'Quetzal'                                                           => 'Кетсаль',
    'Guyana Dollar'                                                     => 'Гаянскі долар',
    'Hong Kong Dollar'                                                  => 'Ганконгскі долар',
    'Lempira'                                                           => 'Лемпіра',
    'Kuna'                                                              => 'Куна',
    'Gourde'                                                            => 'Гурд',
    'Forint'                                                            => 'Форынт',
    'Rupiah'                                                            => 'Рупія',
    'New Israeli Sheqel'                                                => 'Новы ізраільскі шэкель',
    'Indian Rupee'                                                      => 'Індыйская рупія',
    'Iraqi Dinar'                                                       => 'Іракскі дынар',
    'Iranian Rial'                                                      => 'Іранскі рыал',
    'Iceland Krona'                                                     => 'Ісландская крона',
    'Jamaican Dollar'                                                   => 'Ямайскі долар',
    'Jordanian Dinar'                                                   => 'Іарданскі дынар',
    'Yen'                                                               => 'Іена',
    'Kenyan Shilling'                                                   => 'Кенійскі шылінг',
    'Som'                                                               => 'Кыргызскі сом',
    'Riel'                                                              => 'Рыель',
    'Comorian Franc'                                                    => 'Каморскі франк',
    'North Korean Won'                                                  => 'Паўночнакарэйская вона',
    'Won'                                                               => 'Вона',
    'Kuwaiti Dinar'                                                     => 'Кувейцкі дынар',
    'Cayman Islands Dollar'                                             => 'Кайманскі даляр',
    'Tenge'                                                             => 'Тэнге',
    'Lao Kip'                                                           => 'Лаоскі кіп',
    'Lebanese Pound'                                                    => 'Ліванскі фунт',
    'Sri Lanka Rupee'                                                   => 'Рупія Шры-Ланкі',
    'Liberian Dollar'                                                   => 'Ліберыйскі долар',
    'Loti'                                                              => 'Лоці',
    'Libyan Dinar'                                                      => 'Лівійскі дынар',
    'Moroccan Dirham'                                                   => 'Мараканскі дырхам',
    'Moldovan Leu'                                                      => 'Малдаўскі лей',
    'Malagasy Ariary'                                                   => 'Малагасійскі арыяры',
    'Denar'                                                             => 'Македонскі дэнар',
    'Kyat'                                                              => 'К’ят',
    'Tugrik'                                                            => 'Тугрык',
    'Pataca'                                                            => 'Патака',
    'Ouguiya'                                                           => 'Угія',
    'Mauritius Rupee'                                                   => 'Маўрыкійская рупія',
    'Rufiyaa'                                                           => 'Руфія',
    'Malawi Kwacha'                                                     => 'Малавійская квача',
    'Mexican Peso'                                                      => 'Мексіканскае песа',
    'Mexican Unidad de Inversion (UDI)'                                 => 'Мексіканская адзінка абмену (UDI)',
    'Malaysian Ringgit'                                                 => 'Малайзійскі рынгіт',
    'Mozambique Metical'                                                => 'Мазамбікскі мецікал',
    'Namibia Dollar'                                                    => 'Намібійскі долар',
    'Naira'                                                             => 'Найра',
    'Cordoba Oro'                                                       => 'Залатая кардоба',
    'Norwegian Krone'                                                   => 'Нарвежская крона',
    'Nepalese Rupee'                                                    => 'Непальская рупія',
    'New Zealand Dollar'                                                => 'Новазеландскі долар',
    'Rial Omani'                                                        => 'Аманскі рыал',
    'Balboa'                                                            => 'Бальбоа',
    'Sol'                                                               => 'Соль',
    'Kina'                                                              => 'Кіна',
    'Philippine Peso'                                                   => 'Філіпінскае песа',
    'Pakistan Rupee'                                                    => 'Пакістанская рупія',
    'Zloty'                                                             => 'Злоты',
    'Guarani'                                                           => 'Гуарані',
    'Qatari Rial'                                                       => 'Катарскі рыял',
    'Romanian Leu'                                                      => 'Румынскі лей',
    'Serbian Dinar'                                                     => 'Сербскі дынар',
    'Russian Ruble'                                                     => 'Расійскі рубель',
    'Rwanda Franc'                                                      => 'Руандыйскі франк',
    'Saudi Riyal'                                                       => 'Саудаўскі рыял',
    'Solomon Islands Dollar'                                            => 'Долар Саламонавых выспаў',
    'Seychelles Rupee'                                                  => 'Сейшэльская рупія',
    'Sudanese Pound'                                                    => 'Суданскі фунт',
    'Swedish Krona'                                                     => 'Шведская крона',
    'Singapore Dollar'                                                  => 'Сінгапурскі долар',
    'Saint Helena Pound'                                                => 'Фунт Святой Алены',
    'Leone'                                                             => 'Леонэ',
    'Somali Shilling'                                                   => 'Самалійскі шылінг',
    'Surinam Dollar'                                                    => 'Сурынамскі долар',
    'South Sudanese Pound'                                              => 'Паўднёвасуданскі фунт',
    'Dobra'                                                             => 'Добра',
    'El Salvador Colon'                                                 => 'Сальвадорскі калон',
    'Syrian Pound'                                                      => 'Сірыйскі фунт',
    'Lilangeni'                                                         => 'Лілангені',
    'Baht'                                                              => 'Бат',
    'Somoni'                                                            => 'Самоні',
    'Turkmenistan New Manat'                                            => 'Туркменскі новы манат',
    'Tunisian Dinar'                                                    => 'Туніскі дынар',
    'Pa’anga'                                                           => 'Паанга',
    'Turkish Lira'                                                      => 'Турэцкая ліра',
    'Trinidad and Tobago Dollar'                                        => 'Долар Трынідада і Табага',
    'New Taiwan Dollar'                                                 => 'Новы тайванскі долар',
    'Tanzanian Shilling'                                                => 'Танзанійскі шылінг',
    'Hryvnia'                                                           => 'Грыўня',
    'Uganda Shilling'                                                   => 'Угандыйскі шылінг',
    'US Dollar'                                                         => 'Долар ЗША',
    'US Dollar (Next day)'                                              => 'Долар ЗША (наступны дзень)',
    'Uruguay Peso en Unidades Indexadas (UI)'                           => 'Адзінка індэксавання уругвайскага песа (UI)',
    'Peso Uruguayo'                                                     => 'Уругвайскае песа',
    'Unidad Previsional'                                                => 'Уругавайская пенсійная адзінка',
    'Uzbekistan Sum'                                                    => 'Узбекскі сум',
    'Bolívar Soberano'                                                  => 'Суверэнны балівар',
    'Dong'                                                              => 'Донг',
    'Vatu'                                                              => 'Вату',
    'Tala'                                                              => 'Тала',
    'CFA Franc BEAC'                                                    => 'Франк КФА',
    'Silver'                                                            => 'Серабро',
    'Gold'                                                              => 'Золата',
    'Bond Markets Unit European Composite Unit (EURCO)'                 => 'Еўрапейская кампазітная адзінка рынку аблігацый (EURCO)',
    'Bond Markets Unit European Monetary Unit (E.M.U.-6)'               => 'Еўрапейская вылютная адзінка рынку аблігацый (E.M.U.-6)',
    'Bond Markets Unit European Unit of Account 9 (E.U.A.-9)'           => 'Еўрапейская разліковая адзінка 9 рынку аблігацый (E.U.A.-9)',
    'Bond Markets Unit European Unit of Account 17 (E.U.A.-17)'         => 'Еўрапейская разліковая адзінка 17 рынку аблігацый (E.U.A.-17)',
    'East Caribbean Dollar'                                             => 'Усходнекарыбскі долар',
    'SDR (Special Drawing Right)'                                       => 'Адмысловыя правы запазычання',
    'CFA Franc BCEAO'                                                   => 'Франк КФА BCEAO',
    'Palladium'                                                         => 'Паладый',
    'CFP Franc'                                                         => 'Французскі ціхаакіянскі франк',
    'Platinum'                                                          => 'Плаціна',
    'Sucre'                                                             => 'Сукрэ',
    'Codes specifically reserved for testing purposes'                  => 'Коды, адмыслова прызначаныя для тэставання',
    'ADB Unit of Account'                                               => 'Разліковая адзінка ADB',
    'The codes assigned for transactions where no currency is involved' => 'Коды для безвалютных транзакцый',
    'Yemeni Rial'                                                       => 'Еменскі рыял',
    'Rand'                                                              => 'Ранд',
    'Zambian Kwacha'                                                    => 'Замбійская квача',
    'Zimbabwe Dollar'                                                   => 'Зімбабвійскі долар',
];
