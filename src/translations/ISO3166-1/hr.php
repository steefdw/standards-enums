<?php

/**
 * Translations in Croatian.
 *
 * @noinspection SpellCheckingInspection
 */

return [
    'Aruba'                                                => 'Aruba',
    'Afghanistan'                                          => 'Afganistan',
    'Islamic Republic of Afghanistan'                      => 'Islamska Republika Afganistan',
    'Angola'                                               => 'Angola',
    'Republic of Angola'                                   => 'Republika Angola',
    'Anguilla'                                             => 'Anguila',
    'Åland Islands'                                        => 'Alandski otoci',
    'Albania'                                              => 'Albanija',
    'Republic of Albania'                                  => 'Republika Albanija',
    'Andorra'                                              => 'Andora',
    'Principality of Andorra'                              => 'Kneževina Andora',
    'United Arab Emirates'                                 => 'Ujedinjeni Arapski Emirati',
    'Argentina'                                            => 'Argentina',
    'Argentine Republic'                                   => 'Argentinska Republika',
    'Armenia'                                              => 'Armenija',
    'Republic of Armenia'                                  => 'Republika Armenija',
    'American Samoa'                                       => 'Američka Samoa',
    'Antarctica'                                           => 'Antarktika',
    'French Southern Territories'                          => 'Francuski Južni Teritoriji',
    'Antigua and Barbuda'                                  => 'Antigua i Barbuda',
    'Australia'                                            => 'Australija',
    'Austria'                                              => 'Austrija',
    'Republic of Austria'                                  => 'Republika Austrija',
    'Azerbaijan'                                           => 'Azerbajdžan',
    'Republic of Azerbaijan'                               => 'Republika Azerbajdžan',
    'Burundi'                                              => 'Burundi',
    'Republic of Burundi'                                  => 'Republika Burundi',
    'Belgium'                                              => 'Belgija',
    'Kingdom of Belgium'                                   => 'Kraljevina Belgija',
    'Benin'                                                => 'Benin',
    'Republic of Benin'                                    => 'Republika Benin',
    'Bonaire, Sint Eustatius and Saba'                     => 'Bonaire, Sveti Eustahije i Saba',
    'Burkina Faso'                                         => 'Burkina Faso',
    'Bangladesh'                                           => 'Bangladeš',
    'People\'s Republic of Bangladesh'                     => 'Narodna Republika Bangladeš',
    'Bulgaria'                                             => 'Bugarska',
    'Republic of Bulgaria'                                 => 'Republika Bugarska',
    'Bahrain'                                              => 'Bahrein',
    'Kingdom of Bahrain'                                   => 'Kraljevina Bahrein',
    'Bahamas'                                              => 'Bahami',
    'Commonwealth of the Bahamas'                          => 'Commonwealth Bahama',
    'Bosnia and Herzegovina'                               => 'Bosna i Hercegovina',
    'Republic of Bosnia and Herzegovina'                   => 'Republika Bosna i Hercegovina',
    'Saint Barthélemy'                                     => 'Sveti Bartolomej',
    'Belarus'                                              => 'Bjelorusija',
    'Republic of Belarus'                                  => 'Republika Bjelorusija',
    'Belize'                                               => 'Belize',
    'Bermuda'                                              => 'Bermudi',
    'Bolivia, Plurinational State of'                      => 'Bolivija, Plurinacionalna država',
    'Plurinational State of Bolivia'                       => 'Plurinacionalna država Bolivija',
    'Bolivia'                                              => 'Bolivija',
    'Brazil'                                               => 'Brazil',
    'Federative Republic of Brazil'                        => 'Savezna Republika Brazil',
    'Barbados'                                             => 'Barbados',
    'Brunei Darussalam'                                    => 'Brunej Darussalam',
    'Bhutan'                                               => 'Butan',
    'Kingdom of Bhutan'                                    => 'Kraljevina Butan',
    'Bouvet Island'                                        => 'Otok Bouvet',
    'Botswana'                                             => 'Bocvana',
    'Republic of Botswana'                                 => 'Republika Bocvana',
    'Central African Republic'                             => 'Srednjoafrička Republika',
    'Canada'                                               => 'Kanada',
    'Cocos (Keeling) Islands'                              => 'Cocos (Keeling) otoci',
    'Switzerland'                                          => 'Švicarska',
    'Swiss Confederation'                                  => 'Švicarska Konfederacija',
    'Chile'                                                => 'Čile',
    'Republic of Chile'                                    => 'Republika Čile',
    'China'                                                => 'Kina',
    'People\'s Republic of China'                          => 'Narodna Republika Kina',
    'Côte d\'Ivoire'                                       => 'Obala Bjelokosti',
    'Republic of Côte d\'Ivoire'                           => 'Republika Obale Bjelokosti',
    'Cameroon'                                             => 'Kamerun',
    'Republic of Cameroon'                                 => 'Republika Kamerun',
    'Congo, The Democratic Republic of the'                => 'Kongo, Demokratska Repubilika',
    'Congo'                                                => 'Kongo',
    'Republic of the Congo'                                => 'Republika Kongo',
    'Cook Islands'                                         => 'Cookovo Otočje',
    'Colombia'                                             => 'Kolumbija',
    'Republic of Colombia'                                 => 'Republika Kolumbija',
    'Comoros'                                              => 'Komori',
    'Union of the Comoros'                                 => 'Unija Komorâ',
    'Cabo Verde'                                           => 'Zelenortski otoci',
    'Republic of Cabo Verde'                               => 'Republika Kabo Verde',
    'Costa Rica'                                           => 'Kostarika',
    'Republic of Costa Rica'                               => 'Republika Kostarika',
    'Cuba'                                                 => 'Kuba',
    'Republic of Cuba'                                     => 'Republika Kuba',
    'Curaçao'                                              => 'Curaçao',
    'Christmas Island'                                     => 'Božićni Otok',
    'Cayman Islands'                                       => 'Kajmanski otoci',
    'Cyprus'                                               => 'Cipar',
    'Republic of Cyprus'                                   => 'Republika Cipar',
    'Czechia'                                              => 'Češka',
    'Czech Republic'                                       => 'Češka Republika',
    'Germany'                                              => 'Njemačka',
    'Federal Republic of Germany'                          => 'Savezna Republika Njemačka',
    'Djibouti'                                             => 'Džibuti',
    'Republic of Djibouti'                                 => 'Republika Džibuti',
    'Dominica'                                             => 'Dominika',
    'Commonwealth of Dominica'                             => 'Commonwealth Dominike',
    'Denmark'                                              => 'Danska',
    'Kingdom of Denmark'                                   => 'Kraljevina Danska',
    'Dominican Republic'                                   => 'Dominikanska Republika',
    'Algeria'                                              => 'Alžir',
    'People\'s Democratic Republic of Algeria'             => 'Narodna Demokratska Republika Alžir',
    'Ecuador'                                              => 'Ekvador',
    'Republic of Ecuador'                                  => 'Republika Ekvador',
    'Egypt'                                                => 'Egipat',
    'Arab Republic of Egypt'                               => 'Arapska Republika Egipat',
    'Eritrea'                                              => 'Eritreja',
    'the State of Eritrea'                                 => 'Država Eritreja',
    'Western Sahara'                                       => 'Zapadna Sahara',
    'Spain'                                                => 'Španjolska',
    'Kingdom of Spain'                                     => 'Kraljevina Španjolska',
    'Estonia'                                              => 'Estonija',
    'Republic of Estonia'                                  => 'Republika Estonija',
    'Ethiopia'                                             => 'Etiopija',
    'Federal Democratic Republic of Ethiopia'              => 'Savezna Demokratska Republika Etiopija',
    'Finland'                                              => 'Finska',
    'Republic of Finland'                                  => 'Republika Finska',
    'Fiji'                                                 => 'Fidži',
    'Republic of Fiji'                                     => 'Republika Fidži',
    'Falkland Islands (Malvinas)'                          => 'Falklandski otoci',
    'France'                                               => 'Francuska',
    'French Republic'                                      => 'Republika Francuska',
    'Faroe Islands'                                        => 'Farski otoci',
    'Micronesia, Federated States of'                      => 'Mikronezija, Savezne Države',
    'Federated States of Micronesia'                       => 'Savezne Države Mikronezije',
    'Gabon'                                                => 'Gabon',
    'Gabonese Republic'                                    => 'Republika Gabon',
    'United Kingdom'                                       => 'Ujedinjeno Kraljevstvo',
    'United Kingdom of Great Britain and Northern Ireland' => 'Ujedinjeno Kraljevstvo Velike Britanije i Sjeverne Irske',
    'Georgia'                                              => 'Gruzija',
    'Guernsey'                                             => 'Guernsey',
    'Ghana'                                                => 'Gana',
    'Republic of Ghana'                                    => 'Republika Gana',
    'Gibraltar'                                            => 'Gibraltar',
    'Guinea'                                               => 'Gvineja',
    'Republic of Guinea'                                   => 'Republika Gvineja',
    'Guadeloupe'                                           => 'Gvadalupa',
    'Gambia'                                               => 'Gambija',
    'Republic of the Gambia'                               => 'Republika Gambija',
    'Guinea-Bissau'                                        => 'Gvineja Bisau',
    'Republic of Guinea-Bissau'                            => 'Republika Gvineja Bisau',
    'Equatorial Guinea'                                    => 'Ekvatorijalna Gvineja',
    'Republic of Equatorial Guinea'                        => 'Republika Ekvatorijalna Gvineja',
    'Greece'                                               => 'Grčka',
    'Hellenic Republic'                                    => 'Helenska Republika',
    'Grenada'                                              => 'Grenada',
    'Greenland'                                            => 'Grenland',
    'Guatemala'                                            => 'Gvatemala',
    'Republic of Guatemala'                                => 'Republika Gvatemala',
    'French Guiana'                                        => 'Francuska Gijana',
    'Guam'                                                 => 'Guam',
    'Guyana'                                               => 'Gvajana',
    'Republic of Guyana'                                   => 'Republika Gvajana',
    'Hong Kong'                                            => 'Hong Kong',
    'Hong Kong Special Administrative Region of China'     => 'Hong Kong posebna upravna regija Narodne Republike Kine',
    'Heard Island and McDonald Islands'                    => 'Otok Heard i otočje McDonald',
    'Honduras'                                             => 'Honduras',
    'Republic of Honduras'                                 => 'Republika Honduras',
    'Croatia'                                              => 'Hrvatska',
    'Republic of Croatia'                                  => 'Republika Hrvatska',
    'Haiti'                                                => 'Haiti',
    'Republic of Haiti'                                    => 'Republika Haiti',
    'Hungary'                                              => 'Mađarska',
    'Indonesia'                                            => 'Indonezija',
    'Republic of Indonesia'                                => 'Republika Indonezija',
    'Isle of Man'                                          => 'Otok Man',
    'India'                                                => 'Indija',
    'Republic of India'                                    => 'Republika Indija',
    'British Indian Ocean Territory'                       => 'Britanski Indijskooceanski teritorij',
    'Ireland'                                              => 'Irska',
    'Iran, Islamic Republic of'                            => 'Iran, Islamska Republika',
    'Islamic Republic of Iran'                             => 'Islamska Republika Iran',
    'Iran'                                                 => 'Iran, Islamska Republika',
    'Iraq'                                                 => 'Irak',
    'Republic of Iraq'                                     => 'Republika Irak',
    'Iceland'                                              => 'Island',
    'Republic of Iceland'                                  => 'Republika Island',
    'Israel'                                               => 'Izrael',
    'State of Israel'                                      => 'Država Izrael',
    'Italy'                                                => 'Italija',
    'Italian Republic'                                     => 'Talijanska Republika',
    'Jamaica'                                              => 'Jamajka',
    'Jersey'                                               => 'Jersey',
    'Jordan'                                               => 'Jordan',
    'Hashemite Kingdom of Jordan'                          => 'Kraljevina Jordan',
    'Japan'                                                => 'Japan',
    'Kazakhstan'                                           => 'Kazahstan',
    'Republic of Kazakhstan'                               => 'Republika Kazahstan',
    'Kenya'                                                => 'Kenija',
    'Republic of Kenya'                                    => 'Republika Kenija',
    'Kyrgyzstan'                                           => 'Kirgistan',
    'Kyrgyz Republic'                                      => 'Kirgiska Republika',
    'Cambodia'                                             => 'Kambodža',
    'Kingdom of Cambodia'                                  => 'Kraljevina Kambodža',
    'Kiribati'                                             => 'Kiribati',
    'Republic of Kiribati'                                 => 'Republika Kirbati',
    'Saint Kitts and Nevis'                                => 'Sveti Kristofor i Nevis',
    'Korea, Republic of'                                   => 'Koreja, Republika',
    'South Korea'                                          => 'Južna Korea',
    'Kuwait'                                               => 'Kuvajt',
    'State of Kuwait'                                      => 'Država Kuvajt',
    'Lao People\'s Democratic Republic'                    => 'Laoska Narodna Demokratska Republika',
    'Laos'                                                 => 'Laoska Narodna Demokratska Republika',
    'Lebanon'                                              => 'Libanon',
    'Lebanese Republic'                                    => 'Libanonska Republika',
    'Liberia'                                              => 'Liberija',
    'Republic of Liberia'                                  => 'Republika Liberija',
    'Libya'                                                => 'Libija',
    'Saint Lucia'                                          => 'Sveta Lucija',
    'Liechtenstein'                                        => 'Lihtenštajn',
    'Principality of Liechtenstein'                        => 'Kneževina Lihtenštajn',
    'Sri Lanka'                                            => 'Šri Lanka',
    'Democratic Socialist Republic of Sri Lanka'           => 'Demokratska Socijalištička Republika Šri Lanka',
    'Lesotho'                                              => 'Lesoto',
    'Kingdom of Lesotho'                                   => 'Kraljevina Lesoto',
    'Lithuania'                                            => 'Litva',
    'Republic of Lithuania'                                => 'Republika Litva',
    'Luxembourg'                                           => 'Luksemburg',
    'Grand Duchy of Luxembourg'                            => 'Kneževina Luksemburg',
    'Latvia'                                               => 'Latvija',
    'Republic of Latvia'                                   => 'Republika Latvija',
    'Macao'                                                => 'Makao',
    'Macao Special Administrative Region of China'         => 'Posebna upravna regija Makao',
    'Saint Martin (French part)'                           => 'Sveti Martin (francuski dio)',
    'Morocco'                                              => 'Maroko',
    'Kingdom of Morocco'                                   => 'Kraljevina Maroko',
    'Monaco'                                               => 'Monako',
    'Principality of Monaco'                               => 'Kneževina Monako',
    'Moldova, Republic of'                                 => 'Moldavija, Republika',
    'Republic of Moldova'                                  => 'Republika Moldavija',
    'Moldova'                                              => 'Moldavija',
    'Madagascar'                                           => 'Madagaskar',
    'Republic of Madagascar'                               => 'Republika Madagaskar',
    'Maldives'                                             => 'Maldivi',
    'Republic of Maldives'                                 => 'Republika Maldivi',
    'Mexico'                                               => 'Meksiko',
    'United Mexican States'                                => 'Sjedinjene Meksičke Države',
    'Marshall Islands'                                     => 'Maršalovi otoci',
    'Republic of the Marshall Islands'                     => 'Republika Maršalovi Otoci',
    'North Macedonia'                                      => 'Sjeverna Makedonija',
    'Republic of North Macedonia'                          => 'Republika Sjeverna Makedonija',
    'Mali'                                                 => 'Mali',
    'Republic of Mali'                                     => 'Republika Mali',
    'Malta'                                                => 'Malta',
    'Republic of Malta'                                    => 'Republika Malta',
    'Myanmar'                                              => 'Mjanmar',
    'Republic of Myanmar'                                  => 'Republika Mjanmar',
    'Montenegro'                                           => 'Crna Gora',
    'Mongolia'                                             => 'Mongolija',
    'Northern Mariana Islands'                             => 'Sjevernomarijanski otoci',
    'Commonwealth of the Northern Mariana Islands'         => 'Commonwealth Sjevernomarijanskog otočja',
    'Mozambique'                                           => 'Mozambik',
    'Republic of Mozambique'                               => 'Republika Mozambik',
    'Mauritania'                                           => 'Mauretanija',
    'Islamic Republic of Mauritania'                       => 'Islamska Republika Mauretanija',
    'Montserrat'                                           => 'Montserrat',
    'Martinique'                                           => 'Martinik',
    'Mauritius'                                            => 'Mauricijus',
    'Republic of Mauritius'                                => 'Republika Mauricijus',
    'Malawi'                                               => 'Malavi',
    'Republic of Malawi'                                   => 'Republika Malavi',
    'Malaysia'                                             => 'Malezija',
    'Mayotte'                                              => 'Mayotte',
    'Namibia'                                              => 'Namibija',
    'Republic of Namibia'                                  => 'Republika Namibija',
    'New Caledonia'                                        => 'Nova Kaledonija',
    'Niger'                                                => 'Niger',
    'Republic of the Niger'                                => 'Republika Niger',
    'Norfolk Island'                                       => 'Otok Norfolk',
    'Nigeria'                                              => 'Nigerija',
    'Federal Republic of Nigeria'                          => 'Savezna Republika Nigerija',
    'Nicaragua'                                            => 'Nikaragva',
    'Republic of Nicaragua'                                => 'Republika Nikaragva',
    'Niue'                                                 => 'Niue',
    'Netherlands'                                          => 'Nizozemska',
    'Kingdom of the Netherlands'                           => 'Kraljevina Nizozemska',
    'Norway'                                               => 'Norveška',
    'Kingdom of Norway'                                    => 'Kraljevina Norveška',
    'Nepal'                                                => 'Nepal',
    'Federal Democratic Republic of Nepal'                 => 'Savezna Demokratska Republika Nepal',
    'Nauru'                                                => 'Nauru',
    'Republic of Nauru'                                    => 'Republika Nauru',
    'New Zealand'                                          => 'Novi Zeland',
    'Oman'                                                 => 'Oman',
    'Sultanate of Oman'                                    => 'Sultanat Oman',
    'Pakistan'                                             => 'Pakistan',
    'Islamic Republic of Pakistan'                         => 'Islamska Republika Pakistan',
    'Panama'                                               => 'Panama',
    'Republic of Panama'                                   => 'Republika Panama',
    'Pitcairn'                                             => 'Pitcairnovo Otočje',
    'Peru'                                                 => 'Peru',
    'Republic of Peru'                                     => 'Republika Peru',
    'Philippines'                                          => 'Filipini',
    'Republic of the Philippines'                          => 'Republika Filipini',
    'Palau'                                                => 'Palau',
    'Republic of Palau'                                    => 'Republika Palau',
    'Papua New Guinea'                                     => 'Papua Nova Gvineja',
    'Independent State of Papua New Guinea'                => 'Nezavisna Država Papua Nova Gvineja',
    'Poland'                                               => 'Poljska',
    'Republic of Poland'                                   => 'Republika Poljska',
    'Puerto Rico'                                          => 'Portoriko',
    'Korea, Democratic People\'s Republic of'              => 'Koreja, Demokratska Narodna Republika',
    'Democratic People\'s Republic of Korea'               => 'Demokratska Narodna Republika Koreja',
    'North Korea'                                          => 'Sjeverna Koreja',
    'Portugal'                                             => 'Portugal',
    'Portuguese Republic'                                  => 'Portugalska Republika',
    'Paraguay'                                             => 'Paragvaj',
    'Republic of Paraguay'                                 => 'Republika Paragvaj',
    'Palestine, State of'                                  => 'Palestina',
    'the State of Palestine'                               => 'Država Palestina',
    'French Polynesia'                                     => 'Francuska Polinezija',
    'Qatar'                                                => 'Katar',
    'State of Qatar'                                       => 'Država Katar',
    'Réunion'                                              => 'Réunion',
    'Romania'                                              => 'Rumunjska',
    'Russian Federation'                                   => 'Ruska Federacija',
    'Rwanda'                                               => 'Ruanda',
    'Rwandese Republic'                                    => 'Ruandska Republika',
    'Saudi Arabia'                                         => 'Saudijska Arabija',
    'Kingdom of Saudi Arabia'                              => 'Kraljevina Saudijska Arabija',
    'Sudan'                                                => 'Sudan',
    'Republic of the Sudan'                                => 'Republika Sudan',
    'Senegal'                                              => 'Senegal',
    'Republic of Senegal'                                  => 'Republika Senegal',
    'Singapore'                                            => 'Singapur',
    'Republic of Singapore'                                => 'Republika Singapur',
    'South Georgia and the South Sandwich Islands'         => 'Južna Georgija i otočje Južni Sandwich',
    'Saint Helena, Ascension and Tristan da Cunha'         => 'Sveta Helena, Ascension i Tristan da Cunha',
    'Svalbard and Jan Mayen'                               => 'Svalbard i Jan Mayen',
    'Solomon Islands'                                      => 'Salomonski Otoci',
    'Sierra Leone'                                         => 'Sijera Leone',
    'Republic of Sierra Leone'                             => 'Republika Sijera Leone',
    'El Salvador'                                          => 'Salvador',
    'Republic of El Salvador'                              => 'Republika El Salvador',
    'San Marino'                                           => 'San Marino',
    'Republic of San Marino'                               => 'Republika San Marino',
    'Somalia'                                              => 'Somalija',
    'Federal Republic of Somalia'                          => 'Savezna Republika Somalija',
    'Saint Pierre and Miquelon'                            => 'Sveti Petar i Mikelon',
    'Serbia'                                               => 'Srbija',
    'Republic of Serbia'                                   => 'Republika Srbija',
    'South Sudan'                                          => 'Južni Sudan',
    'Republic of South Sudan'                              => 'Republika Južni Sudan',
    'Sao Tome and Principe'                                => 'Sveti Toma i Princip',
    'Democratic Republic of Sao Tome and Principe'         => 'Demokratska Republika Sveti Toma i Princip',
    'Suriname'                                             => 'Surinam',
    'Republic of Suriname'                                 => 'Republika Surinam',
    'Slovakia'                                             => 'Slovačka',
    'Slovak Republic'                                      => 'Slovačka Republika',
    'Slovenia'                                             => 'Slovenija',
    'Republic of Slovenia'                                 => 'Republika Slovenija',
    'Sweden'                                               => 'Švedska',
    'Kingdom of Sweden'                                    => 'Kraljevina Švedska',
    'Eswatini'                                             => 'Esvatini',
    'Kingdom of Eswatini'                                  => 'Kraljevina Esvatini',
    'Sint Maarten (Dutch part)'                            => 'Sveti Martin (nizozemski dio)',
    'Seychelles'                                           => 'Sejšeli',
    'Republic of Seychelles'                               => 'Republika Sejšeli',
    'Syrian Arab Republic'                                 => 'Sirijska Arapska Republika',
    'Syria'                                                => 'Sirijska Arapska Republika',
    'Turks and Caicos Islands'                             => 'Otoci Turks i Caicos',
    'Chad'                                                 => 'Čad',
    'Republic of Chad'                                     => 'Republika Čad',
    'Togo'                                                 => 'Togo',
    'Togolese Republic'                                    => 'Togoanska Republika',
    'Thailand'                                             => 'Tajland',
    'Kingdom of Thailand'                                  => 'Kraljevina Tajland',
    'Tajikistan'                                           => 'Tadžikistan',
    'Republic of Tajikistan'                               => 'Republika Tadžikistan',
    'Tokelau'                                              => 'Tokelau',
    'Turkmenistan'                                         => 'Turkmenistan',
    'Timor-Leste'                                          => 'Istočni Timor',
    'Democratic Republic of Timor-Leste'                   => 'Demokratska Republika Timor-Leste',
    'Tonga'                                                => 'Tonga',
    'Kingdom of Tonga'                                     => 'Kraljevina Tonga',
    'Trinidad and Tobago'                                  => 'Trinidad i Tobago',
    'Republic of Trinidad and Tobago'                      => 'Republika Trinidad i Tobago',
    'Tunisia'                                              => 'Tunis',
    'Republic of Tunisia'                                  => 'Republika Tunis',
    'Türkiye'                                              => 'Turska',
    'Republic of Türkiye'                                  => 'Republika Turska',
    'Tuvalu'                                               => 'Tuvalu',
    'Taiwan, Province of China'                            => 'Tajvan (provincija, NR Kina)',
    'Taiwan'                                               => 'Tajvan',
    'Tanzania, United Republic of'                         => 'Tanzanija',
    'United Republic of Tanzania'                          => 'Ujedinjena Republika Tanzanija',
    'Tanzania'                                             => 'Tanzanija',
    'Uganda'                                               => 'Uganda',
    'Republic of Uganda'                                   => 'Republika Uganda',
    'Ukraine'                                              => 'Ukrajina',
    'United States Minor Outlying Islands'                 => 'Američki mali izvanjski otoci',
    'Uruguay'                                              => 'Urugvaj',
    'Eastern Republic of Uruguay'                          => 'Istočna Republika Urugvaj',
    'United States'                                        => 'Sjedinjene Države',
    'United States of America'                             => 'Sjedinjene Američke Države',
    'Uzbekistan'                                           => 'Uzbekistan',
    'Republic of Uzbekistan'                               => 'Republika Uzbekistan',
    'Holy See (Vatican City State)'                        => 'Vatikan (Država Vatikanskoga Grada)',
    'Saint Vincent and the Grenadines'                     => 'Sveti Vincent i Grenadini',
    'Venezuela, Bolivarian Republic of'                    => 'Venezuela, Bolivarska Republika',
    'Bolivarian Republic of Venezuela'                     => 'Bolivarijanska Republika Venezuela',
    'Venezuela'                                            => 'Venezuela',
    'Virgin Islands, British'                              => 'Djevičanski Otoci, Britanski',
    'British Virgin Islands'                               => 'Britanski Djevičanski Otoci',
    'Virgin Islands, U.S.'                                 => 'Djevičanski Otoci, SAD',
    'Virgin Islands of the United States'                  => 'Američki Djevičanski Otoci',
    'Viet Nam'                                             => 'Vijetnam',
    'Socialist Republic of Viet Nam'                       => 'Socijalistička Republika Vijetnam',
    'Vietnam'                                              => 'Vijetnam',
    'Vanuatu'                                              => 'Vanuatu',
    'Republic of Vanuatu'                                  => 'Republika Vanuatu',
    'Wallis and Futuna'                                    => 'Wallis i Futuna',
    'Samoa'                                                => 'Samoa',
    'Independent State of Samoa'                           => 'Nezavisna Država Samoa',
    'Yemen'                                                => 'Jemen',
    'Republic of Yemen'                                    => 'Republika Jemen',
    'South Africa'                                         => 'Južnoafrička Republika',
    'Republic of South Africa'                             => 'Republika Južna Afrika',
    'Zambia'                                               => 'Zambija',
    'Republic of Zambia'                                   => 'Republika Zambija',
    'Zimbabwe'                                             => 'Zimbabve',
    'Republic of Zimbabwe'                                 => 'Republika Zimbabve',
];
