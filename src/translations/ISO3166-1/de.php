<?php

/**
 * Translations in German.
 *
 * @noinspection SpellCheckingInspection
 */

return [
    'Aruba'                                                => 'Aruba',
    'Afghanistan'                                          => 'Afghanistan',
    'Islamic Republic of Afghanistan'                      => 'Islamische Republik Afghanistan',
    'Angola'                                               => 'Angola',
    'Republic of Angola'                                   => 'Republik Angola',
    'Anguilla'                                             => 'Anguilla',
    'Åland Islands'                                        => 'Åland-Inseln',
    'Albania'                                              => 'Albanien',
    'Republic of Albania'                                  => 'Republik Albanien',
    'Andorra'                                              => 'Andorra',
    'Principality of Andorra'                              => 'Fürstentum Andorra',
    'United Arab Emirates'                                 => 'Vereinigte Arabische Emirate',
    'Argentina'                                            => 'Argentinien',
    'Argentine Republic'                                   => 'Argentinische Republik',
    'Armenia'                                              => 'Armenien',
    'Republic of Armenia'                                  => 'Republik Armenien',
    'American Samoa'                                       => 'Amerikanisch-Samoa',
    'Antarctica'                                           => 'Antarktis',
    'French Southern Territories'                          => 'Französische Süd- und Antarktisgebiete',
    'Antigua and Barbuda'                                  => 'Antigua und Barbuda',
    'Australia'                                            => 'Australien',
    'Austria'                                              => 'Österreich',
    'Republic of Austria'                                  => 'Republik Österreich',
    'Azerbaijan'                                           => 'Aserbaidschan',
    'Republic of Azerbaijan'                               => 'Republik Aserbaidschan',
    'Burundi'                                              => 'Burundi',
    'Republic of Burundi'                                  => 'Republik Burundi',
    'Belgium'                                              => 'Belgien',
    'Kingdom of Belgium'                                   => 'Königreich Belgien',
    'Benin'                                                => 'Benin',
    'Republic of Benin'                                    => 'Republik Benin',
    'Bonaire, Sint Eustatius and Saba'                     => 'Bonaire, Sint Eustatius und Saba',
    'Burkina Faso'                                         => 'Burkina Faso',
    'Bangladesh'                                           => 'Bangladesch',
    'People\'s Republic of Bangladesh'                     => 'Volksrepublik Bangladesh',
    'Bulgaria'                                             => 'Bulgarien',
    'Republic of Bulgaria'                                 => 'Republik Bulgarien',
    'Bahrain'                                              => 'Bahrain',
    'Kingdom of Bahrain'                                   => 'Königreich Bahrain',
    'Bahamas'                                              => 'Bahamas',
    'Commonwealth of the Bahamas'                          => 'Commonwealth der Bahamas',
    'Bosnia and Herzegovina'                               => 'Bosnien und Herzegowina',
    'Republic of Bosnia and Herzegovina'                   => 'Bosnien und Herzegowina',
    'Saint Barthélemy'                                     => 'Saint-Barthélemy',
    'Belarus'                                              => 'Belarus',
    'Republic of Belarus'                                  => 'Republik Belarus',
    'Belize'                                               => 'Belize',
    'Bermuda'                                              => 'Bermuda',
    'Bolivia, Plurinational State of'                      => 'Bolivien, Plurinationaler Staat',
    'Plurinational State of Bolivia'                       => 'Plurinationaler Staat Bolivien',
    'Bolivia'                                              => 'Bolivien',
    'Brazil'                                               => 'Brasilien',
    'Federative Republic of Brazil'                        => 'Föderative Republik Brasilien',
    'Barbados'                                             => 'Barbados',
    'Brunei Darussalam'                                    => 'Brunei Darussalam',
    'Bhutan'                                               => 'Bhutan',
    'Kingdom of Bhutan'                                    => 'Königreich Bhutan',
    'Bouvet Island'                                        => 'Bouvet-Insel',
    'Botswana'                                             => 'Botsuana',
    'Republic of Botswana'                                 => 'Republik Botsuana',
    'Central African Republic'                             => 'Zentralafrikanische Republik',
    'Canada'                                               => 'Kanada',
    'Cocos (Keeling) Islands'                              => 'Kokos-(Keeling-)Inseln',
    'Switzerland'                                          => 'Schweiz',
    'Swiss Confederation'                                  => 'Schweizerische Eidgenossenschaft',
    'Chile'                                                => 'Chile',
    'Republic of Chile'                                    => 'Republik Chile',
    'China'                                                => 'China',
    'People\'s Republic of China'                          => 'Volksrepublik China',
    'Côte d\'Ivoire'                                       => 'Côte d\'Ivoire',
    'Republic of Côte d\'Ivoire'                           => 'Republik Côte d\'Ivoire',
    'Cameroon'                                             => 'Kamerun',
    'Republic of Cameroon'                                 => 'Republik Kamerun',
    'Congo, The Democratic Republic of the'                => 'Demokratische Republik Kongo',
    'Congo'                                                => 'Kongo',
    'Republic of the Congo'                                => 'Republik Kongo',
    'Cook Islands'                                         => 'Cookinseln',
    'Colombia'                                             => 'Kolumbien',
    'Republic of Colombia'                                 => 'Republik Kolumbien',
    'Comoros'                                              => 'Komoren',
    'Union of the Comoros'                                 => 'Vereinigung der Komoren',
    'Cabo Verde'                                           => 'Kap Verde',
    'Republic of Cabo Verde'                               => 'Republik Kap Verde',
    'Costa Rica'                                           => 'Costa Rica',
    'Republic of Costa Rica'                               => 'Republik Costa Rica',
    'Cuba'                                                 => 'Kuba',
    'Republic of Cuba'                                     => 'Republik Kuba',
    'Curaçao'                                              => 'Curaçao',
    'Christmas Island'                                     => 'Weihnachtsinseln',
    'Cayman Islands'                                       => 'Cayman-Inseln',
    'Cyprus'                                               => 'Zypern',
    'Republic of Cyprus'                                   => 'Republik Zypern',
    'Czechia'                                              => 'Tschechien',
    'Czech Republic'                                       => 'Tschechische Republik',
    'Germany'                                              => 'Deutschland',
    'Federal Republic of Germany'                          => 'Bundesrepublik Deutschland',
    'Djibouti'                                             => 'Dschibuti',
    'Republic of Djibouti'                                 => 'Republik Dschibuti',
    'Dominica'                                             => 'Dominica',
    'Commonwealth of Dominica'                             => 'Commonwealth Dominica',
    'Denmark'                                              => 'Dänemark',
    'Kingdom of Denmark'                                   => 'Königreich Dänemark',
    'Dominican Republic'                                   => 'Dominikanische Republik',
    'Algeria'                                              => 'Algerien',
    'People\'s Democratic Republic of Algeria'             => 'Demokratische Volksrepublik Algerien',
    'Ecuador'                                              => 'Ecuador',
    'Republic of Ecuador'                                  => 'Republik Ecuador',
    'Egypt'                                                => 'Ägypten',
    'Arab Republic of Egypt'                               => 'Arabische Republik Ägypten',
    'Eritrea'                                              => 'Eritrea',
    'the State of Eritrea'                                 => 'Staat Eritrea',
    'Western Sahara'                                       => 'Westsahara',
    'Spain'                                                => 'Spanien',
    'Kingdom of Spain'                                     => 'Königreich Spanien',
    'Estonia'                                              => 'Estland',
    'Republic of Estonia'                                  => 'Republik Estland',
    'Ethiopia'                                             => 'Äthiopien',
    'Federal Democratic Republic of Ethiopia'              => 'Demokratische Bundesrepublik Äthiopien',
    'Finland'                                              => 'Finnland',
    'Republic of Finland'                                  => 'Republik Finnland',
    'Fiji'                                                 => 'Fidschi',
    'Republic of Fiji'                                     => 'Republik Fidschi',
    'Falkland Islands (Malvinas)'                          => 'Falklandinseln (Malwinen)',
    'France'                                               => 'Frankreich',
    'French Republic'                                      => 'Französische Republik',
    'Faroe Islands'                                        => 'Färöer-Inseln',
    'Micronesia, Federated States of'                      => 'Mikronesien, Föderierte Staaten von',
    'Federated States of Micronesia'                       => 'Föderierte Staaten von Mikronesien',
    'Gabon'                                                => 'Gabun',
    'Gabonese Republic'                                    => 'Gabunische Republik',
    'United Kingdom'                                       => 'Vereinigtes Königreich',
    'United Kingdom of Great Britain and Northern Ireland' => 'Vereinigtes Königreich Großbritannien und Nordirland',
    'Georgia'                                              => 'Georgien',
    'Guernsey'                                             => 'Guernsey',
    'Ghana'                                                => 'Ghana',
    'Republic of Ghana'                                    => 'Republik Ghana',
    'Gibraltar'                                            => 'Gibraltar',
    'Guinea'                                               => 'Guinea',
    'Republic of Guinea'                                   => 'Republik Guinea',
    'Guadeloupe'                                           => 'Guadeloupe',
    'Gambia'                                               => 'Gambia',
    'Republic of the Gambia'                               => 'Republik Gambia',
    'Guinea-Bissau'                                        => 'Guinea-Bissau',
    'Republic of Guinea-Bissau'                            => 'Republik Guinea-Bissau',
    'Equatorial Guinea'                                    => 'Äquatorialguinea',
    'Republic of Equatorial Guinea'                        => 'Republik Äquatorialguinea',
    'Greece'                                               => 'Griechenland',
    'Hellenic Republic'                                    => 'Hellenische Republik',
    'Grenada'                                              => 'Grenada',
    'Greenland'                                            => 'Grönland',
    'Guatemala'                                            => 'Guatemala',
    'Republic of Guatemala'                                => 'Republik Guatemala',
    'French Guiana'                                        => 'Französisch-Guyana',
    'Guam'                                                 => 'Guam',
    'Guyana'                                               => 'Guyana',
    'Republic of Guyana'                                   => 'Kooperative Republik Guyana',
    'Hong Kong'                                            => 'Hongkong',
    'Hong Kong Special Administrative Region of China'     => 'Sonderverwaltungsregion Hongkong',
    'Heard Island and McDonald Islands'                    => 'Heard und McDonaldinseln',
    'Honduras'                                             => 'Honduras',
    'Republic of Honduras'                                 => 'Republik Honduras',
    'Croatia'                                              => 'Kroatien',
    'Republic of Croatia'                                  => 'Republik Kroatien',
    'Haiti'                                                => 'Haiti',
    'Republic of Haiti'                                    => 'Republik Haiti',
    'Hungary'                                              => 'Ungarn',
    'Indonesia'                                            => 'Indonesien',
    'Republic of Indonesia'                                => 'Republik Indonesien',
    'Isle of Man'                                          => 'Insel Man',
    'India'                                                => 'Indien',
    'Republic of India'                                    => 'Republik Indien',
    'British Indian Ocean Territory'                       => 'Britisches Territorium im Indischen Ozean',
    'Ireland'                                              => 'Irland',
    'Iran, Islamic Republic of'                            => 'Iran, Islamische Republik',
    'Islamic Republic of Iran'                             => 'Islamische Republik Iran',
    'Iran'                                                 => 'Iran',
    'Iraq'                                                 => 'Irak',
    'Republic of Iraq'                                     => 'Republik Irak',
    'Iceland'                                              => 'Island',
    'Republic of Iceland'                                  => 'Republik Island',
    'Israel'                                               => 'Israel',
    'State of Israel'                                      => 'Staat Israel',
    'Italy'                                                => 'Italien',
    'Italian Republic'                                     => 'Italienische Republik',
    'Jamaica'                                              => 'Jamaika',
    'Jersey'                                               => 'Jersey',
    'Jordan'                                               => 'Jordanien',
    'Hashemite Kingdom of Jordan'                          => 'Haschemitisches Königreich Jordanien',
    'Japan'                                                => 'Japan',
    'Kazakhstan'                                           => 'Kasachstan',
    'Republic of Kazakhstan'                               => 'Republik Kasachstan',
    'Kenya'                                                => 'Kenia',
    'Republic of Kenya'                                    => 'Republik Kenia',
    'Kyrgyzstan'                                           => 'Kirgisistan',
    'Kyrgyz Republic'                                      => 'Kirgisische Republik',
    'Cambodia'                                             => 'Kambodscha',
    'Kingdom of Cambodia'                                  => 'Königreich Kambodscha',
    'Kiribati'                                             => 'Kiribati',
    'Republic of Kiribati'                                 => 'Republik Kiribati',
    'Saint Kitts and Nevis'                                => 'St. Kitts und Nevis',
    'Korea, Republic of'                                   => 'Korea, Republik',
    'South Korea'                                          => 'Südkorea',
    'Kuwait'                                               => 'Kuwait',
    'State of Kuwait'                                      => 'Staat Kuwait',
    'Lao People\'s Democratic Republic'                    => 'Laos, Demokratische Volksrepublik',
    'Laos'                                                 => 'Laos',
    'Lebanon'                                              => 'Libanon',
    'Lebanese Republic'                                    => 'Libanesische Republik',
    'Liberia'                                              => 'Liberia',
    'Republic of Liberia'                                  => 'Republik Liberia',
    'Libya'                                                => 'Libyen',
    'Saint Lucia'                                          => 'St. Lucia',
    'Liechtenstein'                                        => 'Liechtenstein',
    'Principality of Liechtenstein'                        => 'Fürstentum Liechtenstein',
    'Sri Lanka'                                            => 'Sri Lanka',
    'Democratic Socialist Republic of Sri Lanka'           => 'Demokratische sozialistische Republik Sri Lanka',
    'Lesotho'                                              => 'Lesotho',
    'Kingdom of Lesotho'                                   => 'Königreich Lesotho',
    'Lithuania'                                            => 'Litauen',
    'Republic of Lithuania'                                => 'Republik Litauen',
    'Luxembourg'                                           => 'Luxemburg',
    'Grand Duchy of Luxembourg'                            => 'Großherzogtum Luxemburg',
    'Latvia'                                               => 'Lettland',
    'Republic of Latvia'                                   => 'Republik Lettland',
    'Macao'                                                => 'Macao',
    'Macao Special Administrative Region of China'         => 'Sonderverwaltungsregion Macao',
    'Saint Martin (French part)'                           => 'Saint Martin (Französischer Teil)',
    'Morocco'                                              => 'Marokko',
    'Kingdom of Morocco'                                   => 'Königreich Marokko',
    'Monaco'                                               => 'Monaco',
    'Principality of Monaco'                               => 'Fürstentum Monaco',
    'Moldova, Republic of'                                 => 'Moldau, Republik',
    'Republic of Moldova'                                  => 'Republik Moldau',
    'Moldova'                                              => 'Moldau',
    'Madagascar'                                           => 'Madagaskar',
    'Republic of Madagascar'                               => 'Republik Madagaskar',
    'Maldives'                                             => 'Malediven',
    'Republic of Maldives'                                 => 'Republik Malediven',
    'Mexico'                                               => 'Mexiko',
    'United Mexican States'                                => 'Vereinigte Mexikanische Staaten',
    'Marshall Islands'                                     => 'Marshallinseln',
    'Republic of the Marshall Islands'                     => 'Republik Marshallinseln',
    'North Macedonia'                                      => 'Nordmazedonien',
    'Republic of North Macedonia'                          => 'Republik Nordmazedonien',
    'Mali'                                                 => 'Mali',
    'Republic of Mali'                                     => 'Republik Mali',
    'Malta'                                                => 'Malta',
    'Republic of Malta'                                    => 'Republik Malta',
    'Myanmar'                                              => 'Myanmar',
    'Republic of Myanmar'                                  => 'Republik Myanmar',
    'Montenegro'                                           => 'Montenegro',
    'Mongolia'                                             => 'Mongolei',
    'Northern Mariana Islands'                             => 'Nördliche Marianen',
    'Commonwealth of the Northern Mariana Islands'         => 'Commonwealth Nördliche Mariana-Inseln',
    'Mozambique'                                           => 'Mosambik',
    'Republic of Mozambique'                               => 'Republik Mosambik',
    'Mauritania'                                           => 'Mauretanien',
    'Islamic Republic of Mauritania'                       => 'Islamische Republik Mauretanien',
    'Montserrat'                                           => 'Montserrat',
    'Martinique'                                           => 'Martinique',
    'Mauritius'                                            => 'Mauritius',
    'Republic of Mauritius'                                => 'Republik Mauritius',
    'Malawi'                                               => 'Malawi',
    'Republic of Malawi'                                   => 'Republik Malawi',
    'Malaysia'                                             => 'Malaysia',
    'Mayotte'                                              => 'Mayotte',
    'Namibia'                                              => 'Namibia',
    'Republic of Namibia'                                  => 'Republik Namibia',
    'New Caledonia'                                        => 'Neukaledonien',
    'Niger'                                                => 'Niger',
    'Republic of the Niger'                                => 'Republik Niger',
    'Norfolk Island'                                       => 'Norfolkinsel',
    'Nigeria'                                              => 'Nigeria',
    'Federal Republic of Nigeria'                          => 'Bundesrepublik Nigeria',
    'Nicaragua'                                            => 'Nicaragua',
    'Republic of Nicaragua'                                => 'Republik Nicaragua',
    'Niue'                                                 => 'Niue',
    'Netherlands'                                          => 'Niederlande',
    'Kingdom of the Netherlands'                           => 'Königreich der Niederlande',
    'Norway'                                               => 'Norwegen',
    'Kingdom of Norway'                                    => 'Königreich Norwegen',
    'Nepal'                                                => 'Nepal',
    'Federal Democratic Republic of Nepal'                 => 'Demokratische Bundesrepublik Nepal',
    'Nauru'                                                => 'Nauru',
    'Republic of Nauru'                                    => 'Republik Nauru',
    'New Zealand'                                          => 'Neuseeland',
    'Oman'                                                 => 'Oman',
    'Sultanate of Oman'                                    => 'Sultanat Oman',
    'Pakistan'                                             => 'Pakistan',
    'Islamic Republic of Pakistan'                         => 'Islamische Republik Pakistan',
    'Panama'                                               => 'Panama',
    'Republic of Panama'                                   => 'Republik Panama',
    'Pitcairn'                                             => 'Pitcairn',
    'Peru'                                                 => 'Peru',
    'Republic of Peru'                                     => 'Republik Peru',
    'Philippines'                                          => 'Philippinen',
    'Republic of the Philippines'                          => 'Republik der Philippinen',
    'Palau'                                                => 'Palau',
    'Republic of Palau'                                    => 'Republik Palau',
    'Papua New Guinea'                                     => 'Papua-Neuguinea',
    'Independent State of Papua New Guinea'                => 'Unabhängiger Staat Papua-Neuguinea',
    'Poland'                                               => 'Polen',
    'Republic of Poland'                                   => 'Republik Polen',
    'Puerto Rico'                                          => 'Puerto Rico',
    'Korea, Democratic People\'s Republic of'              => 'Korea, Demokratische Volksrepublik',
    'Democratic People\'s Republic of Korea'               => 'Demokratische Volksrepublik Korea',
    'North Korea'                                          => 'Nordkorea',
    'Portugal'                                             => 'Portugal',
    'Portuguese Republic'                                  => 'Portugiesische Republik',
    'Paraguay'                                             => 'Paraguay',
    'Republic of Paraguay'                                 => 'Republik Paraguay',
    'Palestine, State of'                                  => 'Palästina, Staat',
    'the State of Palestine'                               => 'Staat Palästina',
    'French Polynesia'                                     => 'Französisch-Polynesien',
    'Qatar'                                                => 'Katar',
    'State of Qatar'                                       => 'Staat Katar',
    'Réunion'                                              => 'Réunion',
    'Romania'                                              => 'Rumänien',
    'Russian Federation'                                   => 'Russische Föderation',
    'Rwanda'                                               => 'Ruanda',
    'Rwandese Republic'                                    => 'Republik Ruanda',
    'Saudi Arabia'                                         => 'Saudi-Arabien',
    'Kingdom of Saudi Arabia'                              => 'Königreich Saudi-Arabien',
    'Sudan'                                                => 'Sudan',
    'Republic of the Sudan'                                => 'Republik Sudan',
    'Senegal'                                              => 'Senegal',
    'Republic of Senegal'                                  => 'Republik Senegal',
    'Singapore'                                            => 'Singapur',
    'Republic of Singapore'                                => 'Republik Singapur',
    'South Georgia and the South Sandwich Islands'         => 'South Georgia und die Südlichen Sandwichinseln',
    'Saint Helena, Ascension and Tristan da Cunha'         => 'St. Helena, Ascension und Tristan da Cunha',
    'Svalbard and Jan Mayen'                               => 'Svalbard und Jan Mayen',
    'Solomon Islands'                                      => 'Salomoninseln',
    'Sierra Leone'                                         => 'Sierra Leone',
    'Republic of Sierra Leone'                             => 'Republik Sierra Leone',
    'El Salvador'                                          => 'El Salvador',
    'Republic of El Salvador'                              => 'Republik El Salvador',
    'San Marino'                                           => 'San Marino',
    'Republic of San Marino'                               => 'Republik San Marino',
    'Somalia'                                              => 'Somalia',
    'Federal Republic of Somalia'                          => 'Bundesrepublik Somalia',
    'Saint Pierre and Miquelon'                            => 'St. Pierre und Miquelon',
    'Serbia'                                               => 'Serbien',
    'Republic of Serbia'                                   => 'Republik Serbien',
    'South Sudan'                                          => 'Südsudan',
    'Republic of South Sudan'                              => 'Republik Südsudan',
    'Sao Tome and Principe'                                => 'São Tomé und Príncipe',
    'Democratic Republic of Sao Tome and Principe'         => 'Demokratische Republik São Tomé und Príncipe',
    'Suriname'                                             => 'Suriname',
    'Republic of Suriname'                                 => 'Republik Suriname',
    'Slovakia'                                             => 'Slowakei',
    'Slovak Republic'                                      => 'Slowakische Republik',
    'Slovenia'                                             => 'Slowenien',
    'Republic of Slovenia'                                 => 'Republik Slowenien',
    'Sweden'                                               => 'Schweden',
    'Kingdom of Sweden'                                    => 'Königreich Schweden',
    'Eswatini'                                             => 'Eswatini',
    'Kingdom of Eswatini'                                  => 'Königreich Eswatini',
    'Sint Maarten (Dutch part)'                            => 'Saint-Martin (Niederländischer Teil)',
    'Seychelles'                                           => 'Seychellen',
    'Republic of Seychelles'                               => 'Republik Seychellen',
    'Syrian Arab Republic'                                 => 'Syrien, Arabische Republik',
    'Syria'                                                => 'Syrien',
    'Turks and Caicos Islands'                             => 'Turks- und Caicosinseln',
    'Chad'                                                 => 'Tschad',
    'Republic of Chad'                                     => 'Republik Tschad',
    'Togo'                                                 => 'Togo',
    'Togolese Republic'                                    => 'Republik Togo',
    'Thailand'                                             => 'Thailand',
    'Kingdom of Thailand'                                  => 'Königreich Thailand',
    'Tajikistan'                                           => 'Tadschikistan',
    'Republic of Tajikistan'                               => 'Republik Tadschikistan',
    'Tokelau'                                              => 'Tokelau',
    'Turkmenistan'                                         => 'Turkmenistan',
    'Timor-Leste'                                          => 'Timor-Leste',
    'Democratic Republic of Timor-Leste'                   => 'Demokratische Republik Timor-Leste',
    'Tonga'                                                => 'Tonga',
    'Kingdom of Tonga'                                     => 'Königreich Tonga',
    'Trinidad and Tobago'                                  => 'Trinidad und Tobago',
    'Republic of Trinidad and Tobago'                      => 'Republik Trinidad und Tobago',
    'Tunisia'                                              => 'Tunesien',
    'Republic of Tunisia'                                  => 'Tunesische Republik',
    'Türkiye'                                              => 'Türkei',
    'Republic of Türkiye'                                  => 'Republik Türkei',
    'Tuvalu'                                               => 'Tuvalu',
    'Taiwan, Province of China'                            => 'Taiwan, Chinesische Provinz',
    'Taiwan'                                               => 'Taiwan',
    'Tanzania, United Republic of'                         => 'Tansania, Vereinigte Republik',
    'United Republic of Tanzania'                          => 'Vereinigte Republik Tansania',
    'Tanzania'                                             => 'Tansania',
    'Uganda'                                               => 'Uganda',
    'Republic of Uganda'                                   => 'Republik Uganda',
    'Ukraine'                                              => 'Ukraine',
    'United States Minor Outlying Islands'                 => 'United States Minor Outlying Islands',
    'Uruguay'                                              => 'Uruguay',
    'Eastern Republic of Uruguay'                          => 'Republik Östlich des Uruguay',
    'United States'                                        => 'Vereinigte Staaten',
    'United States of America'                             => 'Vereinigte Staaten von Amerika',
    'Uzbekistan'                                           => 'Usbekistan',
    'Republic of Uzbekistan'                               => 'Republik Usbekistan',
    'Holy See (Vatican City State)'                        => 'Heiliger Stuhl (Staat Vatikanstadt)',
    'Saint Vincent and the Grenadines'                     => 'St. Vincent und die Grenadinen',
    'Venezuela, Bolivarian Republic of'                    => 'Venezuela, Bolivarische Republik',
    'Bolivarian Republic of Venezuela'                     => 'Bolivarische Republik Venezuela',
    'Venezuela'                                            => 'Venezuela',
    'Virgin Islands, British'                              => 'Britische Jungferninseln',
    'British Virgin Islands'                               => 'Britische Jungferninseln',
    'Virgin Islands, U.S.'                                 => 'Amerikanische Jungferninseln',
    'Virgin Islands of the United States'                  => 'Amerikanische Jungferninseln',
    'Viet Nam'                                             => 'Vietnam',
    'Socialist Republic of Viet Nam'                       => 'Sozialistische Republik Vietnam',
    'Vietnam'                                              => 'Vietnam',
    'Vanuatu'                                              => 'Vanuatu',
    'Republic of Vanuatu'                                  => 'Republik Vanuatu',
    'Wallis and Futuna'                                    => 'Wallis und Futuna',
    'Samoa'                                                => 'Samoa',
    'Independent State of Samoa'                           => 'Unabhängiger Staat Samoa',
    'Yemen'                                                => 'Jemen',
    'Republic of Yemen'                                    => 'Republik Jemen',
    'South Africa'                                         => 'Südafrika',
    'Republic of South Africa'                             => 'Republik Südafrika',
    'Zambia'                                               => 'Sambia',
    'Republic of Zambia'                                   => 'Republik Sambia',
    'Zimbabwe'                                             => 'Simbabwe',
    'Republic of Zimbabwe'                                 => 'Republik Simbabwe',
];
