<?php

/**
 * Translations in Central Khmer.
 *
 * @noinspection SpellCheckingInspection
 */

return [
    'Aruba'                                                => 'អារូបា',
    'Afghanistan'                                          => 'អាហ្វហ្កានីស្ថាន',
    'Islamic Republic of Afghanistan'                      => 'សាធារណរដ្ឋអ៊ីស្លាមអាហ្វហ្កានីស្ថាន',
    'Angola'                                               => 'អង់ហ្គោឡា',
    'Republic of Angola'                                   => 'សាធារណ​រដ្ឋ​អង់ហ្គោឡា',
    'Anguilla'                                             => 'អង់ហ្កីឡា',
    'Åland Islands'                                        => 'កោះ​អាល់ឡង់',
    'Albania'                                              => 'អាល់បានី',
    'Republic of Albania'                                  => 'សាធារណ​រដ្ឋ​អាល់បានី',
    'Andorra'                                              => 'អង់ដូរ៉ា',
    'Principality of Andorra'                              => 'ក្សត្របុរីអង់ដូរ៉ា',
    'United Arab Emirates'                                 => 'អេមីរ៉ាតអារ៉ាប់រួម',
    'Argentina'                                            => 'អាហ្សង់ទីន',
    'Argentine Republic'                                   => 'សាធារណរដ្ឋ​អាហ្សង់ទីន',
    'Armenia'                                              => 'អាមេនី',
    'Republic of Armenia'                                  => 'សាធារណរដ្ឋ​អាមេនី',
    'American Samoa'                                       => 'សាម័រអាមេរិក',
    'Antarctica'                                           => 'អង់តាក់ទិច',
    'French Southern Territories'                          => 'ដែនដីភាគខាងត្បូងបារាំង',
    'Antigua and Barbuda'                                  => 'អង់ទីហ្កា និង បាប៊ុយដា',
    'Australia'                                            => 'អូស្ត្រាលី',
    'Austria'                                              => 'អូទ្រីស',
    'Republic of Austria'                                  => 'សាធារណរដ្ឋ​អូទ្រីស',
    'Azerbaijan'                                           => 'អាស៊ែបៃហ្សង់',
    'Republic of Azerbaijan'                               => 'សាសាធារណរដ្ឋអាស៊ែបៃហ្សង់',
    'Burundi'                                              => 'ប៊ូរុនឌី',
    'Republic of Burundi'                                  => 'សាធារណរដ្ឋ​ប៊ូរុនឌី',
    'Belgium'                                              => 'បែល​ហ្ស៊ិក',
    'Kingdom of Belgium'                                   => 'ព្រះរាជាណាចក្រ​បែល​ហ្ស៊ិក',
    'Benin'                                                => 'បេណាំង',
    'Republic of Benin'                                    => 'សាធារណរដ្ឋ​បេណាំង',
    'Bonaire, Sint Eustatius and Saba'                     => 'បូស្នី ស៊ីន អ៊ីស្តាំធូស និង​សាបា',
    'Burkina Faso'                                         => 'បួគីណាហ្វាសូ',
    'Bangladesh'                                           => 'បង់ក្លាដែស',
    'People\'s Republic of Bangladesh'                     => 'សាធារណរដ្ឋប្រជាមានិតបង់ក្លាដែស',
    'Bulgaria'                                             => 'ប៊ុលហ្ការី',
    'Republic of Bulgaria'                                 => 'សាធារណរដ្ឋប៊ុលហ្ការី',
    'Bahrain'                                              => 'បារ៉ែន',
    'Kingdom of Bahrain'                                   => 'ព្រះរាជាណាចក្រ​​បារ៉ែន',
    'Bahamas'                                              => 'បាហាម៉ា',
    'Commonwealth of the Bahamas'                          => 'សហសុខុមាលភាពបាហាម៉ា',
    'Bosnia and Herzegovina'                               => 'បូស្ន៊ី និង ហឺហ្ស៊េហ្កោវីណា',
    'Republic of Bosnia and Herzegovina'                   => 'សាធារណ​រដ្ឋ​បូស្ន៊ី និង ​ហឺហ្ស៊េហ្កោវីណា',
    'Saint Barthélemy'                                     => 'សាំងបាថូឡូមី',
    'Belarus'                                              => 'បេឡារុស',
    'Republic of Belarus'                                  => 'សាធារណរដ្ឋ​​បេឡារុស',
    'Belize'                                               => 'បេលី',
    'Bermuda'                                              => 'ប៊ែម៊ុដា',
    'Bolivia, Plurinational State of'                      => 'បូលីវី (រដ្ឋពហុជាតិ)',
    'Plurinational State of Bolivia'                       => 'រដ្ឋពហុជាតិបូលីវី',
    'Bolivia'                                              => 'បូលីវី',
    'Brazil'                                               => 'ប្រេស៊ីល',
    'Federative Republic of Brazil'                        => 'សាធារណរដ្ឋសហព័ន្ធប្រេស៊ីល',
    'Barbados'                                             => 'បាបាដុស',
    'Brunei Darussalam'                                    => 'ព្រុយណេដារ៉ូសាឡឹម',
    'Bhutan'                                               => 'ប៊ូតង់',
    'Kingdom of Bhutan'                                    => 'ព្រះរាជាណាចក្រ​ប៊ូតង់',
    'Bouvet Island'                                        => 'កោះ​ប៊ូវ៉ែត',
    'Botswana'                                             => 'បុតស្វាណា',
    'Republic of Botswana'                                 => 'សាធារណ​រដ្ឋ​បុតស្វាណា',
    'Central African Republic'                             => 'សាធារណរដ្ឋ​អាហ្វ្រិក​កណ្ដាល',
    'Canada'                                               => 'កាណាដា',
    'Cocos (Keeling) Islands'                              => 'កោះ​កូកូ (ឃីលីង)',
    'Switzerland'                                          => 'ស្វ៊ីស',
    'Swiss Confederation'                                  => 'សហព័ន្ធស្វ៊ីស',
    'Chile'                                                => 'ស៊ីលី',
    'Republic of Chile'                                    => 'សាធារណរដ្ឋ​ស៊ី​លី',
    'China'                                                => 'ចិន',
    'People\'s Republic of China'                          => 'សាធារណរដ្ឋ​ប្រជាមានិត​ចិន',
    'Côte d\'Ivoire'                                       => 'កូតឌីវ័រ',
    'Republic of Côte d\'Ivoire'                           => 'សាធារណរដ្ឋកូតឌីវ័រ',
    'Cameroon'                                             => 'កាមេរូន',
    'Republic of Cameroon'                                 => 'សាធារណរដ្ឋ​កាមេរូន',
    'Congo, The Democratic Republic of the'                => 'កុងហ្គោ (សាធារណរដ្ឋប្រជាធិបតេយ្យ)',
    'Congo'                                                => 'កុងហ្គោ',
    'Republic of the Congo'                                => 'សាធារណរដ្ឋ​កុងហ្គោ',
    'Cook Islands'                                         => 'កោះកូក',
    'Colombia'                                             => 'កូឡុំប៊ី',
    'Republic of Colombia'                                 => 'សាធារណ​រដ្ឋ​កូឡុំប៊ី',
    'Comoros'                                              => 'កូម័រ',
    'Union of the Comoros'                                 => 'សហភាព​​កូម័រ',
    'Cabo Verde'                                           => 'កាប់វែរ',
    'Republic of Cabo Verde'                               => 'សាធារណរដ្ឋ​កាប់វែរ',
    'Costa Rica'                                           => 'កូស្តារីកា',
    'Republic of Costa Rica'                               => 'សាធារណរដ្ឋ​កូស្តារីកា',
    'Cuba'                                                 => 'គុយបា',
    'Republic of Cuba'                                     => 'សាធារណរដ្ឋ​គុយបា',
    'Curaçao'                                              => 'កួរ៉ាកាវ',
    'Christmas Island'                                     => 'កោះ​គ្រីស្តម៉ាស',
    'Cayman Islands'                                       => 'កោះ​កៃម៉ង់',
    'Cyprus'                                               => 'ស៊ីប',
    'Republic of Cyprus'                                   => 'សាធារណរដ្ឋ​ស៊ីប',
    'Czechia'                                              => 'ឆែក',
    'Czech Republic'                                       => 'សាធារណរដ្ឋ​ឆែក',
    'Germany'                                              => 'អាល្លឺម៉ង់',
    'Federal Republic of Germany'                          => 'សាធារណរដ្ឋសហព័ន្ធអាល្លឺម៉ង់',
    'Djibouti'                                             => 'ជីប៊ូទី',
    'Republic of Djibouti'                                 => 'សាធារណរដ្ឋ​ជីប៊ូទី',
    'Dominica'                                             => 'ដូមីនីក',
    'Commonwealth of Dominica'                             => 'សហសុខុមាលភាព​ដូមីនីក',
    'Denmark'                                              => 'ដាណឺម៉ាក',
    'Kingdom of Denmark'                                   => 'ព្រះរាជាណាចក្រ​ដាណឺម៉ាក',
    'Dominican Republic'                                   => 'សាធារណរដ្ឋ​ដូមីនីកែន',
    'Algeria'                                              => 'អាល់ហ្សេរី',
    'People\'s Democratic Republic of Algeria'             => 'សាធារណរដ្ឋប្រជាធិបតេយ្យប្រជាមានិតអាល់ហ្សេរី',
    'Ecuador'                                              => 'អេក្វាទ័រ',
    'Republic of Ecuador'                                  => 'សាធារណរដ្ឋអេក្វាទ័រ',
    'Egypt'                                                => 'អេហ្ស៊ីប',
    'Arab Republic of Egypt'                               => 'សាធារណរដ្ឋអារ៉ាប់អេហ្ស៊ីប',
    'Eritrea'                                              => 'អេរីត្រេ',
    'the State of Eritrea'                                 => 'រដ្ឋអេរីត្រេ',
    'Western Sahara'                                       => 'សាហារ៉ា​ខាងលិច',
    'Spain'                                                => 'អេស្ប៉ាញ',
    'Kingdom of Spain'                                     => 'ព្រះរាជាណាចក្រ​អេស្ប៉ាញ',
    'Estonia'                                              => 'អេស្តូនី',
    'Republic of Estonia'                                  => 'សាធារណរដ្ឋ​អេស្តូនី',
    'Ethiopia'                                             => 'អេត្យូពី',
    'Federal Democratic Republic of Ethiopia'              => 'សាធារណរដ្ឋ​ប្រជាធិបតេយ្យ​សហព័ន្ធ​អេត្យូពី',
    'Finland'                                              => 'ហ្វាំងឡង់',
    'Republic of Finland'                                  => 'សាធារណរដ្ឋ​ហ្វាំងឡង់​',
    'Fiji'                                                 => 'ហ្វ៊ីជី',
    'Republic of Fiji'                                     => 'សាធារណរដ្ឋ​​ហ្វ៊ីជី',
    'Falkland Islands (Malvinas)'                          => 'កោះ​ហ្វ៉កឡង់ (ម៉ាល់វីណា)',
    'France'                                               => 'បារាំង',
    'French Republic'                                      => 'សាធារណរដ្ឋ​បារាំង​',
    'Faroe Islands'                                        => 'កោះ​ហ្វារ៉ូ',
    'Micronesia, Federated States of'                      => 'មីក្រូណេស៊ី (រដ្ឋសហព័ន្ធ)',
    'Federated States of Micronesia'                       => 'រដ្ឋសហព័ន្ធមីក្រូណេស៊ី',
    'Gabon'                                                => 'ហ្គាបុង',
    'Gabonese Republic'                                    => 'សាធារណរដ្ឋ​ហ្គាបុង',
    'United Kingdom'                                       => 'សហរាជាណាចក្រ',
    'United Kingdom of Great Britain and Northern Ireland' => 'សហរាជាណាចក្រអង់គ្លេស ​និងអៀរឡង់​ខាងជើង​',
    'Georgia'                                              => 'ហ្ស៊កហ្ស៊ី',
    'Guernsey'                                             => 'ហ្គេនស៊ី',
    'Ghana'                                                => 'ហ្កាណា',
    'Republic of Ghana'                                    => 'សាធារណរដ្ឋ​ហ្កាណា',
    'Gibraltar'                                            => 'ហ្ស៊ីប្រាល់តា',
    'Guinea'                                               => 'ហ្គីណេ',
    'Republic of Guinea'                                   => 'សាធារណរដ្ឋហ្គីណេ',
    'Guadeloupe'                                           => 'ក្វាដឡូប',
    'Gambia'                                               => 'ហ្កំប៊ី',
    'Republic of the Gambia'                               => 'សាធារណរដ្ឋហ្កំប៊ី',
    'Guinea-Bissau'                                        => 'ហ្គីណេប៊ីស្សូ',
    'Republic of Guinea-Bissau'                            => 'សាធារណរដ្ឋ​ហ្គីណេប៊ីស្សូ',
    'Equatorial Guinea'                                    => 'ហ្គីណេ​អេក្វាទ័រ',
    'Republic of Equatorial Guinea'                        => 'សាធារណរដ្ឋ​ហ្គីណេ​អេក្វាទ័រ',
    'Greece'                                               => 'ក្រិក',
    'Hellenic Republic'                                    => 'សាធារណរដ្ឋ​ក្រិក',
    'Grenada'                                              => 'ហ្គ្រើណាដ',
    'Greenland'                                            => 'ហ្គ្រោអង់ឡង់',
    'Guatemala'                                            => 'ក្វាតេម៉ាឡា',
    'Republic of Guatemala'                                => 'សាធារណរដ្ឋក្វាតេម៉ាឡា',
    'French Guiana'                                        => 'ហ្កីណាបារាំង',
    'Guam'                                                 => 'ហ្កាំ',
    'Guyana'                                               => 'ហ្គីយ៉ាន',
    'Republic of Guyana'                                   => 'សាធារណរដ្ឋហ្គីយ៉ាន',
    'Hong Kong'                                            => 'ហុងកុង',
    'Hong Kong Special Administrative Region of China'     => 'តំបន់រដ្ឋបាលពិសេសហុងកុងនៃប្រទេសចិន',
    'Heard Island and McDonald Islands'                    => 'កោះហឺដ និងកោះមេកដូណាល់',
    'Honduras'                                             => 'ហុងឌូរ៉ាស',
    'Republic of Honduras'                                 => 'សាធារណរដ្ឋ​ហុងឌូរ៉ាស',
    'Croatia'                                              => 'ក្រូអាស៊ី',
    'Republic of Croatia'                                  => 'សាធារណរដ្ឋក្រូអាស៊ី',
    'Haiti'                                                => 'ហៃទី',
    'Republic of Haiti'                                    => 'សាធារណរដ្ឋ​ហៃទី​',
    'Hungary'                                              => 'ហុងគ្រី',
    'Indonesia'                                            => 'ឥណ្ឌូណេស៊ី',
    'Republic of Indonesia'                                => 'សាធារណរដ្ឋឥណ្ឌូណេស៊ី',
    'Isle of Man'                                          => 'កោះម៉ង់',
    'India'                                                => 'ឥណ្ឌា',
    'Republic of India'                                    => 'សាធារណរដ្ឋ​ឥណ្ឌា​',
    'British Indian Ocean Territory'                       => 'ដែនដីអង់គ្លេសឯមហាសមុទ្រឥណ្ឌា',
    'Ireland'                                              => 'អៀរឡង់',
    'Iran, Islamic Republic of'                            => '​អ៊ីរ៉ង់ (សាធារណ​រដ្ឋ​អ៊ីស្លាម​)',
    'Islamic Republic of Iran'                             => 'សាធារណ​រដ្ឋ​អ៊ីស្លាម​អ៊ីរ៉ង់',
    'Iran'                                                 => 'អ៊ីរ៉ង់',
    'Iraq'                                                 => 'អ៊ីរ៉ាក់',
    'Republic of Iraq'                                     => 'សាធារណរដ្ឋ​អ៊ីរ៉ាក់​',
    'Iceland'                                              => 'អ៊ីស្លង់',
    'Republic of Iceland'                                  => 'សាធារណរដ្ឋ​អ៊ីស្លង់',
    'Israel'                                               => 'អ៊ីស្រាអែល',
    'State of Israel'                                      => 'រដ្ឋអ៊ីស្រាអែល',
    'Italy'                                                => 'អ៊ីតាលី',
    'Italian Republic'                                     => 'សាធារណ​រដ្ឋ​អ៊ីតាលី',
    'Jamaica'                                              => 'ហ្សាម៉ាអ៊ីក',
    'Jersey'                                               => 'ហ្ស៊េស៊ី',
    'Jordan'                                               => 'ហ្ស៊កដានី',
    'Hashemite Kingdom of Jordan'                          => 'ព្រះរាជាណាចក្រហ្ស៊កដានី',
    'Japan'                                                => 'ជប៉ុន',
    'Kazakhstan'                                           => 'កាហ្សាក់ស្ថាន',
    'Republic of Kazakhstan'                               => 'សាធារណរដ្ឋកាហ្សាក់ស្ថាន',
    'Kenya'                                                => 'កេនយ៉ា',
    'Republic of Kenya'                                    => 'សាធារណរដ្ឋ​កេនយ៉ា',
    'Kyrgyzstan'                                           => 'កៀហ្ស៊ីស៊ីស្ថាន',
    'Kyrgyz Republic'                                      => 'សាធារណរដ្ឋកៀហ្ស៊ីស៊ីស្ថាន',
    'Cambodia'                                             => 'កម្ពុជា',
    'Kingdom of Cambodia'                                  => 'ព្រះរាជាណាចក្រ​កម្ពុជា',
    'Kiribati'                                             => 'គិរីបាទី',
    'Republic of Kiribati'                                 => 'សាធារណរដ្ឋគិរីបាទី',
    'Saint Kitts and Nevis'                                => 'សាំងគ្រីស្តូហ្វ និងនេវីស',
    'Korea, Republic of'                                   => 'កូរ៉េ (សាធារណរដ្ឋ)',
    'South Korea'                                          => 'កូរ៉េខាងត្បូង',
    'Kuwait'                                               => 'គូវ៉ែត',
    'State of Kuwait'                                      => 'រដ្ឋគូវ៉ែត',
    'Lao People\'s Democratic Republic'                    => 'សាធារណ​រដ្ឋ​ប្រជាធិបតេយ្យ​​ប្រជាមានិត​ឡាវ',
    'Laos'                                                 => 'ឡាវ',
    'Lebanon'                                              => 'លីបង់',
    'Lebanese Republic'                                    => 'សាធារណរដ្ឋ​លីបង់',
    'Liberia'                                              => 'លីបេរីយ៉ា',
    'Republic of Liberia'                                  => 'សាធារណរដ្ឋ​លីបេរីយ៉ា',
    'Libya'                                                => 'លីប៊ី',
    'Saint Lucia'                                          => 'សាំងលូស៊ី',
    'Liechtenstein'                                        => 'លិចតិនស្តាញ',
    'Principality of Liechtenstein'                        => 'ក្សត្របុរីលិចតិនស្តាញ',
    'Sri Lanka'                                            => 'ស្រីលង្កា',
    'Democratic Socialist Republic of Sri Lanka'           => 'សាធារណរដ្ឋសង្គមនិយមប្រជាធិបតេយ្យស្រីលង្កា',
    'Lesotho'                                              => 'ឡេសូតូ',
    'Kingdom of Lesotho'                                   => 'ព្រះរាជាណាចក្រ​ឡេសូតូ',
    'Lithuania'                                            => 'លីទុយអានី',
    'Republic of Lithuania'                                => 'សាធារណរដ្ឋ​លីទុយអានី',
    'Luxembourg'                                           => 'លុចសំបួ',
    'Grand Duchy of Luxembourg'                            => 'ចក្រពត្តិលុចសំបួ',
    'Latvia'                                               => 'ឡេតូនី',
    'Republic of Latvia'                                   => 'សាធារណរដ្ឋឡេតូនី',
    'Macao'                                                => 'ម៉ាកាវ',
    'Macao Special Administrative Region of China'         => 'តំបន់រដ្ឋបាលពិសេសម៉ាកាវនៃប្រទេសចិន',
    'Saint Martin (French part)'                           => 'សាំងម៉ាតាន់ (ប៉ែកបារាំង)',
    'Morocco'                                              => 'ម៉ារ៉ុក',
    'Kingdom of Morocco'                                   => 'ព្រះរាជាណាចក្រ​ម៉ារ៉ុក',
    'Monaco'                                               => 'ម៉ូណាកូ',
    'Principality of Monaco'                               => 'ក្សត្របុរីម៉ូណាកូ',
    'Moldova, Republic of'                                 => 'ម៉ុលដាវី (សាធារណរដ្ឋ)',
    'Republic of Moldova'                                  => 'សាធារណរដ្ឋម៉ុលដាវី',
    'Moldova'                                              => 'ម៉ុលដាវី',
    'Madagascar'                                           => 'ម៉ាដាហ្កាស្កា',
    'Republic of Madagascar'                               => 'សាធារណរដ្ឋម៉ាដាហ្កាស្កា',
    'Maldives'                                             => 'ម៉ាល់ឌីវ',
    'Republic of Maldives'                                 => 'សាធារណរដ្ឋ​ម៉ាល់ឌីវ',
    'Mexico'                                               => 'មុិកស៊ិក',
    'United Mexican States'                                => 'រដ្ឋសហមុិកស៊ិក',
    'Marshall Islands'                                     => 'កោះម៉ាសាល់',
    'Republic of the Marshall Islands'                     => 'សាធារណរដ្ឋកោះម៉ាសាល់',
    'North Macedonia'                                      => 'ម៉ាសេដ្វានខាងជើង',
    'Republic of North Macedonia'                          => 'សាធារណរដ្ឋម៉ាសេដ្វានខាងជើង',
    'Mali'                                                 => 'ម៉ាលី',
    'Republic of Mali'                                     => 'សាធារណរដ្ឋ​ម៉ាលី',
    'Malta'                                                => 'ម៉ាល់ត៍',
    'Republic of Malta'                                    => 'សាធារណរដ្ឋម៉ាល់ត៍',
    'Myanmar'                                              => 'មីយ៉ាន់ម៉ា',
    'Republic of Myanmar'                                  => 'សាធារណរដ្ឋ​មីយ៉ាន់ម៉ា',
    'Montenegro'                                           => 'ម៉ុងតេណេហ្គ្រោ',
    'Mongolia'                                             => 'ម៉ុងហ្គោលី',
    'Northern Mariana Islands'                             => 'កោះ​ម៉ារៀណា​ភាគ​ខាង​ជើង',
    'Commonwealth of the Northern Mariana Islands'         => 'សហសុខុមាលភាព​កោះ​ម៉ារៀណា​ភាគ​ខាង​ជើង',
    'Mozambique'                                           => 'ម៉ូសំប៊ិក',
    'Republic of Mozambique'                               => 'សាធារណរដ្ឋម៉ូសំប៊ិក',
    'Mauritania'                                           => 'ម៉ូរីតានី',
    'Islamic Republic of Mauritania'                       => 'សាធារណ​រដ្ឋ​អ៊ីស្លាម​ម៉ូរីតានី​',
    'Montserrat'                                           => 'ម៉ុងស៊ែរ៉ា',
    'Martinique'                                           => 'ម៉ាសជីនិក',
    'Mauritius'                                            => 'ម៉ូរីស',
    'Republic of Mauritius'                                => 'សាធារណរដ្ឋម៉ូរីស',
    'Malawi'                                               => 'ម៉ាឡាវី',
    'Republic of Malawi'                                   => 'សាធារណរដ្ឋ​ម៉ាឡាវី​',
    'Malaysia'                                             => 'ម៉ាឡេស៊ី',
    'Mayotte'                                              => 'ម៉ាយុត្តិ',
    'Namibia'                                              => 'ណាមីប៊ី',
    'Republic of Namibia'                                  => 'សាធារណរដ្ឋ​ណាមីប៊ី',
    'New Caledonia'                                        => 'នូវែលកាលេដូនី',
    'Niger'                                                => 'នីហ្សេ',
    'Republic of the Niger'                                => 'សាធារណរដ្ឋនីហ្សេ',
    'Norfolk Island'                                       => 'កោះន័រហ្វូក',
    'Nigeria'                                              => 'នីហ្សេរីយ៉ា',
    'Federal Republic of Nigeria'                          => 'សាធារណរដ្ឋសហព័ន្ធនីហ្សេរីយ៉ា',
    'Nicaragua'                                            => 'នីការ៉ាហ្កា',
    'Republic of Nicaragua'                                => 'សាធារណរដ្ឋ​នីការ៉ាហ្កា',
    'Niue'                                                 => 'នីវេ',
    'Netherlands'                                          => 'ហូឡង់',
    'Kingdom of the Netherlands'                           => 'ព្រះរាជាណាចក្រហូឡង់',
    'Norway'                                               => 'ន័រវែស',
    'Kingdom of Norway'                                    => 'ព្រះរាជាណាចក្រ​ន័រវែស',
    'Nepal'                                                => 'នេប៉ាល់',
    'Federal Democratic Republic of Nepal'                 => 'សាធារណរដ្ឋប្រជាធិបតេយ្យសហព័ន្ធនេប៉ាល់',
    'Nauru'                                                => 'ណូរូ',
    'Republic of Nauru'                                    => 'សាធារណរដ្ឋណូរូ',
    'New Zealand'                                          => 'នូវែលសេឡង់',
    'Oman'                                                 => 'អូម៉ង់',
    'Sultanate of Oman'                                    => 'រាជាណាចក្រស៊ុលតង់អូម៉ង់',
    'Pakistan'                                             => 'ប៉ាគីស្ថាន',
    'Islamic Republic of Pakistan'                         => 'សាធារណ​រដ្ឋ​អ៊ីស្លាម​ប៉ាគីស្ថាន',
    'Panama'                                               => 'ប៉ាណាម៉ា',
    'Republic of Panama'                                   => 'សាធារណរដ្ឋ​ប៉ាណាម៉ា',
    'Pitcairn'                                             => 'ពិតគៀរ',
    'Peru'                                                 => 'ប៉េរូ',
    'Republic of Peru'                                     => 'សាធារណរដ្ឋ​ប៉េរូ',
    'Philippines'                                          => 'ហ្វ៊ីលីពីន',
    'Republic of the Philippines'                          => 'សាធារណរដ្ឋហ្វ៊ីលីពីន',
    'Palau'                                                => 'ប៉ាឡូ',
    'Republic of Palau'                                    => 'សាធារណរដ្ឋ​ប៉ាឡូ',
    'Papua New Guinea'                                     => 'ប៉ាពូអាស៊ីនូវែលហ្គីណេ',
    'Independent State of Papua New Guinea'                => 'រដ្ឋឯករាជ្យប៉ាពូអាស៊ីនូវែលហ្គីណេ',
    'Poland'                                               => 'ប៉ូឡូញ',
    'Republic of Poland'                                   => 'សាធារណរដ្ឋ​ប៉ូឡូញ',
    'Puerto Rico'                                          => 'ព័រតូរីកូ',
    'Korea, Democratic People\'s Republic of'              => 'កូរ៉េ (សាធារណ​រដ្ឋ​ប្រជាមានិត​ប្រជាធិបតេយ្យ​​)',
    'Democratic People\'s Republic of Korea'               => 'សាធារណ​រដ្ឋ​ប្រជាមានិត​ប្រជាធិបតេយ្យ​កូរ៉េ',
    'North Korea'                                          => 'កូរ៉េខាងជើង',
    'Portugal'                                             => 'ព័រទុយហ្កាល់',
    'Portuguese Republic'                                  => 'សាធារណរដ្ឋព័រទុយហ្កាល់',
    'Paraguay'                                             => 'ប៉ារ៉ាហ្កាយ',
    'Republic of Paraguay'                                 => 'សាធារណរដ្ឋប៉ារ៉ាហ្កាយ',
    'Palestine, State of'                                  => 'ប៉ាឡេស្ទីន (រដ្ឋ)',
    'the State of Palestine'                               => 'រដ្ឋ​ប៉ាឡេស្ទីន',
    'French Polynesia'                                     => 'ប៉ូលីណេស៊ី​បារាំង',
    'Qatar'                                                => 'កាតា',
    'State of Qatar'                                       => 'រដ្ឋ​កាតា',
    'Réunion'                                              => 'រ៉េអ៊ីនញ៊ុង',
    'Romania'                                              => 'រូម៉ានី',
    'Russian Federation'                                   => 'សហព័ន្ធ​រុស្ស៊ី',
    'Rwanda'                                               => 'រវ៉ាន់ដា',
    'Rwandese Republic'                                    => 'សាធារណរដ្ឋ​រវ៉ាន់ដា',
    'Saudi Arabia'                                         => 'អារ៉ាប៊ីសាអូឌីត',
    'Kingdom of Saudi Arabia'                              => 'ព្រះរាជាណាចក្រ​អារ៉ាប៊ីសាអូឌីត',
    'Sudan'                                                => 'ស៊ូដង់',
    'Republic of the Sudan'                                => 'សាធារណរដ្ឋ​ស៊ូដង់',
    'Senegal'                                              => 'សេណេហ្កាល់',
    'Republic of Senegal'                                  => 'សាធារណរដ្ឋ​សេណេហ្កាល់',
    'Singapore'                                            => 'សិង្ហបុរី',
    'Republic of Singapore'                                => 'សាធារណរដ្ឋសិង្ហបុរី',
    'South Georgia and the South Sandwich Islands'         => 'ប្រជុំកោះហ្ស៊កហ្ស៊ី​ខាង​ត្បូង និង សាន់វិច​ខាង​ត្បូង',
    'Saint Helena, Ascension and Tristan da Cunha'         => 'សាំងហេឡិន, អាសង់ស្យុង និងទ្រីស្ថានដឹគូនៀ',
    'Svalbard and Jan Mayen'                               => 'ស្វាលបាដ និង ហ្សង់ម៉ាយេន',
    'Solomon Islands'                                      => 'កោះ​សូឡូម៉ុង',
    'Sierra Leone'                                         => 'សៀរ៉ាឡេអូន',
    'Republic of Sierra Leone'                             => 'សាធារណរដ្ឋសៀរ៉ាឡេអូន',
    'El Salvador'                                          => 'អែលសាល់វ៉ាឌ័រ',
    'Republic of El Salvador'                              => 'សាធារណរដ្ឋ​អែលសាល់វ៉ាឌ័រ',
    'San Marino'                                           => 'សាំងម៉ារ៊ាំង',
    'Republic of San Marino'                               => 'សាធារណរដ្ឋសាំងម៉ារ៊ាំង',
    'Somalia'                                              => 'សូម៉ាលី',
    'Federal Republic of Somalia'                          => 'សាធារណរដ្ឋសហព័ន្ធសូម៉ាលី',
    'Saint Pierre and Miquelon'                            => 'សាំងព្យែរ និង ម៊ាំងឡង់',
    'Serbia'                                               => 'ស៊ែប៊ី',
    'Republic of Serbia'                                   => 'សាធារណរដ្ឋស៊ែប៊ី',
    'South Sudan'                                          => 'ស៊ូដង់​ខាង​ត្បូង',
    'Republic of South Sudan'                              => 'សាធារណរដ្ឋ​ស៊ូដង់​ខាង​ត្បូង',
    'Sao Tome and Principe'                                => 'សៅតូម៉េ និង ប្រាំងស៊ីប',
    'Democratic Republic of Sao Tome and Principe'         => 'សាធារណរដ្ឋប្រជាធិបតេយ្យសៅតូម៉េ និង ប្រាំងស៊ីប',
    'Suriname'                                             => 'សូរីណាម',
    'Republic of Suriname'                                 => 'សាធារណរដ្ឋសូរីណាម',
    'Slovakia'                                             => 'ស្លូវ៉ាគី',
    'Slovak Republic'                                      => 'សាធារណរដ្ឋ​ស្លូវ៉ាគី',
    'Slovenia'                                             => 'ស្លូវេនី',
    'Republic of Slovenia'                                 => 'សាធារណរដ្ឋស្លូវេនី',
    'Sweden'                                               => 'ស៊ុយអែត',
    'Kingdom of Sweden'                                    => 'ព្រះរាជាណាចក្រ​ស៊ុយអែត',
    'Eswatini'                                             => 'ស្វាស៊ីឡង់',
    'Kingdom of Eswatini'                                  => 'ព្រះរាជាណាចក្រស្វាស៊ីឡង់',
    'Sint Maarten (Dutch part)'                            => 'សាំងម៉ាតាន់ (ប៉ែកហូឡង់)',
    'Seychelles'                                           => 'សីស្ហែល',
    'Republic of Seychelles'                               => 'សាធារណរដ្ឋ​សីស្ហែល',
    'Syrian Arab Republic'                                 => 'សាធារណរដ្ឋអារ៉ាប់ស៊ីរី',
    'Syria'                                                => 'ស៊ីរី',
    'Turks and Caicos Islands'                             => 'កោះតួក និងគែគី',
    'Chad'                                                 => 'ឆាដ',
    'Republic of Chad'                                     => 'សាធារណរដ្ឋ​ឆាដ',
    'Togo'                                                 => 'តូហ្គោ',
    'Togolese Republic'                                    => 'សាធារណ​រដ្ឋ​តូហ្គោ',
    'Thailand'                                             => 'ថៃ',
    'Kingdom of Thailand'                                  => 'ព្រះរាជាណាចក្រ​ថៃ',
    'Tajikistan'                                           => 'តាហ្ស៊ីគីស្ថាន',
    'Republic of Tajikistan'                               => 'សាធារណរដ្ឋតាហ្ស៊ីគីស្ថាន',
    'Tokelau'                                              => 'តួគីឡាអ៊ូ',
    'Turkmenistan'                                         => 'តួកមេនីស្ថាន',
    'Timor-Leste'                                          => 'ទីម័រខាងកើត',
    'Democratic Republic of Timor-Leste'                   => 'សាធារណរដ្ឋ​ប្រជាធិបតេយ្យ​​ទីម័រខាងកើត',
    'Tonga'                                                => 'តុងហ្កា',
    'Kingdom of Tonga'                                     => 'ព្រះរាជាណាចក្រ​តុងហ្កា',
    'Trinidad and Tobago'                                  => 'ទ្រីនីដាដ និង​ តូបាហ្គោ',
    'Republic of Trinidad and Tobago'                      => 'សាធារណរដ្ឋ​ទ្រីនីដាដ និង​ តូបាហ្គោ',
    'Tunisia'                                              => 'ទុយនីស៊ី',
    'Republic of Tunisia'                                  => 'សាធារណរដ្ឋ​ទុយនេស៊ី',
    'Türkiye'                                              => 'តួកគីយេ',
    'Republic of Türkiye'                                  => 'សាធារណរដ្ឋតួកគីយេ',
    'Tuvalu'                                               => 'ទូវ៉ាលូ',
    'Taiwan, Province of China'                            => 'តៃវ៉ាន់​ (ខេត្តនៃចិន)',
    'Taiwan'                                               => 'តៃវ៉ាន់',
    'Tanzania, United Republic of'                         => 'តង់សានី (សាធារណរដ្ឋរួម)',
    'United Republic of Tanzania'                          => 'សាធារណ​រដ្ឋ​រួម​តង់សានី​',
    'Tanzania'                                             => 'តង់សានី',
    'Uganda'                                               => 'អ៊ូហ្កង់ដា',
    'Republic of Uganda'                                   => 'សាធារណរដ្ឋអ៊ូហ្កង់ដា',
    'Ukraine'                                              => 'អ៊ុយក្រែន',
    'United States Minor Outlying Islands'                 => 'ប្រជុំកោះតូចៗ ដាច់ស្រយោលសហរដ្ឋអាមេរិក',
    'Uruguay'                                              => 'អ៊ុយរូហ្កាយ',
    'Eastern Republic of Uruguay'                          => 'សាធារណរដ្ឋភាគខាងកើតអ៊ុយរូហ្កាយ',
    'United States'                                        => 'អាមេរិក (សហរដ្ឋ)',
    'United States of America'                             => 'សហរដ្ឋ​អាមេរិក',
    'Uzbekistan'                                           => 'អ៊ូសបេគីស្ថាន',
    'Republic of Uzbekistan'                               => 'សាធារណរដ្ឋអ៊ូសបេគីស្ថាន',
    'Holy See (Vatican City State)'                        => 'ព្រះរាជវាំងពិសិដ្ឋ (បុរីវ៉ាទីកង់)',
    'Saint Vincent and the Grenadines'                     => 'សាំងវីនសិន និង ហ្គ្រើណាដិន',
    'Venezuela, Bolivarian Republic of'                    => 'វ៉េណេស៊ុយអេឡា (សាធារណរដ្ឋបូលីវា)',
    'Bolivarian Republic of Venezuela'                     => 'សាធារណរដ្ឋបូលីវាវ៉េណេស៊ុយអេឡា',
    'Venezuela'                                            => 'វ៉េណេស៊ុយអេឡា',
    'Virgin Islands, British'                              => 'កោះ​វីជីន (អង់គ្លេស)',
    'British Virgin Islands'                               => 'កោះ​វីជីនអង់គ្លេស',
    'Virgin Islands, U.S.'                                 => 'កោះវីជីន (អាមេរិក)',
    'Virgin Islands of the United States'                  => 'កោះវីជីនអាមេរិក',
    'Viet Nam'                                             => 'វៀតណាម',
    'Socialist Republic of Viet Nam'                       => 'សាធារណរដ្ឋ​សង្គមនិយម​វៀតណាម',
    'Vietnam'                                              => 'វៀតណាម',
    'Vanuatu'                                              => 'វ៉ានូទូ',
    'Republic of Vanuatu'                                  => 'សាធារណរដ្ឋវ៉ានូទូ',
    'Wallis and Futuna'                                    => 'វ៉ាលីហ្ស និង ហ្វូទូណា',
    'Samoa'                                                => 'សាម័រ',
    'Independent State of Samoa'                           => 'រដ្ឋឯករាជ្យសាម័រ',
    'Yemen'                                                => 'យេម៉ែន',
    'Republic of Yemen'                                    => 'សាធារណរដ្ឋយេម៉ែន',
    'South Africa'                                         => 'អាហ្វ្រិក​ខាងត្បូង',
    'Republic of South Africa'                             => 'សាធារណរដ្ឋ​អាហ្វ្រិក​ខាងត្បូង',
    'Zambia'                                               => 'សំប៊ី',
    'Republic of Zambia'                                   => 'សាធារណរដ្ឋសំប៊ី',
    'Zimbabwe'                                             => 'ស៊ីមបាវ៉េ',
    'Republic of Zimbabwe'                                 => 'សាធារណរដ្ឋស៊ីមបាវ៉េ',
];
