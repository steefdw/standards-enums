<?php

/**
 * Translations in Armenian.
 *
 * @noinspection SpellCheckingInspection
 */

return [
    'Aruba'                                                => 'Արուբա',
    'Afghanistan'                                          => 'Աֆղանստան',
    'Islamic Republic of Afghanistan'                      => 'Աֆղանստանի Իսլամական Հանրապետություն',
    'Angola'                                               => 'Անգոլա',
    'Republic of Angola'                                   => 'Անգոլայի Հանրապետություն',
    'Anguilla'                                             => 'Անգիլիա',
    'Åland Islands'                                        => 'Ալանդյան կղզիներ',
    'Albania'                                              => 'Ալբանիա',
    'Republic of Albania'                                  => 'Ալբանիայի Հանրապետություն',
    'Andorra'                                              => 'Անդորա',
    'Principality of Andorra'                              => 'Անդորայի Իշխանություն',
    'United Arab Emirates'                                 => 'Արաբական Միացյալ Էմիրություններ',
    'Argentina'                                            => 'Արգենտինա',
    'Argentine Republic'                                   => 'Արգենտինայի Հանրապետություն',
    'Armenia'                                              => 'Հայաստան',
    'Republic of Armenia'                                  => 'Հայաստանի Հանրապետություն',
    'American Samoa'                                       => 'Ամերիկյան Սամոա',
    'Antarctica'                                           => 'Անտարկտիկա',
    'French Southern Territories'                          => 'Ֆրանսիական հարավային տարածքներ',
    'Antigua and Barbuda'                                  => 'Անտիգուա և Բարբուդա',
    'Australia'                                            => 'Ավստրալիա',
    'Austria'                                              => 'Ավստրիա',
    'Republic of Austria'                                  => 'Ավստրիայի Հանրապետություն',
    'Azerbaijan'                                           => 'Ադրբեջան',
    'Republic of Azerbaijan'                               => 'Ադրբեջանի Հանրապետություն',
    'Burundi'                                              => 'Բուրունդի',
    'Republic of Burundi'                                  => 'Բուրունդի Հանրապետություն',
    'Belgium'                                              => 'Բելգիա',
    'Kingdom of Belgium'                                   => 'Բելգիայի Թագավորություն',
    'Benin'                                                => 'Բենին',
    'Republic of Benin'                                    => 'Բենինի Հանրապետություն',
    'Bonaire, Sint Eustatius and Saba'                     => 'Բոնեյրե, Սինտ Էվստատիուս և Սաբա',
    'Burkina Faso'                                         => 'Բուրկինա Ֆասո',
    'Bangladesh'                                           => 'Բանգլադեշ',
    'People\'s Republic of Bangladesh'                     => 'Բանգլադեշի Ժողովրդական Հանրապետություն',
    'Bulgaria'                                             => 'Բուլղարիա',
    'Republic of Bulgaria'                                 => 'Բուլղարիայի Հանրապետություն',
    'Bahrain'                                              => 'Բահրեյն',
    'Kingdom of Bahrain'                                   => 'Բահրեյնի Թագավորություն',
    'Bahamas'                                              => 'Բահամյան Կղզիներ',
    'Commonwealth of the Bahamas'                          => 'Բահամյան Կղզիների Ընկերակցություն',
    'Bosnia and Herzegovina'                               => 'Բոսնիա և Հերցեգովինա',
    'Republic of Bosnia and Herzegovina'                   => 'Բոսնիա և Հերցեգովինայի Հանրապետություն',
    'Saint Barthélemy'                                     => 'Սուրբ Բարդուղիմեոսի կղզի',
    'Belarus'                                              => 'Բելառուս',
    'Republic of Belarus'                                  => 'Բելառուսի Հանրապետություն',
    'Belize'                                               => 'Բելիզ',
    'Bermuda'                                              => 'Բերմուդյան Կղզիներ',
    'Bolivia, Plurinational State of'                      => 'Բոլիվիայի Բազմազգ Պետություն',
    'Plurinational State of Bolivia'                       => 'Բոլիվիայի Բազմազգ Պետություն',
    'Bolivia'                                              => 'Բոլիվիա',
    'Brazil'                                               => 'Բրազիլիա',
    'Federative Republic of Brazil'                        => 'Բրազիլիայի Դաշնային Հանրապետություն',
    'Barbados'                                             => 'Բարբադոս',
    'Brunei Darussalam'                                    => 'Բրունեյ',
    'Bhutan'                                               => 'Բութան',
    'Kingdom of Bhutan'                                    => 'Բութանի Թագավորություն',
    'Bouvet Island'                                        => 'Բուվե կղզի',
    'Botswana'                                             => 'Բոտսվանա',
    'Republic of Botswana'                                 => 'Բոտսվանայի Հանրապետություն',
    'Central African Republic'                             => 'Կենտրոնական Աֆրիկյան Հանրապետություն',
    'Canada'                                               => 'Կանադա',
    'Cocos (Keeling) Islands'                              => 'Կոկոսյան (Քիլինգ) կղզիներ',
    'Switzerland'                                          => 'Շվեյցարիա',
    'Swiss Confederation'                                  => 'Շվեյցարիայի Համադաշնություն',
    'Chile'                                                => 'Չիլի',
    'Republic of Chile'                                    => 'Չիլիի Հանրապետություն',
    'China'                                                => 'Չինաստան',
    'People\'s Republic of China'                          => 'Չինաստանի Ժողովրդական Հանրապետություն',
    'Côte d\'Ivoire'                                       => 'Կոտ դ\'Իվուար',
    'Republic of Côte d\'Ivoire'                           => 'Կոտ դ\'Իվուարի Հանրապետություն',
    'Cameroon'                                             => 'Կամերուն',
    'Republic of Cameroon'                                 => 'Կամերունի Հանրապետություն',
    'Congo, The Democratic Republic of the'                => 'Կոնգոյի Դեմոկրատական Հանրապետություն',
    'Congo'                                                => 'Կոնգո',
    'Republic of the Congo'                                => 'Կոնգոյի Հանրապետություն',
    'Cook Islands'                                         => 'Կուկի Կղզիներ',
    'Colombia'                                             => 'Կոլումբիա',
    'Republic of Colombia'                                 => 'Կոլումբիայի Հանրապետություն',
    'Comoros'                                              => 'Կոմորոս',
    'Union of the Comoros'                                 => 'Կոմորոսի Միություն',
    'Cabo Verde'                                           => 'Կաբո Վերդե',
    'Republic of Cabo Verde'                               => 'Կաբո Վերդեի Հանրապետություն',
    'Costa Rica'                                           => 'Կոստա Ռիկա',
    'Republic of Costa Rica'                               => 'Կոստա Ռիկայի Հանրապետություն',
    'Cuba'                                                 => 'Կուբա',
    'Republic of Cuba'                                     => 'Կուբայի Հանրապետություն',
    'Curaçao'                                              => 'Կյուրասաո',
    'Christmas Island'                                     => 'Ծննդյան Կղզի',
    'Cayman Islands'                                       => 'Կայմանյան Կղզիներ',
    'Cyprus'                                               => 'Կիպրոս',
    'Republic of Cyprus'                                   => 'Կիպրոսի Հանրապետություն',
    'Czechia'                                              => 'Չեխիա',
    'Czech Republic'                                       => 'Չեխիայի Հանրապետություն',
    'Germany'                                              => 'Գերմանիա',
    'Federal Republic of Germany'                          => 'Գերմանիայի Դաշնային Հանրապետություն',
    'Djibouti'                                             => 'Ջիբութի',
    'Republic of Djibouti'                                 => 'Ջիբութի Հանրապետություն',
    'Dominica'                                             => 'Դոմինիկա',
    'Commonwealth of Dominica'                             => 'Դոմինիկայի Համադաշնություն',
    'Denmark'                                              => 'Դանիա',
    'Kingdom of Denmark'                                   => 'Դանիայի Թագավորություն',
    'Dominican Republic'                                   => 'Դոմինիկյան Հանրապետություն',
    'Algeria'                                              => 'Ալժիր',
    'People\'s Democratic Republic of Algeria'             => 'Ալժիրի Ժողովրդական Ժողովրդավարական Հանրապետություն',
    'Ecuador'                                              => 'Էկվադոր',
    'Republic of Ecuador'                                  => 'Էկվադորի Հանրապետություն',
    'Egypt'                                                => 'Եգիպտոս',
    'Arab Republic of Egypt'                               => 'Եգիպտոսի Արաբական Հանրապետություն',
    'Eritrea'                                              => 'Էրիտրեա',
    'the State of Eritrea'                                 => 'Էրիտրեայի Հանրապետություն',
    'Western Sahara'                                       => 'Արեվմտյան Սահարա',
    'Spain'                                                => 'Իսպանիա',
    'Kingdom of Spain'                                     => 'Իսպանիա Թագավորություն',
    'Estonia'                                              => 'Էստոնիա',
    'Republic of Estonia'                                  => 'Էստոնիայի Հանրապետություն',
    'Ethiopia'                                             => 'Եթովպիա',
    'Federal Democratic Republic of Ethiopia'              => 'Եթովպիայի Դաշնային Ժողովրդավարական Հանրապետություն',
    'Finland'                                              => 'Ֆինլանդիա',
    'Republic of Finland'                                  => 'Ֆինլանդիայի Հանրապետություն',
    'Fiji'                                                 => 'Ֆիջի',
    'Republic of Fiji'                                     => 'Ֆիջիի Հանրապետություն',
    'Falkland Islands (Malvinas)'                          => 'Ֆոլկլոնդյան (Մալվինյան) Կղզիներ',
    'France'                                               => 'Ֆրանսիա',
    'French Republic'                                      => 'Ֆրանսիայի Հանրապետություն',
    'Faroe Islands'                                        => 'Ֆարերյան Կղզիներ',
    'Micronesia, Federated States of'                      => 'Միկրոնեզիայի Դաշնային Նահանգներ',
    'Federated States of Micronesia'                       => 'Միկրոնեզիայի Դաշնային Նահանգներ',
    'Gabon'                                                => 'Գաբոն',
    'Gabonese Republic'                                    => 'Գաբոնի Հանրապետություն',
    'United Kingdom'                                       => 'Մեծ Բրիտանիա',
    'United Kingdom of Great Britain and Northern Ireland' => 'Մեծ Բրիտանիայի եւ Հյուսիսային Իռլանդիայի Միացյալ Թագավորություն',
    'Georgia'                                              => 'Վրաստան',
    'Guernsey'                                             => 'Գերնսի',
    'Ghana'                                                => 'Գանա',
    'Republic of Ghana'                                    => 'Գանայի Հանրապետություն',
    'Gibraltar'                                            => 'Ջիբրալթար',
    'Guinea'                                               => 'Գվինեա',
    'Republic of Guinea'                                   => 'Գվինեայի Հանրապետություն',
    'Guadeloupe'                                           => 'Գվադելուպա',
    'Gambia'                                               => 'Գամբիա',
    'Republic of the Gambia'                               => 'Գամբիայի Հանրապետություն',
    'Guinea-Bissau'                                        => 'Գվինեա-Բիսաու',
    'Republic of Guinea-Bissau'                            => 'Գվինեա-Բիսաույի Հանրապետություն',
    'Equatorial Guinea'                                    => 'Հասարակածային Գվինեա',
    'Republic of Equatorial Guinea'                        => 'Հասարակածային Գվինեայի Հանրապետություն',
    'Greece'                                               => 'Հունաստան',
    'Hellenic Republic'                                    => 'Հունաստանի Հանրապետություն',
    'Grenada'                                              => 'Գրենադա',
    'Greenland'                                            => 'Գրենլանդիա',
    'Guatemala'                                            => 'Գվատեմալա',
    'Republic of Guatemala'                                => 'Գվատեմալայի Հանրապետություն',
    'French Guiana'                                        => 'Ֆրանսիական Գվիանա',
    'Guam'                                                 => 'Գուամ',
    'Guyana'                                               => 'Գայանա',
    'Republic of Guyana'                                   => 'Գայանայի Հանրապետություն',
    'Hong Kong'                                            => 'Հոնկոնգ',
    'Hong Kong Special Administrative Region of China'     => 'Չինաստանի Հոնկոնգ հատուկ վարչական տարածք',
    'Heard Island and McDonald Islands'                    => 'Հերդ Կղզի և ՄաքԴոնալդ Կղզիներ',
    'Honduras'                                             => 'Հոնդուրաս',
    'Republic of Honduras'                                 => 'Հոնդուրասի Հանրապետություն',
    'Croatia'                                              => 'Խորվաթիա',
    'Republic of Croatia'                                  => 'Խորվաթիայի Հանրապետություն',
    'Haiti'                                                => 'Հաիթի',
    'Republic of Haiti'                                    => 'Հաիթիի Հանրապետություն',
    'Hungary'                                              => 'Հունգարիա',
    'Indonesia'                                            => 'Ինդոնեզիա',
    'Republic of Indonesia'                                => 'Ինդոնեզիայի Հանրապետություն',
    'Isle of Man'                                          => 'Մեն Կղզի',
    'India'                                                => 'Հնդկաստան',
    'Republic of India'                                    => 'Հնդկաստանի Հանրապետություն',
    'British Indian Ocean Territory'                       => 'Հնդկական օվկիանոսի բրիտանական տարածքներ',
    'Ireland'                                              => 'Իռլանդիա',
    'Iran, Islamic Republic of'                            => 'Իրանի Իսլամական Հանրապետություն',
    'Islamic Republic of Iran'                             => 'Իրանի Իսլամական Հանրապետություն',
    'Iran'                                                 => 'Իրան',
    'Iraq'                                                 => 'Իրաք',
    'Republic of Iraq'                                     => 'Իրաքի Հանրապետություն',
    'Iceland'                                              => 'Իսլանդիա',
    'Republic of Iceland'                                  => 'Իսլանդիայի Հանրապետություն',
    'Israel'                                               => 'Իսրայել',
    'State of Israel'                                      => 'Իսրայել Պետություն',
    'Italy'                                                => 'Իտալիա',
    'Italian Republic'                                     => 'Իտալիայի Հանրապետություն',
    'Jamaica'                                              => 'Ջամայկա',
    'Jersey'                                               => 'Ջերսի',
    'Jordan'                                               => 'Հորդանան',
    'Hashemite Kingdom of Jordan'                          => 'Հորդանանի Հաշիմյան Թագավորություն',
    'Japan'                                                => 'Ճապոնիա',
    'Kazakhstan'                                           => 'Ղազախստան',
    'Republic of Kazakhstan'                               => 'Ղազախստանի Հանրապետություն',
    'Kenya'                                                => 'Քենիա',
    'Republic of Kenya'                                    => 'Քենիայի Հանրապետություն',
    'Kyrgyzstan'                                           => 'Ղրղզստան',
    'Kyrgyz Republic'                                      => 'Ղրղզական Հանրապետություն',
    'Cambodia'                                             => 'Կամբոջա',
    'Kingdom of Cambodia'                                  => 'Կամբոջայի Թագավորություն',
    'Kiribati'                                             => 'Կիրիբատի',
    'Republic of Kiribati'                                 => 'Կիրիբատիի Հանրապետություն',
    'Saint Kitts and Nevis'                                => 'Սենթ Քիթս և Նևիս',
    'Korea, Republic of'                                   => 'Կորեայի Հանարպետություն',
    'South Korea'                                          => 'Հարավային Կորեա',
    'Kuwait'                                               => 'Քուվեյթ',
    'State of Kuwait'                                      => 'Քուվեյթի Պետություն',
    'Lao People\'s Democratic Republic'                    => 'Լաոսի Ժողովրդական Ժողովրդավարական Հանրապետություն',
    'Laos'                                                 => 'Լաոս',
    'Lebanon'                                              => 'Լիբանան',
    'Lebanese Republic'                                    => 'Լիբանանի Հանրապետություն',
    'Liberia'                                              => 'Լիբերիա',
    'Republic of Liberia'                                  => 'Լիբերիայի Հանրապետություն',
    'Libya'                                                => 'Լիբիա',
    'Saint Lucia'                                          => 'Սանտա Լուչիա',
    'Liechtenstein'                                        => 'Լիխտենշտեյն',
    'Principality of Liechtenstein'                        => 'Լիխտենշտեյնի Իշխանություն',
    'Sri Lanka'                                            => 'Շրի Լանկա',
    'Democratic Socialist Republic of Sri Lanka'           => 'Շրի Լանկայի Ժողովրդավարական Սոցիալիստական Հանրապետություն',
    'Lesotho'                                              => 'Լեսոթո',
    'Kingdom of Lesotho'                                   => 'Լեսոթոյի Թագավորություն',
    'Lithuania'                                            => 'Լիտվա',
    'Republic of Lithuania'                                => 'Լիտվայի Հանրապետություն',
    'Luxembourg'                                           => 'Լյուքսեմբուրգ',
    'Grand Duchy of Luxembourg'                            => 'Լյուքսեմբուրգի Մեծ Դքսություն',
    'Latvia'                                               => 'Լատվիա',
    'Republic of Latvia'                                   => 'Լատվիայի Հանրապետություն',
    'Macao'                                                => 'Մակաո',
    'Macao Special Administrative Region of China'         => 'Չինաստանի Մակաո հատուկ վարչական տարածք',
    'Saint Martin (French part)'                           => 'Սեն Մարտեն (ֆրանսիական մաս)',
    'Morocco'                                              => 'Մարոկո',
    'Kingdom of Morocco'                                   => 'Մարոկոյի Թագավորություն',
    'Monaco'                                               => 'Մոնակո',
    'Principality of Monaco'                               => 'Մոնակոյի Իշխանություն',
    'Moldova, Republic of'                                 => 'Մոլդովայի Հանրապետություն',
    'Republic of Moldova'                                  => 'Մոլդովայի Հանրապետություն',
    'Moldova'                                              => 'Մոլդովա',
    'Madagascar'                                           => 'Մադագասկար',
    'Republic of Madagascar'                               => 'Մադագասկարի Հանրապետություն',
    'Maldives'                                             => 'Մալդիվներ',
    'Republic of Maldives'                                 => 'Մալդիվների Հանրապետություն',
    'Mexico'                                               => 'Մեքսիկա',
    'United Mexican States'                                => 'Մեքսիկայի Միացյալ Նահանգներ',
    'Marshall Islands'                                     => 'Մարշալյան Կղզիներ',
    'Republic of the Marshall Islands'                     => 'Մարշալյան Կղզիների Հանրապետություն',
    'North Macedonia'                                      => 'Հյուսիսային Մակեդոնիա',
    'Republic of North Macedonia'                          => 'Հյուսիսային Մակեդոնիայի Հանրապետություն',
    'Mali'                                                 => 'Մալի',
    'Republic of Mali'                                     => 'Մալիի Հանրապետություն',
    'Malta'                                                => 'Մալթա',
    'Republic of Malta'                                    => 'Մալթայի Հանրապետություն',
    'Myanmar'                                              => 'Մյանմա',
    'Republic of Myanmar'                                  => 'Մյանմայի Հանրապետություն',
    'Montenegro'                                           => 'Մոնտենեգրո',
    'Mongolia'                                             => 'Մոնղոլիա',
    'Northern Mariana Islands'                             => 'Հյուսիսային Մարիանյան Կղզիներ',
    'Commonwealth of the Northern Mariana Islands'         => 'Հյուսիսային Մարիանյան Կղզիների Համագործակցություն',
    'Mozambique'                                           => 'Մոզամբիկ',
    'Republic of Mozambique'                               => 'Մոզամբիկյի Հանրապետություն',
    'Mauritania'                                           => 'Մավրիտանիա',
    'Islamic Republic of Mauritania'                       => 'Մավրիտանիայի Իսլամական Հանրապետություն',
    'Montserrat'                                           => 'Մոնտսերատ',
    'Martinique'                                           => 'Մարտինիկա',
    'Mauritius'                                            => 'Մավրիկիոս',
    'Republic of Mauritius'                                => 'Մավրիկիոսի Հանրապետություն',
    'Malawi'                                               => 'Մալավի',
    'Republic of Malawi'                                   => 'Մալավի Հանրապետություն',
    'Malaysia'                                             => 'Մալայզիա',
    'Mayotte'                                              => 'Մայոտ',
    'Namibia'                                              => 'Նամիբիա',
    'Republic of Namibia'                                  => 'Նամիբիայի Հանրապետություն',
    'New Caledonia'                                        => 'Նոր Կալեդոնիա',
    'Niger'                                                => 'Նիգեր',
    'Republic of the Niger'                                => 'Նիգերի Հանրապետություն',
    'Norfolk Island'                                       => 'Նորֆոլք Կղզի',
    'Nigeria'                                              => 'Նիգերիա',
    'Federal Republic of Nigeria'                          => 'Նիգերիայի Դաշնայի Հանրապետություն',
    'Nicaragua'                                            => 'Նիկարագուա',
    'Republic of Nicaragua'                                => 'Նիկարագուայի Հանրապետություն',
    'Niue'                                                 => 'Նյուեյ',
    'Netherlands'                                          => 'Նիդերլանդեր',
    'Kingdom of the Netherlands'                           => 'Նիդերլանդների Թագավորություն',
    'Norway'                                               => 'Նորվեգիա',
    'Kingdom of Norway'                                    => 'Նորվեգիայի Թագավորություն',
    'Nepal'                                                => 'Նեպալ',
    'Federal Democratic Republic of Nepal'                 => 'Նեպալի Դաշնային Ժողովրդավարական Հանրապետություն',
    'Nauru'                                                => 'Նաուրու',
    'Republic of Nauru'                                    => 'Նաուրուի Հանրապետություն',
    'New Zealand'                                          => 'Նոր Զելանդիա',
    'Oman'                                                 => 'Օման',
    'Sultanate of Oman'                                    => 'Օմանի Սուլթանություն',
    'Pakistan'                                             => 'Պակիստան',
    'Islamic Republic of Pakistan'                         => 'Պակիստանի Իսլամական Հանրապետություն',
    'Panama'                                               => 'Պանամա',
    'Republic of Panama'                                   => 'Պանամայի Հանրապետություն',
    'Pitcairn'                                             => 'Փիթքերն կղզիներ',
    'Peru'                                                 => 'Պերու',
    'Republic of Peru'                                     => 'Պերուի Հանրապետություն',
    'Philippines'                                          => 'Ֆիլիպիններ',
    'Republic of the Philippines'                          => 'Ֆիլիպինների Հանրապետություն',
    'Palau'                                                => 'Պալաու',
    'Republic of Palau'                                    => 'Պալաուի Հանրապետություն',
    'Papua New Guinea'                                     => 'Պապուա Նոր Գվինեա',
    'Independent State of Papua New Guinea'                => 'Պապուա Նոր Գվինեայի Անկախ Պետության',
    'Poland'                                               => 'Լեհաստան',
    'Republic of Poland'                                   => 'Լեհաստանի Հանրապետություն',
    'Puerto Rico'                                          => 'Պուերտո Ռիկո',
    'Korea, Democratic People\'s Republic of'              => 'Կորեայի Ժողովրդական Դեմոկրատական Հանրապետություն',
    'Democratic People\'s Republic of Korea'               => 'Կորեայի Ժողովրդական Դեմոկրատական Հանրապետություն',
    'North Korea'                                          => 'Հյուսիսային Կորեա',
    'Portugal'                                             => 'Պորտուգալիա',
    'Portuguese Republic'                                  => 'Պորտուգալիայի Հանրապետություն',
    'Paraguay'                                             => 'Պարագվայ',
    'Republic of Paraguay'                                 => 'Պարագվայի Հանրապետություն',
    'Palestine, State of'                                  => 'Պաղեստին Պետություն',
    'the State of Palestine'                               => 'Պաղեստին Պետություն',
    'French Polynesia'                                     => 'Ֆրանսիական Պոլինեզիա',
    'Qatar'                                                => 'Քաթար',
    'State of Qatar'                                       => 'Քաթարի Պետություն',
    'Réunion'                                              => 'Ռեյունյոն',
    'Romania'                                              => 'Ռումինիա',
    'Russian Federation'                                   => 'Ռուսաստանի Դաշնություն',
    'Rwanda'                                               => 'Ռուանդա',
    'Rwandese Republic'                                    => 'Ռուանդայի Հանրապետություն',
    'Saudi Arabia'                                         => 'Սաուդյան Արաբիա',
    'Kingdom of Saudi Arabia'                              => 'Սաուդյան Արաբիայի Թագավորություն',
    'Sudan'                                                => 'Սուդան',
    'Republic of the Sudan'                                => 'Սուդանի Հանրապետություն',
    'Senegal'                                              => 'Սենեգալ',
    'Republic of Senegal'                                  => 'Սենեգալյի Հանրապետություն',
    'Singapore'                                            => 'Սինգապուր',
    'Republic of Singapore'                                => 'Սինգապուրի Հանրապետություն',
    'South Georgia and the South Sandwich Islands'         => 'Հարավային Ջորջիա և Հարավային Սանդվիչյան Կղզիներ',
    'Saint Helena, Ascension and Tristan da Cunha'         => 'Սուրբ Հեղինեի կղզի, Համբարձման կղզի և Տրիստան դա Կունյա',
    'Svalbard and Jan Mayen'                               => 'Սվալբարդ և Յան Մայեն',
    'Solomon Islands'                                      => 'Սոլոմոնյան կղզիներ',
    'Sierra Leone'                                         => 'Սիերա Լեոնե',
    'Republic of Sierra Leone'                             => 'Սիերա Լեոնեի Հանրապետություն',
    'El Salvador'                                          => 'Էլ Սալվադոր',
    'Republic of El Salvador'                              => 'Էլ Սալվադորի Հանրապետություն',
    'San Marino'                                           => 'Սան Մարինո',
    'Republic of San Marino'                               => 'Սան Մարինոյի Հանրապետություն',
    'Somalia'                                              => 'Սոմալի',
    'Federal Republic of Somalia'                          => 'Սոմալիի Դաշնային Հանրապետություն',
    'Saint Pierre and Miquelon'                            => 'Սեն Պիեռ և Միքելոն',
    'Serbia'                                               => 'Սերբիա',
    'Republic of Serbia'                                   => 'Սերբիայի Հանրապետություն',
    'South Sudan'                                          => 'Հարավային Սուդան',
    'Republic of South Sudan'                              => 'Հարավային Սուդանի Հանրապետություն',
    'Sao Tome and Principe'                                => 'Սան Տոմե և Պրինսիպի',
    'Democratic Republic of Sao Tome and Principe'         => 'Սան Տոմեի և Պրինսիպիի Դեմոկրատական Հանրապետություն',
    'Suriname'                                             => 'Սուրինամ',
    'Republic of Suriname'                                 => 'Սուրինամի Հանրապետություն',
    'Slovakia'                                             => 'Սլովակիա',
    'Slovak Republic'                                      => 'Սլովակիայի Հանրապետություն',
    'Slovenia'                                             => 'Սլովենիա',
    'Republic of Slovenia'                                 => 'Սլովենիայի Հանրապետություն',
    'Sweden'                                               => 'Շվեդիա',
    'Kingdom of Sweden'                                    => 'Շվեդիայի Թագավորություն',
    'Eswatini'                                             => 'Էսվատինի',
    'Kingdom of Eswatini'                                  => 'Էսվատինիի Թագավորություն',
    'Sint Maarten (Dutch part)'                            => 'Սինտ Մարտեն (հոլանդական մաս)',
    'Seychelles'                                           => 'Սեյշելյան կղզիներ',
    'Republic of Seychelles'                               => 'Սեյշելների Հանրապետություն',
    'Syrian Arab Republic'                                 => 'Սիրիայի Արաբական Հանրապետություն',
    'Syria'                                                => 'Սիրիա',
    'Turks and Caicos Islands'                             => 'Թերքս և Քայքոս կղզիներ',
    'Chad'                                                 => 'Չադ',
    'Republic of Chad'                                     => 'Չադի Հանրապետություն',
    'Togo'                                                 => 'Տոգո',
    'Togolese Republic'                                    => 'Տոգոյի Հանարպետություն',
    'Thailand'                                             => 'Թաիլանդ',
    'Kingdom of Thailand'                                  => 'Թաիլանդի Թագավորություն',
    'Tajikistan'                                           => 'Տաջիկստան',
    'Republic of Tajikistan'                               => 'Տաջիկստանի Հանրապետություն',
    'Tokelau'                                              => 'Տոկելաու',
    'Turkmenistan'                                         => 'Թուրքմենստան',
    'Timor-Leste'                                          => 'Թիմոր-Լեստե',
    'Democratic Republic of Timor-Leste'                   => 'Թիմոր-Լեստեի Ժողովրդավարական Հանրապետություն',
    'Tonga'                                                => 'Տոնգա',
    'Kingdom of Tonga'                                     => 'Տոնգայի Թագավորություն',
    'Trinidad and Tobago'                                  => 'Տրինիդադ և Տոբագո',
    'Republic of Trinidad and Tobago'                      => 'Տրինիդադի և Տոբագոյի Հանրապետություն',
    'Tunisia'                                              => 'Թունիս',
    'Republic of Tunisia'                                  => 'Թունիսի Հանրապետություն',
    'Türkiye'                                              => 'Թուրքիա',
    'Republic of Türkiye'                                  => 'Թուրքիայի Հանրապետություն',
    'Tuvalu'                                               => 'Տուվալու',
    'Taiwan, Province of China'                            => 'Չինաստանի Թայվան նահանգ',
    'Taiwan'                                               => 'Թայվան',
    'Tanzania, United Republic of'                         => 'Տանզանիայի Միացյալ Հանրապետություն',
    'United Republic of Tanzania'                          => 'Տանզանիայի Միացյալ Հանրապետություն',
    'Tanzania'                                             => 'Տանզանիա',
    'Uganda'                                               => 'Ուգանդա',
    'Republic of Uganda'                                   => 'Ուգանդայի Հանրապետություն',
    'Ukraine'                                              => 'Ուկրաինա',
    'United States Minor Outlying Islands'                 => 'ԱՄՆ արտաքին փոքր կղզիներ',
    'Uruguay'                                              => 'Ուրուգվայ',
    'Eastern Republic of Uruguay'                          => 'Ուրուգվայի Արևելյան Հանրապետության',
    'United States'                                        => 'ԱՄՆ',
    'United States of America'                             => 'Ամերիկայի Միացյալ Նահանգներ',
    'Uzbekistan'                                           => 'Ուզբեկստան',
    'Republic of Uzbekistan'                               => 'Ուզբեկստանի Հանրապետություն',
    'Holy See (Vatican City State)'                        => 'Սուրբ աթոռ (Վատիկան)',
    'Saint Vincent and the Grenadines'                     => 'Սենթ Վինսենթ և Գրենադիններ',
    'Venezuela, Bolivarian Republic of'                    => 'Վենեսուելայի Բոլիվարական Հանրապետություն',
    'Bolivarian Republic of Venezuela'                     => 'Վենեսուելայի Բոլիվարական Հանրապետություն',
    'Venezuela'                                            => 'Վենեսուելա',
    'Virgin Islands, British'                              => 'Բրիտանական Վիրջինյան կղզիներ',
    'British Virgin Islands'                               => 'Բրիտանական Վիրջինյան կղզիներ',
    'Virgin Islands, U.S.'                                 => 'ԱՄՆ Վիրջինյան կղզիներ',
    'Virgin Islands of the United States'                  => 'Ամերիկայի Միացյալ Նահանգների Վիրջինյան կղզիներ',
    'Viet Nam'                                             => 'Վիետնամ',
    'Socialist Republic of Viet Nam'                       => 'Վիետնամի Սոցիալիստական Հանրապետություն',
    'Vietnam'                                              => 'Վիետնամ',
    'Vanuatu'                                              => 'Վանուատու',
    'Republic of Vanuatu'                                  => 'Վանուատուի Հանրապետություն',
    'Wallis and Futuna'                                    => 'Ուոլիս և Ֆուտունա',
    'Samoa'                                                => 'Սամոա',
    'Independent State of Samoa'                           => 'Սամոայի Անկախ Պետություն',
    'Yemen'                                                => 'Եմեն',
    'Republic of Yemen'                                    => 'Եմենի Հանրապետություն',
    'South Africa'                                         => 'Հարավային Աֆրիկա',
    'Republic of South Africa'                             => 'Հարավային Աֆրիկայի Հանրապետություն',
    'Zambia'                                               => 'Զամբիա',
    'Republic of Zambia'                                   => 'Զամբիայի Հանրապետություն',
    'Zimbabwe'                                             => 'Զիմբաբվե',
    'Republic of Zimbabwe'                                 => 'Զիմբաբվեի Հանրապետություն',
];
