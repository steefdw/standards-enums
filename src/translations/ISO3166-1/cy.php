<?php

/**
 * Translations in Welsh.
 *
 * @noinspection SpellCheckingInspection
 */

return [
    'Aruba'                                                => 'Aruba',
    'Afghanistan'                                          => 'Afghanistan',
    'Islamic Republic of Afghanistan'                      => 'Gweriniaeth Islamaidd Pacistan',
    'Angola'                                               => 'Angola',
    'Republic of Angola'                                   => 'Gweriniaeth Angola',
    'Anguilla'                                             => 'Anguilla',
    'Åland Islands'                                        => 'Ynysoedd Åland',
    'Albania'                                              => 'Albania',
    'Republic of Albania'                                  => 'Gweriniaeth Albania',
    'Andorra'                                              => 'Andorra',
    'Principality of Andorra'                              => 'Tywysogaeth Andorra',
    'United Arab Emirates'                                 => 'Yr Emiradau Arabaidd Unedig',
    'Argentina'                                            => 'Yr Ariannin',
    'Argentine Republic'                                   => 'Gweriniaeth yr Ariannin',
    'Armenia'                                              => 'Armenia',
    'Republic of Armenia'                                  => 'Gweriniaeth Armenia',
    'American Samoa'                                       => 'Samoa America',
    'Antarctica'                                           => 'Antarctica',
    'French Southern Territories'                          => 'French Southern Territories',
    'Antigua and Barbuda'                                  => 'Antigua a Barbuda',
    'Australia'                                            => 'Awstralia',
    'Austria'                                              => 'Awstria',
    'Republic of Austria'                                  => 'Gweriniaeth Awstria',
    'Azerbaijan'                                           => 'Azerbaijan',
    'Republic of Azerbaijan'                               => 'Gweriniaeth Azerbaijan',
    'Burundi'                                              => 'Bwrwndi',
    'Republic of Burundi'                                  => 'Gweriniaeth Burundi',
    'Belgium'                                              => 'Gwlad Belg',
    'Kingdom of Belgium'                                   => 'Brenhiniaeth Belg',
    'Benin'                                                => 'Benin',
    'Republic of Benin'                                    => 'Gweriniaeth Benin',
    'Bonaire, Sint Eustatius and Saba'                     => 'Bonaire, Sint Eustatius a Saba',
    'Burkina Faso'                                         => 'Burkina Faso',
    'Bangladesh'                                           => 'Bangladesh',
    'People\'s Republic of Bangladesh'                     => 'Gweriniaeth Pobl Bangladesh',
    'Bulgaria'                                             => 'Bwlgaria',
    'Republic of Bulgaria'                                 => 'Gweriniaeth Bwlgaria',
    'Bahrain'                                              => 'Bahrain',
    'Kingdom of Bahrain'                                   => 'Teyrnas Bahrain',
    'Bahamas'                                              => 'Y Bahamas',
    'Commonwealth of the Bahamas'                          => 'Cymanwlad y Bahamas',
    'Bosnia and Herzegovina'                               => 'Bosnia a Hercegovina',
    'Republic of Bosnia and Herzegovina'                   => 'Gweriniaeth Bosnia a Herzegovina',
    'Saint Barthélemy'                                     => 'Saint Barthélemy',
    'Belarus'                                              => 'Belarws',
    'Republic of Belarus'                                  => 'Gweriniaeth Belarws',
    'Belize'                                               => 'Belîs',
    'Bermuda'                                              => 'Bermuda',
    'Bolivia, Plurinational State of'                      => 'Bolifia, Gwladwriaeth Amlgenedlaethol',
    'Plurinational State of Bolivia'                       => 'Gwladwriaeth Amlgenedlaethol Bolifia',
    'Bolivia'                                              => 'Bolifia',
    'Brazil'                                               => 'Brasil',
    'Federative Republic of Brazil'                        => 'Gweriniaeth Ffedereiddiol Brasil',
    'Barbados'                                             => 'Barbados',
    'Brunei Darussalam'                                    => 'Brunei Darussalam',
    'Bhutan'                                               => 'Bhutan',
    'Kingdom of Bhutan'                                    => 'Brenhiniaeth Bhutan',
    'Bouvet Island'                                        => 'Ynys Bouvet',
    'Botswana'                                             => 'Botswana',
    'Republic of Botswana'                                 => 'Gweriniaeth Botswana',
    'Central African Republic'                             => 'Gweriniaeth Canolbarth Affrica',
    'Canada'                                               => 'Canada',
    'Cocos (Keeling) Islands'                              => 'Ynysoedd Cocos (Keeling)',
    'Switzerland'                                          => 'Y Swistir',
    'Swiss Confederation'                                  => 'Y Conffederasiwn Swisaidd',
    'Chile'                                                => 'Chile',
    'Republic of Chile'                                    => 'Gweriniaeth Chile',
    'China'                                                => 'Tsieina',
    'People\'s Republic of China'                          => 'Gweriniaeth Pobl Tsieina',
    'Côte d\'Ivoire'                                       => 'Côte d\'Ivoire',
    'Republic of Côte d\'Ivoire'                           => 'Gweriniaeth Côte d\'Ivoire',
    'Cameroon'                                             => 'Camerŵn',
    'Republic of Cameroon'                                 => 'Gweriniaeth Cameroon',
    'Congo, The Democratic Republic of the'                => 'Congo, Gweriniaeth Ddemocrataidd y',
    'Congo'                                                => 'Congo',
    'Republic of the Congo'                                => 'Gweriniaeth y Congo',
    'Cook Islands'                                         => 'Ynysoedd Cook',
    'Colombia'                                             => 'Colombia',
    'Republic of Colombia'                                 => 'Gweriniaeth Colombia',
    'Comoros'                                              => 'Comoros',
    'Union of the Comoros'                                 => 'Undeb y Comoros',
    'Cabo Verde'                                           => 'Penrhyn Verde',
    'Republic of Cabo Verde'                               => 'Gweriniaeth Penrhyn Verde',
    'Costa Rica'                                           => 'Costa Rica',
    'Republic of Costa Rica'                               => 'Gweriniaeth Costa Rica',
    'Cuba'                                                 => 'Ciwba',
    'Republic of Cuba'                                     => 'Gweriniaeth Ciwba',
    'Curaçao'                                              => 'Curaçao',
    'Christmas Island'                                     => 'Ynys y Nadolig',
    'Cayman Islands'                                       => 'Ynysoedd Cayman',
    'Cyprus'                                               => 'Cyprus',
    'Republic of Cyprus'                                   => 'Gweriniaeth Cyprus',
    'Czechia'                                              => 'Tsiecia',
    'Czech Republic'                                       => 'Gweriniaeth Tsiec',
    'Germany'                                              => 'Yr Almaen',
    'Federal Republic of Germany'                          => 'Gweriniaeth Ffederal yr Almaen',
    'Djibouti'                                             => 'Djibouti',
    'Republic of Djibouti'                                 => 'Gweriniaeth Djibouti',
    'Dominica'                                             => 'Dominica',
    'Commonwealth of Dominica'                             => 'Cymanwlad Dominica',
    'Denmark'                                              => 'Denmarc',
    'Kingdom of Denmark'                                   => 'Brenhiniaeth Denmarc',
    'Dominican Republic'                                   => 'Gweriniaeth Dominica',
    'Algeria'                                              => 'Algeria',
    'People\'s Democratic Republic of Algeria'             => 'Gweriniaeth Democratic Pobl Algeria',
    'Ecuador'                                              => 'Ecwador',
    'Republic of Ecuador'                                  => 'Gweriniaeth Ecuador',
    'Egypt'                                                => 'Yr Aifft',
    'Arab Republic of Egypt'                               => 'Gweriniaeth Arabaidd yr Aifft',
    'Eritrea'                                              => 'Eritrea',
    'the State of Eritrea'                                 => 'Gwladwriaeth Eritrea',
    'Western Sahara'                                       => 'Gorllewin Sahara',
    'Spain'                                                => 'Sbaen',
    'Kingdom of Spain'                                     => 'Brenhiniaeth Sbaen',
    'Estonia'                                              => 'Estonia',
    'Republic of Estonia'                                  => 'Gweriniaeth Estonia',
    'Ethiopia'                                             => 'Ethiopia',
    'Federal Democratic Republic of Ethiopia'              => 'Gweriniaeth Democratic Ffederal Ethiopia',
    'Finland'                                              => 'Y Ffindir',
    'Republic of Finland'                                  => 'Gweriniaeth y Ffindir',
    'Fiji'                                                 => 'Ffiji',
    'Republic of Fiji'                                     => 'Gweriniaeth Fiji',
    'Falkland Islands (Malvinas)'                          => 'Ynysoedd y Falklands (Malvinas)',
    'France'                                               => 'Ffrainc',
    'French Republic'                                      => 'Gweriniaeth Ffrainc',
    'Faroe Islands'                                        => 'Ynysoedd Ffaro',
    'Micronesia, Federated States of'                      => 'Micronesia, Taleithiau Cyfunol',
    'Federated States of Micronesia'                       => 'Taleithiau Cyfunol Micronesia',
    'Gabon'                                                => 'Gabon',
    'Gabonese Republic'                                    => 'Gweriniaeth Gabon',
    'United Kingdom'                                       => 'Y Deyrnas Unedig',
    'United Kingdom of Great Britain and Northern Ireland' => 'Teyrnas Unedig Prydain Fawr a Gogledd Iwerddon',
    'Georgia'                                              => 'Georgia',
    'Guernsey'                                             => 'Ynys y Garn',
    'Ghana'                                                => 'Ghana',
    'Republic of Ghana'                                    => 'Gweriniaeth Ghana',
    'Gibraltar'                                            => 'Gibraltar',
    'Guinea'                                               => 'Guinea',
    'Republic of Guinea'                                   => 'Gweriniaeth Guinea',
    'Guadeloupe'                                           => 'Guadeloupe',
    'Gambia'                                               => 'Gambia',
    'Republic of the Gambia'                               => 'Gweriniaeth Gambia',
    'Guinea-Bissau'                                        => 'Guinea-Bissau',
    'Republic of Guinea-Bissau'                            => 'Gweriniaeth Guinea-Bissau',
    'Equatorial Guinea'                                    => 'Guinea Gyhydeddol',
    'Republic of Equatorial Guinea'                        => 'Gweriniaeth Guinea Cyhydeddol',
    'Greece'                                               => 'Groeg',
    'Hellenic Republic'                                    => 'Gweriniaeth Groeg',
    'Grenada'                                              => 'Grenada',
    'Greenland'                                            => 'Yr Ynys Las',
    'Guatemala'                                            => 'Gwatemala',
    'Republic of Guatemala'                                => 'Gweriniaeth Gwatemala',
    'French Guiana'                                        => 'Guiana Ffrangeg',
    'Guam'                                                 => 'Guam',
    'Guyana'                                               => 'Guyana',
    'Republic of Guyana'                                   => 'Gweriniaeth Guyana',
    'Hong Kong'                                            => 'Hong Kong',
    'Hong Kong Special Administrative Region of China'     => 'Hong Kong, Rhanbarth Gweinyddol Arbennig o Tseina',
    'Heard Island and McDonald Islands'                    => 'Ynysoedd Heard a McDonald',
    'Honduras'                                             => 'Honduras',
    'Republic of Honduras'                                 => 'Gweriniaeth Honduras',
    'Croatia'                                              => 'Croatia',
    'Republic of Croatia'                                  => 'Gweriniaeth Croatia',
    'Haiti'                                                => 'Haiti',
    'Republic of Haiti'                                    => 'Gweriniaeth Haiti',
    'Hungary'                                              => 'Hwngari',
    'Indonesia'                                            => 'Indonesia',
    'Republic of Indonesia'                                => 'Gweriniaeth Indonesia',
    'Isle of Man'                                          => 'Ynys Manaw',
    'India'                                                => 'India',
    'Republic of India'                                    => 'Gweriniaeth India',
    'British Indian Ocean Territory'                       => 'Tiriogaeth Brydeinig Cefnfor India',
    'Ireland'                                              => 'Iwerddon',
    'Iran, Islamic Republic of'                            => 'Iran, Gweriniaeth Islamaidd',
    'Islamic Republic of Iran'                             => 'Gweriniaeth Islamaidd Iran',
    'Iran'                                                 => 'Iran',
    'Iraq'                                                 => 'Irac',
    'Republic of Iraq'                                     => 'Gweriniaeth Irac',
    'Iceland'                                              => 'Gwlad yr Iâ',
    'Republic of Iceland'                                  => 'Gweriniaeth Gwlad yr Iâ',
    'Israel'                                               => 'Israel',
    'State of Israel'                                      => 'Talaith Israel',
    'Italy'                                                => 'Yr Eidal',
    'Italian Republic'                                     => 'Gweriniaeth yr Eidal',
    'Jamaica'                                              => 'Jamaica',
    'Jersey'                                               => 'Jersey',
    'Jordan'                                               => 'Gwlad Iorddonen',
    'Hashemite Kingdom of Jordan'                          => 'Gweriniaeth Hashemeit yr Iorddonen',
    'Japan'                                                => 'Japan',
    'Kazakhstan'                                           => 'Kazakstan',
    'Republic of Kazakhstan'                               => 'Gweriniaeth Kazakhstan',
    'Kenya'                                                => 'Kenya',
    'Republic of Kenya'                                    => 'Gweriniaeth Kenya',
    'Kyrgyzstan'                                           => 'Kyrgyzstan',
    'Kyrgyz Republic'                                      => 'Gweriniaeth Kyrgyz',
    'Cambodia'                                             => 'Cambodia',
    'Kingdom of Cambodia'                                  => 'Brenhiniaeth Cambodia',
    'Kiribati'                                             => 'Kiribati',
    'Republic of Kiribati'                                 => 'Gweriniaeth y Kiribati',
    'Saint Kitts and Nevis'                                => 'Saint Kitts a Nevis',
    'Korea, Republic of'                                   => 'Corëa, Gweriniaeth',
    'South Korea'                                          => 'De Corea',
    'Kuwait'                                               => 'Coweit',
    'State of Kuwait'                                      => 'Talaith Coweit',
    'Lao People\'s Democratic Republic'                    => 'Gweriniaeth Ddemocrataidd y Bobl Lao',
    'Laos'                                                 => 'Laos',
    'Lebanon'                                              => 'Libanus',
    'Lebanese Republic'                                    => 'Gweriniaeth Lebanon',
    'Liberia'                                              => 'Liberia',
    'Republic of Liberia'                                  => 'Gweriniaeth Liberia',
    'Libya'                                                => 'Libya',
    'Saint Lucia'                                          => 'Saint Lucia',
    'Liechtenstein'                                        => 'Liechtenstein',
    'Principality of Liechtenstein'                        => 'Tywysogaeth Liechtenstein',
    'Sri Lanka'                                            => 'Sri Lanka',
    'Democratic Socialist Republic of Sri Lanka'           => 'Gweriniaeth Sosialaidd Democrataidd Sri Lanka',
    'Lesotho'                                              => 'Lesotho',
    'Kingdom of Lesotho'                                   => 'Brenhiniaeth Lesotho',
    'Lithuania'                                            => 'Lithwania',
    'Republic of Lithuania'                                => 'Gweriniaeth Lithwania',
    'Luxembourg'                                           => 'Lwcsembwrg',
    'Grand Duchy of Luxembourg'                            => 'Archddugiaeth Lwcsembwrg',
    'Latvia'                                               => 'Latfia',
    'Republic of Latvia'                                   => 'Gweriniaeth Latfia',
    'Macao'                                                => 'Macao',
    'Macao Special Administrative Region of China'         => 'Rhanbarth Gweinyddol Arbennig o Tseina Macao',
    'Saint Martin (French part)'                           => 'Saint Martin (rhan Ffrengig)',
    'Morocco'                                              => 'Moroco',
    'Kingdom of Morocco'                                   => 'Brenhiniaeth Morocco',
    'Monaco'                                               => 'Monaco',
    'Principality of Monaco'                               => 'Tywysogaeth Monaco',
    'Moldova, Republic of'                                 => 'Moldofa, Gweriniaeth',
    'Republic of Moldova'                                  => 'Gweriniaeth Moldova',
    'Moldova'                                              => 'Moldofa',
    'Madagascar'                                           => 'Madagascar',
    'Republic of Madagascar'                               => 'Gweriniaeth Madagascar',
    'Maldives'                                             => 'Maldives',
    'Republic of Maldives'                                 => 'Gweriniaeth y Maldives',
    'Mexico'                                               => 'Mecsico',
    'United Mexican States'                                => 'Taleithiau Unedig Mecsico',
    'Marshall Islands'                                     => 'Ynysoedd Marshall',
    'Republic of the Marshall Islands'                     => 'Gweriniaeth Ynysoedd Marshall',
    'North Macedonia'                                      => 'Gogledd Macedonia',
    'Republic of North Macedonia'                          => 'Gweriniaeth Gogledd Macedonia',
    'Mali'                                                 => 'Mali',
    'Republic of Mali'                                     => 'Gweriniaeth Mali',
    'Malta'                                                => 'Malta',
    'Republic of Malta'                                    => 'Gweriniaeth Malta',
    'Myanmar'                                              => 'Myanmar',
    'Republic of Myanmar'                                  => 'Gweriniaeth Myanmar',
    'Montenegro'                                           => 'Montenegro',
    'Mongolia'                                             => 'Mongolia',
    'Northern Mariana Islands'                             => 'Ynysoedd Gogledd Mariana',
    'Commonwealth of the Northern Mariana Islands'         => 'Cymanwlad Ynysoedd Gogleddol Mariana',
    'Mozambique'                                           => 'Mosambic',
    'Republic of Mozambique'                               => 'Gweriniaeth Mozambique',
    'Mauritania'                                           => 'Mauritania',
    'Islamic Republic of Mauritania'                       => 'Gweriniaeth Islamaidd Mauritania',
    'Montserrat'                                           => 'Montserrat',
    'Martinique'                                           => 'Martinique',
    'Mauritius'                                            => 'Mauritius',
    'Republic of Mauritius'                                => 'Gweriniaeth Mauritius',
    'Malawi'                                               => 'Malawi',
    'Republic of Malawi'                                   => 'Gweriniaeth Malawi',
    'Malaysia'                                             => 'Malaysia',
    'Mayotte'                                              => 'Mayotte',
    'Namibia'                                              => 'Namibia',
    'Republic of Namibia'                                  => 'Gweriniaeth Namibia',
    'New Caledonia'                                        => 'Caledonia Newydd',
    'Niger'                                                => 'Niger',
    'Republic of the Niger'                                => 'Gweriniaeth Nigeria',
    'Norfolk Island'                                       => 'Ynys Norfolk',
    'Nigeria'                                              => 'Nigeria',
    'Federal Republic of Nigeria'                          => 'Gweriniaeth Ffederal Nigeria',
    'Nicaragua'                                            => 'Nicaragua',
    'Republic of Nicaragua'                                => 'Gweriniaeth Nicaragua',
    'Niue'                                                 => 'Niue',
    'Netherlands'                                          => 'Yr Iseldiroedd',
    'Kingdom of the Netherlands'                           => 'Brenhiniaeth yr Iseldiroedd',
    'Norway'                                               => 'Norwy',
    'Kingdom of Norway'                                    => 'Brenhiniaeth Norwy',
    'Nepal'                                                => 'Nepal',
    'Federal Democratic Republic of Nepal'                 => 'Gweriniaeth Ddemocrataidd Ffederal Nepal',
    'Nauru'                                                => 'Nauru',
    'Republic of Nauru'                                    => 'Gweriniaeth Nauru',
    'New Zealand'                                          => 'Seland Newydd',
    'Oman'                                                 => 'Oman',
    'Sultanate of Oman'                                    => 'Swltaniaeth Oman',
    'Pakistan'                                             => 'Pacistan',
    'Islamic Republic of Pakistan'                         => 'Gweriniaeth Islamaidd Pacistan',
    'Panama'                                               => 'Panama',
    'Republic of Panama'                                   => 'Gweriniaeth Panama',
    'Pitcairn'                                             => 'Pitcairn',
    'Peru'                                                 => 'Periw',
    'Republic of Peru'                                     => 'Gweriniaeth Periw',
    'Philippines'                                          => 'Y Philipinau',
    'Republic of the Philippines'                          => 'Gweriniaeth y Pilipinas',
    'Palau'                                                => 'Palau',
    'Republic of Palau'                                    => 'Gweriniaeth Palau',
    'Papua New Guinea'                                     => 'Papua Guinea Newydd',
    'Independent State of Papua New Guinea'                => 'Talaith Annibynol Papua Gini Newydd',
    'Poland'                                               => 'Gwlad Pwyl',
    'Republic of Poland'                                   => 'Gweriniaeth Gwlad Pwyl',
    'Puerto Rico'                                          => 'Puerto Rico',
    'Korea, Democratic People\'s Republic of'              => 'Korea (Gweriniaeth Democrataidd Pobl)',
    'Democratic People\'s Republic of Korea'               => 'Gweriniaeth Democratic Corëa',
    'North Korea'                                          => 'Gogledd Corea',
    'Portugal'                                             => 'Portiwgal',
    'Portuguese Republic'                                  => 'Gweriniaeth Portiwgal',
    'Paraguay'                                             => 'Paraguay',
    'Republic of Paraguay'                                 => 'Gweriniaeth Paraguay',
    'Palestine, State of'                                  => 'Palestina, Gwladwriaeth',
    'the State of Palestine'                               => 'Gwladwriaeth Palesteina',
    'French Polynesia'                                     => 'Polynesia Ffrengig',
    'Qatar'                                                => 'Qatar',
    'State of Qatar'                                       => 'Talaith Qatar',
    'Réunion'                                              => 'Réunion',
    'Romania'                                              => 'Rwmania',
    'Russian Federation'                                   => 'Ffederasiwn Rwsia',
    'Rwanda'                                               => 'Rwanda',
    'Rwandese Republic'                                    => 'Gweriniaeth Rwanda',
    'Saudi Arabia'                                         => 'Saudi Arabia',
    'Kingdom of Saudi Arabia'                              => 'Gweriniaeth Saudi Arabia',
    'Sudan'                                                => 'Y Swdan',
    'Republic of the Sudan'                                => 'Gweriniaeth y Swdan',
    'Senegal'                                              => 'Senegal',
    'Republic of Senegal'                                  => 'Gweriniaeth Senegal',
    'Singapore'                                            => 'Singapore',
    'Republic of Singapore'                                => 'Gweriniaeth Singapore',
    'South Georgia and the South Sandwich Islands'         => 'De Georgia ac Ynysoedd Sandwich y De',
    'Saint Helena, Ascension and Tristan da Cunha'         => 'Saint Helena, Dyrchafael a Tristan da Cunha',
    'Svalbard and Jan Mayen'                               => 'Svalbard a Jan Mayen',
    'Solomon Islands'                                      => 'Ynysoedd Solomon',
    'Sierra Leone'                                         => 'Sierra Leone',
    'Republic of Sierra Leone'                             => 'Gweriniaeth Sierra Leone',
    'El Salvador'                                          => 'El Salvador',
    'Republic of El Salvador'                              => 'Gweriniaeth El Salvador',
    'San Marino'                                           => 'San Marino',
    'Republic of San Marino'                               => 'Gweriniaeth San Marino',
    'Somalia'                                              => 'Somalia',
    'Federal Republic of Somalia'                          => 'Gweriniaeth Ffederal Somalia',
    'Saint Pierre and Miquelon'                            => 'Saint Pierre a Miquelon',
    'Serbia'                                               => 'Serbia',
    'Republic of Serbia'                                   => 'Gweriniaeth Serbia',
    'South Sudan'                                          => 'De Swdan',
    'Republic of South Sudan'                              => 'Gweriniaeth De Swdan',
    'Sao Tome and Principe'                                => 'Sao Tome a Principe',
    'Democratic Republic of Sao Tome and Principe'         => 'Gweriniaeth Democrataidd Sao Tome a Principe',
    'Suriname'                                             => 'Suriname',
    'Republic of Suriname'                                 => 'Gweriniaeth y Suriname',
    'Slovakia'                                             => 'Slofacia',
    'Slovak Republic'                                      => 'Gweriniaeth Slofacaidd',
    'Slovenia'                                             => 'Slofenia',
    'Republic of Slovenia'                                 => 'Gweriniaeth Slofenia',
    'Sweden'                                               => 'Sweden',
    'Kingdom of Sweden'                                    => 'Gweriniaeth y Swdan',
    'Eswatini'                                             => 'Eswatini',
    'Kingdom of Eswatini'                                  => 'Deyrnas Eswatini',
    'Sint Maarten (Dutch part)'                            => 'Sint Maarten (Rhan Iseldiraidd)',
    'Seychelles'                                           => 'Seychelles',
    'Republic of Seychelles'                               => 'Gweriniaeth y Seychelles',
    'Syrian Arab Republic'                                 => 'Gweriniaeth Arabaidd Syria',
    'Syria'                                                => 'Syria',
    'Turks and Caicos Islands'                             => 'Ynysoedd Turks a Caicos',
    'Chad'                                                 => 'Tchad',
    'Republic of Chad'                                     => 'Gweriniaeth Chad',
    'Togo'                                                 => 'Togo',
    'Togolese Republic'                                    => 'Gweriniaeth Togo',
    'Thailand'                                             => 'Gwlad Thai',
    'Kingdom of Thailand'                                  => 'Brenhiniaeth Gwlad Thai',
    'Tajikistan'                                           => 'Tajikistan',
    'Republic of Tajikistan'                               => 'Gweriniaeth Tajikistan',
    'Tokelau'                                              => 'Tokelau',
    'Turkmenistan'                                         => 'Turkmenistan',
    'Timor-Leste'                                          => 'Timor-Leste',
    'Democratic Republic of Timor-Leste'                   => 'Gweriniaeth Democratic Timor-Leste',
    'Tonga'                                                => 'Tonga',
    'Kingdom of Tonga'                                     => 'Brenhiniaeth Tonga',
    'Trinidad and Tobago'                                  => 'Trinidad a Tobago',
    'Republic of Trinidad and Tobago'                      => 'Gweriniaeth Trinidad a Tobago',
    'Tunisia'                                              => 'Twnisia',
    'Republic of Tunisia'                                  => 'Gweriniaeth Twnisia',
    'Türkiye'                                              => 'Twrci',
    'Republic of Türkiye'                                  => 'Gweriniaeth Twrci',
    'Tuvalu'                                               => 'Twfalw',
    'Taiwan, Province of China'                            => 'Taiwan, Talaith o Tseina',
    'Taiwan'                                               => 'Taiwan',
    'Tanzania, United Republic of'                         => 'Tanzania, Gweriniaeth Unedig',
    'United Republic of Tanzania'                          => 'Gweriniaeth Unedig Tanzania',
    'Tanzania'                                             => 'Tansanïa',
    'Uganda'                                               => 'Uganda',
    'Republic of Uganda'                                   => 'Gweriniaeth Uganda',
    'Ukraine'                                              => 'Wcrain',
    'United States Minor Outlying Islands'                 => 'Ynysoedd Pellennig Bychain yr Unol Daleithiau',
    'Uruguay'                                              => 'Uruguay',
    'Eastern Republic of Uruguay'                          => 'Gweriniaeth Dwyreiniol Uruguay',
    'United States'                                        => 'Yr Unol Daleithiau',
    'United States of America'                             => 'Unol Daleithiau America',
    'Uzbekistan'                                           => 'Wsbecistan',
    'Republic of Uzbekistan'                               => 'Gweriniaeth Wsbecistan',
    'Holy See (Vatican City State)'                        => 'Yr Esgobaeth Sanctaidd (Talaith Dinas y Fatican)',
    'Saint Vincent and the Grenadines'                     => 'Saint Vincent a\'r Grenadines',
    'Venezuela, Bolivarian Republic of'                    => 'Venezuela, Gweriniaeth Bolifaraidd',
    'Bolivarian Republic of Venezuela'                     => 'Gweriniaeth Bolifiaraidd Venezuela',
    'Venezuela'                                            => 'Feneswela',
    'Virgin Islands, British'                              => 'Ynysoedd Virgin, Prydeinig',
    'British Virgin Islands'                               => 'Ynysoedd Prydeinig y Wyryf',
    'Virgin Islands, U.S.'                                 => 'Ynysoedd Virgin, U.D.A.',
    'Virgin Islands of the United States'                  => 'Ynysoedd Virgin Unol Daleithiau America',
    'Viet Nam'                                             => 'Fietnam',
    'Socialist Republic of Viet Nam'                       => 'Gweriniaeth Sosialaidd Fietnam',
    'Vietnam'                                              => 'Fietnam',
    'Vanuatu'                                              => 'Vanuatu',
    'Republic of Vanuatu'                                  => 'Gweriniaeth Vanuatu',
    'Wallis and Futuna'                                    => 'Wallis a Futuna',
    'Samoa'                                                => 'Samoa',
    'Independent State of Samoa'                           => 'Talaith Annibynol Samoa',
    'Yemen'                                                => 'Yemen',
    'Republic of Yemen'                                    => 'Gweriniaeth Yemen',
    'South Africa'                                         => 'De Affrica',
    'Republic of South Africa'                             => 'Gweriniaeth De Affrica',
    'Zambia'                                               => 'Zambia',
    'Republic of Zambia'                                   => 'Gweriniaeth Zambia',
    'Zimbabwe'                                             => 'Zimbabwe',
    'Republic of Zimbabwe'                                 => 'Gweriniaeth Zimbabwe',
];
