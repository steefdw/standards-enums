<?php

/**
 * Translations in Hindi.
 *
 * @noinspection SpellCheckingInspection
 */

return [
    'Aruba'                                                => 'अरूबा',
    'Afghanistan'                                          => 'अफ़ग़ानिस्तान',
    'Islamic Republic of Afghanistan'                      => 'अफ़ग़ानिस्तान इस्लामी गणराज्य',
    'Angola'                                               => 'अंगोला',
    'Republic of Angola'                                   => 'अंगोला गणराज्य',
    'Anguilla'                                             => 'अंगुइला',
    'Åland Islands'                                        => 'ऑलैण्ड द्वीपसमूह',
    'Albania'                                              => 'अल्बानिया',
    'Republic of Albania'                                  => 'अल्बानिया गणराज्य',
    'Andorra'                                              => 'अंडोरा',
    'Principality of Andorra'                              => 'अंडोरा की रियासत',
    'United Arab Emirates'                                 => 'संयुक्त अरब अमीरात',
    'Argentina'                                            => 'अर्जेंटीना',
    'Argentine Republic'                                   => 'अर्जेंटीना गणराज्य',
    'Armenia'                                              => 'आर्मेनिया',
    'Republic of Armenia'                                  => 'आर्मेनिया गणराज्य',
    'American Samoa'                                       => 'अमेरिकी समोआ',
    'Antarctica'                                           => 'अंटार्कटिका',
    'French Southern Territories'                          => 'दक्षिणी फ्राँसिसी क्षेत्र',
    'Antigua and Barbuda'                                  => 'अण्टीगुआ और बारबूडा',
    'Australia'                                            => 'ऑस्ट्रेलिया',
    'Austria'                                              => 'ऑस्ट्रिया',
    'Republic of Austria'                                  => 'ऑस्ट्रिया गणराज्य',
    'Azerbaijan'                                           => 'अज़रबैजान',
    'Republic of Azerbaijan'                               => 'अज़रबैजान गणराज्य',
    'Burundi'                                              => 'बुरुंडी',
    'Republic of Burundi'                                  => 'बुरुंडी गणराज्य',
    'Belgium'                                              => 'बेल्जियम',
    'Kingdom of Belgium'                                   => 'बेल्जियम का साम्राज्य',
    'Benin'                                                => 'बेनिन',
    'Republic of Benin'                                    => 'बेनिन गणराज्य',
    'Bonaire, Sint Eustatius and Saba'                     => 'बोनैर, सिंट यूस्टेटीयस एंड साबा',
    'Burkina Faso'                                         => 'बुर्किना फासो',
    'Bangladesh'                                           => 'बांग्लादेश',
    'People\'s Republic of Bangladesh'                     => 'बांग्लादेश जनवादी गणराज्य',
    'Bulgaria'                                             => 'बुल्गारिया',
    'Republic of Bulgaria'                                 => 'बुल्गारिया गणराज्य',
    'Bahrain'                                              => 'बहरीन',
    'Kingdom of Bahrain'                                   => 'बहरीन की सल्तनत',
    'Bahamas'                                              => 'बहामास',
    'Commonwealth of the Bahamas'                          => 'बहामास का राष्ट्रमंडल',
    'Bosnia and Herzegovina'                               => 'बोस्निया और हर्जेगोविना',
    'Republic of Bosnia and Herzegovina'                   => 'बोस्निया और हर्जेगोविना गणराज्य',
    'Saint Barthélemy'                                     => 'सेंट बार्थेलेमी',
    'Belarus'                                              => 'बेलारूस',
    'Republic of Belarus'                                  => 'बेलारूस गणराज्य',
    'Belize'                                               => 'बेलीज़',
    'Bermuda'                                              => 'बरमूडा',
    'Bolivia, Plurinational State of'                      => 'बोलीविया, बहुराष्ट्रीय राज्य',
    'Plurinational State of Bolivia'                       => 'बोलीविया का बहुराष्ट्रीय राज्य',
    'Bolivia'                                              => 'बोलीविया',
    'Brazil'                                               => 'ब्राजील',
    'Federative Republic of Brazil'                        => 'ब्राजील संघीय गणराज्य',
    'Barbados'                                             => 'बारबाडोस',
    'Brunei Darussalam'                                    => 'ब्रुनेई दरउस्सलाम',
    'Bhutan'                                               => 'भूटान',
    'Kingdom of Bhutan'                                    => 'भूटान का साम्राज्य',
    'Bouvet Island'                                        => 'बोउवेट आइलैंड',
    'Botswana'                                             => 'बोत्सवाना',
    'Republic of Botswana'                                 => 'बोत्सवाना गणराज्य',
    'Central African Republic'                             => 'मध्य अफ़्रीकी गणराज्य',
    'Canada'                                               => 'कनाडा',
    'Cocos (Keeling) Islands'                              => 'कोकोस (कीलिंग) द्वीपसमूह',
    'Switzerland'                                          => 'स्विट्ज़रलैंड',
    'Swiss Confederation'                                  => 'स्विस परिसंघ',
    'Chile'                                                => 'चिली',
    'Republic of Chile'                                    => 'चिली गणराज्य',
    'China'                                                => 'चीन',
    'People\'s Republic of China'                          => 'चीनी जनवादी गणराज्य',
    'Côte d\'Ivoire'                                       => 'कोटे डी आइवर',
    'Republic of Côte d\'Ivoire'                           => 'कोटे डी आइवर गणराज्य',
    'Cameroon'                                             => 'कैमरुन',
    'Republic of Cameroon'                                 => 'कैमरून गणराज्य',
    'Congo, The Democratic Republic of the'                => 'कांगो, लोकतांत्रिक गणराज्य',
    'Congo'                                                => 'कॉंगो',
    'Republic of the Congo'                                => 'कांगो गणराज्य',
    'Cook Islands'                                         => 'कुक द्वीपसमूह',
    'Colombia'                                             => 'कोलंबिया',
    'Republic of Colombia'                                 => 'कोलंबिया गणराज्य',
    'Comoros'                                              => 'कोमोरोस',
    'Union of the Comoros'                                 => 'कोमोरोस का संघ',
    'Cabo Verde'                                           => 'काबो वर्डे',
    'Republic of Cabo Verde'                               => 'काबो वर्डे गणराज्य',
    'Costa Rica'                                           => 'कोस्टा रीका',
    'Republic of Costa Rica'                               => 'कोस्टा रिका गणराज्य',
    'Cuba'                                                 => 'क्यूबा',
    'Republic of Cuba'                                     => 'क्यूबा गणराज्य',
    'Curaçao'                                              => 'कुराकाओ',
    'Christmas Island'                                     => 'क्रिसमस द्वीप',
    'Cayman Islands'                                       => 'केमन द्वीपसमूह',
    'Cyprus'                                               => 'साइप्रस',
    'Republic of Cyprus'                                   => 'साइप्रस गणराज्य',
    'Czechia'                                              => 'चेकिया',
    'Czech Republic'                                       => 'चेक गणराज्य',
    'Germany'                                              => 'जर्मनी',
    'Federal Republic of Germany'                          => 'जर्मनी संघीय गणराज्य',
    'Djibouti'                                             => 'जिबूती',
    'Republic of Djibouti'                                 => 'जिबूती गणराज्य',
    'Dominica'                                             => 'डोमिनिका',
    'Commonwealth of Dominica'                             => 'डोमिनिका का राष्ट्रमंडल',
    'Denmark'                                              => 'डेनमार्क',
    'Kingdom of Denmark'                                   => 'डेनमार्क का साम्राज्य',
    'Dominican Republic'                                   => 'डोमिनिकन गणराज्य',
    'Algeria'                                              => 'अल्जीरिया',
    'People\'s Democratic Republic of Algeria'             => 'अल्जीरिया का लोक लोकतांत्रिक गणराज्य',
    'Ecuador'                                              => 'इक्वेडोर',
    'Republic of Ecuador'                                  => 'इक्वेडोर गणराज्य',
    'Egypt'                                                => 'मिस्र',
    'Arab Republic of Egypt'                               => 'मिस्र का अरब गणराज्य',
    'Eritrea'                                              => 'इरीट्रिया',
    'the State of Eritrea'                                 => 'इरीट्रिया राज्य',
    'Western Sahara'                                       => 'पश्चिमी सहारा',
    'Spain'                                                => 'स्पेन',
    'Kingdom of Spain'                                     => 'स्पेन का साम्राज्य',
    'Estonia'                                              => 'एस्टोनिया',
    'Republic of Estonia'                                  => 'एस्टोनिया गणराज्य',
    'Ethiopia'                                             => 'इथियोपिया',
    'Federal Democratic Republic of Ethiopia'              => 'इथियोपिया का संघीय लोकतांत्रिक गणराज्य',
    'Finland'                                              => 'फिनलैंड',
    'Republic of Finland'                                  => 'फिनलैंड गणराज्य',
    'Fiji'                                                 => 'फिजी',
    'Republic of Fiji'                                     => 'फिजी गणराज्य',
    'Falkland Islands (Malvinas)'                          => 'फॉकलैंड आइलैंड्स (मालविनास)',
    'France'                                               => 'फ्रांस',
    'French Republic'                                      => 'फ़्रेंच गणराज्य',
    'Faroe Islands'                                        => 'फ़रो द्वीप समूह',
    'Micronesia, Federated States of'                      => 'माइक्रोनेशिया, संघीय राज्य',
    'Federated States of Micronesia'                       => 'माइक्रोनेशिया के संघीय राज्य',
    'Gabon'                                                => 'गबॉन',
    'Gabonese Republic'                                    => 'गैबोनीज़ गणराज्य',
    'United Kingdom'                                       => 'यूनाइटेड किंगडम',
    'United Kingdom of Great Britain and Northern Ireland' => 'ग्रेट ब्रिटेन और उत्तरी आयरलैंड का यूनाइटेड किंगडम',
    'Georgia'                                              => 'जॉर्जिया',
    'Guernsey'                                             => 'ग्वेर्नसे',
    'Ghana'                                                => 'घाना',
    'Republic of Ghana'                                    => 'घाना गणराज्य',
    'Gibraltar'                                            => 'जिब्राल्टर',
    'Guinea'                                               => 'गिनी',
    'Republic of Guinea'                                   => 'गिनी गणराज्य',
    'Guadeloupe'                                           => 'गुआदेलूप',
    'Gambia'                                               => 'गाम्बिया',
    'Republic of the Gambia'                               => 'गाम्बिया गणराज्य',
    'Guinea-Bissau'                                        => 'गिनी-बिसाऊ',
    'Republic of Guinea-Bissau'                            => 'गिनी-बिसाऊ गणराज्य',
    'Equatorial Guinea'                                    => 'भूमध्यरेखीय गिनी',
    'Republic of Equatorial Guinea'                        => 'भूमध्यरेखीय गिनी गणराज्य',
    'Greece'                                               => 'यूनान',
    'Hellenic Republic'                                    => 'हेलेनिक गणराज्य',
    'Grenada'                                              => 'ग्रेनाडा',
    'Greenland'                                            => 'ग्रीनलैण्ड',
    'Guatemala'                                            => 'ग्वाटेमाला',
    'Republic of Guatemala'                                => 'ग्वाटेमाला गणराज्य',
    'French Guiana'                                        => 'फ्रेंच गयाना',
    'Guam'                                                 => 'गुआम',
    'Guyana'                                               => 'गयाना',
    'Republic of Guyana'                                   => 'गुयाना गणराज्य',
    'Hong Kong'                                            => 'हांगकांग',
    'Hong Kong Special Administrative Region of China'     => 'हांगकांग चीन का विशेष प्रशासनिक क्षेत्र',
    'Heard Island and McDonald Islands'                    => 'हर्ड द्वीप और मैकडोनाल्ड द्वीप समूह',
    'Honduras'                                             => 'होंडुरस',
    'Republic of Honduras'                                 => 'होंडुरास गणराज्य',
    'Croatia'                                              => 'क्रोएशिया',
    'Republic of Croatia'                                  => 'क्रोएशिया गणराज्य',
    'Haiti'                                                => 'हैती',
    'Republic of Haiti'                                    => 'हैती गणराज्य',
    'Hungary'                                              => 'हंगरी',
    'Indonesia'                                            => 'इंडोनेशिया',
    'Republic of Indonesia'                                => 'इंडोनेशिया गणराज्य',
    'Isle of Man'                                          => 'आइल ऑफ मैन',
    'India'                                                => 'भारत',
    'Republic of India'                                    => 'भारतीय गणराज्य',
    'British Indian Ocean Territory'                       => 'ब्रिटिश हिंद महासागर क्षेत्र',
    'Ireland'                                              => 'आयरलैण्ड',
    'Iran, Islamic Republic of'                            => 'ईरान, इस्लामिक गणराज्य',
    'Islamic Republic of Iran'                             => 'ईरान की इस्लामी गणराज्य',
    'Iran'                                                 => 'ईरान',
    'Iraq'                                                 => 'इराक़',
    'Republic of Iraq'                                     => 'इराक गणराज्य',
    'Iceland'                                              => 'आइसलैंड',
    'Republic of Iceland'                                  => 'आइसलैंड गणराज्य',
    'Israel'                                               => 'इज़राइल',
    'State of Israel'                                      => 'इज़राइल राज्य',
    'Italy'                                                => 'इटली',
    'Italian Republic'                                     => 'इतालवी गणराज्य',
    'Jamaica'                                              => 'जमैका',
    'Jersey'                                               => 'जर्सी',
    'Jordan'                                               => 'जॉर्डन',
    'Hashemite Kingdom of Jordan'                          => 'जॉर्डन का हाशमाइट साम्राज्य',
    'Japan'                                                => 'जापान',
    'Kazakhstan'                                           => 'कजाकिस्तान',
    'Republic of Kazakhstan'                               => 'कजाकिस्तान गणराज्य',
    'Kenya'                                                => 'केन्या',
    'Republic of Kenya'                                    => 'केन्या गणराज्य',
    'Kyrgyzstan'                                           => 'किर्गिज़स्तान',
    'Kyrgyz Republic'                                      => 'किर्गिज गणराज्य',
    'Cambodia'                                             => 'कंबोडिया',
    'Kingdom of Cambodia'                                  => 'कंबोडिया का साम्राज्य',
    'Kiribati'                                             => 'किरिबाती',
    'Republic of Kiribati'                                 => 'किरिबाती गणराज्य',
    'Saint Kitts and Nevis'                                => 'सन्त किट्स और नेविस',
    'Korea, Republic of'                                   => 'कोरिया गणराज्य',
    'South Korea'                                          => 'दक्षिण कोरिया',
    'Kuwait'                                               => 'कुवैत',
    'State of Kuwait'                                      => 'कुवैत राज्य',
    'Lao People\'s Democratic Republic'                    => 'लाओस लोक लोकतांत्रिक गणराज्य',
    'Laos'                                                 => 'लाओस',
    'Lebanon'                                              => 'लेबनान',
    'Lebanese Republic'                                    => 'लेबनानी गणराज्य',
    'Liberia'                                              => 'लाइबेरिया',
    'Republic of Liberia'                                  => 'लाइबेरिया गणराज्य',
    'Libya'                                                => 'लीबिया',
    'Saint Lucia'                                          => 'सेंट लूसिया',
    'Liechtenstein'                                        => 'लिकटेंस्टीन',
    'Principality of Liechtenstein'                        => 'लिकटेंस्टीन की रियासत',
    'Sri Lanka'                                            => 'श्री लंका',
    'Democratic Socialist Republic of Sri Lanka'           => 'श्रीलंका लोकतांत्रिक समाजवादी गणराज्य',
    'Lesotho'                                              => 'लेसोथो',
    'Kingdom of Lesotho'                                   => 'लेसोथो साम्राज्य',
    'Lithuania'                                            => 'लिथुआनिया',
    'Republic of Lithuania'                                => 'लिथुआनिया गणराज्य',
    'Luxembourg'                                           => 'लक्ज़मबर्ग',
    'Grand Duchy of Luxembourg'                            => 'लक्ज़मबर्ग की ग्रैंड डची',
    'Latvia'                                               => 'लातविया',
    'Republic of Latvia'                                   => 'लातविया गणराज्य',
    'Macao'                                                => 'मकाउ',
    'Macao Special Administrative Region of China'         => 'चीन का मकाउ विशेष प्रशासनिक क्षेत्र',
    'Saint Martin (French part)'                           => 'सेंट मार्टिन (फ्रेंच भाग)',
    'Morocco'                                              => 'मोरक्को',
    'Kingdom of Morocco'                                   => 'मोरक्को का साम्राज्य',
    'Monaco'                                               => 'मोनैको',
    'Principality of Monaco'                               => 'मोनाको की रियासत',
    'Moldova, Republic of'                                 => 'मोल्दोवा, गणराज्य',
    'Republic of Moldova'                                  => 'मोल्दोवा गणराज्य',
    'Moldova'                                              => 'मोल्दोवा',
    'Madagascar'                                           => 'मेडागास्कर',
    'Republic of Madagascar'                               => 'मेडागास्कर गणराज्य',
    'Maldives'                                             => 'मालदीव',
    'Republic of Maldives'                                 => 'मालदीव गणराज्य',
    'Mexico'                                               => 'मेक्सिको',
    'United Mexican States'                                => 'संयुक्त मैक्सिकन राज्य',
    'Marshall Islands'                                     => 'मार्शल द्वीपसमूह',
    'Republic of the Marshall Islands'                     => 'मार्शल द्वीपसमूह गणराज्य',
    'North Macedonia'                                      => 'उत्तर मैसेडोनिया',
    'Republic of North Macedonia'                          => 'उत्तर मैसेडोनिया गणराज्य',
    'Mali'                                                 => 'माली',
    'Republic of Mali'                                     => 'माली गणराज्य',
    'Malta'                                                => 'माल्टा',
    'Republic of Malta'                                    => 'माल्टा गणराज्य',
    'Myanmar'                                              => 'म्यांमार',
    'Republic of Myanmar'                                  => 'म्यांमार गणराज्य',
    'Montenegro'                                           => 'मॉन्टेनीग्रो',
    'Mongolia'                                             => 'मंगोलिया',
    'Northern Mariana Islands'                             => 'उत्तरी मारियाना द्वीप',
    'Commonwealth of the Northern Mariana Islands'         => 'उत्तरी मारियाना द्वीप समूह का राष्ट्रमंडल',
    'Mozambique'                                           => 'मोज़ाम्बीक',
    'Republic of Mozambique'                               => 'मोज़ाम्बिक गणराज्य',
    'Mauritania'                                           => 'मॉरीतानिया',
    'Islamic Republic of Mauritania'                       => 'मॉरिटानिया इस्लामी गणराज्य',
    'Montserrat'                                           => 'मॉण्टसेराट',
    'Martinique'                                           => 'मार्टीनिक',
    'Mauritius'                                            => 'मॉरिशस',
    'Republic of Mauritius'                                => 'मॉरीशस गणराज्य',
    'Malawi'                                               => 'मलावी',
    'Republic of Malawi'                                   => 'मलावी गणराज्य',
    'Malaysia'                                             => 'मलेशिया',
    'Mayotte'                                              => 'मेयोट',
    'Namibia'                                              => 'नामीबिया',
    'Republic of Namibia'                                  => 'नामीबिया गणराज्य',
    'New Caledonia'                                        => 'न्यू कैलिडोनिया',
    'Niger'                                                => 'नाइजर',
    'Republic of the Niger'                                => 'नाइजर गणराज्य',
    'Norfolk Island'                                       => 'नॉर्फ़ोक द्वीप',
    'Nigeria'                                              => 'नाइजीरिया',
    'Federal Republic of Nigeria'                          => 'नाइजीरिया संघीय गणराज्य',
    'Nicaragua'                                            => 'निकारागुआ',
    'Republic of Nicaragua'                                => 'निकारागुआ गणराज्य',
    'Niue'                                                 => 'निउए',
    'Netherlands'                                          => 'नीदरलैण्ड',
    'Kingdom of the Netherlands'                           => 'नीदरलैंड का साम्राज्य',
    'Norway'                                               => 'नॉर्वे',
    'Kingdom of Norway'                                    => 'नॉर्वे का साम्राज्य',
    'Nepal'                                                => 'नेपाल',
    'Federal Democratic Republic of Nepal'                 => 'नेपाल संघीय लोकतांत्रिक गणराज्य',
    'Nauru'                                                => 'नाउरू',
    'Republic of Nauru'                                    => 'नाउरू गणराज्य',
    'New Zealand'                                          => 'न्यूज़ीलैण्ड',
    'Oman'                                                 => 'ओमान',
    'Sultanate of Oman'                                    => 'ओमान की सल्तनत',
    'Pakistan'                                             => 'पाकिस्तान',
    'Islamic Republic of Pakistan'                         => 'इस्लामी गणतंत्र पाकिस्तान',
    'Panama'                                               => 'पनामा',
    'Republic of Panama'                                   => 'पनामा गणराज्य',
    'Pitcairn'                                             => 'पिटकेर्न',
    'Peru'                                                 => 'पेरू',
    'Republic of Peru'                                     => 'पेरू गणराज्य',
    'Philippines'                                          => 'फिलीपींस',
    'Republic of the Philippines'                          => 'फिलीपींस गणराज्य',
    'Palau'                                                => 'पलाउ',
    'Republic of Palau'                                    => 'पलाऊ गणराज्य',
    'Papua New Guinea'                                     => 'पापुआ न्यू गिनी',
    'Independent State of Papua New Guinea'                => 'पापुआ न्यू गिनी का स्वतंत्र राज्य',
    'Poland'                                               => 'पोलैंड',
    'Republic of Poland'                                   => 'पोलैंड गणराज्य',
    'Puerto Rico'                                          => 'प्युर्तो रिको',
    'Korea, Democratic People\'s Republic of'              => 'कोरिया, लोकतांत्रिक जनवादी गणराज्य',
    'Democratic People\'s Republic of Korea'               => 'कोरिया लोकतांत्रिक जनवादी गणराज्य',
    'North Korea'                                          => 'उत्तर कोरिया',
    'Portugal'                                             => 'पुर्तगाल',
    'Portuguese Republic'                                  => 'पुर्तगाली गणराज्य',
    'Paraguay'                                             => 'पैराग्वे',
    'Republic of Paraguay'                                 => 'पराग्वे गणराज्य',
    'Palestine, State of'                                  => 'फ़िलिस्तीन, राज्य',
    'the State of Palestine'                               => 'फ़िलिस्तीन राज्य',
    'French Polynesia'                                     => 'फ़्रेंच पोलिनेशिया',
    'Qatar'                                                => 'कतर',
    'State of Qatar'                                       => 'कतर राज्य',
    'Réunion'                                              => 'रियूनियन',
    'Romania'                                              => 'रोमानिया',
    'Russian Federation'                                   => 'रूसी संघ',
    'Rwanda'                                               => 'रवांडा',
    'Rwandese Republic'                                    => 'रवांडा गणराज्य',
    'Saudi Arabia'                                         => 'सउदी अरब',
    'Kingdom of Saudi Arabia'                              => 'सऊदी अरब का साम्राज्य',
    'Sudan'                                                => 'सूडान',
    'Republic of the Sudan'                                => 'सूडान गणराज्य',
    'Senegal'                                              => 'सेनेगल',
    'Republic of Senegal'                                  => 'सेनेगल गणराज्य',
    'Singapore'                                            => 'सिंगापुर',
    'Republic of Singapore'                                => 'सिंगापुर गणराज्य',
    'South Georgia and the South Sandwich Islands'         => 'दक्षिण जॉर्जिया और दक्षिण सैंडविच द्वीप समूह',
    'Saint Helena, Ascension and Tristan da Cunha'         => 'सेंट हेलेना, असेंशन और त्रिस्तान दा कुन्हा',
    'Svalbard and Jan Mayen'                               => 'स्वालबार्ड एन्ड जैन माएन',
    'Solomon Islands'                                      => 'सोलोमन द्वीपसमूह',
    'Sierra Leone'                                         => 'सिएरा लियोन',
    'Republic of Sierra Leone'                             => 'सिएरा लियोन गणराज्य',
    'El Salvador'                                          => 'अल साल्वाडोर',
    'Republic of El Salvador'                              => 'अल साल्वाडोर गणराज्य',
    'San Marino'                                           => 'सान मारिनो',
    'Republic of San Marino'                               => 'सैन मैरिनो गणराज्य',
    'Somalia'                                              => 'सोमालिया',
    'Federal Republic of Somalia'                          => 'सोमालिया संघीय गणराज्य',
    'Saint Pierre and Miquelon'                            => 'साँ-प्येर और मीकेलों',
    'Serbia'                                               => 'सर्बिया',
    'Republic of Serbia'                                   => 'सर्बिया गणराज्य',
    'South Sudan'                                          => 'दक्षिण सूडान',
    'Republic of South Sudan'                              => 'दक्षिण सूडान गणराज्य',
    'Sao Tome and Principe'                                => 'साओ तोमे और प्रिन्सिपी',
    'Democratic Republic of Sao Tome and Principe'         => 'साओ टोम और प्रिंसिपे लोकतांत्रिक गणराज्य',
    'Suriname'                                             => 'सूरीनाम',
    'Republic of Suriname'                                 => 'सूरीनाम गणराज्य',
    'Slovakia'                                             => 'स्लोवाकिया',
    'Slovak Republic'                                      => 'स्लोवाक गणराज्य',
    'Slovenia'                                             => 'स्लोवेनिया',
    'Republic of Slovenia'                                 => 'स्लोवेनिया गणराज्य',
    'Sweden'                                               => 'स्वीडन',
    'Kingdom of Sweden'                                    => 'स्वीडन का साम्राज्य',
    'Eswatini'                                             => 'इस्वातिनी',
    'Kingdom of Eswatini'                                  => 'इस्वातिनी का साम्राज्य',
    'Sint Maarten (Dutch part)'                            => 'सेंट मार्टिन (डच भाग)',
    'Seychelles'                                           => 'सेशेल्स',
    'Republic of Seychelles'                               => 'सेशेल्स गणराज्य',
    'Syrian Arab Republic'                                 => 'सीरियाई अरब गणराज्य',
    'Syria'                                                => 'सीरिया',
    'Turks and Caicos Islands'                             => 'तुर्क और केकोस द्वीपसमूह',
    'Chad'                                                 => 'चाड',
    'Republic of Chad'                                     => 'चाड गणराज्य',
    'Togo'                                                 => 'टोगो',
    'Togolese Republic'                                    => 'टोगोलिस गणराज्य',
    'Thailand'                                             => 'थाईलैण्ड',
    'Kingdom of Thailand'                                  => 'थाईलैंड का साम्राज्य',
    'Tajikistan'                                           => 'ताजिकिस्तान',
    'Republic of Tajikistan'                               => 'ताजिकिस्तान गणराज्य',
    'Tokelau'                                              => 'टोकेलाऊ',
    'Turkmenistan'                                         => 'तुर्कमेनिस्तान',
    'Timor-Leste'                                          => 'तिमोर-लेस्टे',
    'Democratic Republic of Timor-Leste'                   => 'तिमोर-लेस्ते लोकतांत्रिक गणराज्य',
    'Tonga'                                                => 'टोंगा',
    'Kingdom of Tonga'                                     => 'टोंगा साम्राज्य',
    'Trinidad and Tobago'                                  => 'त्रिनिदाद और टोबैगो',
    'Republic of Trinidad and Tobago'                      => 'त्रिनिदाद और टोबैगो गणराज्य',
    'Tunisia'                                              => 'ट्यूनीशिया',
    'Republic of Tunisia'                                  => 'ट्यूनीशिया गणराज्य',
    'Türkiye'                                              => 'तुर्किये',
    'Republic of Türkiye'                                  => 'तुर्किये गणराज्य',
    'Tuvalu'                                               => 'तुवालू',
    'Taiwan, Province of China'                            => 'ताइवान, चीन प्रांत',
    'Taiwan'                                               => 'ताइवान',
    'Tanzania, United Republic of'                         => 'तंजानिया, संयुक्त गणराज्य',
    'United Republic of Tanzania'                          => 'संयुक्त गणराज्य तंजानिया',
    'Tanzania'                                             => 'तंजानिया',
    'Uganda'                                               => 'युगांडा',
    'Republic of Uganda'                                   => 'युगांडा गणराज्य',
    'Ukraine'                                              => 'युक्रेन',
    'United States Minor Outlying Islands'                 => 'संयुक्त राज्य अमेरिका के छोटे दूरस्थ द्वीपसमूह',
    'Uruguay'                                              => 'उरुग्वे',
    'Eastern Republic of Uruguay'                          => 'उरुग्वे का पूर्वी गणराज्य',
    'United States'                                        => 'संयुक्त राज्य',
    'United States of America'                             => 'संयुक्त राज्य अमेरिका',
    'Uzbekistan'                                           => 'उज़्बेकिस्तान',
    'Republic of Uzbekistan'                               => 'उज़्बेकिस्तान गणराज्य',
    'Holy See (Vatican City State)'                        => 'होली सी (वेटिकन सिटी राज्य)',
    'Saint Vincent and the Grenadines'                     => 'सेंट विंसेंट और ग्रेनेडाइंस',
    'Venezuela, Bolivarian Republic of'                    => 'वेनेजुएला, बोलिवेरियन गणराज्य',
    'Bolivarian Republic of Venezuela'                     => 'वेनेजुएला का बोलिवेरियन गणराज्य',
    'Venezuela'                                            => 'वेनेजुएला',
    'Virgin Islands, British'                              => 'वर्जिन द्वीप समूह, ब्रिटिश',
    'British Virgin Islands'                               => 'ब्रिटिश वर्जिन द्वीपसमूह',
    'Virgin Islands, U.S.'                                 => 'वर्जिन द्वीप समूह, यू.एस।',
    'Virgin Islands of the United States'                  => 'संयुक्त राज्य अमेरिका के वर्जिन द्वीप समूह',
    'Viet Nam'                                             => 'वियतनाम',
    'Socialist Republic of Viet Nam'                       => 'वियतनाम समाजवादी गणराज्य',
    'Vietnam'                                              => 'वियतनाम',
    'Vanuatu'                                              => 'वानुअतु',
    'Republic of Vanuatu'                                  => 'वानुअतु गणराज्य',
    'Wallis and Futuna'                                    => 'वालिस और फ्यूटुना',
    'Samoa'                                                => 'समोआ',
    'Independent State of Samoa'                           => 'समोआ का स्वतंत्र राज्य',
    'Yemen'                                                => 'यमन',
    'Republic of Yemen'                                    => 'यमन गणराज्य',
    'South Africa'                                         => 'दक्षिण अफ़्रीका',
    'Republic of South Africa'                             => 'दक्षिण अफ्रिकीय गणतंत्र',
    'Zambia'                                               => 'जाम्बिया',
    'Republic of Zambia'                                   => 'जाम्बिया गणराज्य',
    'Zimbabwe'                                             => 'जिम्बाब्वे',
    'Republic of Zimbabwe'                                 => 'जिम्बाब्वे गणराज्य',
];
