<?php

/**
 * Translations in Occitan (post 1500); Provençal.
 *
 * @noinspection SpellCheckingInspection
 */

return [
    'Aruba'                                                => 'Aruba',
    'Afghanistan'                                          => 'Afganistan',
    'Islamic Republic of Afghanistan'                      => 'Republica Dominicana',
    'Angola'                                               => 'Angòla',
    'Republic of Angola'                                   => 'Republica d’Angòla',
    'Anguilla'                                             => 'Anguilla',
    'Åland Islands'                                        => 'llas Åland',
    'Albania'                                              => 'Albania',
    'Republic of Albania'                                  => 'Republica d\'Albania',
    'Andorra'                                              => 'Andòrra',
    'Principality of Andorra'                              => 'Principat d\'Andòrra',
    'United Arab Emirates'                                 => 'Emirats Arabs Units',
    'Argentina'                                            => 'Argentina',
    'Argentine Republic'                                   => 'Republica argentina',
    'Armenia'                                              => 'Armenia',
    'Republic of Armenia'                                  => 'Republicar d\'Armenia',
    'American Samoa'                                       => 'Samoa americana',
    'Antarctica'                                           => 'Antartica',
    'French Southern Territories'                          => 'Tèrras australas francesas',
    'Antigua and Barbuda'                                  => 'Antigua e Barbuda',
    'Australia'                                            => 'Australia',
    'Austria'                                              => 'Àustria',
    'Republic of Austria'                                  => 'Republica d\'Àustria',
    'Azerbaijan'                                           => 'Azerbaitjan',
    'Republic of Azerbaijan'                               => 'Republica d’Azerbaitjan',
    'Burundi'                                              => 'Burundi',
    'Republic of Burundi'                                  => 'Republica de Burundi',
    'Belgium'                                              => 'Belgica',
    'Kingdom of Belgium'                                   => 'Reialme de Belgica',
    'Benin'                                                => 'Benin',
    'Republic of Benin'                                    => 'Republica del Benin',
    'Bonaire, Sint Eustatius and Saba'                     => 'Bonaire, Sant Eustaqui e Saba',
    'Burkina Faso'                                         => 'Burkina Faso',
    'Bangladesh'                                           => 'Bangladèsh',
    'People\'s Republic of Bangladesh'                     => 'Republica populara de Bangladèsh',
    'Bulgaria'                                             => 'Bulgaria',
    'Republic of Bulgaria'                                 => 'Republica de Bulgaria',
    'Bahrain'                                              => 'Barein',
    'Kingdom of Bahrain'                                   => 'Reialme de Bahrain',
    'Bahamas'                                              => 'Bahamas',
    'Commonwealth of the Bahamas'                          => 'Commonwealth de las Bahamas',
    'Bosnia and Herzegovina'                               => 'Bòsnia e Ercegovina',
    'Republic of Bosnia and Herzegovina'                   => 'Republica de Bòsnia e Ercegovina',
    'Saint Barthélemy'                                     => 'Sant Bertomieu',
    'Belarus'                                              => 'Bielorussia',
    'Republic of Belarus'                                  => 'Republica de Bielorussia',
    'Belize'                                               => 'Belize',
    'Bermuda'                                              => 'Bermudas',
    'Bolivia, Plurinational State of'                      => 'Bolívia, Estat plurinacional de',
    'Plurinational State of Bolivia'                       => 'Estat plurinacional de Bolívia',
    'Bolivia'                                              => 'Bolivia',
    'Brazil'                                               => 'Brasil',
    'Federative Republic of Brazil'                        => 'Republica federativa de Brasil',
    'Barbados'                                             => 'Barbada',
    'Brunei Darussalam'                                    => 'Brunei',
    'Bhutan'                                               => 'Botan',
    'Kingdom of Bhutan'                                    => 'Reialme de Botan',
    'Bouvet Island'                                        => 'Illa Bouvet',
    'Botswana'                                             => 'Botswana',
    'Republic of Botswana'                                 => 'Republica de Botswana',
    'Central African Republic'                             => 'Republica de Centrafrica',
    'Canada'                                               => 'Canadà',
    'Cocos (Keeling) Islands'                              => 'Illas Cocos (Keeling)',
    'Switzerland'                                          => 'Soïssa',
    'Swiss Confederation'                                  => 'Confederacion Soïssa',
    'Chile'                                                => 'Chile',
    'Republic of Chile'                                    => 'Republica del Chile',
    'China'                                                => 'China',
    'People\'s Republic of China'                          => 'Republica populara de China',
    'Côte d\'Ivoire'                                       => 'Còsta d\'Evòri',
    'Republic of Côte d\'Ivoire'                           => 'Republica de Còsta d\'Evòri',
    'Cameroon'                                             => 'Cameron',
    'Republic of Cameroon'                                 => 'Republica de Cameron',
    'Congo, The Democratic Republic of the'                => 'Còngo, Republica Democratica de',
    'Congo'                                                => 'Còngo',
    'Republic of the Congo'                                => 'Republica de Còngo',
    'Cook Islands'                                         => 'Illas Cook',
    'Colombia'                                             => 'Colombia',
    'Republic of Colombia'                                 => 'Republica de Colómbia',
    'Comoros'                                              => 'Comòras',
    'Union of the Comoros'                                 => 'Union de las Comòras',
    'Cabo Verde'                                           => 'Cap Verd',
    'Republic of Cabo Verde'                               => 'Republica del Cap Verd',
    'Costa Rica'                                           => 'Còsta Rica',
    'Republic of Costa Rica'                               => 'Republica de la Còsta Rica',
    'Cuba'                                                 => 'Cuba',
    'Republic of Cuba'                                     => 'Republica de Cuba',
    'Curaçao'                                              => 'Curaçao',
    'Christmas Island'                                     => 'Illa Cristmas',
    'Cayman Islands'                                       => 'Illas Caiman',
    'Cyprus'                                               => 'Chipre',
    'Republic of Cyprus'                                   => 'Republica de Chipre',
    'Czechia'                                              => 'Chequia',
    'Czech Republic'                                       => 'Republica chèca',
    'Germany'                                              => 'Alemanha',
    'Federal Republic of Germany'                          => 'Republica federala d’Alemanha',
    'Djibouti'                                             => 'Jiboti',
    'Republic of Djibouti'                                 => 'Republica de Jiboti',
    'Dominica'                                             => 'Dominica',
    'Commonwealth of Dominica'                             => 'Commonwealth de la Dominica',
    'Denmark'                                              => 'Danemarc',
    'Kingdom of Denmark'                                   => 'Reialme de Danemarc',
    'Dominican Republic'                                   => 'Republica Dominicana',
    'Algeria'                                              => 'Argeria',
    'People\'s Democratic Republic of Algeria'             => 'Republica Argeriana Democratica e Populara',
    'Ecuador'                                              => 'Eqüator',
    'Republic of Ecuador'                                  => 'Republica de l\'Eqüator',
    'Egypt'                                                => 'Egipte',
    'Arab Republic of Egypt'                               => 'Republica araba d’Egipte',
    'Eritrea'                                              => 'Eritrèa',
    'the State of Eritrea'                                 => 'l’Estat d’Eritrèa',
    'Western Sahara'                                       => 'Sahara occidental',
    'Spain'                                                => 'Espanha',
    'Kingdom of Spain'                                     => 'Reialme d\'Espanha',
    'Estonia'                                              => 'Estònia',
    'Republic of Estonia'                                  => 'Republica d\'Estònia',
    'Ethiopia'                                             => 'Etiopia',
    'Federal Democratic Republic of Ethiopia'              => 'Republica Democratica Federala d\'Etiopia',
    'Finland'                                              => 'Finlàndia',
    'Republic of Finland'                                  => 'Republica de Finlàndia',
    'Fiji'                                                 => 'Fiji',
    'Republic of Fiji'                                     => 'Republica de Fiji',
    'Falkland Islands (Malvinas)'                          => 'Illas Falkand (Malvinas)',
    'France'                                               => 'França',
    'French Republic'                                      => 'Republica francesa',
    'Faroe Islands'                                        => 'Illas Feròe',
    'Micronesia, Federated States of'                      => 'Micronesia, Estats Federats de',
    'Federated States of Micronesia'                       => 'Estats federats de Micronesia',
    'Gabon'                                                => 'Gabon',
    'Gabonese Republic'                                    => 'Republica gabonesa',
    'United Kingdom'                                       => 'Reialme Unit',
    'United Kingdom of Great Britain and Northern Ireland' => 'Reialme Unit de Grand Bretanha e d’Irlanda del Nòrd',
    'Georgia'                                              => 'Georgia',
    'Guernsey'                                             => 'Guernesey',
    'Ghana'                                                => 'Gana',
    'Republic of Ghana'                                    => 'Republica del Gana',
    'Gibraltar'                                            => 'Gibartar',
    'Guinea'                                               => 'Guinèa',
    'Republic of Guinea'                                   => 'Republica de Guinèa',
    'Guadeloupe'                                           => 'Guadalope',
    'Gambia'                                               => 'Gàmbia',
    'Republic of the Gambia'                               => 'Republica de Gàmbia',
    'Guinea-Bissau'                                        => 'Guinèa-Bissau',
    'Republic of Guinea-Bissau'                            => 'Republica de Guinèa Bissau',
    'Equatorial Guinea'                                    => 'Guinèa eqüatoriala',
    'Republic of Equatorial Guinea'                        => 'Republica de Guinèa Eqüatoriala',
    'Greece'                                               => 'Grècia',
    'Hellenic Republic'                                    => 'Republica ellenica',
    'Grenada'                                              => 'Granada',
    'Greenland'                                            => 'Groenlàndia',
    'Guatemala'                                            => 'Guatemala',
    'Republic of Guatemala'                                => 'Republica del Guatemala',
    'French Guiana'                                        => 'Guaiana francesa',
    'Guam'                                                 => 'Guam',
    'Guyana'                                               => 'Guyana',
    'Republic of Guyana'                                   => 'Republica de Guyana',
    'Hong Kong'                                            => 'Hong Kong',
    'Hong Kong Special Administrative Region of China'     => 'Region administrativa especiala chinesa de Honk Hong',
    'Heard Island and McDonald Islands'                    => 'Illas Heard e McDonald',
    'Honduras'                                             => 'Honduras',
    'Republic of Honduras'                                 => 'Republica d\'Honduras',
    'Croatia'                                              => 'Croàcia',
    'Republic of Croatia'                                  => 'Republica de Croàcia',
    'Haiti'                                                => 'Haití',
    'Republic of Haiti'                                    => 'Republica d’Haití',
    'Hungary'                                              => 'Ongria',
    'Indonesia'                                            => 'Indonesia',
    'Republic of Indonesia'                                => 'Republica d’Indonesia',
    'Isle of Man'                                          => 'Illa de Man',
    'India'                                                => 'Índia',
    'Republic of India'                                    => 'Republica d’Índia',
    'British Indian Ocean Territory'                       => 'Territòris britanics de l\'ocean indian',
    'Ireland'                                              => 'Irlanda',
    'Iran, Islamic Republic of'                            => 'Republica Dominicana',
    'Islamic Republic of Iran'                             => 'Republica Dominicana',
    'Iran'                                                 => 'Iran',
    'Iraq'                                                 => 'Iraq',
    'Republic of Iraq'                                     => 'Republica d’Iraq',
    'Iceland'                                              => 'Islàndia',
    'Republic of Iceland'                                  => 'Republica d\'Islàndia',
    'Israel'                                               => 'Israel',
    'State of Israel'                                      => 'Estat d\'Israèl',
    'Italy'                                                => 'Itàlia',
    'Italian Republic'                                     => 'Republica italiana',
    'Jamaica'                                              => 'Jamaica',
    'Jersey'                                               => 'Jersei',
    'Jordan'                                               => 'Jordania',
    'Hashemite Kingdom of Jordan'                          => 'Reialme Hashimita de Jordania',
    'Japan'                                                => 'Japon',
    'Kazakhstan'                                           => 'Cazacstan',
    'Republic of Kazakhstan'                               => 'Republica del Cazacstan',
    'Kenya'                                                => 'Kenya',
    'Republic of Kenya'                                    => 'Republica del Kenya',
    'Kyrgyzstan'                                           => 'Kirguizstan',
    'Kyrgyz Republic'                                      => 'Republica Dominicana',
    'Cambodia'                                             => 'Cambòtja',
    'Kingdom of Cambodia'                                  => 'Reialme de Cambòtja',
    'Kiribati'                                             => 'Kiribati',
    'Republic of Kiribati'                                 => 'Republica de Kiribati',
    'Saint Kitts and Nevis'                                => 'St. Kitts e Nevis',
    'Korea, Republic of'                                   => 'Corèa, republica de',
    'South Korea'                                          => 'Corèa del Sud',
    'Kuwait'                                               => 'Koweit',
    'State of Kuwait'                                      => 'Estat de Kowait',
    'Lao People\'s Democratic Republic'                    => 'Republica democratica populara del Laos',
    'Laos'                                                 => 'Laos',
    'Lebanon'                                              => 'Liban',
    'Lebanese Republic'                                    => 'Republica libanesa',
    'Liberia'                                              => 'Libèria',
    'Republic of Liberia'                                  => 'Republica de Libèria',
    'Libya'                                                => 'Libia',
    'Saint Lucia'                                          => 'St. Lucia',
    'Liechtenstein'                                        => 'Liechtenstein',
    'Principality of Liechtenstein'                        => 'Principat de Liechtenstein',
    'Sri Lanka'                                            => 'Sri Lanka',
    'Democratic Socialist Republic of Sri Lanka'           => 'Republica Democratica Socialista d\'Sri Lanka',
    'Lesotho'                                              => 'Lesoto',
    'Kingdom of Lesotho'                                   => 'Reialme de Lesotho',
    'Lithuania'                                            => 'Lituània',
    'Republic of Lithuania'                                => 'Republica de Lituània',
    'Luxembourg'                                           => 'Luxemborg',
    'Grand Duchy of Luxembourg'                            => 'Grand Ducat de Luxemborg',
    'Latvia'                                               => 'Letònia',
    'Republic of Latvia'                                   => 'Republica letona',
    'Macao'                                                => 'Macau',
    'Macao Special Administrative Region of China'         => 'Region administrativa especiala chinesa de Macau',
    'Saint Martin (French part)'                           => 'Sant Martin (Part francesa)',
    'Morocco'                                              => 'Marròc',
    'Kingdom of Morocco'                                   => 'Reialme de Marròc',
    'Monaco'                                               => 'Mónegue',
    'Principality of Monaco'                               => 'Principat de Mónegue',
    'Moldova, Republic of'                                 => 'Moldàvia, Republica de',
    'Republic of Moldova'                                  => 'Republica de Moldàvia',
    'Moldova'                                              => 'Moldàvia',
    'Madagascar'                                           => 'Madagascar',
    'Republic of Madagascar'                               => 'Republica de Madagascar',
    'Maldives'                                             => 'Maldivas',
    'Republic of Maldives'                                 => 'Republica de las Maldivas',
    'Mexico'                                               => 'Mexic',
    'United Mexican States'                                => 'Estats Units del Mexic',
    'Marshall Islands'                                     => 'Illas Marshall',
    'Republic of the Marshall Islands'                     => 'Republica de las Illas Marshall',
    'North Macedonia'                                      => 'Macedònia del Nòrd',
    'Republic of North Macedonia'                          => 'Republica de Macedònia del Nòrd',
    'Mali'                                                 => 'Mali',
    'Republic of Mali'                                     => 'Republica de Mali',
    'Malta'                                                => 'Malta',
    'Republic of Malta'                                    => 'Republica de Malta',
    'Myanmar'                                              => 'Birmania',
    'Republic of Myanmar'                                  => 'Republica de Birmania',
    'Montenegro'                                           => 'Montenegro',
    'Mongolia'                                             => 'Mongolia',
    'Northern Mariana Islands'                             => 'Illas Marianas del Nòrd',
    'Commonwealth of the Northern Mariana Islands'         => 'Commonwealth de las islas Mariannas del Nòrd',
    'Mozambique'                                           => 'Moçambic',
    'Republic of Mozambique'                               => 'Republica de Moçambic',
    'Mauritania'                                           => 'Mauritània',
    'Islamic Republic of Mauritania'                       => 'Republica islamica de Mauritània',
    'Montserrat'                                           => 'Montserrat',
    'Martinique'                                           => 'Martinica',
    'Mauritius'                                            => 'Maurici',
    'Republic of Mauritius'                                => 'Republica de Maurici',
    'Malawi'                                               => 'Malawi',
    'Republic of Malawi'                                   => 'Republica de Malawi',
    'Malaysia'                                             => 'Malàisia',
    'Mayotte'                                              => 'Maiòta',
    'Namibia'                                              => 'Namibia',
    'Republic of Namibia'                                  => 'Republica de Namibia',
    'New Caledonia'                                        => 'Nòva Caledònia',
    'Niger'                                                => 'Nigèr',
    'Republic of the Niger'                                => 'Republica de Nigèr',
    'Norfolk Island'                                       => 'Illa Norfolk',
    'Nigeria'                                              => 'Nigèria',
    'Federal Republic of Nigeria'                          => 'Republica federala del Nigèria',
    'Nicaragua'                                            => 'Nicaragua',
    'Republic of Nicaragua'                                => 'Paraguai',
    'Niue'                                                 => 'Niue',
    'Netherlands'                                          => 'Païses Basses',
    'Kingdom of the Netherlands'                           => 'Reialme dels Païses Basses',
    'Norway'                                               => 'Norvègia',
    'Kingdom of Norway'                                    => 'Reialme de Norvègia',
    'Nepal'                                                => 'Nepal',
    'Federal Democratic Republic of Nepal'                 => 'Republica democratica federala del Nepam',
    'Nauru'                                                => 'Nauru',
    'Republic of Nauru'                                    => 'Republica de Nauru',
    'New Zealand'                                          => 'Novèla Zelanda',
    'Oman'                                                 => 'Oman',
    'Sultanate of Oman'                                    => 'Soudanat d’Oman',
    'Pakistan'                                             => 'Paquistan',
    'Islamic Republic of Pakistan'                         => 'Republica islamica de Paquistan',
    'Panama'                                               => 'Panamà',
    'Republic of Panama'                                   => 'Republica del Panamà',
    'Pitcairn'                                             => 'Illa de Pitcairn',
    'Peru'                                                 => 'Peró',
    'Republic of Peru'                                     => 'Republica del Peró',
    'Philippines'                                          => 'Filipinas',
    'Republic of the Philippines'                          => 'Republica de las Filipinas',
    'Palau'                                                => 'Belau',
    'Republic of Palau'                                    => 'Republica de Belau',
    'Papua New Guinea'                                     => 'Papoa Nòva Guinèa',
    'Independent State of Papua New Guinea'                => 'Estat independent de Papoa-Nòva Guinèa',
    'Poland'                                               => 'Polonha',
    'Republic of Poland'                                   => 'Republica de Polonha',
    'Puerto Rico'                                          => 'Puerto Rico',
    'Korea, Democratic People\'s Republic of'              => 'Corèa, Republica Populara Democratica de',
    'Democratic People\'s Republic of Korea'               => 'Republica populara democratica de Corèa',
    'North Korea'                                          => 'Corèa del Nòrd',
    'Portugal'                                             => 'Portugal',
    'Portuguese Republic'                                  => 'Republica portuguesa',
    'Paraguay'                                             => 'Paraguai',
    'Republic of Paraguay'                                 => 'Republica de Paraguai',
    'Palestine, State of'                                  => 'Palestina, estat de',
    'the State of Palestine'                               => 'l’estat de Palestina',
    'French Polynesia'                                     => 'Polinesia francesa',
    'Qatar'                                                => 'Qatar',
    'State of Qatar'                                       => 'Estat de Qatar',
    'Réunion'                                              => 'Réunion',
    'Romania'                                              => 'Romania',
    'Russian Federation'                                   => 'Federacion russa',
    'Rwanda'                                               => 'Rwanda',
    'Rwandese Republic'                                    => 'Republica rewandesa',
    'Saudi Arabia'                                         => 'Arabia saudita',
    'Kingdom of Saudi Arabia'                              => 'Reialme d’Arabia Saudita',
    'Sudan'                                                => 'Sodan',
    'Republic of the Sudan'                                => 'Republica de Sodan',
    'Senegal'                                              => 'Senegal',
    'Republic of Senegal'                                  => 'Republica de Senegal',
    'Singapore'                                            => 'Singapor',
    'Republic of Singapore'                                => 'Republica de Singapor',
    'South Georgia and the South Sandwich Islands'         => 'Illas Georgia del Sud e Sandwich del Sud',
    'Saint Helena, Ascension and Tristan da Cunha'         => 'Santa Elena, Ascension, e Tristan da Cunha',
    'Svalbard and Jan Mayen'                               => 'Svalbard e Jan Mayen',
    'Solomon Islands'                                      => 'Illas Salomon',
    'Sierra Leone'                                         => 'Sierra Leone',
    'Republic of Sierra Leone'                             => 'Republica de Sierra Leone',
    'El Salvador'                                          => 'El Salvador',
    'Republic of El Salvador'                              => 'Republica del Salvador',
    'San Marino'                                           => 'Sant Marin',
    'Republic of San Marino'                               => 'Republica de Sant Marin',
    'Somalia'                                              => 'Somalia',
    'Federal Republic of Somalia'                          => 'Republica federala de Somalia',
    'Saint Pierre and Miquelon'                            => 'Sant Pèire e Miquelon',
    'Serbia'                                               => 'Serbia',
    'Republic of Serbia'                                   => 'Republica de Serbia',
    'South Sudan'                                          => 'Sodan del Sud',
    'Republic of South Sudan'                              => 'Republica de Sodan del Sud',
    'Sao Tome and Principe'                                => 'Sao Tomé e Principe',
    'Democratic Republic of Sao Tome and Principe'         => 'Republica Democratica de São Tomé e Príncipe',
    'Suriname'                                             => 'Surinam',
    'Republic of Suriname'                                 => 'Republica de Surinam',
    'Slovakia'                                             => 'Eslovaquia',
    'Slovak Republic'                                      => 'Republica eslovaquia',
    'Slovenia'                                             => 'Eslovènia',
    'Republic of Slovenia'                                 => 'Republica d\'Eslovènia',
    'Sweden'                                               => 'Suècia',
    'Kingdom of Sweden'                                    => 'Reialme de Suècia',
    'Eswatini'                                             => 'Eswatini',
    'Kingdom of Eswatini'                                  => 'Reialme d\'Eswatini',
    'Sint Maarten (Dutch part)'                            => 'Sant Martin (Part neerlandesa)',
    'Seychelles'                                           => 'Seichèlas',
    'Republic of Seychelles'                               => 'Republica de las Seichèlas',
    'Syrian Arab Republic'                                 => 'Republica Dominicana',
    'Syria'                                                => 'Siria',
    'Turks and Caicos Islands'                             => 'Illas Turcas e Caïcas',
    'Chad'                                                 => 'Chad',
    'Republic of Chad'                                     => 'Republica del Chad',
    'Togo'                                                 => 'Tògo',
    'Togolese Republic'                                    => 'Republica togolesa',
    'Thailand'                                             => 'Tailàndia',
    'Kingdom of Thailand'                                  => 'Reialme de Tailàndia',
    'Tajikistan'                                           => 'Tatgiquistan',
    'Republic of Tajikistan'                               => 'Republica de Tatgiquistan',
    'Tokelau'                                              => 'Tokelau',
    'Turkmenistan'                                         => 'Turcmenistan',
    'Timor-Leste'                                          => 'Timòr Èst',
    'Democratic Republic of Timor-Leste'                   => 'Republica democratica del Timòr Èst',
    'Tonga'                                                => 'Tònga',
    'Kingdom of Tonga'                                     => 'Reialme de Tònga',
    'Trinidad and Tobago'                                  => 'Trinitat e Tobago',
    'Republic of Trinidad and Tobago'                      => 'Republica de Trinitat e Tobago',
    'Tunisia'                                              => 'Tunisia',
    'Republic of Tunisia'                                  => 'Republica tunisiana',
    'Türkiye'                                              => 'Turquia',
    'Republic of Türkiye'                                  => 'Republica de Turquia',
    'Tuvalu'                                               => 'Tuvalu',
    'Taiwan, Province of China'                            => 'Taiwan, província de China',
    'Taiwan'                                               => 'Taiwan',
    'Tanzania, United Republic of'                         => 'Tanzania, republica unida de',
    'United Republic of Tanzania'                          => 'Republica unida de Tanzania',
    'Tanzania'                                             => 'Tanzania',
    'Uganda'                                               => 'Oganda',
    'Republic of Uganda'                                   => 'Republica d’Oganda',
    'Ukraine'                                              => 'Ucraïna',
    'United States Minor Outlying Islands'                 => 'Islas perifericas Menores dels Estats Units',
    'Uruguay'                                              => 'Uruguai',
    'Eastern Republic of Uruguay'                          => 'Republica orientala d’Uruguai',
    'United States'                                        => 'Estats Units',
    'United States of America'                             => 'Estats Units d’America',
    'Uzbekistan'                                           => 'Ozbequistan',
    'Republic of Uzbekistan'                               => 'Republica d\'Ozbequistan',
    'Holy See (Vatican City State)'                        => 'Santa Ses (Estat de la Ciutat de Vatican)',
    'Saint Vincent and the Grenadines'                     => 'Sant Vincenç e las Grenadinas',
    'Venezuela, Bolivarian Republic of'                    => 'Veneçuèla, Republica Dominicana bolivariana del',
    'Bolivarian Republic of Venezuela'                     => 'Republica bolivariana de Venecuèla',
    'Venezuela'                                            => 'Veneçuèla',
    'Virgin Islands, British'                              => 'Illas Verges, britanicas',
    'British Virgin Islands'                               => 'Illas Verges britanicas',
    'Virgin Islands, U.S.'                                 => 'illas Verges, EU',
    'Virgin Islands of the United States'                  => 'Islas Verges dels Estats Units d’America',
    'Viet Nam'                                             => 'Viet Nam',
    'Socialist Republic of Viet Nam'                       => 'Republica socialista de Vietnam',
    'Vietnam'                                              => 'Vietnam',
    'Vanuatu'                                              => 'Vanuatu',
    'Republic of Vanuatu'                                  => 'Republica de Vanuatu',
    'Wallis and Futuna'                                    => 'Wallis e Futuna',
    'Samoa'                                                => 'Samoa',
    'Independent State of Samoa'                           => 'Estat independent de Samoa',
    'Yemen'                                                => 'Iemèn',
    'Republic of Yemen'                                    => 'Republica de Iemèn',
    'South Africa'                                         => 'Sudafrica',
    'Republic of South Africa'                             => 'Republica de Sudafrica',
    'Zambia'                                               => 'Zambia',
    'Republic of Zambia'                                   => 'Republica de Zambia',
    'Zimbabwe'                                             => 'Zimbabwe',
    'Republic of Zimbabwe'                                 => 'Republica de Zimbabwe',
];
