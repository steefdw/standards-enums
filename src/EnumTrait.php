<?php

namespace Steefdw\StandardsEnums;

trait EnumTrait
{
    public static function tryFromName(string $name): ?static
    {
        $inputAsName = str_replace('-', '_', mb_strtoupper($name));
        foreach (static::cases() as $case) {
            if ($case->name === $inputAsName) {
                return $case;
            }
        }

        return null;
    }

    /**
     * @return array<int, string>
     */
    public static function values(): array
    {
        return array_column(static::cases(), 'value');
    }

    /**
     * @return array<int, string>
     */
    public static function names(): array
    {
        return array_column(static::cases(), 'name');
    }

    /**
     * @return array<string, string>
     */
    public static function toArray(): array
    {
        $cases = [];
        foreach (self::cases() as $case) {
            $cases[$case->name] = $case->value;
        }

        return $cases;
    }

    /**
     * @return array<int, self>
     */
    public static function casesSorted(): array
    {
        $cases = self::getIndexedCases();

        ksort($cases);

        return array_values($cases);
    }

    /**
     * @return array<string, self>
     */
    private static function getIndexedCases(): array
    {
        $cases = [];
        foreach (self::cases() as $case) {
            $cases[$case->name] = $case;
        }

        return $cases;
    }
}
