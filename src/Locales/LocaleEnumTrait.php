<?php

namespace Steefdw\StandardsEnums\Locales;

use Steefdw\StandardsEnums\EnumTrait;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha2;
use Steefdw\StandardsEnums\ISO639\LanguageName;

trait LocaleEnumTrait
{
    use EnumTrait;

    public function getCountry(): ?CountryName
    {
        $localeParts = explode('_', $this->name);
        $country = end($localeParts);

        return CountryName::tryFromName($country);
    }

    public function getLanguage(): ?LanguageName
    {
        $localeParts = explode('_', $this->name);
        $language = mb_strtolower(current($localeParts));

        return LanguageAlpha2::tryFrom($language)?->getName();
    }
}
