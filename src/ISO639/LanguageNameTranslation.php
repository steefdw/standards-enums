<?php

/** @noinspection SpellCheckingInspection */

namespace Steefdw\StandardsEnums\ISO639;

/**
 * @see https://en.wikipedia.org/wiki/ISO_639-2
 */
enum LanguageNameTranslation: string implements LanguageEnumInterface
{
    use LanguageEnumTrait;

    // <editor-fold desc="Cases">
    case AS_IN = 'as_IN'; // Assamese (India)
    case BE_BY = 'be_BY'; // Belarusian (Belarus)
    case CA_AD = 'ca_AD'; // Catalan; Valencian (Andorra)
    case CA_ES = 'ca_ES'; // Catalan; Valencian (Spain)
    case CA_FR = 'ca_FR'; // Catalan; Valencian (France)
    case CA_IT = 'ca_IT'; // Catalan; Valencian (Italy)
    case CS_CZ = 'cs_CZ'; // Czech (Czechia)
    case DE_AT = 'de_AT'; // German (Austria)
    case DE_BE = 'de_BE'; // German (Belgium)
    case DE_CH = 'de_CH'; // German (Switzerland)
    case DE_DE = 'de_DE'; // German (Germany)
    case DE_IT = 'de_IT'; // German (Italy)
    case DE_LI = 'de_LI'; // German (Liechtenstein)
    case DE_LU = 'de_LU'; // German (Luxembourg)
    case ES_AR = 'es_AR'; // Spanish; Castilian (Argentina)
    case ES_BO = 'es_BO'; // Spanish; Castilian (Bolivia)
    case ES_BR = 'es_BR'; // Spanish; Castilian (Brazil)
    case ES_BZ = 'es_BZ'; // Spanish; Castilian (Belize)
    case ES_CL = 'es_CL'; // Spanish; Castilian (Chile)
    case ES_CO = 'es_CO'; // Spanish; Castilian (Colombia)
    case ES_CR = 'es_CR'; // Spanish; Castilian (Costa Rica)
    case ES_CU = 'es_CU'; // Spanish; Castilian (Cuba)
    case ES_DO = 'es_DO'; // Spanish; Castilian (Dominican Republic)
    case ES_EC = 'es_EC'; // Spanish; Castilian (Ecuador)
    case ES_ES = 'es_ES'; // Spanish; Castilian (Spain)
    case ES_GQ = 'es_GQ'; // Spanish; Castilian (Equatorial Guinea)
    case ES_GT = 'es_GT'; // Spanish; Castilian (Guatemala)
    case ES_HN = 'es_HN'; // Spanish; Castilian (Honduras)
    case ES_MX = 'es_MX'; // Spanish; Castilian (Mexico)
    case ES_NI = 'es_NI'; // Spanish; Castilian (Nicaragua)
    case ES_PA = 'es_PA'; // Spanish; Castilian (Panama)
    case ES_PE = 'es_PE'; // Spanish; Castilian (Peru)
    case ES_PH = 'es_PH'; // Spanish; Castilian (Philippines)
    case ES_PR = 'es_PR'; // Spanish; Castilian (Puerto Rico)
    case ES_PY = 'es_PY'; // Spanish; Castilian (Paraguay)
    case ES_SV = 'es_SV'; // Spanish; Castilian (El Salvador)
    case ES_US = 'es_US'; // Spanish; Castilian (United States)
    case ES_UY = 'es_UY'; // Spanish; Castilian (Uruguay)
    case ES_VE = 'es_VE'; // Spanish; Castilian (Venezuela)
    case ET_EE = 'et_EE'; // Estonian (Estonia)
    case FR_BE = 'fr_BE'; // French (Belgium)
    case FR_BF = 'fr_BF'; // French (Burkina Faso)
    case FR_BI = 'fr_BI'; // French (Burundi)
    case FR_BJ = 'fr_BJ'; // French (Benin)
    case FR_BL = 'fr_BL'; // French (Saint Barthélemy)
    case FR_CA = 'fr_CA'; // French (Canada)
    case FR_CD = 'fr_CD'; // French (Congo, The Democratic Republic of the)
    case FR_CF = 'fr_CF'; // French (Central African Republic)
    case FR_CG = 'fr_CG'; // French (Congo)
    case FR_CH = 'fr_CH'; // French (Switzerland)
    case FR_CI = 'fr_CI'; // French (Côte d'Ivoire)
    case FR_CM = 'fr_CM'; // French (Cameroon)
    case FR_DJ = 'fr_DJ'; // French (Djibouti)
    case FR_DZ = 'fr_DZ'; // French (Algeria)
    case FR_FR = 'fr_FR'; // French (France)
    case FR_GA = 'fr_GA'; // French (Gabon)
    case FR_GF = 'fr_GF'; // French (French Guiana)
    case FR_GN = 'fr_GN'; // French (Guinea)
    case FR_GP = 'fr_GP'; // French (Guadeloupe)
    case FR_GQ = 'fr_GQ'; // French (Equatorial Guinea)
    case FR_HT = 'fr_HT'; // French (Haiti)
    case FR_KM = 'fr_KM'; // French (Comoros)
    case FR_LU = 'fr_LU'; // French (Luxembourg)
    case FR_MA = 'fr_MA'; // French (Morocco)
    case FR_MC = 'fr_MC'; // French (Monaco)
    case FR_MF = 'fr_MF'; // French (Saint Martin (French part))
    case FR_MG = 'fr_MG'; // French (Madagascar)
    case FR_ML = 'fr_ML'; // French (Mali)
    case FR_MQ = 'fr_MQ'; // French (Martinique)
    case FR_MR = 'fr_MR'; // French (Mauritania)
    case FR_MU = 'fr_MU'; // French (Mauritius)
    case FR_NC = 'fr_NC'; // French (New Caledonia)
    case FR_NE = 'fr_NE'; // French (Niger)
    case FR_PF = 'fr_PF'; // French (French Polynesia)
    case FR_PM = 'fr_PM'; // French (Saint Pierre and Miquelon)
    case FR_RE = 'fr_RE'; // French (Réunion)
    case FR_RW = 'fr_RW'; // French (Rwanda)
    case FR_SC = 'fr_SC'; // French (Seychelles)
    case FR_SN = 'fr_SN'; // French (Senegal)
    case FR_SY = 'fr_SY'; // French (Syria)
    case FR_TD = 'fr_TD'; // French (Chad)
    case FR_TG = 'fr_TG'; // French (Togo)
    case FR_TN = 'fr_TN'; // French (Tunisia)
    case FR_VU = 'fr_VU'; // French (Vanuatu)
    case FR_WF = 'fr_WF'; // French (Wallis and Futuna)
    case FR_YT = 'fr_YT'; // French (Mayotte)
    case GA_GB = 'ga_GB'; // Irish (United Kingdom)
    case GA_IE = 'ga_IE'; // Irish (Ireland)
    case GU_IN = 'gu_IN'; // Gujarati (India)
    case HI_IN = 'hi_IN'; // Hindi (India)
    case HU_HU = 'hu_HU'; // Hungarian (Hungary)
    case ID_ID = 'id_ID'; // Indonesian (Indonesia)
    case IS_IS = 'is_IS'; // Icelandic (Iceland)
    case IT_CH = 'it_CH'; // Italian (Switzerland)
    case IT_IT = 'it_IT'; // Italian (Italy)
    case IT_SM = 'it_SM'; // Italian (San Marino)
    case IT_VA = 'it_VA'; // Italian (Holy See (Vatican City State))
    case KO_KP = 'ko_KP'; // Korean (North Korea)
    case KO_KR = 'ko_KR'; // Korean (South Korea)
    case LT_LT = 'lt_LT'; // Lithuanian (Lithuania)
    case NL_AW = 'nl_AW'; // Dutch; Flemish (Aruba)
    case NL_BE = 'nl_BE'; // Dutch; Flemish (Belgium)
    case NL_BQ = 'nl_BQ'; // Dutch; Flemish (Bonaire, Sint Eustatius and Saba)
    case NL_CW = 'nl_CW'; // Dutch; Flemish (Curaçao)
    case NL_NL = 'nl_NL'; // Dutch; Flemish (Netherlands)
    case NL_SR = 'nl_SR'; // Dutch; Flemish (Suriname)
    case NL_SX = 'nl_SX'; // Dutch; Flemish (Sint Maarten (Dutch part))
    case PA_PK = 'pa_PK'; // Panjabi; Punjabi (Pakistan)
    case PA_IN = 'pa_IN'; // Panjabi; Punjabi (India)
    case PL_PL = 'pl_PL'; // Polish (Poland)
    case PT_AO = 'pt_AO'; // Portuguese (Angola)
    case PT_BR = 'pt_BR'; // Portuguese (Timor-Leste)
    case PT_CH = 'pt_CH'; // Portuguese (Switzerland)
    case PT_CV = 'pt_CV'; // Portuguese (Cabo Verde)
    case PT_GQ = 'pt_GQ'; // Portuguese (Equatorial Guinea)
    case PT_GW = 'pt_GW'; // Portuguese (Guinea-Bissau)
    case PT_LU = 'pt_LU'; // Portuguese (Luxembourg)
    case PT_MO = 'pt_MO'; // Portuguese (Macao)
    case PT_MZ = 'pt_MZ'; // Portuguese (Mozambique)
    case PT_PT = 'pt_PT'; // Portuguese (Portugal)
    case PT_ST = 'pt_ST'; // Portuguese (Sao Tome and Principe)
    case PT_TL = 'pt_TL'; // Portuguese (Timor-Leste)
    case RO_MD = 'ro_MD'; // Romanian; Moldavian; Moldovan (Moldova)
    case RO_RO = 'ro_RO'; // Romanian; Moldavian; Moldovan (Romania)
    case RU_BY = 'ru_BY'; // Russian (Belarus)
    case RU_KG = 'ru_KG'; // Russian (Kyrgyzstan)
    case RU_KZ = 'ru_KZ'; // Russian (Kazakhstan)
    case RU_MD = 'ru_MD'; // Russian (Moldova)
    case RU_RU = 'ru_RU'; // Russian (Russian Federation)
    case RU_UA = 'ru_UA'; // Russian (Ukraine)
    case SC_IT = 'sc_IT'; // Sardinian (Italy)
    case SK_SK = 'sk_SK'; // Slovak (Slovakia)
    case SQ_AL = 'sq_AL'; // Albanian (Albania)
    case SQ_MK = 'sq_MK'; // Albanian (North Macedonia)
    case SV_AX = 'sv_AX'; // Swedish (Åland Islands)
    case SV_FI = 'sv_FI'; // Swedish (Finland)
    case SV_SE = 'sv_SE'; // Swedish (Sweden)
    case TA_IN = 'ta_IN'; // Tamil (India)
    case TA_LK = 'ta_LK'; // Tamil (Sri Lanka)
    case TA_MY = 'ta_MY'; // Tamil (Malaysia)
    case TA_SG = 'ta_SG'; // Tamil (Singapore)
    case TR_CY = 'tr_CY'; // Turkish (Cyprus)
    case TR_TR = 'tr_TR'; // Turkish (Türkiye)
    case UK_UA = 'uk_UA'; // Ukrainian (Ukraine)
    case ZH_CN = 'zh_CN'; // Chinese (Taiwan)
    case ZH_HK = 'zh_HK'; // Chinese (Taiwan)
    case ZH_TW = 'zh_TW'; // Chinese (Taiwan)
    // </editor-fold>

    /**
     * @throws \Exception
     *
     * @return array<string, string>
     */
    public function getTranslations(): array
    {
        $filePath = dirname(__DIR__) . "/translations/ISO639-2/$this->value.php";
        if (is_file($filePath)) {
            return (array) include $filePath;
        }

        $languageCode = substr($this->value, 0, 2);
        $filePath = dirname(__DIR__) . "/translations/ISO639-2/$languageCode.php";
        if (is_file($filePath)) {
            return (array) include $filePath;
        }

        throw new \Exception("Language $this->value is not fully translated yet");
    }
}
