<?php

namespace Steefdw\StandardsEnums\ISO639;

use BackedEnum;

interface LanguageEnumInterface extends BackedEnum
{
    public static function tryFromAlpha2(LanguageAlpha2 $enum): ?static;

    public static function tryFromAlpha3(LanguageAlpha3 $enum): ?static;

    public static function tryFromLanguageName(LanguageName $enum): ?static;

    /**
     * @return array<int, static>
     */
    public static function casesSortedByLanguageName(?LanguageNameTranslation $translation = null): array;
}
