<?php

/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Steefdw\StandardsEnums\ISO639;

/**
 * @see https://en.wikipedia.org/wiki/ISO_639-2
 */
enum LanguageAlpha3: string implements LanguageEnumInterface
{
    use LanguageEnumTrait;

    // <editor-fold desc="Cases">
    case AAR = 'aar'; // Afar
    case ABK = 'abk'; // Abkhazian
    case ACE = 'ace'; // Achinese
    case ACH = 'ach'; // Acoli
    case ADA = 'ada'; // Adangme
    case ADY = 'ady'; // Adyghe; Adygei
    case AFA = 'afa'; // Afro-Asiatic languages
    case AFH = 'afh'; // Afrihili
    case AFR = 'afr'; // Afrikaans
    case AIN = 'ain'; // Ainu
    case AKA = 'aka'; // Akan
    case AKK = 'akk'; // Akkadian
    case ALE = 'ale'; // Aleut
    case ALG = 'alg'; // Algonquian languages
    case ALT = 'alt'; // Southern Altai
    case AMH = 'amh'; // Amharic
    case ANG = 'ang'; // English, Old (ca. 450-1100)
    case ANP = 'anp'; // Angika
    case APA = 'apa'; // Apache languages
    case ARA = 'ara'; // Arabic
    case ARC = 'arc'; // Official Aramaic (700-300 BCE); Imperial Aramaic (700-300 BCE)
    case ARG = 'arg'; // Aragonese
    case ARN = 'arn'; // Mapudungun; Mapuche
    case ARP = 'arp'; // Arapaho
    case ART = 'art'; // Artificial languages
    case ARW = 'arw'; // Arawak
    case ASM = 'asm'; // Assamese
    case AST = 'ast'; // Asturian; Bable; Leonese; Asturleonese
    case ATH = 'ath'; // Athapascan languages
    case AUS = 'aus'; // Australian languages
    case AVA = 'ava'; // Avaric
    case AVE = 'ave'; // Avestan
    case AWA = 'awa'; // Awadhi
    case AYM = 'aym'; // Aymara
    case AZE = 'aze'; // Azerbaijani
    case BAD = 'bad'; // Banda languages
    case BAI = 'bai'; // Bamileke languages
    case BAK = 'bak'; // Bashkir
    case BAL = 'bal'; // Baluchi
    case BAM = 'bam'; // Bambara
    case BAN = 'ban'; // Balinese
    case BAS = 'bas'; // Basa
    case BAT = 'bat'; // Baltic languages
    case BEJ = 'bej'; // Beja; Bedawiyet
    case BEL = 'bel'; // Belarusian
    case BEM = 'bem'; // Bemba
    case BEN = 'ben'; // Bengali
    case BER = 'ber'; // Berber languages
    case BHO = 'bho'; // Bhojpuri
    case BIH = 'bih'; // Bihari languages
    case BIK = 'bik'; // Bikol
    case BIN = 'bin'; // Bini; Edo
    case BIS = 'bis'; // Bislama
    case BLA = 'bla'; // Siksika
    case BNT = 'bnt'; // Bantu (Other)
    case BOD = 'bod'; // Tibetan
    case BOS = 'bos'; // Bosnian
    case BRA = 'bra'; // Braj
    case BRE = 'bre'; // Breton
    case BTK = 'btk'; // Batak languages
    case BUA = 'bua'; // Buriat
    case BUG = 'bug'; // Buginese
    case BUL = 'bul'; // Bulgarian
    case BYN = 'byn'; // Blin; Bilin
    case CAD = 'cad'; // Caddo
    case CAI = 'cai'; // Central American Indian languages
    case CAR = 'car'; // Galibi Carib
    case CAT = 'cat'; // Catalan; Valencian
    case CAU = 'cau'; // Caucasian languages
    case CEB = 'ceb'; // Cebuano
    case CEL = 'cel'; // Celtic languages
    case CES = 'ces'; // Czech
    case CHA = 'cha'; // Chamorro
    case CHB = 'chb'; // Chibcha
    case CHE = 'che'; // Chechen
    case CHG = 'chg'; // Chagatai
    case CHK = 'chk'; // Chuukese
    case CHM = 'chm'; // Mari
    case CHN = 'chn'; // Chinook jargon
    case CHO = 'cho'; // Choctaw
    case CHP = 'chp'; // Chipewyan; Dene Suline
    case CHR = 'chr'; // Cherokee
    case CHU = 'chu'; // Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic
    case CHV = 'chv'; // Chuvash
    case CHY = 'chy'; // Cheyenne
    case CMC = 'cmc'; // Chamic languages
    case CNR = 'cnr'; // Montenegrin
    case COP = 'cop'; // Coptic
    case COR = 'cor'; // Cornish
    case COS = 'cos'; // Corsican
    case CPE = 'cpe'; // Creoles and pidgins, English based
    case CPF = 'cpf'; // Creoles and pidgins, French-based
    case CPP = 'cpp'; // Creoles and pidgins, Portuguese-based
    case CRE = 'cre'; // Cree
    case CRH = 'crh'; // Crimean Tatar; Crimean Turkish
    case CRP = 'crp'; // Creoles and pidgins
    case CSB = 'csb'; // Kashubian
    case CUS = 'cus'; // Cushitic languages
    case CYM = 'cym'; // Welsh
    case DAK = 'dak'; // Dakota
    case DAN = 'dan'; // Danish
    case DAR = 'dar'; // Dargwa
    case DAY = 'day'; // Land Dayak languages
    case DEL = 'del'; // Delaware
    case DEN = 'den'; // Slave (Athapascan)
    case DEU = 'deu'; // German
    case DGR = 'dgr'; // Dogrib
    case DIN = 'din'; // Dinka
    case DIV = 'div'; // Divehi; Dhivehi; Maldivian
    case DOI = 'doi'; // Dogri
    case DRA = 'dra'; // Dravidian languages
    case DSB = 'dsb'; // Lower Sorbian
    case DUA = 'dua'; // Duala
    case DUM = 'dum'; // Dutch, Middle (ca. 1050-1350)
    case DYU = 'dyu'; // Dyula
    case DZO = 'dzo'; // Dzongkha
    case EFI = 'efi'; // Efik
    case EGY = 'egy'; // Egyptian (Ancient)
    case EKA = 'eka'; // Ekajuk
    case ELL = 'ell'; // Greek, Modern (1453-)
    case ELX = 'elx'; // Elamite
    case ENG = 'eng'; // English
    case ENM = 'enm'; // English, Middle (1100-1500)
    case EPO = 'epo'; // Esperanto
    case EST = 'est'; // Estonian
    case EUS = 'eus'; // Basque
    case EWE = 'ewe'; // Ewe
    case EWO = 'ewo'; // Ewondo
    case FAN = 'fan'; // Fang
    case FAO = 'fao'; // Faroese
    case FAS = 'fas'; // Persian
    case FAT = 'fat'; // Fanti
    case FIJ = 'fij'; // Fijian
    case FIL = 'fil'; // Filipino; Pilipino
    case FIN = 'fin'; // Finnish
    case FIU = 'fiu'; // Finno-Ugrian languages
    case FON = 'fon'; // Fon
    case FRA = 'fra'; // French
    case FRM = 'frm'; // French, Middle (ca. 1400-1600)
    case FRO = 'fro'; // French, Old (842-ca. 1400)
    case FRR = 'frr'; // Northern Frisian
    case FRS = 'frs'; // Eastern Frisian
    case FRY = 'fry'; // Western Frisian
    case FUL = 'ful'; // Fulah
    case FUR = 'fur'; // Friulian
    case GAA = 'gaa'; // Ga
    case GAY = 'gay'; // Gayo
    case GBA = 'gba'; // Gbaya
    case GEM = 'gem'; // Germanic languages
    case GEZ = 'gez'; // Geez
    case GIL = 'gil'; // Gilbertese
    case GLA = 'gla'; // Gaelic; Scottish Gaelic
    case GLE = 'gle'; // Irish
    case GLG = 'glg'; // Galician
    case GLV = 'glv'; // Manx
    case GMH = 'gmh'; // German, Middle High (ca. 1050-1500)
    case GOH = 'goh'; // German, Old High (ca. 750-1050)
    case GON = 'gon'; // Gondi
    case GOR = 'gor'; // Gorontalo
    case GOT = 'got'; // Gothic
    case GRB = 'grb'; // Grebo
    case GRC = 'grc'; // Greek, Ancient (to 1453)
    case GRN = 'grn'; // Guarani
    case GSW = 'gsw'; // Swiss German; Alemannic; Alsatian
    case GUJ = 'guj'; // Gujarati
    case GWI = 'gwi'; // Gwich'in
    case HAI = 'hai'; // Haida
    case HAT = 'hat'; // Haitian; Haitian Creole
    case HAU = 'hau'; // Hausa
    case HAW = 'haw'; // Hawaiian
    case HEB = 'heb'; // Hebrew
    case HER = 'her'; // Herero
    case HIL = 'hil'; // Hiligaynon
    case HIM = 'him'; // Himachali languages; Western Pahari languages
    case HIN = 'hin'; // Hindi
    case HIT = 'hit'; // Hittite
    case HMN = 'hmn'; // Hmong; Mong
    case HMO = 'hmo'; // Hiri Motu
    case HRV = 'hrv'; // Croatian
    case HSB = 'hsb'; // Upper Sorbian
    case HUN = 'hun'; // Hungarian
    case HUP = 'hup'; // Hupa
    case HYE = 'hye'; // Armenian
    case IBA = 'iba'; // Iban
    case IBO = 'ibo'; // Igbo
    case IDO = 'ido'; // Ido
    case III = 'iii'; // Sichuan Yi; Nuosu
    case IJO = 'ijo'; // Ijo languages
    case IKU = 'iku'; // Inuktitut
    case ILE = 'ile'; // Interlingue; Occidental
    case ILO = 'ilo'; // Iloko
    case INA = 'ina'; // Interlingua (International Auxiliary Language Association)
    case INC = 'inc'; // Indic languages
    case IND = 'ind'; // Indonesian
    case INE = 'ine'; // Indo-European languages
    case INH = 'inh'; // Ingush
    case IPK = 'ipk'; // Inupiaq
    case IRA = 'ira'; // Iranian languages
    case IRO = 'iro'; // Iroquoian languages
    case ISL = 'isl'; // Icelandic
    case ITA = 'ita'; // Italian
    case JAV = 'jav'; // Javanese
    case JBO = 'jbo'; // Lojban
    case JPN = 'jpn'; // Japanese
    case JPR = 'jpr'; // Judeo-Persian
    case JRB = 'jrb'; // Judeo-Arabic
    case KAA = 'kaa'; // Kara-Kalpak
    case KAB = 'kab'; // Kabyle
    case KAC = 'kac'; // Kachin; Jingpho
    case KAL = 'kal'; // Kalaallisut; Greenlandic
    case KAM = 'kam'; // Kamba
    case KAN = 'kan'; // Kannada
    case KAR = 'kar'; // Karen languages
    case KAS = 'kas'; // Kashmiri
    case KAT = 'kat'; // Georgian
    case KAU = 'kau'; // Kanuri
    case KAW = 'kaw'; // Kawi
    case KAZ = 'kaz'; // Kazakh
    case KBD = 'kbd'; // Kabardian
    case KHA = 'kha'; // Khasi
    case KHI = 'khi'; // Khoisan languages
    case KHM = 'khm'; // Central Khmer
    case KHO = 'kho'; // Khotanese; Sakan
    case KIK = 'kik'; // Kikuyu; Gikuyu
    case KIN = 'kin'; // Kinyarwanda
    case KIR = 'kir'; // Kirghiz; Kyrgyz
    case KMB = 'kmb'; // Kimbundu
    case KOK = 'kok'; // Konkani
    case KOM = 'kom'; // Komi
    case KON = 'kon'; // Kongo
    case KOR = 'kor'; // Korean
    case KOS = 'kos'; // Kosraean
    case KPE = 'kpe'; // Kpelle
    case KRC = 'krc'; // Karachay-Balkar
    case KRL = 'krl'; // Karelian
    case KRO = 'kro'; // Kru languages
    case KRU = 'kru'; // Kurukh
    case KUA = 'kua'; // Kuanyama; Kwanyama
    case KUM = 'kum'; // Kumyk
    case KUR = 'kur'; // Kurdish
    case KUT = 'kut'; // Kutenai
    case LAD = 'lad'; // Ladino
    case LAH = 'lah'; // Lahnda
    case LAM = 'lam'; // Lamba
    case LAO = 'lao'; // Lao
    case LAT = 'lat'; // Latin
    case LAV = 'lav'; // Latvian
    case LEZ = 'lez'; // Lezghian
    case LIM = 'lim'; // Limburgan; Limburger; Limburgish
    case LIN = 'lin'; // Lingala
    case LIT = 'lit'; // Lithuanian
    case LOL = 'lol'; // Mongo
    case LOZ = 'loz'; // Lozi
    case LTZ = 'ltz'; // Luxembourgish; Letzeburgesch
    case LUA = 'lua'; // Luba-Lulua
    case LUB = 'lub'; // Luba-Katanga
    case LUG = 'lug'; // Ganda
    case LUI = 'lui'; // Luiseno
    case LUN = 'lun'; // Lunda
    case LUO = 'luo'; // Luo (Kenya and Tanzania)
    case LUS = 'lus'; // Lushai
    case MAD = 'mad'; // Madurese
    case MAG = 'mag'; // Magahi
    case MAH = 'mah'; // Marshallese
    case MAI = 'mai'; // Maithili
    case MAK = 'mak'; // Makasar
    case MAL = 'mal'; // Malayalam
    case MAN = 'man'; // Mandingo
    case MAP = 'map'; // Austronesian languages
    case MAR = 'mar'; // Marathi
    case MAS = 'mas'; // Masai
    case MDF = 'mdf'; // Moksha
    case MDR = 'mdr'; // Mandar
    case MEN = 'men'; // Mende
    case MGA = 'mga'; // Irish, Middle (900-1200)
    case MIC = 'mic'; // Mi'kmaq; Micmac
    case MIN = 'min'; // Minangkabau
    case MIS = 'mis'; // Uncoded languages
    case MKD = 'mkd'; // Macedonian
    case MKH = 'mkh'; // Mon-Khmer languages
    case MLG = 'mlg'; // Malagasy
    case MLT = 'mlt'; // Maltese
    case MNC = 'mnc'; // Manchu
    case MNI = 'mni'; // Manipuri
    case MNO = 'mno'; // Manobo languages
    case MOH = 'moh'; // Mohawk
    case MON = 'mon'; // Mongolian
    case MOS = 'mos'; // Mossi
    case MRI = 'mri'; // Maori
    case MSA = 'msa'; // Malay
    case MUL = 'mul'; // Multiple languages
    case MUN = 'mun'; // Munda languages
    case MUS = 'mus'; // Creek
    case MWL = 'mwl'; // Mirandese
    case MWR = 'mwr'; // Marwari
    case MYA = 'mya'; // Burmese
    case MYN = 'myn'; // Mayan languages
    case MYV = 'myv'; // Erzya
    case NAH = 'nah'; // Nahuatl languages
    case NAI = 'nai'; // North American Indian languages
    case NAP = 'nap'; // Neapolitan
    case NAU = 'nau'; // Nauru
    case NAV = 'nav'; // Navajo; Navaho
    case NBL = 'nbl'; // Ndebele, South; South Ndebele
    case NDE = 'nde'; // Ndebele, North; North Ndebele
    case NDO = 'ndo'; // Ndonga
    case NDS = 'nds'; // Low German; Low Saxon; German, Low; Saxon, Low
    case NEP = 'nep'; // Nepali
    case NEW = 'new'; // Nepal Bhasa; Newari
    case NIA = 'nia'; // Nias
    case NIC = 'nic'; // Niger-Kordofanian languages
    case NIU = 'niu'; // Niuean
    case NLD = 'nld'; // Dutch; Flemish
    case NNO = 'nno'; // Norwegian Nynorsk; Nynorsk, Norwegian
    case NOB = 'nob'; // Bokmål, Norwegian; Norwegian Bokmål
    case NOG = 'nog'; // Nogai
    case NON = 'non'; // Norse, Old
    case NOR = 'nor'; // Norwegian
    case NQO = 'nqo'; // N'Ko
    case NSO = 'nso'; // Pedi; Sepedi; Northern Sotho
    case NUB = 'nub'; // Nubian languages
    case NWC = 'nwc'; // Classical Newari; Old Newari; Classical Nepal Bhasa
    case NYA = 'nya'; // Chichewa; Chewa; Nyanja
    case NYM = 'nym'; // Nyamwezi
    case NYN = 'nyn'; // Nyankole
    case NYO = 'nyo'; // Nyoro
    case NZI = 'nzi'; // Nzima
    case OCI = 'oci'; // Occitan (post 1500); Provençal
    case OJI = 'oji'; // Ojibwa
    case ORI = 'ori'; // Oriya
    case ORM = 'orm'; // Oromo
    case OSA = 'osa'; // Osage
    case OSS = 'oss'; // Ossetian; Ossetic
    case OTA = 'ota'; // Turkish, Ottoman (1500-1928)
    case OTO = 'oto'; // Otomian languages
    case PAA = 'paa'; // Papuan languages
    case PAG = 'pag'; // Pangasinan
    case PAL = 'pal'; // Pahlavi
    case PAM = 'pam'; // Pampanga; Kapampangan
    case PAN = 'pan'; // Panjabi; Punjabi
    case PAP = 'pap'; // Papiamento
    case PAU = 'pau'; // Palauan
    case PEO = 'peo'; // Persian, Old (ca. 600-400 B.C.)
    case PHI = 'phi'; // Philippine languages
    case PHN = 'phn'; // Phoenician
    case PLI = 'pli'; // Pali
    case POL = 'pol'; // Polish
    case PON = 'pon'; // Pohnpeian
    case POR = 'por'; // Portuguese
    case PRA = 'pra'; // Prakrit languages
    case PRO = 'pro'; // Provençal, Old (to 1500)
    case PUS = 'pus'; // Pushto; Pashto
    case QAA_QTZ = 'qaa-qtz'; // Reserved for local use
    case QUE = 'que'; // Quechua
    case RAJ = 'raj'; // Rajasthani
    case RAP = 'rap'; // Rapanui
    case RAR = 'rar'; // Rarotongan; Cook Islands Maori
    case ROA = 'roa'; // Romance languages
    case ROH = 'roh'; // Romansh
    case ROM = 'rom'; // Romany
    case RON = 'ron'; // Romanian; Moldavian; Moldovan
    case RUN = 'run'; // Rundi
    case RUP = 'rup'; // Aromanian; Arumanian; Macedo-Romanian
    case RUS = 'rus'; // Russian
    case SAD = 'sad'; // Sandawe
    case SAG = 'sag'; // Sango
    case SAH = 'sah'; // Yakut
    case SAI = 'sai'; // South American Indian (Other)
    case SAL = 'sal'; // Salishan languages
    case SAM = 'sam'; // Samaritan Aramaic
    case SAN = 'san'; // Sanskrit
    case SAS = 'sas'; // Sasak
    case SAT = 'sat'; // Santali
    case SCN = 'scn'; // Sicilian
    case SCO = 'sco'; // Scots
    case SEL = 'sel'; // Selkup
    case SEM = 'sem'; // Semitic languages
    case SGA = 'sga'; // Irish, Old (to 900)
    case SGN = 'sgn'; // Sign Languages
    case SHN = 'shn'; // Shan
    case SID = 'sid'; // Sidamo
    case SIN = 'sin'; // Sinhala; Sinhalese
    case SIO = 'sio'; // Siouan languages
    case SIT = 'sit'; // Sino-Tibetan languages
    case SLA = 'sla'; // Slavic languages
    case SLK = 'slk'; // Slovak
    case SLV = 'slv'; // Slovenian
    case SMA = 'sma'; // Southern Sami
    case SME = 'sme'; // Northern Sami
    case SMI = 'smi'; // Sami languages
    case SMJ = 'smj'; // Lule Sami
    case SMN = 'smn'; // Inari Sami
    case SMO = 'smo'; // Samoan
    case SMS = 'sms'; // Skolt Sami
    case SNA = 'sna'; // Shona
    case SND = 'snd'; // Sindhi
    case SNK = 'snk'; // Soninke
    case SOG = 'sog'; // Sogdian
    case SOM = 'som'; // Somali
    case SON = 'son'; // Songhai languages
    case SOT = 'sot'; // Sotho, Southern
    case SPA = 'spa'; // Spanish; Castilian
    case SQI = 'sqi'; // Albanian
    case SRD = 'srd'; // Sardinian
    case SRN = 'srn'; // Sranan Tongo
    case SRP = 'srp'; // Serbian
    case SRR = 'srr'; // Serer
    case SSA = 'ssa'; // Nilo-Saharan languages
    case SSW = 'ssw'; // Swati
    case SUK = 'suk'; // Sukuma
    case SUN = 'sun'; // Sundanese
    case SUS = 'sus'; // Susu
    case SUX = 'sux'; // Sumerian
    case SWA = 'swa'; // Swahili
    case SWE = 'swe'; // Swedish
    case SYC = 'syc'; // Classical Syriac
    case SYR = 'syr'; // Syriac
    case TAH = 'tah'; // Tahitian
    case TAI = 'tai'; // Tai languages
    case TAM = 'tam'; // Tamil
    case TAT = 'tat'; // Tatar
    case TEL = 'tel'; // Telugu
    case TEM = 'tem'; // Timne
    case TER = 'ter'; // Tereno
    case TET = 'tet'; // Tetum
    case TGK = 'tgk'; // Tajik
    case TGL = 'tgl'; // Tagalog
    case THA = 'tha'; // Thai
    case TIG = 'tig'; // Tigre
    case TIR = 'tir'; // Tigrinya
    case TIV = 'tiv'; // Tiv
    case TKL = 'tkl'; // Tokelau
    case TLH = 'tlh'; // Klingon; tlhIngan-Hol
    case TLI = 'tli'; // Tlingit
    case TMH = 'tmh'; // Tamashek
    case TOG = 'tog'; // Tonga (Nyasa)
    case TON = 'ton'; // Tonga (Tonga Islands)
    case TPI = 'tpi'; // Tok Pisin
    case TSI = 'tsi'; // Tsimshian
    case TSN = 'tsn'; // Tswana
    case TSO = 'tso'; // Tsonga
    case TUK = 'tuk'; // Turkmen
    case TUM = 'tum'; // Tumbuka
    case TUP = 'tup'; // Tupi languages
    case TUR = 'tur'; // Turkish
    case TUT = 'tut'; // Altaic languages
    case TVL = 'tvl'; // Tuvalu
    case TWI = 'twi'; // Twi
    case TYV = 'tyv'; // Tuvinian
    case UDM = 'udm'; // Udmurt
    case UGA = 'uga'; // Ugaritic
    case UIG = 'uig'; // Uighur; Uyghur
    case UKR = 'ukr'; // Ukrainian
    case UMB = 'umb'; // Umbundu
    case UND = 'und'; // Undetermined
    case URD = 'urd'; // Urdu
    case UZB = 'uzb'; // Uzbek
    case VAI = 'vai'; // Vai
    case VEN = 'ven'; // Venda
    case VIE = 'vie'; // Vietnamese
    case VOL = 'vol'; // Volapük
    case VOT = 'vot'; // Votic
    case WAK = 'wak'; // Wakashan languages
    case WAL = 'wal'; // Walamo
    case WAR = 'war'; // Waray
    case WAS = 'was'; // Washo
    case WEN = 'wen'; // Sorbian languages
    case WLN = 'wln'; // Walloon
    case WOL = 'wol'; // Wolof
    case XAL = 'xal'; // Kalmyk; Oirat
    case XHO = 'xho'; // Xhosa
    case YAO = 'yao'; // Yao
    case YAP = 'yap'; // Yapese
    case YID = 'yid'; // Yiddish
    case YOR = 'yor'; // Yoruba
    case YPK = 'ypk'; // Yupik languages
    case ZAP = 'zap'; // Zapotec
    case ZBL = 'zbl'; // Blissymbols; Blissymbolics; Bliss
    case ZEN = 'zen'; // Zenaga
    case ZGH = 'zgh'; // Standard Moroccan Tamazight
    case ZHA = 'zha'; // Zhuang; Chuang
    case ZHO = 'zho'; // Chinese
    case ZND = 'znd'; // Zande languages
    case ZUL = 'zul'; // Zulu
    case ZUN = 'zun'; // Zuni
    case ZXX = 'zxx'; // No linguistic content; Not applicable
    case ZZA = 'zza'; // Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki
    // </editor-fold>
}
