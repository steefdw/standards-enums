<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\ISO639;

use Steefdw\StandardsEnums\EnumTrait;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use Steefdw\StandardsEnums\Locales\LocaleDescription;

trait LanguageEnumTrait
{
    use EnumTrait;

    public function getName(): ?LanguageName
    {
        return LanguageName::tryFromName($this->getLanguageCode());
    }

    public function getAlpha2(): ?LanguageAlpha2
    {
        return LanguageAlpha2::tryFromName($this->getLanguageCode());
    }

    public function getAlpha3(): ?LanguageAlpha3
    {
        return LanguageAlpha3::tryFromName($this->getLanguageCode());
    }

    /**
     * @return array<string,LocaleDescription>
     */
    public function getLocales(): array
    {
        $localeDescriptions = LocaleDescription::cases();
        if (!$this->getAlpha2()) {
            return [];
        }
        $currentLanguage = strtoupper($this->getAlpha2()->value) . '_';
        $matches = [];
        foreach ($localeDescriptions as $localeDescription) {
            if (str_starts_with($localeDescription->name, $currentLanguage)) {
                $matches[$localeDescription->name] = $localeDescription;
            }
        }

        return $matches;
    }

    /**
     * @return array<string,CountryName>
     */
    public function getCountries(): array
    {
        $locales = $this->getLocales();
        $countries = [];
        foreach (array_keys($locales) as $locale) {
            $countryCode = substr($locale, -2);
            if ($country = CountryName::tryFromName($countryCode)) {
                $countries[$countryCode] = $country;
            }
        }

        return $countries;
    }

    public static function tryFromAlpha2(LanguageAlpha2 $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    public static function tryFromAlpha3(LanguageAlpha3 $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    public static function tryFromLanguageName(LanguageName $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    /**
     * All Language* enums have the Alpha3 code as the name.
     */
    private function getLanguageCode(): string
    {
        return mb_strtoupper($this->name);
    }

    /**
     * @return array<int, static>
     */
    public static function casesSortedByLanguageName(?LanguageNameTranslation $translation = null): array
    {
        $translations = $translation ? LanguageName::toArrayTranslated($translation) : LanguageName::toArray();
        $sortedNames = array_flip($translations);
        ksort($sortedNames);
        $cases = static::getIndexedCases();
        $sortedCases = [];
        foreach ($sortedNames as $alpha3Code) {
            if (!isset($cases[$alpha3Code])) {
                continue; // Alpha2 has fewer languages than there are Alpha3 language names.
            }
            $sortedCases[$alpha3Code] = $cases[$alpha3Code];
        }

        return array_values($sortedCases);
    }
}
