<?php

/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Steefdw\StandardsEnums\ISO4217;

/**
 * ISO 4217 is a standard published by the International Organization for Standardization (ISO) that defines
 * alpha codes and numeric codes for the representation of currencies and provides information about the relationships
 * between individual currencies and their minor units.
 *
 * @see https://en.wikipedia.org/wiki/ISO_4217
 */
enum CurrencyNumeric: string implements CurrencyEnumInterface
{
    use CurrencyEnumTrait;

    // <editor-fold desc="Cases">
    case AED = '784'; // UAE Dirham
    case AFN = '971'; // Afghani
    case ALL = '008'; // Lek
    case AMD = '051'; // Armenian Dram
    case ANG = '532'; // Netherlands Antillean Guilder
    case AOA = '973'; // Kwanza
    case ARS = '032'; // Argentine Peso
    case AUD = '036'; // Australian Dollar
    case AWG = '533'; // Aruban Florin
    case AZN = '944'; // Azerbaijan Manat
    case BAM = '977'; // Convertible Mark
    case BBD = '052'; // Barbados Dollar
    case BDT = '050'; // Taka
    case BGN = '975'; // Bulgarian Lev
    case BHD = '048'; // Bahraini Dinar
    case BIF = '108'; // Burundi Franc
    case BMD = '060'; // Bermudian Dollar
    case BND = '096'; // Brunei Dollar
    case BOB = '068'; // Boliviano
    case BOV = '984'; // Mvdol
    case BRL = '986'; // Brazilian Real
    case BSD = '044'; // Bahamian Dollar
    case BTN = '064'; // Ngultrum
    case BWP = '072'; // Pula
    case BYN = '933'; // Belarusian Ruble
    case BZD = '084'; // Belize Dollar
    case CAD = '124'; // Canadian Dollar
    case CDF = '976'; // Congolese Franc
    case CHE = '947'; // WIR Euro
    case CHF = '756'; // Swiss Franc
    case CHW = '948'; // WIR Franc
    case CLF = '990'; // Unidad de Fomento
    case CLP = '152'; // Chilean Peso
    case CNY = '156'; // Yuan Renminbi
    case COP = '170'; // Colombian Peso
    case COU = '970'; // Unidad de Valor Real
    case CRC = '188'; // Costa Rican Colon
    case CUC = '931'; // Peso Convertible
    case CUP = '192'; // Cuban Peso
    case CVE = '132'; // Cabo Verde Escudo
    case CZK = '203'; // Czech Koruna
    case DJF = '262'; // Djibouti Franc
    case DKK = '208'; // Danish Krone
    case DOP = '214'; // Dominican Peso
    case DZD = '012'; // Algerian Dinar
    case EGP = '818'; // Egyptian Pound
    case ERN = '232'; // Nakfa
    case ETB = '230'; // Ethiopian Birr
    case EUR = '978'; // Euro
    case FJD = '242'; // Fiji Dollar
    case FKP = '238'; // Falkland Islands Pound
    case GBP = '826'; // Pound Sterling
    case GEL = '981'; // Lari
    case GHS = '936'; // Ghana Cedi
    case GIP = '292'; // Gibraltar Pound
    case GMD = '270'; // Dalasi
    case GNF = '324'; // Guinean Franc
    case GTQ = '320'; // Quetzal
    case GYD = '328'; // Guyana Dollar
    case HKD = '344'; // Hong Kong Dollar
    case HNL = '340'; // Lempira
    case HRK = '191'; // Kuna
    case HTG = '332'; // Gourde
    case HUF = '348'; // Forint
    case IDR = '360'; // Rupiah
    case ILS = '376'; // New Israeli Sheqel
    case INR = '356'; // Indian Rupee
    case IQD = '368'; // Iraqi Dinar
    case IRR = '364'; // Iranian Rial
    case ISK = '352'; // Iceland Krona
    case JMD = '388'; // Jamaican Dollar
    case JOD = '400'; // Jordanian Dinar
    case JPY = '392'; // Yen
    case KES = '404'; // Kenyan Shilling
    case KGS = '417'; // Som
    case KHR = '116'; // Riel
    case KMF = '174'; // Comorian Franc
    case KPW = '408'; // North Korean Won
    case KRW = '410'; // Won
    case KWD = '414'; // Kuwaiti Dinar
    case KYD = '136'; // Cayman Islands Dollar
    case KZT = '398'; // Tenge
    case LAK = '418'; // Lao Kip
    case LBP = '422'; // Lebanese Pound
    case LKR = '144'; // Sri Lanka Rupee
    case LRD = '430'; // Liberian Dollar
    case LSL = '426'; // Loti
    case LYD = '434'; // Libyan Dinar
    case MAD = '504'; // Moroccan Dirham
    case MDL = '498'; // Moldovan Leu
    case MGA = '969'; // Malagasy Ariary
    case MKD = '807'; // Denar
    case MMK = '104'; // Kyat
    case MNT = '496'; // Tugrik
    case MOP = '446'; // Pataca
    case MRU = '929'; // Ouguiya
    case MUR = '480'; // Mauritius Rupee
    case MVR = '462'; // Rufiyaa
    case MWK = '454'; // Malawi Kwacha
    case MXN = '484'; // Mexican Peso
    case MXV = '979'; // Mexican Unidad de Inversion (UDI)
    case MYR = '458'; // Malaysian Ringgit
    case MZN = '943'; // Mozambique Metical
    case NAD = '516'; // Namibia Dollar
    case NGN = '566'; // Naira
    case NIO = '558'; // Cordoba Oro
    case NOK = '578'; // Norwegian Krone
    case NPR = '524'; // Nepalese Rupee
    case NZD = '554'; // New Zealand Dollar
    case OMR = '512'; // Rial Omani
    case PAB = '590'; // Balboa
    case PEN = '604'; // Sol
    case PGK = '598'; // Kina
    case PHP = '608'; // Philippine Peso
    case PKR = '586'; // Pakistan Rupee
    case PLN = '985'; // Zloty
    case PYG = '600'; // Guarani
    case QAR = '634'; // Qatari Rial
    case RON = '946'; // Romanian Leu
    case RSD = '941'; // Serbian Dinar
    case RUB = '643'; // Russian Ruble
    case RWF = '646'; // Rwanda Franc
    case SAR = '682'; // Saudi Riyal
    case SBD = '090'; // Solomon Islands Dollar
    case SCR = '690'; // Seychelles Rupee
    case SDG = '938'; // Sudanese Pound
    case SEK = '752'; // Swedish Krona
    case SGD = '702'; // Singapore Dollar
    case SHP = '654'; // Saint Helena Pound
    case SLE = '925'; // Leone
    case SLL = '694'; // Leone
    case SOS = '706'; // Somali Shilling
    case SRD = '968'; // Surinam Dollar
    case SSP = '728'; // South Sudanese Pound
    case STN = '930'; // Dobra
    case SVC = '222'; // El Salvador Colon
    case SYP = '760'; // Syrian Pound
    case SZL = '748'; // Lilangeni
    case THB = '764'; // Baht
    case TJS = '972'; // Somoni
    case TMT = '934'; // Turkmenistan New Manat
    case TND = '788'; // Tunisian Dinar
    case TOP = '776'; // Pa’anga
    case TRY = '949'; // Turkish Lira
    case TTD = '780'; // Trinidad and Tobago Dollar
    case TWD = '901'; // New Taiwan Dollar
    case TZS = '834'; // Tanzanian Shilling
    case UAH = '980'; // Hryvnia
    case UGX = '800'; // Uganda Shilling
    case USD = '840'; // US Dollar
    case USN = '997'; // US Dollar (Next day)
    case UYI = '940'; // Uruguay Peso en Unidades Indexadas (UI)
    case UYU = '858'; // Peso Uruguayo
    case UYW = '927'; // Unidad Previsional
    case UZS = '860'; // Uzbekistan Sum
    case VED = '926'; // Bolívar Soberano
    case VES = '928'; // Bolívar Soberano
    case VND = '704'; // Dong
    case VUV = '548'; // Vatu
    case WST = '882'; // Tala
    case XAF = '950'; // CFA Franc BEAC
    case XAG = '961'; // Silver
    case XAU = '959'; // Gold
    case XBA = '955'; // Bond Markets Unit European Composite Unit (EURCO)
    case XBB = '956'; // Bond Markets Unit European Monetary Unit (E.M.U.-6)
    case XBC = '957'; // Bond Markets Unit European Unit of Account 9 (E.U.A.-9)
    case XBD = '958'; // Bond Markets Unit European Unit of Account 17 (E.U.A.-17)
    case XCD = '951'; // East Caribbean Dollar
    case XDR = '960'; // SDR (Special Drawing Right)
    case XOF = '952'; // CFA Franc BCEAO
    case XPD = '964'; // Palladium
    case XPF = '953'; // CFP Franc
    case XPT = '962'; // Platinum
    case XSU = '994'; // Sucre
    case XTS = '963'; // Codes specifically reserved for testing purposes
    case XUA = '965'; // ADB Unit of Account
    case XXX = '999'; // The codes assigned for transactions where no currency is involved
    case YER = '886'; // Yemeni Rial
    case ZAR = '710'; // Rand
    case ZMW = '967'; // Zambian Kwacha
    case ZWL = '932'; // Zimbabwe Dollar
    // </editor-fold>
}
