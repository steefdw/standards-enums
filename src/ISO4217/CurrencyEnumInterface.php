<?php

namespace Steefdw\StandardsEnums\ISO4217;

use BackedEnum;

interface CurrencyEnumInterface extends BackedEnum
{
    public static function tryFromAlpha3(CurrencyAlpha3 $enum): ?static;

    public static function tryFromCurrencyName(CurrencyName $enum): ?static;

    public static function tryFromNumeric(CurrencyNumeric $enum): ?static;

    /**
     * @return array<int, self>
     */
    public static function casesSortedByCurrencyName(?CurrencyNameTranslation $translation = null): array;
}
