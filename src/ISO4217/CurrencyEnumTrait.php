<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\ISO4217;

use Steefdw\StandardsEnums\EnumTrait;
use Steefdw\StandardsEnums\ISO3166\CountryName;

trait CurrencyEnumTrait
{
    use EnumTrait;

    public function getName(): ?CurrencyName
    {
        return CurrencyName::tryFromName($this->getCurrencyCode());
    }

    public function getAlpha3(): ?CurrencyAlpha3
    {
        return CurrencyAlpha3::tryFromName($this->getCurrencyCode());
    }

    public function getNumeric(): ?CurrencyNumeric
    {
        return CurrencyNumeric::tryFromName($this->getCurrencyCode());
    }

    /**
     * @return array<string,CountryName>
     */
    public function getCountries(): array
    {
        /** @var array<string, array<string,CountryName>> $currencyCountries */
        $currencyCountries = [];
        include dirname(__DIR__) . '/maps/currencyCountries.php';

        return $currencyCountries[$this->getCurrencyCode()] ?? [];
    }

    public static function tryFromAlpha3(CurrencyAlpha3 $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    public static function tryFromCurrencyName(CurrencyName $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    public static function tryFromNumeric(CurrencyNumeric $enum): ?static
    {
        return self::tryFromName($enum->name);
    }

    /**
     * All Currency* enums have the Alpha3 code as the name.
     */
    private function getCurrencyCode(): string
    {
        return mb_strtoupper($this->name);
    }

    /**
     * @return array<int, self>
     */
    public static function casesSortedByCurrencyName(?CurrencyNameTranslation $translation = null): array
    {
        $translations = $translation ? CurrencyName::toArrayTranslated($translation) : CurrencyName::toArray();
        $sortedNames = array_flip($translations);
        ksort($sortedNames, SORT_FLAG_CASE | SORT_NATURAL);
        $cases = self::getIndexedCases();

        $sortedCases = [];
        foreach ($sortedNames as $alpha3Code) {
            $sortedCases[$alpha3Code] = $cases[$alpha3Code];
        }

        return array_values($sortedCases);
    }
}
