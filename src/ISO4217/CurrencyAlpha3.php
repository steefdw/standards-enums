<?php

/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Steefdw\StandardsEnums\ISO4217;

/**
 * ISO 4217 is a standard published by the International Organization for Standardization (ISO) that defines
 * alpha codes and numeric codes for the representation of currencies and provides information about the relationships
 * between individual currencies and their minor units.
 *
 * @see https://en.wikipedia.org/wiki/ISO_4217
 */
enum CurrencyAlpha3: string implements CurrencyEnumInterface
{
    use CurrencyEnumTrait;

    // <editor-fold desc="Cases">
    case AED = 'AED'; // UAE Dirham
    case AFN = 'AFN'; // Afghani
    case ALL = 'ALL'; // Lek
    case AMD = 'AMD'; // Armenian Dram
    case ANG = 'ANG'; // Netherlands Antillean Guilder
    case AOA = 'AOA'; // Kwanza
    case ARS = 'ARS'; // Argentine Peso
    case AUD = 'AUD'; // Australian Dollar
    case AWG = 'AWG'; // Aruban Florin
    case AZN = 'AZN'; // Azerbaijan Manat
    case BAM = 'BAM'; // Convertible Mark
    case BBD = 'BBD'; // Barbados Dollar
    case BDT = 'BDT'; // Taka
    case BGN = 'BGN'; // Bulgarian Lev
    case BHD = 'BHD'; // Bahraini Dinar
    case BIF = 'BIF'; // Burundi Franc
    case BMD = 'BMD'; // Bermudian Dollar
    case BND = 'BND'; // Brunei Dollar
    case BOB = 'BOB'; // Boliviano
    case BOV = 'BOV'; // Mvdol
    case BRL = 'BRL'; // Brazilian Real
    case BSD = 'BSD'; // Bahamian Dollar
    case BTN = 'BTN'; // Ngultrum
    case BWP = 'BWP'; // Pula
    case BYN = 'BYN'; // Belarusian Ruble
    case BZD = 'BZD'; // Belize Dollar
    case CAD = 'CAD'; // Canadian Dollar
    case CDF = 'CDF'; // Congolese Franc
    case CHE = 'CHE'; // WIR Euro
    case CHF = 'CHF'; // Swiss Franc
    case CHW = 'CHW'; // WIR Franc
    case CLF = 'CLF'; // Unidad de Fomento
    case CLP = 'CLP'; // Chilean Peso
    case CNY = 'CNY'; // Yuan Renminbi
    case COP = 'COP'; // Colombian Peso
    case COU = 'COU'; // Unidad de Valor Real
    case CRC = 'CRC'; // Costa Rican Colon
    case CUC = 'CUC'; // Peso Convertible
    case CUP = 'CUP'; // Cuban Peso
    case CVE = 'CVE'; // Cabo Verde Escudo
    case CZK = 'CZK'; // Czech Koruna
    case DJF = 'DJF'; // Djibouti Franc
    case DKK = 'DKK'; // Danish Krone
    case DOP = 'DOP'; // Dominican Peso
    case DZD = 'DZD'; // Algerian Dinar
    case EGP = 'EGP'; // Egyptian Pound
    case ERN = 'ERN'; // Nakfa
    case ETB = 'ETB'; // Ethiopian Birr
    case EUR = 'EUR'; // Euro
    case FJD = 'FJD'; // Fiji Dollar
    case FKP = 'FKP'; // Falkland Islands Pound
    case GBP = 'GBP'; // Pound Sterling
    case GEL = 'GEL'; // Lari
    case GHS = 'GHS'; // Ghana Cedi
    case GIP = 'GIP'; // Gibraltar Pound
    case GMD = 'GMD'; // Dalasi
    case GNF = 'GNF'; // Guinean Franc
    case GTQ = 'GTQ'; // Quetzal
    case GYD = 'GYD'; // Guyana Dollar
    case HKD = 'HKD'; // Hong Kong Dollar
    case HNL = 'HNL'; // Lempira
    case HRK = 'HRK'; // Kuna
    case HTG = 'HTG'; // Gourde
    case HUF = 'HUF'; // Forint
    case IDR = 'IDR'; // Rupiah
    case ILS = 'ILS'; // New Israeli Sheqel
    case INR = 'INR'; // Indian Rupee
    case IQD = 'IQD'; // Iraqi Dinar
    case IRR = 'IRR'; // Iranian Rial
    case ISK = 'ISK'; // Iceland Krona
    case JMD = 'JMD'; // Jamaican Dollar
    case JOD = 'JOD'; // Jordanian Dinar
    case JPY = 'JPY'; // Yen
    case KES = 'KES'; // Kenyan Shilling
    case KGS = 'KGS'; // Som
    case KHR = 'KHR'; // Riel
    case KMF = 'KMF'; // Comorian Franc
    case KPW = 'KPW'; // North Korean Won
    case KRW = 'KRW'; // Won
    case KWD = 'KWD'; // Kuwaiti Dinar
    case KYD = 'KYD'; // Cayman Islands Dollar
    case KZT = 'KZT'; // Tenge
    case LAK = 'LAK'; // Lao Kip
    case LBP = 'LBP'; // Lebanese Pound
    case LKR = 'LKR'; // Sri Lanka Rupee
    case LRD = 'LRD'; // Liberian Dollar
    case LSL = 'LSL'; // Loti
    case LYD = 'LYD'; // Libyan Dinar
    case MAD = 'MAD'; // Moroccan Dirham
    case MDL = 'MDL'; // Moldovan Leu
    case MGA = 'MGA'; // Malagasy Ariary
    case MKD = 'MKD'; // Denar
    case MMK = 'MMK'; // Kyat
    case MNT = 'MNT'; // Tugrik
    case MOP = 'MOP'; // Pataca
    case MRU = 'MRU'; // Ouguiya
    case MUR = 'MUR'; // Mauritius Rupee
    case MVR = 'MVR'; // Rufiyaa
    case MWK = 'MWK'; // Malawi Kwacha
    case MXN = 'MXN'; // Mexican Peso
    case MXV = 'MXV'; // Mexican Unidad de Inversion (UDI)
    case MYR = 'MYR'; // Malaysian Ringgit
    case MZN = 'MZN'; // Mozambique Metical
    case NAD = 'NAD'; // Namibia Dollar
    case NGN = 'NGN'; // Naira
    case NIO = 'NIO'; // Cordoba Oro
    case NOK = 'NOK'; // Norwegian Krone
    case NPR = 'NPR'; // Nepalese Rupee
    case NZD = 'NZD'; // New Zealand Dollar
    case OMR = 'OMR'; // Rial Omani
    case PAB = 'PAB'; // Balboa
    case PEN = 'PEN'; // Sol
    case PGK = 'PGK'; // Kina
    case PHP = 'PHP'; // Philippine Peso
    case PKR = 'PKR'; // Pakistan Rupee
    case PLN = 'PLN'; // Zloty
    case PYG = 'PYG'; // Guarani
    case QAR = 'QAR'; // Qatari Rial
    case RON = 'RON'; // Romanian Leu
    case RSD = 'RSD'; // Serbian Dinar
    case RUB = 'RUB'; // Russian Ruble
    case RWF = 'RWF'; // Rwanda Franc
    case SAR = 'SAR'; // Saudi Riyal
    case SBD = 'SBD'; // Solomon Islands Dollar
    case SCR = 'SCR'; // Seychelles Rupee
    case SDG = 'SDG'; // Sudanese Pound
    case SEK = 'SEK'; // Swedish Krona
    case SGD = 'SGD'; // Singapore Dollar
    case SHP = 'SHP'; // Saint Helena Pound
    case SLE = 'SLE'; // Leone
    case SLL = 'SLL'; // Leone
    case SOS = 'SOS'; // Somali Shilling
    case SRD = 'SRD'; // Surinam Dollar
    case SSP = 'SSP'; // South Sudanese Pound
    case STN = 'STN'; // Dobra
    case SVC = 'SVC'; // El Salvador Colon
    case SYP = 'SYP'; // Syrian Pound
    case SZL = 'SZL'; // Lilangeni
    case THB = 'THB'; // Baht
    case TJS = 'TJS'; // Somoni
    case TMT = 'TMT'; // Turkmenistan New Manat
    case TND = 'TND'; // Tunisian Dinar
    case TOP = 'TOP'; // Pa’anga
    case TRY = 'TRY'; // Turkish Lira
    case TTD = 'TTD'; // Trinidad and Tobago Dollar
    case TWD = 'TWD'; // New Taiwan Dollar
    case TZS = 'TZS'; // Tanzanian Shilling
    case UAH = 'UAH'; // Hryvnia
    case UGX = 'UGX'; // Uganda Shilling
    case USD = 'USD'; // US Dollar
    case USN = 'USN'; // US Dollar (Next day)
    case UYI = 'UYI'; // Uruguay Peso en Unidades Indexadas (UI)
    case UYU = 'UYU'; // Peso Uruguayo
    case UYW = 'UYW'; // Unidad Previsional
    case UZS = 'UZS'; // Uzbekistan Sum
    case VED = 'VED'; // Bolívar Soberano
    case VES = 'VES'; // Bolívar Soberano
    case VND = 'VND'; // Dong
    case VUV = 'VUV'; // Vatu
    case WST = 'WST'; // Tala
    case XAF = 'XAF'; // CFA Franc BEAC
    case XAG = 'XAG'; // Silver
    case XAU = 'XAU'; // Gold
    case XBA = 'XBA'; // Bond Markets Unit European Composite Unit (EURCO)
    case XBB = 'XBB'; // Bond Markets Unit European Monetary Unit (E.M.U.-6)
    case XBC = 'XBC'; // Bond Markets Unit European Unit of Account 9 (E.U.A.-9)
    case XBD = 'XBD'; // Bond Markets Unit European Unit of Account 17 (E.U.A.-17)
    case XCD = 'XCD'; // East Caribbean Dollar
    case XDR = 'XDR'; // SDR (Special Drawing Right)
    case XOF = 'XOF'; // CFA Franc BCEAO
    case XPD = 'XPD'; // Palladium
    case XPF = 'XPF'; // CFP Franc
    case XPT = 'XPT'; // Platinum
    case XSU = 'XSU'; // Sucre
    case XTS = 'XTS'; // Codes specifically reserved for testing purposes
    case XUA = 'XUA'; // ADB Unit of Account
    case XXX = 'XXX'; // The codes assigned for transactions where no currency is involved
    case YER = 'YER'; // Yemeni Rial
    case ZAR = 'ZAR'; // Rand
    case ZMW = 'ZMW'; // Zambian Kwacha
    case ZWL = 'ZWL'; // Zimbabwe Dollar
    // </editor-fold>
}
