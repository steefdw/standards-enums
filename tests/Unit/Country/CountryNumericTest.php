<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Country;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha2;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use Steefdw\StandardsEnums\ISO3166\CountryNumeric;
use ValueError;

final class CountryNumericTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_a_country_numeric_code(): void
    {
        $countryNumeric = CountryNumeric::tryFromName(CountryAlpha2::NL->value);
        $this->assertSame(CountryNumeric::NL, $countryNumeric);
    }

    /**
     * @test
     *
     * @noinspection PhpCaseWithValueNotFoundInEnumInspection
     * @noinspection PhpExpressionResultUnusedInspection
     */
    public function it_should_not_get_non_existing_country_numeric_codes(): void
    {
        $this->expectException(ValueError::class);
        CountryNumeric::from('::not-found::');
    }

    /**
     * @test
     */
    public function it_should_get_the_alpha2_of_a_country_numeric_code(): void
    {
        $countryNumeric = CountryNumeric::tryFromName(CountryAlpha2::NL->value);
        $this->assertSame(CountryNumeric::NL, $countryNumeric);
        /** @var CountryNumeric $countryNumeric */
        $countryAlpha2 = $countryNumeric->getAlpha2();

        $this->assertSame(CountryAlpha2::NL, $countryAlpha2);
    }

    /**
     * @test
     */
    public function it_should_get_the_numeric_code_of_a_country_name(): void
    {
        $countryName = CountryName::tryFromName(CountryAlpha2::NL->value);
        $this->assertSame(CountryName::NL, $countryName);
        /** @var CountryName $countryName */
        $countryFlag = $countryName->getNumeric();

        $this->assertSame(CountryNumeric::NL, $countryFlag);
    }
}
