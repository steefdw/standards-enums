<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Country;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha2;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha3;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use ValueError;

final class CountryAlpha3Test extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_a_country_alpha3(): void
    {
        $country = CountryAlpha3::tryFromName(CountryAlpha2::NL->value);
        $this->assertInstanceOf(CountryAlpha3::class, $country);
        /** @var CountryAlpha3 $country */
        $this->assertTrue(strlen($country->name) === 2);
        $this->assertTrue(strlen($country->value) === 3);
        $this->assertSame(CountryAlpha3::NL, $country);
    }

    /**
     * @test
     *
     * @noinspection PhpCaseWithValueNotFoundInEnumInspection
     * @noinspection PhpExpressionResultUnusedInspection
     */
    public function it_should_not_get_non_existing_country_alpha3(): void
    {
        $this->expectException(ValueError::class);
        CountryAlpha3::from('404');
    }

    /**
     * @test
     */
    public function it_should_get_the_name_of_a_country_alpha3(): void
    {
        $country = CountryAlpha3::tryFromName(CountryAlpha2::NL->value);
        $this->assertInstanceOf(CountryAlpha3::class, $country);
        /** @var CountryAlpha3 $country */
        $countryName = $country->getName();

        $this->assertSame(CountryName::NL, $countryName);
    }

    /**
     * @test
     */
    public function it_should_get_the_alpha2_of_a_country_alpha3(): void
    {
        $countryAlpha3 = CountryAlpha3::tryFromName(CountryAlpha2::NL->value);
        $this->assertInstanceOf(CountryAlpha3::class, $countryAlpha3);
        /** @var CountryAlpha3 $countryAlpha3 */
        $countryName = $countryAlpha3->getAlpha2();

        $this->assertSame(CountryAlpha2::NL, $countryName);
    }
}
