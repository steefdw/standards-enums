<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Country;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha2;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha3;
use Steefdw\StandardsEnums\ISO3166\CountryEnumInterface;
use Steefdw\StandardsEnums\ISO3166\CountryFlag;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use Steefdw\StandardsEnums\ISO3166\CountryNameTranslation;
use Steefdw\StandardsEnums\ISO3166\CountryNumeric;
use Steefdw\StandardsEnums\Locales\LocaleCode;

final class CountryNameTranslationTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_translated_cases_for_the_country_names_enum(): void
    {
        $dutch = CountryNameTranslation::NL_NL;
        $names = CountryName::toArrayTranslated($dutch);

        $this->assertSame('Nederland', $names[CountryName::NL->name]);
        $this->assertSame('België', $names[CountryName::BE->name]);
    }

    /**
     * @test
     */
    public function it_should_have_translations_for_all_CountryNameTranslation_cases(): void
    {
        $translationLanguages = CountryNameTranslation::cases();
        $countryNameCount = \count(CountryName::cases());
        foreach ($translationLanguages as $translationLanguage) {
            $countryList = CountryName::toArrayTranslated($translationLanguage);
            $this->assertTrue(\count($countryList) === $countryNameCount);
        }
    }

    /**
     * @test
     */
    public function it_should_have_a_translation_file_for_all_CountryNameTranslation_cases(): void
    {
        $cases = CountryNameTranslation::cases();
        foreach ($cases as $case) {
            try {
                $translations = $case->getTranslations();
                $this->assertIsArray($translations);
            } catch (\Exception $exception) {
                $this->fail($exception->getMessage());
            }
        }
    }

    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_sorted_cases_for_an_enum(
        CountryEnumInterface $expectedValue
    ): void {
        $country = $expectedValue::tryFromAlpha2(CountryAlpha2::NL);

        $unsorted = $country::cases(); // @phpstan-ignore-line
        $sorted = $country::casesSorted(); // @phpstan-ignore-line
        $unsortedFirstName = current($unsorted)->name; // AW
        $sortedFirstName = current($sorted)->name;     // AD

        $this->assertLessThan($unsortedFirstName, $sortedFirstName);
    }

    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_sorted_cases_for_an_enum_when_sorted_by_translated_country_names(
        CountryEnumInterface $countryEnum
    ): void {
        /** @var CountryEnumInterface $country */
        $country = $countryEnum::tryFromAlpha2(CountryAlpha2::NL);
        $this->assertInstanceOf($countryEnum::class, $country);
        /** @var CountryNameTranslation $dutch */
        $dutch = CountryNameTranslation::tryFromName(LocaleCode::NL_NL->value);
        $this->assertInstanceOf(CountryNameTranslation::class, $dutch);
        $sortedDutch = $country::casesSortedByCountryName($dutch);
        $indexDistance = $this->getIndexDistance($sortedDutch);
        $message = '"Australië" and "Oostenrijk" should be far apart from each other';
        $this->assertLessThan($indexDistance, 100, $message);

        $sortedEnglish = $country::casesSortedByCountryName();
        $indexDistance = $this->getIndexDistance($sortedEnglish);
        $message = '"Australia" and "Austria" should be near each other';
        $this->assertLessThan(3, $indexDistance, $message);
    }

    /**
     * @param CountryEnumInterface[] $countryEnums
     */
    private function getIndexDistance(array $countryEnums): int
    {
        $countryCodes = [];
        foreach ($countryEnums as $index => $countryEnum) {
            $countryCodes[$countryEnum->name] = $index;
        }

        return $countryCodes['AT'] - $countryCodes['AU'];
    }

    /**
     * @return array<int, array<int, CountryAlpha2|CountryAlpha3|CountryFlag|CountryName|CountryNumeric>>
     */
    public function provideEnums(): array
    {
        return [
            [CountryAlpha2::NL],
            [CountryAlpha3::NL],
            [CountryFlag::NL],
            [CountryName::NL],
            [CountryNumeric::NL],
        ];
    }
}
