<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Country;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha2;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha3;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use ValueError;

final class CountryNameTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_a_country_name(): void
    {
        $countryName = CountryName::tryFromName(CountryAlpha2::NL->value);
        $this->assertSame(CountryName::NL, $countryName);
    }

    /**
     * @test
     *
     * @noinspection PhpCaseWithValueNotFoundInEnumInspection
     * @noinspection PhpExpressionResultUnusedInspection
     */
    public function it_should_not_get_non_existing_country_names(): void
    {
        $this->expectException(ValueError::class);
        CountryName::from('404');
    }

    /**
     * @test
     */
    public function it_should_get_the_alpha2_of_a_country_name(): void
    {
        $countryName = CountryName::tryFromName(CountryAlpha2::NL->value);
        $this->assertSame(CountryName::NL, $countryName);
        /** @var CountryName $countryName */
        $countryAlpha2 = $countryName->getAlpha2();

        $this->assertSame(CountryAlpha2::NL, $countryAlpha2);
    }

    /**
     * @test
     */
    public function it_should_get_the_alpha3_of_a_country_name(): void
    {
        $countryName = CountryName::tryFromName(CountryAlpha2::NL->value);
        $this->assertSame(CountryName::NL, $countryName);
        /** @var CountryName $countryName */
        $countryAlpha3 = $countryName->getAlpha3();

        $this->assertSame(CountryAlpha3::NL, $countryAlpha3);
    }
}
