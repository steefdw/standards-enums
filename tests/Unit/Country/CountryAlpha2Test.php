<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Country;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha2;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha3;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use ValueError;

final class CountryAlpha2Test extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_a_country(): void
    {
        $country = CountryAlpha2::tryFrom(CountryAlpha2::NL->value);
        $this->assertInstanceOf(CountryAlpha2::class, $country);
        /** @var CountryAlpha2 $country */
        $this->assertTrue(strlen($country->name) === 2);
        $this->assertTrue(strlen($country->value) === 2);
        $this->assertSame(CountryAlpha2::NL, $country);
    }

    /**
     * @test
     *
     * @noinspection PhpCaseWithValueNotFoundInEnumInspection
     * @noinspection PhpExpressionResultUnusedInspection
     */
    public function it_should_not_get_non_existing_countries(): void
    {
        $this->expectException(ValueError::class);
        CountryAlpha2::from('404');
    }

    /**
     * @test
     */
    public function it_should_get_the_name_of_a_country(): void
    {
        $country = CountryAlpha2::tryFrom(CountryAlpha2::NL->value);
        $this->assertInstanceOf(CountryAlpha2::class, $country);
        /** @var CountryAlpha2 $country */
        $countryName = $country->getName();

        $this->assertSame(CountryName::NL, $countryName);
    }

    /**
     * @test
     */
    public function it_should_get_the_alpha3_of_a_country(): void
    {
        $country = CountryAlpha2::tryFrom(CountryAlpha2::NL->value);
        $this->assertInstanceOf(CountryAlpha2::class, $country);
        /** @var CountryAlpha2 $country */
        $countryAlpha3 = $country->getAlpha3();

        $this->assertSame(CountryAlpha3::NL, $countryAlpha3);
    }
}
