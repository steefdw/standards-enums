<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Country;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha2;
use Steefdw\StandardsEnums\ISO3166\CountryAlpha3;
use Steefdw\StandardsEnums\ISO3166\CountryEnumInterface;
use Steefdw\StandardsEnums\ISO3166\CountryFlag;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use Steefdw\StandardsEnums\ISO3166\CountryNumeric;

final class TryFromTest extends TestCase
{
    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_an_enum_from_an_alpha2(
        CountryEnumInterface $expectedValue
    ): void {
        $country = $expectedValue::tryFromAlpha2(CountryAlpha2::NL);

        $this->assertSame($expectedValue, $country);
    }

    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_an_enum_from_an_alpha3(
        CountryEnumInterface $expectedValue
    ): void {
        $country = $expectedValue::tryFromAlpha3(CountryAlpha3::NL);

        $this->assertSame($expectedValue, $country);
    }

    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_an_enum_from_a_flag(
        CountryEnumInterface $expectedValue
    ): void {
        $country = $expectedValue::tryFromFlag(CountryFlag::NL);

        $this->assertSame($expectedValue, $country);
    }

    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_an_enum_from_a_country_name(
        CountryEnumInterface $expectedValue
    ): void {
        $country = $expectedValue::tryFromCountryName(CountryName::NL);

        $this->assertSame($expectedValue, $country);
    }

    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_an_enum_from_a_numeric_code(
        CountryEnumInterface $expectedValue
    ): void {
        $country = $expectedValue::tryFromNumeric(CountryNumeric::NL);

        $this->assertSame($expectedValue, $country);
    }

    /**
     * @return array<int, array<int, CountryAlpha2|CountryAlpha3|CountryFlag|CountryName|CountryNumeric>>
     */
    public function provideEnums(): array
    {
        return [
            [CountryAlpha2::NL],
            [CountryAlpha3::NL],
            [CountryFlag::NL],
            [CountryName::NL],
            [CountryNumeric::NL],
        ];
    }
}
