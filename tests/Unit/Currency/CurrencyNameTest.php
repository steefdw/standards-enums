<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Currency;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use Steefdw\StandardsEnums\ISO4217\CurrencyAlpha3;
use Steefdw\StandardsEnums\ISO4217\CurrencyName;

final class CurrencyNameTest extends TestCase
{
    /** @test */
    public function it_should_get_a_currency_name(): void
    {
        $currencyName = CurrencyName::tryFromName(CurrencyAlpha3::EUR->name);
        $this->assertSame(CurrencyName::EUR, $currencyName);
    }

    /** @test */
    public function it_should_get_currencies_from_a_country(): void
    {
        $belgium = CountryName::BE;
        $currencies = $belgium->getCurrencies();
        $expected = [
            'EUR' => CurrencyName::EUR,
        ];
        $this->assertSame($expected, $currencies);
    }

    /** @test */
    public function it_should_get_countries_from_a_currency(): void
    {
        $euro = CurrencyName::EUR;
        $countries = $euro->getCountries();
        $this->assertTrue(
            count($countries) >= 26,
            'There should be at least 26 countries with the Euro'
        );
        $expected = [
            'BE' => CountryName::BE,
            'NL' => CountryName::NL,
            'DE' => CountryName::DE,
            'FR' => CountryName::FR,
        ];
        foreach ($expected as $countryCode => $countryEnum) {
            $this->assertArrayHasKey($countryCode, $countries);
            $this->assertSame($countryEnum, $countries[$countryCode]);
        }
    }
}
