<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Currency;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO4217\CurrencyAlpha3;
use Steefdw\StandardsEnums\ISO4217\CurrencyEnumInterface;
use Steefdw\StandardsEnums\ISO4217\CurrencyName;
use Steefdw\StandardsEnums\ISO4217\CurrencyNameTranslation;
use Steefdw\StandardsEnums\ISO4217\CurrencyNumeric;
use Steefdw\StandardsEnums\Locales\LocaleCode;

final class CurrencyNameTranslationTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_translated_cases_for_the_currency_names_enum(): void
    {
        $dutch = CurrencyNameTranslation::NL_NL;
        $names = CurrencyName::toArrayTranslated($dutch);

        $this->assertSame('euro', $names[CurrencyName::EUR->name]);
        $this->assertSame('Amerikaanse dollar', $names[CurrencyName::USD->name]);
    }

    /**
     * @test
     */
    public function it_should_have_translations_for_all_CurrencyNameTranslation_cases(): void
    {
        $translationLanguages = CurrencyNameTranslation::cases();
        $currencyNameCount = \count(CurrencyName::cases());
        foreach ($translationLanguages as $translationLanguage) {
            $currencyList = CurrencyName::toArrayTranslated($translationLanguage);
            $this->assertTrue(\count($currencyList) === $currencyNameCount);
        }
    }

    /**
     * @test
     */
    public function it_should_have_a_translation_file_for_all_CurrencyNameTranslation_cases(): void
    {
        $cases = CurrencyNameTranslation::cases();
        foreach ($cases as $case) {
            try {
                $translations = $case->getTranslations();
                $this->assertIsArray($translations);
            } catch (\Exception $exception) {
                $this->fail($exception->getMessage());
            }
        }
    }

    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_sorted_cases_for_an_enum_when_sorted_by_translated_currency_names(
        CurrencyEnumInterface $currencyEnum
    ): void {
        /** @var CurrencyEnumInterface $currency */
        $currency = $currencyEnum::tryFromAlpha3(CurrencyAlpha3::EUR);
        $this->assertInstanceOf($currencyEnum::class, $currency);
        /** @var CurrencyNameTranslation $euro */
        $euro = CurrencyNameTranslation::tryFromName(LocaleCode::NL_NL->value);
        $this->assertInstanceOf(CurrencyNameTranslation::class, $euro);
        $sortedDutch = $currency::casesSortedByCurrencyName($euro);
        $indexDistance = $this->getIndexDistance($sortedDutch);
        $message = '"Australische dollar" (AUD) and "Amerikaanse dollar" (USD) should be near each other';
        $this->assertLessThan(3, $indexDistance, $message);

        $sortedEnglish = $currency::casesSortedByCurrencyName();
        $indexDistance = $this->getIndexDistance($sortedEnglish);
        $message = '"Australian Dollar" (AUD) and "US Dollar" (USD) should be far apart from each other';
        $this->assertLessThan($indexDistance, 100, $message);
    }

    /**
     * @param CurrencyEnumInterface[] $currencyEnums
     */
    private function getIndexDistance(array $currencyEnums): int
    {
        $currencyCodes = [];
        foreach ($currencyEnums as $index => $currencyEnum) {
            $currencyCodes[$currencyEnum->name] = $index;
        }

        return $currencyCodes['USD'] - $currencyCodes['EUR'];
    }

    /**
     * @return array<int, array<int, CurrencyAlpha3|CurrencyName|CurrencyNumeric>>
     */
    public function provideEnums(): array
    {
        return [
            [CurrencyName::EUR],
            [CurrencyNumeric::EUR],
            [CurrencyAlpha3::EUR],
        ];
    }
}
