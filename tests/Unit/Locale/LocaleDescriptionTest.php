<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Locale;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use Steefdw\StandardsEnums\ISO639\LanguageName;
use Steefdw\StandardsEnums\Locales\LocaleCode;
use Steefdw\StandardsEnums\Locales\LocaleDescription;

final class LocaleDescriptionTest extends TestCase
{
    /** @test */
    public function it_should_get_the_country_from_a_locale(): void
    {
        $localeDescription = LocaleDescription::tryFromName('zh_Hans_CN');
        $countryName = $localeDescription?->getCountry();
        $this->assertSame(CountryName::CN, $countryName);
    }

    /** @test */
    public function it_should_get_the_language_from_a_locale(): void
    {
        $localeDescription = LocaleDescription::tryFromName('zh_Hans_CN');
        $languageName = $localeDescription?->getLanguage();
        $this->assertSame(LanguageName::ZHO, $languageName);
    }

    /** @test */
    public function it_should_get_both_from_a_locale(): void
    {
        $localeDescription = LocaleDescription::tryFromName('nl_BE');
        $languageName = $localeDescription?->getLanguage();
        $this->assertSame(LanguageName::NLD, $languageName);
        $countryName = $localeDescription?->getCountry();
        $this->assertSame(CountryName::BE, $countryName);
    }

    /** @test */
    public function it_should_get_the_locale_from_a_localeDescription(): void
    {
        $localeDescription = LocaleDescription::ZH_HANS_CN;
        $locale = $localeDescription->getLocale();
        $this->assertSame(LocaleCode::ZH_HANS_CN, $locale);
    }

    /** @test */
    public function it_should_get_the_language_and_countries_from_all_locales(): void
    {
        foreach (LocaleDescription::cases() as $locale) {
            $language = $locale->getLanguage();
            $this->assertInstanceOf(LanguageName::class, $language);
            $country = $locale->getCountry();
            $this->assertInstanceOf(CountryName::class, $country);
        }
    }

    /** @test */
    public function it_should_get_locales_from_a_country(): void
    {
        $belgium = CountryName::BE;
        $locales = $belgium->getLocales();
        foreach ($locales as $locale) {
            $country = $locale->getCountry();
            $this->assertSame(CountryName::BE, $country);
        }
        $localeCodes = array_keys($locales);
        sort($localeCodes);
        $expected = [
            'DE_BE', // German (Belgium)
            'FR_BE', // French (Belgium)
            'LI_BE', // Limburgish (Belgium)
            'NL_BE', // Dutch (Belgium)
            'WA_BE', // Walloon (Belgium)
        ];
        $this->assertSame($expected, $localeCodes);
    }

    /** @test */
    public function it_should_get_locales_from_a_language(): void
    {
        $dutch = LanguageName::NLD;
        $locales = $dutch->getLocales();
        foreach ($locales as $locale) {
            $language = $locale->getLanguage();
            $this->assertSame($dutch, $language);
        }
        $localeCodes = array_keys($locales);
        sort($localeCodes);
        $expected = [
            'NL_AW', // 'Dutch (Aruba)
            'NL_BE', // 'Dutch (Belgium)
            'NL_BQ', // 'Dutch (Caribbean Netherlands)
            'NL_CW', // 'Dutch (Curaçao)
            'NL_NL', // 'Dutch (Netherlands)
            'NL_SR', // 'Dutch (Suriname)
            'NL_SX', // 'Dutch (Sint Maarten)
        ];
        $this->assertSame($expected, $localeCodes);
    }
}
