<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Locale;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use Steefdw\StandardsEnums\ISO639\LanguageName;
use Steefdw\StandardsEnums\Locales\LocaleCode;
use Steefdw\StandardsEnums\Locales\LocaleDescription;

final class LocaleCodeTest extends TestCase
{
    /** @test */
    public function it_should_get_the_country_from_a_locale(): void
    {
        $locale = LocaleCode::tryFromName('zh_Hans_CN');
        $country = $locale?->getCountry();
        $this->assertSame(CountryName::CN, $country);
    }

    /** @test */
    public function it_should_get_the_language_from_a_locale(): void
    {
        $locale = LocaleCode::tryFromName('zh_Hans_CN');
        $language = $locale?->getLanguage();
        $this->assertSame(LanguageName::ZHO, $language);
    }

    /** @test */
    public function it_should_get_the_description_from_a_locale(): void
    {
        $locale = LocaleCode::ZH_HANS_CN;
        $description = $locale->getDescription();
        $this->assertSame(LocaleDescription::ZH_HANS_CN, $description);
    }

    /** @test */
    public function it_should_get_the_language_and_countries_from_all_locales(): void
    {
        foreach (LocaleCode::cases() as $locale) {
            $language = $locale->getLanguage();
            $this->assertInstanceOf(LanguageName::class, $language);
            $country = $locale->getCountry();
            $this->assertInstanceOf(CountryName::class, $country);
        }
    }

    /** @test */
    public function lets_play_with_a_locale(): void
    {
        $locale = LocaleCode::tryFromName('nl_BE');

        $localeDescription = $locale?->getDescription();
        $this->assertSame(LocaleDescription::NL_BE, $localeDescription);

        $language = $locale?->getLanguage();
        $this->assertSame(LanguageName::NLD, $language);
        /** @var LanguageName $language */
        $this->assertStringContainsString('Flemish', $language->value);

        $country = $locale?->getCountry();
        $this->assertSame(CountryName::BE, $country);
        /** @var CountryName $country */
        $flag = $country->getFlag()?->value;
        $name = $country->getName()?->value;

        $this->assertSame('🇧🇪', $flag);
        $this->assertSame('Belgium', $name);
    }
}
