<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Language;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha2;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha3;
use Steefdw\StandardsEnums\ISO639\LanguageEnumInterface;
use Steefdw\StandardsEnums\ISO639\LanguageName;
use Steefdw\StandardsEnums\ISO639\LanguageNameTranslation;
use Steefdw\StandardsEnums\Locales\LocaleCode;

final class LanguageNameTranslationTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_translated_cases_for_the_language_names_enum(): void
    {
        $dutch = LanguageNameTranslation::tryFrom('nl_NL');
        $names = LanguageName::toArrayTranslated($dutch);

        $this->assertSame('Nederlands', $names[LanguageName::NLD->name]);
        $this->assertSame('Abchazisch', $names[LanguageName::ABK->name]);
    }

    /**
     * @test
     */
    public function it_should_have_translations_for_all_LanguageNameTranslation_cases(): void
    {
        $translationLanguages = LanguageNameTranslation::cases();
        $languageNameCount = count(LanguageName::cases());
        foreach ($translationLanguages as $translationLanguage) {
            $languageList = LanguageName::toArrayTranslated($translationLanguage);
            $this->assertTrue(count($languageList) === $languageNameCount);
        }
    }

    /**
     * @test
     */
    public function it_should_have_a_translation_file_for_all_LanguageNameTranslation_cases(): void
    {
        $cases = LanguageNameTranslation::cases();
        foreach ($cases as $case) {
            try {
                $translations = $case->getTranslations();
                $this->assertIsArray($translations);
            } catch (\Exception $exception) {
                $this->fail($exception->getMessage());
            }
        }
    }

    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_sorted_cases_for_an_enum_when_sorted_by_translated_language_names(
        LanguageEnumInterface $countryEnum
    ): void {
        /** @var LanguageEnumInterface $country */
        $country = $countryEnum::tryFromAlpha2(LanguageAlpha2::NLD);
        $this->assertInstanceOf($countryEnum::class, $country);
        /** @var LanguageNameTranslation $dutch */
        $dutch = LanguageNameTranslation::tryFromName(LocaleCode::NL_NL->value);
        $this->assertInstanceOf(LanguageNameTranslation::class, $dutch);

        $sortedDutch = $country::casesSortedByLanguageName($dutch);
        $indexDistance = $this->getIndexDistance($sortedDutch);
        $message = '"Zoeloe" and "Zweeds" should be close together';
        $this->assertLessThan(6, $indexDistance, $message);

        $sortedEnglish = $country::casesSortedByLanguageName();
        $indexDistance = $this->getIndexDistance($sortedEnglish);
        $message = '"Zulu" and "Swedish" should be far apart from each other';
        $this->assertLessThan(30, $indexDistance, $message);
    }

    /**
     * @return array<int, array<int, LanguageAlpha2|LanguageAlpha3|LanguageName>>
     */
    public function provideEnums(): array
    {
        return [
            [LanguageAlpha2::NLD],
            [LanguageAlpha3::NLD],
            [LanguageName::NLD],
        ];
    }

    /**
     * @param LanguageEnumInterface[] $languageEnums
     */
    private function getIndexDistance(array $languageEnums): int
    {
        $languageCodes = [];
        foreach ($languageEnums as $index => $languageEnum) {
            $languageCodes[$languageEnum->name] = $index;
        }

        return $languageCodes['SWE'] - $languageCodes['ZUL'];
    }
}
