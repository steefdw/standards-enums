<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Language;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha2;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha3;
use Steefdw\StandardsEnums\ISO639\LanguageName;
use ValueError;

final class LanguageNameTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_a_language_name(): void
    {
        $languageName = LanguageName::tryFromName(LanguageAlpha2::NLD->name);
        $this->assertSame(LanguageName::NLD, $languageName);
    }

    /**
     * @test
     *
     * @noinspection PhpCaseWithValueNotFoundInEnumInspection
     * @noinspection PhpExpressionResultUnusedInspection
     */
    public function it_should_not_get_non_existing_language_names(): void
    {
        $this->expectException(ValueError::class);
        LanguageName::from('404');
    }

    /**
     * @test
     */
    public function it_should_get_the_alpha2_of_a_language_name(): void
    {
        $LanguageName = LanguageName::tryFromName(LanguageAlpha2::NLD->name);
        $this->assertSame(LanguageName::NLD, $LanguageName);
        /** @var LanguageName $LanguageName */
        $languageAlpha2 = $LanguageName->getAlpha2();

        $this->assertSame(LanguageAlpha2::NLD, $languageAlpha2);
    }

    /**
     * @test
     */
    public function it_should_get_the_alpha3_of_a_language_name(): void
    {
        $LanguageName = LanguageName::tryFromName(LanguageAlpha2::NLD->name);
        $this->assertSame(LanguageName::NLD, $LanguageName);
        /** @var LanguageName $LanguageName */
        $languageAlpha3 = $LanguageName->getAlpha3();

        $this->assertSame(LanguageAlpha3::NLD, $languageAlpha3);
    }
}
