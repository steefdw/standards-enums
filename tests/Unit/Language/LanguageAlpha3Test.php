<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Language;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha2;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha3;
use Steefdw\StandardsEnums\ISO639\LanguageName;
use ValueError;

final class LanguageAlpha3Test extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_a_language_alpha3(): void
    {
        $language = LanguageAlpha3::tryFromName(LanguageAlpha2::NLD->name);
        $this->assertInstanceOf(LanguageAlpha3::class, $language);
        /** @var LanguageAlpha3 $language */
        $this->assertTrue(strlen($language->name) === 3);
        $this->assertTrue(strlen($language->value) === 3);
        $this->assertSame(LanguageAlpha3::NLD, $language);
    }

    /**
     * @test
     *
     * @noinspection PhpCaseWithValueNotFoundInEnumInspection
     * @noinspection PhpExpressionResultUnusedInspection
     */
    public function it_should_not_get_non_existing_language_alpha3(): void
    {
        $this->expectException(ValueError::class);
        LanguageAlpha3::from('404');
    }

    /**
     * @test
     */
    public function it_should_get_the_name_of_a_language_alpha3(): void
    {
        $language = LanguageAlpha3::tryFromName(LanguageAlpha2::NLD->name);
        $this->assertInstanceOf(LanguageAlpha3::class, $language);
        /** @var LanguageAlpha3 $language */
        $LanguageName = $language->getName();

        $this->assertSame(LanguageName::NLD, $LanguageName);
    }

    /**
     * @test
     */
    public function it_should_get_the_alpha2_of_a_language_alpha3(): void
    {
        $languageAlpha3 = LanguageAlpha3::tryFromName(LanguageAlpha2::NLD->name);
        $this->assertInstanceOf(LanguageAlpha3::class, $languageAlpha3);
        /** @var LanguageAlpha3 $languageAlpha3 */
        $LanguageName = $languageAlpha3->getAlpha2();

        $this->assertSame(LanguageAlpha2::NLD, $LanguageName);
    }
}
