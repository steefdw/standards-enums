<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Language;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO3166\CountryName;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha2;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha3;
use Steefdw\StandardsEnums\ISO639\LanguageName;
use Steefdw\StandardsEnums\Locales\LocaleDescription;
use ValueError;

final class LanguageAlpha2Test extends TestCase
{
    /**
     * @test
     */
    public function it_should_get_a_language(): void
    {
        $language = LanguageAlpha2::tryFrom(LanguageAlpha2::NLD->value);
        $this->assertInstanceOf(LanguageAlpha2::class, $language);
        /** @var LanguageAlpha2 $language */
        $this->assertTrue(strlen($language->name) === 3);
        $this->assertTrue(strlen($language->value) === 2);
        $this->assertSame(LanguageAlpha2::NLD, $language);
    }

    /**
     * @test
     *
     * @noinspection PhpCaseWithValueNotFoundInEnumInspection
     * @noinspection PhpExpressionResultUnusedInspection
     */
    public function it_should_not_get_non_existing_countries(): void
    {
        $this->expectException(ValueError::class);
        LanguageAlpha2::from('404');
    }

    /**
     * @test
     */
    public function it_should_get_the_name_of_a_language(): void
    {
        $language = LanguageAlpha2::tryFrom(LanguageAlpha2::NLD->value);
        $this->assertInstanceOf(LanguageAlpha2::class, $language);
        /** @var LanguageAlpha2 $language */
        $LanguageName = $language->getName();

        $this->assertSame(LanguageName::NLD, $LanguageName);
    }

    /**
     * @test
     */
    public function it_should_get_the_alpha3_of_a_language(): void
    {
        $language = LanguageAlpha2::tryFrom(LanguageAlpha2::NLD->value);
        $this->assertInstanceOf(LanguageAlpha2::class, $language);
        /** @var LanguageAlpha2 $language */
        $languageAlpha3 = $language->getAlpha3();

        $this->assertSame(LanguageAlpha3::NLD, $languageAlpha3);
    }

    /**
     * @test
     */
    public function it_should_get_the_countries_of_a_language(): void
    {
        $language = LanguageAlpha2::tryFrom('nl');
        $this->assertInstanceOf(LanguageAlpha2::class, $language);
        /** @var LanguageAlpha2 $language */
        $countries = $language->getCountries();
        $this->assertArrayHasKey('NL', $countries);
        $this->assertSame(CountryName::NL, $countries['NL']);

        $this->assertArrayHasKey('BE', $countries);
        $this->assertSame(CountryName::BE, $countries['BE']);
    }

    /**
     * @test
     */
    public function it_should_get_the_locales_of_a_language(): void
    {
        $language = LanguageAlpha2::tryFrom('nl');
        $this->assertInstanceOf(LanguageAlpha2::class, $language);
        /** @var LanguageAlpha2 $language */
        $countries = $language->getLocales();
        $this->assertArrayHasKey('NL_NL', $countries);
        $this->assertSame(LocaleDescription::NL_NL, $countries['NL_NL']);

        $this->assertArrayHasKey('NL_BE', $countries);
        $this->assertSame(LocaleDescription::NL_BE, $countries['NL_BE']);
    }
}
