<?php

declare(strict_types=1);

namespace Steefdw\StandardsEnums\Tests\Unit\Language;

use PHPUnit\Framework\TestCase;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha2;
use Steefdw\StandardsEnums\ISO639\LanguageAlpha3;
use Steefdw\StandardsEnums\ISO639\LanguageEnumInterface;
use Steefdw\StandardsEnums\ISO639\LanguageName;

final class TryFromTest extends TestCase
{
    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_an_enum_from_an_alpha2(
        LanguageEnumInterface $expectedValue
    ): void {
        $language = $expectedValue::tryFromAlpha2(LanguageAlpha2::NLD);

        $this->assertSame($expectedValue, $language);
    }

    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_an_enum_from_an_alpha3(
        LanguageEnumInterface $expectedValue
    ): void {
        $language = $expectedValue::tryFromAlpha3(LanguageAlpha3::NLD);

        $this->assertSame($expectedValue, $language);
    }

    /**
     * @test
     *
     * @dataProvider provideEnums
     */
    public function it_should_get_an_enum_from_a_language_name(
        LanguageEnumInterface $expectedValue
    ): void {
        $language = $expectedValue::tryFromLanguageName(LanguageName::NLD);

        $this->assertSame($expectedValue, $language);
    }

    /**
     * @return array<int, array<int, LanguageAlpha2|LanguageAlpha3|LanguageName>>
     */
    public function provideEnums(): array
    {
        return [
            [LanguageAlpha2::NLD],
            [LanguageAlpha3::NLD],
            [LanguageName::NLD],
        ];
    }
}
