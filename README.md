# Standards Enums

The _Standards Enums_ package provides a simple and consistent way to work with countries, languages, currencies, 
and locales using enums.

Each Enum comes with methods for accessing related information, like the name, flag, description, and more.
Additionally, helper methods are provided to easily get all possible values or sort them by their name in a given language. 
These methods can be used with all Enum classes, regardless of whether they represent languages, countries, currencies, or locales.

_Standards Enums_ provides you access to lists of various ISO standards:

| Enum in this package                                                         | ISO standard                                         |
|------------------------------------------------------------------------------|------------------------------------------------------|
| [Countries](https://steefdw.gitlab.io/standards-enums/countries/02-usage/)   | [ISO 3166-1](https://en.wikipedia.org/wiki/ISO_3166) |
| [Currencies](https://steefdw.gitlab.io/standards-enums/currencies/02-usage/) | [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217)   |
| [Languages](https://steefdw.gitlab.io/standards-enums/languages/02-usage/)   | [ISO 639](https://en.wikipedia.org/wiki/ISO_639)     |
| [Locales](https://steefdw.gitlab.io/standards-enums/locales/02-usage/)       | (not ISO)                                            |

Much of the code in this package is automatically generated with the data from
https://salsa.debian.org/iso-codes-team/iso-codes.

## Documentation

* **Full documentation**: https://steefdw.gitlab.io/standards-enums
* **Short documentation**: this readme.

---

## Installation

```bash
composer require steefdw/standards-enums
```

## Usage

### **Countries** example:

```php
$country = CountryAlpha2::tryFrom('nl'); // CountryAlpha2::NL

$countryName = $country->getName()->value;           // 'Netherlands'
$countryAlpha2 = $countryName->getAlpha2()->value;   // 'nl'
$countryAlpha3 = $countryAlpha2->getAlpha3()->value; // 'NLD'
$countryFlag = $countryAlpha3->getFlag()->value;     // '🇳🇱'
$countryNumber = $countryFlag->getNumeric()->value;  // '528'
$countryCurrencies = $country->getCurrencies();      // ['EUR' => CurrencyName::EUR]

// these methods work for all country Enums
```

### **Currencies** example:

```php
$currency = CurrencyName::tryFrom('Euro'); // CurrencyName::EUR

$currencyName = $currency->getName()->value;      // 'Euro'
$currencyAlpha3 = $currency->getAlpha3()->value;  // 'EUR'
$currencyNumber = $currency->getNumeric()->value; // '978'
$currencyCountries = $currency->getCountries();   // [
//    'AD' => CountryName::AD, // Andorra
//    'AT' => CountryName::AT, // Austria
//    'BE' => CountryName::BE, // Belgium
//    ...]

// these methods work for all currency Enums
```

### **Languages** example:

```php
$language = LanguageAlpha2::tryFrom('nl'); // LanguageAlpha2::NLD

$languageName = $language->getName()->value;           // 'Dutch; Flemish'
$languageAlpha2 = $languageName->getAlpha2()->value;   // 'nl'
$languageAlpha3 = $languageAlpha2->getAlpha3()->value; // 'nld'
// these methods work for all language Enums
```

### **Locales** example:

```php
$locale = LocaleCode::tryFromName('nl_BE'); // LocaleCode::NL_BE

$description = $locale->getDescription(); // LocaleDescription::NL_BE
$description->value;                 // 'Dutch (Belgium)'
$locale->getLanguage()->value;       // 'Dutch; Flemish'
$locale->getCountry()->value;        // 'Belgium'
// these methods work for all locale Enums
```

### See the usage pages for more information:

Please refer to these sections of the documentation for more detailed information on how to use each enum type.

* [Using the countries enums](https://steefdw.gitlab.io/standards-enums/countries/02-usage/)
* [Using the currencies enums](https://steefdw.gitlab.io/standards-enums/currencies/02-usage/)
* [Using the languages enums](https://steefdw.gitlab.io/standards-enums/languages/02-usage/)
* [Using the locales enums](https://steefdw.gitlab.io/standards-enums/locales/02-usage/)

## Testing

```bash
composer test
```

Or:

```bash
make test
```

## Contributing

* make sure the code validation succeeds. You can run it with `make validate`. 
* make sure all tests succeed. You can run them with `make test`. 
* make sure all code is tested. 

## License

The MIT License. Please see [Licence File](./LICENCE.md) for more information.
